	 ######
	 isub32!     r0, 0XFFFFFFFF, 0XFFFFFFFF          #  0          -1 -          -1 =          +0 |  z c
	 flags_read  r1                                  #  1
	 assert_eq   r0, 0X00000000                      #  2
	 assert_eq   r1, 0b0101                          #  3

	 isub32!     r0, 0XFFFFFFFF, 0X00000000          #  4          -1 -          +0 =          -1 | n  c
	 flags_read  r1                                  #  5
	 assert_eq   r0, 0XFFFFFFFF                      #  6
	 assert_eq   r1, 0b1001                          #  7

	 isub32!     r0, 0XFFFFFFFF, 0X00000001          #  8          -1 -          +1 =          -2 | n  c
	 flags_read  r1                                  #  9
	 assert_eq   r0, 0XFFFFFFFE                      # 10
	 assert_eq   r1, 0b1001                          # 11

	 isub32!     r0, 0XFFFFFFFF, 0X7FFFFFFF          # 12          -1 - +2147483647 = -2147483648 | n  c
	 flags_read  r1                                  # 13
	 assert_eq   r0, 0X80000000                      # 14
	 assert_eq   r1, 0b1001                          # 15

	 isub32!     r0, 0XFFFFFFFF, 0X80000000          # 16          -1 - -2147483648 = +2147483647 |    c
	 flags_read  r1                                  # 17
	 assert_eq   r0, 0X7FFFFFFF                      # 18
	 assert_eq   r1, 0b0001                          # 19

	 isub32!     r0, 0XFFFFFFFF, 0X80000001          # 20          -1 - -2147483647 = +2147483646 |    c
	 flags_read  r1                                  # 21
	 assert_eq   r0, 0X7FFFFFFE                      # 22
	 assert_eq   r1, 0b0001                          # 23

	 ######
	 isub32!     r0, 0X00000000, 0XFFFFFFFF          # 24          +0 -          -1 =          +1 |     
	 flags_read  r1                                  # 25
	 assert_eq   r0, 0X00000001                      # 26
	 assert_eq   r1, 0b0000                          # 27

	 isub32!     r0, 0X00000000, 0X00000000          # 28          +0 -          +0 =          +0 |  z c
	 flags_read  r1                                  # 29
	 assert_eq   r0, 0X00000000                      # 30
	 assert_eq   r1, 0b0101                          # 31

	 isub32!     r0, 0X00000000, 0X00000001          # 32          +0 -          +1 =          -1 | n   
	 flags_read  r1                                  # 33
	 assert_eq   r0, 0XFFFFFFFF                      # 34
	 assert_eq   r1, 0b1000                          # 35

	 isub32!     r0, 0X00000000, 0X7FFFFFFF          # 36          +0 - +2147483647 = -2147483647 | n   
	 flags_read  r1                                  # 37
	 assert_eq   r0, 0X80000001                      # 38
	 assert_eq   r1, 0b1000                          # 39

	 isub32!     r0, 0X00000000, 0X80000000          # 40          +0 - -2147483648 = -2147483648 | n v 
	 flags_read  r1                                  # 41
	 assert_eq   r0, 0X80000000                      # 42
	 assert_eq   r1, 0b1010                          # 43

	 isub32!     r0, 0X00000000, 0X80000001          # 44          +0 - -2147483647 = +2147483647 |     
	 flags_read  r1                                  # 45
	 assert_eq   r0, 0X7FFFFFFF                      # 46
	 assert_eq   r1, 0b0000                          # 47

	 ######
	 isub32!     r0, 0X00000001, 0XFFFFFFFF          # 48          +1 -          -1 =          +2 |     
	 flags_read  r1                                  # 49
	 assert_eq   r0, 0X00000002                      # 50
	 assert_eq   r1, 0b0000                          # 51

	 isub32!     r0, 0X00000001, 0X00000000          # 52          +1 -          +0 =          +1 |    c
	 flags_read  r1                                  # 53
	 assert_eq   r0, 0X00000001                      # 54
	 assert_eq   r1, 0b0001                          # 55

	 isub32!     r0, 0X00000001, 0X00000001          # 56          +1 -          +1 =          +0 |  z c
	 flags_read  r1                                  # 57
	 assert_eq   r0, 0X00000000                      # 58
	 assert_eq   r1, 0b0101                          # 59

	 isub32!     r0, 0X00000001, 0X7FFFFFFF          # 60          +1 - +2147483647 = -2147483646 | n   
	 flags_read  r1                                  # 61
	 assert_eq   r0, 0X80000002                      # 62
	 assert_eq   r1, 0b1000                          # 63

	 isub32!     r0, 0X00000001, 0X80000000          # 64          +1 - -2147483648 = -2147483647 | n v 
	 flags_read  r1                                  # 65
	 assert_eq   r0, 0X80000001                      # 66
	 assert_eq   r1, 0b1010                          # 67

	 isub32!     r0, 0X00000001, 0X80000001          # 68          +1 - -2147483647 = -2147483648 | n v 
	 flags_read  r1                                  # 69
	 assert_eq   r0, 0X80000000                      # 70
	 assert_eq   r1, 0b1010                          # 71

	 ######
	 isub32!     r0, 0X7FFFFFFF, 0XFFFFFFFF          # 72 +2147483647 -          -1 = -2147483648 | n v 
	 flags_read  r1                                  # 73
	 assert_eq   r0, 0X80000000                      # 74
	 assert_eq   r1, 0b1010                          # 75

	 isub32!     r0, 0X7FFFFFFF, 0X00000000          # 76 +2147483647 -          +0 = +2147483647 |    c
	 flags_read  r1                                  # 77
	 assert_eq   r0, 0X7FFFFFFF                      # 78
	 assert_eq   r1, 0b0001                          # 79

	 isub32!     r0, 0X7FFFFFFF, 0X00000001          # 80 +2147483647 -          +1 = +2147483646 |    c
	 flags_read  r1                                  # 81
	 assert_eq   r0, 0X7FFFFFFE                      # 82
	 assert_eq   r1, 0b0001                          # 83

	 isub32!     r0, 0X7FFFFFFF, 0X7FFFFFFF          # 84 +2147483647 - +2147483647 =          +0 |  z c
	 flags_read  r1                                  # 85
	 assert_eq   r0, 0X00000000                      # 86
	 assert_eq   r1, 0b0101                          # 87

	 isub32!     r0, 0X7FFFFFFF, 0X80000000          # 88 +2147483647 - -2147483648 =          -1 | n v 
	 flags_read  r1                                  # 89
	 assert_eq   r0, 0XFFFFFFFF                      # 90
	 assert_eq   r1, 0b1010                          # 91

	 isub32!     r0, 0X7FFFFFFF, 0X80000001          # 92 +2147483647 - -2147483647 =          -2 | n v 
	 flags_read  r1                                  # 93
	 assert_eq   r0, 0XFFFFFFFE                      # 94
	 assert_eq   r1, 0b1010                          # 95

	 ######
	 isub32!     r0, 0X80000000, 0XFFFFFFFF          # 96 -2147483648 -          -1 = -2147483647 | n   
	 flags_read  r1                                  # 97
	 assert_eq   r0, 0X80000001                      # 98
	 assert_eq   r1, 0b1000                          # 99

	 isub32!     r0, 0X80000000, 0X00000000          #100 -2147483648 -          +0 = -2147483648 | n  c
	 flags_read  r1                                  #101
	 assert_eq   r0, 0X80000000                      #102
	 assert_eq   r1, 0b1001                          #103

	 isub32!     r0, 0X80000000, 0X00000001          #104 -2147483648 -          +1 = +2147483647 |   vc
	 flags_read  r1                                  #105
	 assert_eq   r0, 0X7FFFFFFF                      #106
	 assert_eq   r1, 0b0011                          #107

	 isub32!     r0, 0X80000000, 0X7FFFFFFF          #108 -2147483648 - +2147483647 =          +1 |   vc
	 flags_read  r1                                  #109
	 assert_eq   r0, 0X00000001                      #110
	 assert_eq   r1, 0b0011                          #111

	 isub32!     r0, 0X80000000, 0X80000000          #112 -2147483648 - -2147483648 =          +0 |  z c
	 flags_read  r1                                  #113
	 assert_eq   r0, 0X00000000                      #114
	 assert_eq   r1, 0b0101                          #115

	 isub32!     r0, 0X80000000, 0X80000001          #116 -2147483648 - -2147483647 =          -1 | n   
	 flags_read  r1                                  #117
	 assert_eq   r0, 0XFFFFFFFF                      #118
	 assert_eq   r1, 0b1000                          #119

	 ######
	 isub32!     r0, 0X80000001, 0XFFFFFFFF          #120 -2147483647 -          -1 = -2147483646 | n   
	 flags_read  r1                                  #121
	 assert_eq   r0, 0X80000002                      #122
	 assert_eq   r1, 0b1000                          #123

	 isub32!     r0, 0X80000001, 0X00000000          #124 -2147483647 -          +0 = -2147483647 | n  c
	 flags_read  r1                                  #125
	 assert_eq   r0, 0X80000001                      #126
	 assert_eq   r1, 0b1001                          #127

	 isub32!     r0, 0X80000001, 0X00000001          #128 -2147483647 -          +1 = -2147483648 | n  c
	 flags_read  r1                                  #129
	 assert_eq   r0, 0X80000000                      #130
	 assert_eq   r1, 0b1001                          #131

	 isub32!     r0, 0X80000001, 0X7FFFFFFF          #132 -2147483647 - +2147483647 =          +2 |   vc
	 flags_read  r1                                  #133
	 assert_eq   r0, 0X00000002                      #134
	 assert_eq   r1, 0b0011                          #135

	 isub32!     r0, 0X80000001, 0X80000000          #136 -2147483647 - -2147483648 =          +1 |    c
	 flags_read  r1                                  #137
	 assert_eq   r0, 0X00000001                      #138
	 assert_eq   r1, 0b0001                          #139

	 isub32!     r0, 0X80000001, 0X80000001          #140 -2147483647 - -2147483647 =          +0 |  z c
	 flags_read  r1                                  #141
	 assert_eq   r0, 0X00000000                      #142
	 assert_eq   r1, 0b0101                          #143


	 halt
