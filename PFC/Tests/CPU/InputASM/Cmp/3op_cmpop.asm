			mov r1,       0
			is_ieq       r0, 0X00000000, 0X00000000
			assert_true  r0

			mov r1,       1
			is_ieq       r0, 0X00000000, 0X00000001
			assert_false r0

			mov r1,       2
			is_ieq       r0, 0X00000000, 0X7FFFFFFF
			assert_false r0

			mov r1,       3
			is_ieq       r0, 0X00000000, 0X80000000
			assert_false r0

			mov r1,       4
			is_ieq       r0, 0X00000000, 0X80000001
			assert_false r0

			mov r1,       5
			is_ieq       r0, 0X00000000, 0XFFFFFFFE
			assert_false r0

			mov r1,       6
			is_ieq       r0, 0X00000000, 0XFFFFFFFF
			assert_false r0

			mov r1,       7
			is_ieq       r0, 0X00000001, 0X00000000
			assert_false r0

			mov r1,       8
			is_ieq       r0, 0X00000001, 0X00000001
			assert_true  r0

			mov r1,       9
			is_ieq       r0, 0X00000001, 0X7FFFFFFF
			assert_false r0

			mov r1,      10
			is_ieq       r0, 0X00000001, 0X80000000
			assert_false r0

			mov r1,      11
			is_ieq       r0, 0X00000001, 0X80000001
			assert_false r0

			mov r1,      12
			is_ieq       r0, 0X00000001, 0XFFFFFFFE
			assert_false r0

			mov r1,      13
			is_ieq       r0, 0X00000001, 0XFFFFFFFF
			assert_false r0

			mov r1,      14
			is_ieq       r0, 0X7FFFFFFF, 0X00000000
			assert_false r0

			mov r1,      15
			is_ieq       r0, 0X7FFFFFFF, 0X00000001
			assert_false r0

			mov r1,      16
			is_ieq       r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,      17
			is_ieq       r0, 0X7FFFFFFF, 0X80000000
			assert_false r0

			mov r1,      18
			is_ieq       r0, 0X7FFFFFFF, 0X80000001
			assert_false r0

			mov r1,      19
			is_ieq       r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,      20
			is_ieq       r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_false r0

			mov r1,      21
			is_ieq       r0, 0X80000000, 0X00000000
			assert_false r0

			mov r1,      22
			is_ieq       r0, 0X80000000, 0X00000001
			assert_false r0

			mov r1,      23
			is_ieq       r0, 0X80000000, 0X7FFFFFFF
			assert_false r0

			mov r1,      24
			is_ieq       r0, 0X80000000, 0X80000000
			assert_true  r0

			mov r1,      25
			is_ieq       r0, 0X80000000, 0X80000001
			assert_false r0

			mov r1,      26
			is_ieq       r0, 0X80000000, 0XFFFFFFFE
			assert_false r0

			mov r1,      27
			is_ieq       r0, 0X80000000, 0XFFFFFFFF
			assert_false r0

			mov r1,      28
			is_ieq       r0, 0X80000001, 0X00000000
			assert_false r0

			mov r1,      29
			is_ieq       r0, 0X80000001, 0X00000001
			assert_false r0

			mov r1,      30
			is_ieq       r0, 0X80000001, 0X7FFFFFFF
			assert_false r0

			mov r1,      31
			is_ieq       r0, 0X80000001, 0X80000000
			assert_false r0

			mov r1,      32
			is_ieq       r0, 0X80000001, 0X80000001
			assert_true  r0

			mov r1,      33
			is_ieq       r0, 0X80000001, 0XFFFFFFFE
			assert_false r0

			mov r1,      34
			is_ieq       r0, 0X80000001, 0XFFFFFFFF
			assert_false r0

			mov r1,      35
			is_ieq       r0, 0XFFFFFFFE, 0X00000000
			assert_false r0

			mov r1,      36
			is_ieq       r0, 0XFFFFFFFE, 0X00000001
			assert_false r0

			mov r1,      37
			is_ieq       r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_false r0

			mov r1,      38
			is_ieq       r0, 0XFFFFFFFE, 0X80000000
			assert_false r0

			mov r1,      39
			is_ieq       r0, 0XFFFFFFFE, 0X80000001
			assert_false r0

			mov r1,      40
			is_ieq       r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_true  r0

			mov r1,      41
			is_ieq       r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_false r0

			mov r1,      42
			is_ieq       r0, 0XFFFFFFFF, 0X00000000
			assert_false r0

			mov r1,      43
			is_ieq       r0, 0XFFFFFFFF, 0X00000001
			assert_false r0

			mov r1,      44
			is_ieq       r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,      45
			is_ieq       r0, 0XFFFFFFFF, 0X80000000
			assert_false r0

			mov r1,      46
			is_ieq       r0, 0XFFFFFFFF, 0X80000001
			assert_false r0

			mov r1,      47
			is_ieq       r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,      48
			is_ieq       r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_true  r0


			######
			mov r1,      49
			is_ineq      r0, 0X00000000, 0X00000000
			assert_false r0

			mov r1,      50
			is_ineq      r0, 0X00000000, 0X00000001
			assert_true  r0

			mov r1,      51
			is_ineq      r0, 0X00000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,      52
			is_ineq      r0, 0X00000000, 0X80000000
			assert_true  r0

			mov r1,      53
			is_ineq      r0, 0X00000000, 0X80000001
			assert_true  r0

			mov r1,      54
			is_ineq      r0, 0X00000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,      55
			is_ineq      r0, 0X00000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,      56
			is_ineq      r0, 0X00000001, 0X00000000
			assert_true  r0

			mov r1,      57
			is_ineq      r0, 0X00000001, 0X00000001
			assert_false r0

			mov r1,      58
			is_ineq      r0, 0X00000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,      59
			is_ineq      r0, 0X00000001, 0X80000000
			assert_true  r0

			mov r1,      60
			is_ineq      r0, 0X00000001, 0X80000001
			assert_true  r0

			mov r1,      61
			is_ineq      r0, 0X00000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,      62
			is_ineq      r0, 0X00000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,      63
			is_ineq      r0, 0X7FFFFFFF, 0X00000000
			assert_true  r0

			mov r1,      64
			is_ineq      r0, 0X7FFFFFFF, 0X00000001
			assert_true  r0

			mov r1,      65
			is_ineq      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,      66
			is_ineq      r0, 0X7FFFFFFF, 0X80000000
			assert_true  r0

			mov r1,      67
			is_ineq      r0, 0X7FFFFFFF, 0X80000001
			assert_true  r0

			mov r1,      68
			is_ineq      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,      69
			is_ineq      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_true  r0

			mov r1,      70
			is_ineq      r0, 0X80000000, 0X00000000
			assert_true  r0

			mov r1,      71
			is_ineq      r0, 0X80000000, 0X00000001
			assert_true  r0

			mov r1,      72
			is_ineq      r0, 0X80000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,      73
			is_ineq      r0, 0X80000000, 0X80000000
			assert_false r0

			mov r1,      74
			is_ineq      r0, 0X80000000, 0X80000001
			assert_true  r0

			mov r1,      75
			is_ineq      r0, 0X80000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,      76
			is_ineq      r0, 0X80000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,      77
			is_ineq      r0, 0X80000001, 0X00000000
			assert_true  r0

			mov r1,      78
			is_ineq      r0, 0X80000001, 0X00000001
			assert_true  r0

			mov r1,      79
			is_ineq      r0, 0X80000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,      80
			is_ineq      r0, 0X80000001, 0X80000000
			assert_true  r0

			mov r1,      81
			is_ineq      r0, 0X80000001, 0X80000001
			assert_false r0

			mov r1,      82
			is_ineq      r0, 0X80000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,      83
			is_ineq      r0, 0X80000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,      84
			is_ineq      r0, 0XFFFFFFFE, 0X00000000
			assert_true  r0

			mov r1,      85
			is_ineq      r0, 0XFFFFFFFE, 0X00000001
			assert_true  r0

			mov r1,      86
			is_ineq      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_true  r0

			mov r1,      87
			is_ineq      r0, 0XFFFFFFFE, 0X80000000
			assert_true  r0

			mov r1,      88
			is_ineq      r0, 0XFFFFFFFE, 0X80000001
			assert_true  r0

			mov r1,      89
			is_ineq      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_false r0

			mov r1,      90
			is_ineq      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_true  r0

			mov r1,      91
			is_ineq      r0, 0XFFFFFFFF, 0X00000000
			assert_true  r0

			mov r1,      92
			is_ineq      r0, 0XFFFFFFFF, 0X00000001
			assert_true  r0

			mov r1,      93
			is_ineq      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,      94
			is_ineq      r0, 0XFFFFFFFF, 0X80000000
			assert_true  r0

			mov r1,      95
			is_ineq      r0, 0XFFFFFFFF, 0X80000001
			assert_true  r0

			mov r1,      96
			is_ineq      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,      97
			is_ineq      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_false r0


			######
			mov r1,      98
			is_iugt      r0, 0X00000000, 0X00000000
			assert_false r0

			mov r1,      99
			is_iugt      r0, 0X00000000, 0X00000001
			assert_false r0

			mov r1,     100
			is_iugt      r0, 0X00000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     101
			is_iugt      r0, 0X00000000, 0X80000000
			assert_false r0

			mov r1,     102
			is_iugt      r0, 0X00000000, 0X80000001
			assert_false r0

			mov r1,     103
			is_iugt      r0, 0X00000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     104
			is_iugt      r0, 0X00000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     105
			is_iugt      r0, 0X00000001, 0X00000000
			assert_true  r0

			mov r1,     106
			is_iugt      r0, 0X00000001, 0X00000001
			assert_false r0

			mov r1,     107
			is_iugt      r0, 0X00000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     108
			is_iugt      r0, 0X00000001, 0X80000000
			assert_false r0

			mov r1,     109
			is_iugt      r0, 0X00000001, 0X80000001
			assert_false r0

			mov r1,     110
			is_iugt      r0, 0X00000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     111
			is_iugt      r0, 0X00000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     112
			is_iugt      r0, 0X7FFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     113
			is_iugt      r0, 0X7FFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     114
			is_iugt      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     115
			is_iugt      r0, 0X7FFFFFFF, 0X80000000
			assert_false r0

			mov r1,     116
			is_iugt      r0, 0X7FFFFFFF, 0X80000001
			assert_false r0

			mov r1,     117
			is_iugt      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     118
			is_iugt      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_false r0

			mov r1,     119
			is_iugt      r0, 0X80000000, 0X00000000
			assert_true  r0

			mov r1,     120
			is_iugt      r0, 0X80000000, 0X00000001
			assert_true  r0

			mov r1,     121
			is_iugt      r0, 0X80000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     122
			is_iugt      r0, 0X80000000, 0X80000000
			assert_false r0

			mov r1,     123
			is_iugt      r0, 0X80000000, 0X80000001
			assert_false r0

			mov r1,     124
			is_iugt      r0, 0X80000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     125
			is_iugt      r0, 0X80000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     126
			is_iugt      r0, 0X80000001, 0X00000000
			assert_true  r0

			mov r1,     127
			is_iugt      r0, 0X80000001, 0X00000001
			assert_true  r0

			mov r1,     128
			is_iugt      r0, 0X80000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     129
			is_iugt      r0, 0X80000001, 0X80000000
			assert_true  r0

			mov r1,     130
			is_iugt      r0, 0X80000001, 0X80000001
			assert_false r0

			mov r1,     131
			is_iugt      r0, 0X80000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     132
			is_iugt      r0, 0X80000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     133
			is_iugt      r0, 0XFFFFFFFE, 0X00000000
			assert_true  r0

			mov r1,     134
			is_iugt      r0, 0XFFFFFFFE, 0X00000001
			assert_true  r0

			mov r1,     135
			is_iugt      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_true  r0

			mov r1,     136
			is_iugt      r0, 0XFFFFFFFE, 0X80000000
			assert_true  r0

			mov r1,     137
			is_iugt      r0, 0XFFFFFFFE, 0X80000001
			assert_true  r0

			mov r1,     138
			is_iugt      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_false r0

			mov r1,     139
			is_iugt      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_false r0

			mov r1,     140
			is_iugt      r0, 0XFFFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     141
			is_iugt      r0, 0XFFFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     142
			is_iugt      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     143
			is_iugt      r0, 0XFFFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     144
			is_iugt      r0, 0XFFFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     145
			is_iugt      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     146
			is_iugt      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_false r0


			######
			mov r1,     147
			is_iule      r0, 0X00000000, 0X00000000
			assert_true  r0

			mov r1,     148
			is_iule      r0, 0X00000000, 0X00000001
			assert_true  r0

			mov r1,     149
			is_iule      r0, 0X00000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     150
			is_iule      r0, 0X00000000, 0X80000000
			assert_true  r0

			mov r1,     151
			is_iule      r0, 0X00000000, 0X80000001
			assert_true  r0

			mov r1,     152
			is_iule      r0, 0X00000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     153
			is_iule      r0, 0X00000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     154
			is_iule      r0, 0X00000001, 0X00000000
			assert_false r0

			mov r1,     155
			is_iule      r0, 0X00000001, 0X00000001
			assert_true  r0

			mov r1,     156
			is_iule      r0, 0X00000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     157
			is_iule      r0, 0X00000001, 0X80000000
			assert_true  r0

			mov r1,     158
			is_iule      r0, 0X00000001, 0X80000001
			assert_true  r0

			mov r1,     159
			is_iule      r0, 0X00000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     160
			is_iule      r0, 0X00000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     161
			is_iule      r0, 0X7FFFFFFF, 0X00000000
			assert_false r0

			mov r1,     162
			is_iule      r0, 0X7FFFFFFF, 0X00000001
			assert_false r0

			mov r1,     163
			is_iule      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     164
			is_iule      r0, 0X7FFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     165
			is_iule      r0, 0X7FFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     166
			is_iule      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     167
			is_iule      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_true  r0

			mov r1,     168
			is_iule      r0, 0X80000000, 0X00000000
			assert_false r0

			mov r1,     169
			is_iule      r0, 0X80000000, 0X00000001
			assert_false r0

			mov r1,     170
			is_iule      r0, 0X80000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     171
			is_iule      r0, 0X80000000, 0X80000000
			assert_true  r0

			mov r1,     172
			is_iule      r0, 0X80000000, 0X80000001
			assert_true  r0

			mov r1,     173
			is_iule      r0, 0X80000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     174
			is_iule      r0, 0X80000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     175
			is_iule      r0, 0X80000001, 0X00000000
			assert_false r0

			mov r1,     176
			is_iule      r0, 0X80000001, 0X00000001
			assert_false r0

			mov r1,     177
			is_iule      r0, 0X80000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     178
			is_iule      r0, 0X80000001, 0X80000000
			assert_false r0

			mov r1,     179
			is_iule      r0, 0X80000001, 0X80000001
			assert_true  r0

			mov r1,     180
			is_iule      r0, 0X80000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     181
			is_iule      r0, 0X80000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     182
			is_iule      r0, 0XFFFFFFFE, 0X00000000
			assert_false r0

			mov r1,     183
			is_iule      r0, 0XFFFFFFFE, 0X00000001
			assert_false r0

			mov r1,     184
			is_iule      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_false r0

			mov r1,     185
			is_iule      r0, 0XFFFFFFFE, 0X80000000
			assert_false r0

			mov r1,     186
			is_iule      r0, 0XFFFFFFFE, 0X80000001
			assert_false r0

			mov r1,     187
			is_iule      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_true  r0

			mov r1,     188
			is_iule      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_true  r0

			mov r1,     189
			is_iule      r0, 0XFFFFFFFF, 0X00000000
			assert_false r0

			mov r1,     190
			is_iule      r0, 0XFFFFFFFF, 0X00000001
			assert_false r0

			mov r1,     191
			is_iule      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     192
			is_iule      r0, 0XFFFFFFFF, 0X80000000
			assert_false r0

			mov r1,     193
			is_iule      r0, 0XFFFFFFFF, 0X80000001
			assert_false r0

			mov r1,     194
			is_iule      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     195
			is_iule      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_true  r0


			######
			mov r1,     196
			is_iuge      r0, 0X00000000, 0X00000000
			assert_true  r0

			mov r1,     197
			is_iuge      r0, 0X00000000, 0X00000001
			assert_false r0

			mov r1,     198
			is_iuge      r0, 0X00000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     199
			is_iuge      r0, 0X00000000, 0X80000000
			assert_false r0

			mov r1,     200
			is_iuge      r0, 0X00000000, 0X80000001
			assert_false r0

			mov r1,     201
			is_iuge      r0, 0X00000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     202
			is_iuge      r0, 0X00000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     203
			is_iuge      r0, 0X00000001, 0X00000000
			assert_true  r0

			mov r1,     204
			is_iuge      r0, 0X00000001, 0X00000001
			assert_true  r0

			mov r1,     205
			is_iuge      r0, 0X00000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     206
			is_iuge      r0, 0X00000001, 0X80000000
			assert_false r0

			mov r1,     207
			is_iuge      r0, 0X00000001, 0X80000001
			assert_false r0

			mov r1,     208
			is_iuge      r0, 0X00000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     209
			is_iuge      r0, 0X00000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     210
			is_iuge      r0, 0X7FFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     211
			is_iuge      r0, 0X7FFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     212
			is_iuge      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     213
			is_iuge      r0, 0X7FFFFFFF, 0X80000000
			assert_false r0

			mov r1,     214
			is_iuge      r0, 0X7FFFFFFF, 0X80000001
			assert_false r0

			mov r1,     215
			is_iuge      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     216
			is_iuge      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_false r0

			mov r1,     217
			is_iuge      r0, 0X80000000, 0X00000000
			assert_true  r0

			mov r1,     218
			is_iuge      r0, 0X80000000, 0X00000001
			assert_true  r0

			mov r1,     219
			is_iuge      r0, 0X80000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     220
			is_iuge      r0, 0X80000000, 0X80000000
			assert_true  r0

			mov r1,     221
			is_iuge      r0, 0X80000000, 0X80000001
			assert_false r0

			mov r1,     222
			is_iuge      r0, 0X80000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     223
			is_iuge      r0, 0X80000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     224
			is_iuge      r0, 0X80000001, 0X00000000
			assert_true  r0

			mov r1,     225
			is_iuge      r0, 0X80000001, 0X00000001
			assert_true  r0

			mov r1,     226
			is_iuge      r0, 0X80000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     227
			is_iuge      r0, 0X80000001, 0X80000000
			assert_true  r0

			mov r1,     228
			is_iuge      r0, 0X80000001, 0X80000001
			assert_true  r0

			mov r1,     229
			is_iuge      r0, 0X80000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     230
			is_iuge      r0, 0X80000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     231
			is_iuge      r0, 0XFFFFFFFE, 0X00000000
			assert_true  r0

			mov r1,     232
			is_iuge      r0, 0XFFFFFFFE, 0X00000001
			assert_true  r0

			mov r1,     233
			is_iuge      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_true  r0

			mov r1,     234
			is_iuge      r0, 0XFFFFFFFE, 0X80000000
			assert_true  r0

			mov r1,     235
			is_iuge      r0, 0XFFFFFFFE, 0X80000001
			assert_true  r0

			mov r1,     236
			is_iuge      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_true  r0

			mov r1,     237
			is_iuge      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_false r0

			mov r1,     238
			is_iuge      r0, 0XFFFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     239
			is_iuge      r0, 0XFFFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     240
			is_iuge      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     241
			is_iuge      r0, 0XFFFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     242
			is_iuge      r0, 0XFFFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     243
			is_iuge      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     244
			is_iuge      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_true  r0


			######
			mov r1,     245
			is_iult      r0, 0X00000000, 0X00000000
			assert_false r0

			mov r1,     246
			is_iult      r0, 0X00000000, 0X00000001
			assert_true  r0

			mov r1,     247
			is_iult      r0, 0X00000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     248
			is_iult      r0, 0X00000000, 0X80000000
			assert_true  r0

			mov r1,     249
			is_iult      r0, 0X00000000, 0X80000001
			assert_true  r0

			mov r1,     250
			is_iult      r0, 0X00000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     251
			is_iult      r0, 0X00000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     252
			is_iult      r0, 0X00000001, 0X00000000
			assert_false r0

			mov r1,     253
			is_iult      r0, 0X00000001, 0X00000001
			assert_false r0

			mov r1,     254
			is_iult      r0, 0X00000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     255
			is_iult      r0, 0X00000001, 0X80000000
			assert_true  r0

			mov r1,     256
			is_iult      r0, 0X00000001, 0X80000001
			assert_true  r0

			mov r1,     257
			is_iult      r0, 0X00000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     258
			is_iult      r0, 0X00000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     259
			is_iult      r0, 0X7FFFFFFF, 0X00000000
			assert_false r0

			mov r1,     260
			is_iult      r0, 0X7FFFFFFF, 0X00000001
			assert_false r0

			mov r1,     261
			is_iult      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     262
			is_iult      r0, 0X7FFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     263
			is_iult      r0, 0X7FFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     264
			is_iult      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     265
			is_iult      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_true  r0

			mov r1,     266
			is_iult      r0, 0X80000000, 0X00000000
			assert_false r0

			mov r1,     267
			is_iult      r0, 0X80000000, 0X00000001
			assert_false r0

			mov r1,     268
			is_iult      r0, 0X80000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     269
			is_iult      r0, 0X80000000, 0X80000000
			assert_false r0

			mov r1,     270
			is_iult      r0, 0X80000000, 0X80000001
			assert_true  r0

			mov r1,     271
			is_iult      r0, 0X80000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     272
			is_iult      r0, 0X80000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     273
			is_iult      r0, 0X80000001, 0X00000000
			assert_false r0

			mov r1,     274
			is_iult      r0, 0X80000001, 0X00000001
			assert_false r0

			mov r1,     275
			is_iult      r0, 0X80000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     276
			is_iult      r0, 0X80000001, 0X80000000
			assert_false r0

			mov r1,     277
			is_iult      r0, 0X80000001, 0X80000001
			assert_false r0

			mov r1,     278
			is_iult      r0, 0X80000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     279
			is_iult      r0, 0X80000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     280
			is_iult      r0, 0XFFFFFFFE, 0X00000000
			assert_false r0

			mov r1,     281
			is_iult      r0, 0XFFFFFFFE, 0X00000001
			assert_false r0

			mov r1,     282
			is_iult      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_false r0

			mov r1,     283
			is_iult      r0, 0XFFFFFFFE, 0X80000000
			assert_false r0

			mov r1,     284
			is_iult      r0, 0XFFFFFFFE, 0X80000001
			assert_false r0

			mov r1,     285
			is_iult      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_false r0

			mov r1,     286
			is_iult      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_true  r0

			mov r1,     287
			is_iult      r0, 0XFFFFFFFF, 0X00000000
			assert_false r0

			mov r1,     288
			is_iult      r0, 0XFFFFFFFF, 0X00000001
			assert_false r0

			mov r1,     289
			is_iult      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     290
			is_iult      r0, 0XFFFFFFFF, 0X80000000
			assert_false r0

			mov r1,     291
			is_iult      r0, 0XFFFFFFFF, 0X80000001
			assert_false r0

			mov r1,     292
			is_iult      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     293
			is_iult      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_false r0


			######
			mov r1,     294
			is_isgt      r0, 0X00000000, 0X00000000
			assert_false r0

			mov r1,     295
			is_isgt      r0, 0X00000000, 0X00000001
			assert_false r0

			mov r1,     296
			is_isgt      r0, 0X00000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     297
			is_isgt      r0, 0X00000000, 0X80000000
			assert_true  r0

			mov r1,     298
			is_isgt      r0, 0X00000000, 0X80000001
			assert_true  r0

			mov r1,     299
			is_isgt      r0, 0X00000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     300
			is_isgt      r0, 0X00000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     301
			is_isgt      r0, 0X00000001, 0X00000000
			assert_true  r0

			mov r1,     302
			is_isgt      r0, 0X00000001, 0X00000001
			assert_false r0

			mov r1,     303
			is_isgt      r0, 0X00000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     304
			is_isgt      r0, 0X00000001, 0X80000000
			assert_true  r0

			mov r1,     305
			is_isgt      r0, 0X00000001, 0X80000001
			assert_true  r0

			mov r1,     306
			is_isgt      r0, 0X00000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     307
			is_isgt      r0, 0X00000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     308
			is_isgt      r0, 0X7FFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     309
			is_isgt      r0, 0X7FFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     310
			is_isgt      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     311
			is_isgt      r0, 0X7FFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     312
			is_isgt      r0, 0X7FFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     313
			is_isgt      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     314
			is_isgt      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_true  r0

			mov r1,     315
			is_isgt      r0, 0X80000000, 0X00000000
			assert_false r0

			mov r1,     316
			is_isgt      r0, 0X80000000, 0X00000001
			assert_false r0

			mov r1,     317
			is_isgt      r0, 0X80000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     318
			is_isgt      r0, 0X80000000, 0X80000000
			assert_false r0

			mov r1,     319
			is_isgt      r0, 0X80000000, 0X80000001
			assert_false r0

			mov r1,     320
			is_isgt      r0, 0X80000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     321
			is_isgt      r0, 0X80000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     322
			is_isgt      r0, 0X80000001, 0X00000000
			assert_false r0

			mov r1,     323
			is_isgt      r0, 0X80000001, 0X00000001
			assert_false r0

			mov r1,     324
			is_isgt      r0, 0X80000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     325
			is_isgt      r0, 0X80000001, 0X80000000
			assert_true  r0

			mov r1,     326
			is_isgt      r0, 0X80000001, 0X80000001
			assert_false r0

			mov r1,     327
			is_isgt      r0, 0X80000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     328
			is_isgt      r0, 0X80000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     329
			is_isgt      r0, 0XFFFFFFFE, 0X00000000
			assert_false r0

			mov r1,     330
			is_isgt      r0, 0XFFFFFFFE, 0X00000001
			assert_false r0

			mov r1,     331
			is_isgt      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_false r0

			mov r1,     332
			is_isgt      r0, 0XFFFFFFFE, 0X80000000
			assert_true  r0

			mov r1,     333
			is_isgt      r0, 0XFFFFFFFE, 0X80000001
			assert_true  r0

			mov r1,     334
			is_isgt      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_false r0

			mov r1,     335
			is_isgt      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_false r0

			mov r1,     336
			is_isgt      r0, 0XFFFFFFFF, 0X00000000
			assert_false r0

			mov r1,     337
			is_isgt      r0, 0XFFFFFFFF, 0X00000001
			assert_false r0

			mov r1,     338
			is_isgt      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     339
			is_isgt      r0, 0XFFFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     340
			is_isgt      r0, 0XFFFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     341
			is_isgt      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     342
			is_isgt      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_false r0


			######
			mov r1,     343
			is_isle      r0, 0X00000000, 0X00000000
			assert_true  r0

			mov r1,     344
			is_isle      r0, 0X00000000, 0X00000001
			assert_true  r0

			mov r1,     345
			is_isle      r0, 0X00000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     346
			is_isle      r0, 0X00000000, 0X80000000
			assert_false r0

			mov r1,     347
			is_isle      r0, 0X00000000, 0X80000001
			assert_false r0

			mov r1,     348
			is_isle      r0, 0X00000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     349
			is_isle      r0, 0X00000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     350
			is_isle      r0, 0X00000001, 0X00000000
			assert_false r0

			mov r1,     351
			is_isle      r0, 0X00000001, 0X00000001
			assert_true  r0

			mov r1,     352
			is_isle      r0, 0X00000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     353
			is_isle      r0, 0X00000001, 0X80000000
			assert_false r0

			mov r1,     354
			is_isle      r0, 0X00000001, 0X80000001
			assert_false r0

			mov r1,     355
			is_isle      r0, 0X00000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     356
			is_isle      r0, 0X00000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     357
			is_isle      r0, 0X7FFFFFFF, 0X00000000
			assert_false r0

			mov r1,     358
			is_isle      r0, 0X7FFFFFFF, 0X00000001
			assert_false r0

			mov r1,     359
			is_isle      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     360
			is_isle      r0, 0X7FFFFFFF, 0X80000000
			assert_false r0

			mov r1,     361
			is_isle      r0, 0X7FFFFFFF, 0X80000001
			assert_false r0

			mov r1,     362
			is_isle      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     363
			is_isle      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_false r0

			mov r1,     364
			is_isle      r0, 0X80000000, 0X00000000
			assert_true  r0

			mov r1,     365
			is_isle      r0, 0X80000000, 0X00000001
			assert_true  r0

			mov r1,     366
			is_isle      r0, 0X80000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     367
			is_isle      r0, 0X80000000, 0X80000000
			assert_true  r0

			mov r1,     368
			is_isle      r0, 0X80000000, 0X80000001
			assert_true  r0

			mov r1,     369
			is_isle      r0, 0X80000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     370
			is_isle      r0, 0X80000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     371
			is_isle      r0, 0X80000001, 0X00000000
			assert_true  r0

			mov r1,     372
			is_isle      r0, 0X80000001, 0X00000001
			assert_true  r0

			mov r1,     373
			is_isle      r0, 0X80000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     374
			is_isle      r0, 0X80000001, 0X80000000
			assert_false r0

			mov r1,     375
			is_isle      r0, 0X80000001, 0X80000001
			assert_true  r0

			mov r1,     376
			is_isle      r0, 0X80000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     377
			is_isle      r0, 0X80000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     378
			is_isle      r0, 0XFFFFFFFE, 0X00000000
			assert_true  r0

			mov r1,     379
			is_isle      r0, 0XFFFFFFFE, 0X00000001
			assert_true  r0

			mov r1,     380
			is_isle      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_true  r0

			mov r1,     381
			is_isle      r0, 0XFFFFFFFE, 0X80000000
			assert_false r0

			mov r1,     382
			is_isle      r0, 0XFFFFFFFE, 0X80000001
			assert_false r0

			mov r1,     383
			is_isle      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_true  r0

			mov r1,     384
			is_isle      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_true  r0

			mov r1,     385
			is_isle      r0, 0XFFFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     386
			is_isle      r0, 0XFFFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     387
			is_isle      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     388
			is_isle      r0, 0XFFFFFFFF, 0X80000000
			assert_false r0

			mov r1,     389
			is_isle      r0, 0XFFFFFFFF, 0X80000001
			assert_false r0

			mov r1,     390
			is_isle      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     391
			is_isle      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_true  r0


			######
			mov r1,     392
			is_isge      r0, 0X00000000, 0X00000000
			assert_true  r0

			mov r1,     393
			is_isge      r0, 0X00000000, 0X00000001
			assert_false r0

			mov r1,     394
			is_isge      r0, 0X00000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     395
			is_isge      r0, 0X00000000, 0X80000000
			assert_true  r0

			mov r1,     396
			is_isge      r0, 0X00000000, 0X80000001
			assert_true  r0

			mov r1,     397
			is_isge      r0, 0X00000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     398
			is_isge      r0, 0X00000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     399
			is_isge      r0, 0X00000001, 0X00000000
			assert_true  r0

			mov r1,     400
			is_isge      r0, 0X00000001, 0X00000001
			assert_true  r0

			mov r1,     401
			is_isge      r0, 0X00000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     402
			is_isge      r0, 0X00000001, 0X80000000
			assert_true  r0

			mov r1,     403
			is_isge      r0, 0X00000001, 0X80000001
			assert_true  r0

			mov r1,     404
			is_isge      r0, 0X00000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     405
			is_isge      r0, 0X00000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     406
			is_isge      r0, 0X7FFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     407
			is_isge      r0, 0X7FFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     408
			is_isge      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     409
			is_isge      r0, 0X7FFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     410
			is_isge      r0, 0X7FFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     411
			is_isge      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     412
			is_isge      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_true  r0

			mov r1,     413
			is_isge      r0, 0X80000000, 0X00000000
			assert_false r0

			mov r1,     414
			is_isge      r0, 0X80000000, 0X00000001
			assert_false r0

			mov r1,     415
			is_isge      r0, 0X80000000, 0X7FFFFFFF
			assert_false r0

			mov r1,     416
			is_isge      r0, 0X80000000, 0X80000000
			assert_true  r0

			mov r1,     417
			is_isge      r0, 0X80000000, 0X80000001
			assert_false r0

			mov r1,     418
			is_isge      r0, 0X80000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     419
			is_isge      r0, 0X80000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     420
			is_isge      r0, 0X80000001, 0X00000000
			assert_false r0

			mov r1,     421
			is_isge      r0, 0X80000001, 0X00000001
			assert_false r0

			mov r1,     422
			is_isge      r0, 0X80000001, 0X7FFFFFFF
			assert_false r0

			mov r1,     423
			is_isge      r0, 0X80000001, 0X80000000
			assert_true  r0

			mov r1,     424
			is_isge      r0, 0X80000001, 0X80000001
			assert_true  r0

			mov r1,     425
			is_isge      r0, 0X80000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     426
			is_isge      r0, 0X80000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     427
			is_isge      r0, 0XFFFFFFFE, 0X00000000
			assert_false r0

			mov r1,     428
			is_isge      r0, 0XFFFFFFFE, 0X00000001
			assert_false r0

			mov r1,     429
			is_isge      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_false r0

			mov r1,     430
			is_isge      r0, 0XFFFFFFFE, 0X80000000
			assert_true  r0

			mov r1,     431
			is_isge      r0, 0XFFFFFFFE, 0X80000001
			assert_true  r0

			mov r1,     432
			is_isge      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_true  r0

			mov r1,     433
			is_isge      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_false r0

			mov r1,     434
			is_isge      r0, 0XFFFFFFFF, 0X00000000
			assert_false r0

			mov r1,     435
			is_isge      r0, 0XFFFFFFFF, 0X00000001
			assert_false r0

			mov r1,     436
			is_isge      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     437
			is_isge      r0, 0XFFFFFFFF, 0X80000000
			assert_true  r0

			mov r1,     438
			is_isge      r0, 0XFFFFFFFF, 0X80000001
			assert_true  r0

			mov r1,     439
			is_isge      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_true  r0

			mov r1,     440
			is_isge      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_true  r0


			######
			mov r1,     441
			is_islt      r0, 0X00000000, 0X00000000
			assert_false r0

			mov r1,     442
			is_islt      r0, 0X00000000, 0X00000001
			assert_true  r0

			mov r1,     443
			is_islt      r0, 0X00000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     444
			is_islt      r0, 0X00000000, 0X80000000
			assert_false r0

			mov r1,     445
			is_islt      r0, 0X00000000, 0X80000001
			assert_false r0

			mov r1,     446
			is_islt      r0, 0X00000000, 0XFFFFFFFE
			assert_false r0

			mov r1,     447
			is_islt      r0, 0X00000000, 0XFFFFFFFF
			assert_false r0

			mov r1,     448
			is_islt      r0, 0X00000001, 0X00000000
			assert_false r0

			mov r1,     449
			is_islt      r0, 0X00000001, 0X00000001
			assert_false r0

			mov r1,     450
			is_islt      r0, 0X00000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     451
			is_islt      r0, 0X00000001, 0X80000000
			assert_false r0

			mov r1,     452
			is_islt      r0, 0X00000001, 0X80000001
			assert_false r0

			mov r1,     453
			is_islt      r0, 0X00000001, 0XFFFFFFFE
			assert_false r0

			mov r1,     454
			is_islt      r0, 0X00000001, 0XFFFFFFFF
			assert_false r0

			mov r1,     455
			is_islt      r0, 0X7FFFFFFF, 0X00000000
			assert_false r0

			mov r1,     456
			is_islt      r0, 0X7FFFFFFF, 0X00000001
			assert_false r0

			mov r1,     457
			is_islt      r0, 0X7FFFFFFF, 0X7FFFFFFF
			assert_false r0

			mov r1,     458
			is_islt      r0, 0X7FFFFFFF, 0X80000000
			assert_false r0

			mov r1,     459
			is_islt      r0, 0X7FFFFFFF, 0X80000001
			assert_false r0

			mov r1,     460
			is_islt      r0, 0X7FFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     461
			is_islt      r0, 0X7FFFFFFF, 0XFFFFFFFF
			assert_false r0

			mov r1,     462
			is_islt      r0, 0X80000000, 0X00000000
			assert_true  r0

			mov r1,     463
			is_islt      r0, 0X80000000, 0X00000001
			assert_true  r0

			mov r1,     464
			is_islt      r0, 0X80000000, 0X7FFFFFFF
			assert_true  r0

			mov r1,     465
			is_islt      r0, 0X80000000, 0X80000000
			assert_false r0

			mov r1,     466
			is_islt      r0, 0X80000000, 0X80000001
			assert_true  r0

			mov r1,     467
			is_islt      r0, 0X80000000, 0XFFFFFFFE
			assert_true  r0

			mov r1,     468
			is_islt      r0, 0X80000000, 0XFFFFFFFF
			assert_true  r0

			mov r1,     469
			is_islt      r0, 0X80000001, 0X00000000
			assert_true  r0

			mov r1,     470
			is_islt      r0, 0X80000001, 0X00000001
			assert_true  r0

			mov r1,     471
			is_islt      r0, 0X80000001, 0X7FFFFFFF
			assert_true  r0

			mov r1,     472
			is_islt      r0, 0X80000001, 0X80000000
			assert_false r0

			mov r1,     473
			is_islt      r0, 0X80000001, 0X80000001
			assert_false r0

			mov r1,     474
			is_islt      r0, 0X80000001, 0XFFFFFFFE
			assert_true  r0

			mov r1,     475
			is_islt      r0, 0X80000001, 0XFFFFFFFF
			assert_true  r0

			mov r1,     476
			is_islt      r0, 0XFFFFFFFE, 0X00000000
			assert_true  r0

			mov r1,     477
			is_islt      r0, 0XFFFFFFFE, 0X00000001
			assert_true  r0

			mov r1,     478
			is_islt      r0, 0XFFFFFFFE, 0X7FFFFFFF
			assert_true  r0

			mov r1,     479
			is_islt      r0, 0XFFFFFFFE, 0X80000000
			assert_false r0

			mov r1,     480
			is_islt      r0, 0XFFFFFFFE, 0X80000001
			assert_false r0

			mov r1,     481
			is_islt      r0, 0XFFFFFFFE, 0XFFFFFFFE
			assert_false r0

			mov r1,     482
			is_islt      r0, 0XFFFFFFFE, 0XFFFFFFFF
			assert_true  r0

			mov r1,     483
			is_islt      r0, 0XFFFFFFFF, 0X00000000
			assert_true  r0

			mov r1,     484
			is_islt      r0, 0XFFFFFFFF, 0X00000001
			assert_true  r0

			mov r1,     485
			is_islt      r0, 0XFFFFFFFF, 0X7FFFFFFF
			assert_true  r0

			mov r1,     486
			is_islt      r0, 0XFFFFFFFF, 0X80000000
			assert_false r0

			mov r1,     487
			is_islt      r0, 0XFFFFFFFF, 0X80000001
			assert_false r0

			mov r1,     488
			is_islt      r0, 0XFFFFFFFF, 0XFFFFFFFE
			assert_false r0

			mov r1,     489
			is_islt      r0, 0XFFFFFFFF, 0XFFFFFFFF
			assert_false r0


			######

			halt
