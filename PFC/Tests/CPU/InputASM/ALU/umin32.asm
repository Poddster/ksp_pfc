    umin32    r0, 0, 4             #0 r0 = 0
    assert_eq r0, 0
    umin32    r1, 3, 8             #2 r1 = 3
    assert_eq r1, 3
    umin32    r2, r0, r1           #4 r2 = 0
    assert_eq r2, 0
    umin32    r0, r0, -7           #6 r0 = 0
    assert_eq r0, 0
    umin32    r3, -1, r1           #8 r3 = 3
    assert_eq r3, 3
    umin32    r4, 0xFFFFFFFF, r1   #10 r4 = 3
    assert_eq r4, 3
    umin32    r0, r0, r0           #12 r0 = 0
    assert_eq r0, 0

                halt

