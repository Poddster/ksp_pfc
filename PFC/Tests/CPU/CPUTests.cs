﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using PFC.CPU;
using PFC.ASMParser;

namespace PFC.Tests.CPU
{
    using System.IO;
    using JetBrains.Annotations;

    public static class CPUTests
    {
        /// <summary>
        /// Checks to make sure a test throws an exception.
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="func"></param>
        private static void AssertThrows<TException>([NotNull] Action func) where TException : Exception
        {
            bool bPassed = false;
            try
            {
                func();
            }
            catch (TException e)
            {
                Debug.Assert(e.Message != null); // just to stop compiler complaining e is not used
                bPassed = true;
            }
            Debug.Assert(bPassed);
        }

        [NotNull]
        private static readonly Tuple<SimpleRegister, ulong>[] NoResultsCheck = new Tuple<SimpleRegister, ulong>[0];



        private static void RunTestFile(
            [NotNull] string path,
                      int    maxCycleCount = 100,
                      bool   trace = false)
        {
            // This is terrible and should probably be done using the 'resouce' system in VS and .NET?
            // obviously this only works when run from the dbg/bin dirs, but it's for tests, so it's probably
            // ok.

            var curDir = new DirectoryInfo(Directory.GetCurrentDirectory());
            Debug.Assert(curDir.Parent != null, "curDir.Parent != null");
            Debug.Assert(curDir.Parent.FullName.EndsWith("bin"),
                         "Wrong directory. Run from ConsoleTest/bin/<Release/Debug>/ConsoleTest.exe");

            Debug.Assert(curDir.Parent.Parent != null, "curDir.Parent.Parent != null");
            Debug.Assert(curDir.Parent.Parent.FullName.EndsWith("ConsoleTest"),
                         "Wrong directory. Run from ConsoleTest/bin/<Release/Debug>/ConsoleTest.exe");

            Debug.Assert(curDir.Parent.Parent.Parent != null, "curDir.Parent.Parent != null");
            Debug.Assert(curDir.Parent.Parent.Parent.FullName.EndsWith("PFC"),
                         "Wrong directory. Run from ConsoleTest/bin/<Release/Debug>/ConsoleTest.exe");

            var solRoot = curDir.Parent.Parent.Parent;
            var asmDir = Path.Combine(solRoot.FullName, @"PFC\Tests\CPU\InputASM");
            string startupPath = Path.Combine(asmDir, path);


            //TODO make PFCParser.ParseFile  that throws errors with file/line name etc


            string fileText = File.ReadAllText(startupPath);


            var instructions = PFCParser.ParseAssembly(fileText);

            //TODO: read results check and maxCycleCount stuff from the file itself.

            RunInstructionsAndCheckResultsUntilHalted(
                instructions,
                NoResultsCheck,
                maxCycleCount,
                trace);
        }

        private static void RunUntilHalted(
            [NotNull] string asm,
            [CanBeNull] Tuple<SimpleRegister, UInt64>[] results = null,
            int maxCycleCount = 100,
            bool trace = false)
        {
            if (results == null)
            {
                results = NoResultsCheck;
            }

            var instructions = PFCParser.ParseAssembly(asm);
            RunInstructionsAndCheckResultsUntilHalted(
                instructions,
                results,
                maxCycleCount,
                trace);
        }

        private static void RunInstructionsAndCheckResultsUntilHalted(
            [NotNull]        Instruction[]                  instructions,
            [NotNull]        Tuple<SimpleRegister, ulong>[] results,
            [UsedImplicitly] int                            maxCycleCount,
                             bool                           trace)
        {
            var state32 = new CPUState(0, 32, instructions);
            var cpu32 = new CPU_VM(0, 32, state32);

            cpu32.TraceInst = trace;

            while (!state32.IsHalted)
            {
                Debug.Assert(state32.cycleCounter <= maxCycleCount);
                cpu32.Clock();
            }

            foreach (var pair in results)
            {
                Debug.Assert(pair != null, "pair != null");
                Debug.Assert(pair.Item1 != null, "pair.Item1 != null");
                if (pair.Item1.RegType != RegisterType.scalar_reg)
                {
                    throw new NotImplementedException();
                }
                int index = (int)pair.Item1.Index;
                ulong val = pair.Item2;
                Debug.Assert(state32.RegisterBank != null, "state32.RegisterBank != null");
                Debug.Assert(val == state32.RegisterBank[index]);
            }
        }

        #region CPUTestBasic
        private static void CPUTestBasicInstructionEmpty()
        {
            var cpu = new CPU_VM(0, 32, PFCParser.ParseAssembly(""));

            Debug.Assert(cpu.ROState.CodeROM.Count == 0);

            Debug.Assert(cpu.ROState.cycleCounter == 0);
            Debug.Assert(cpu.ROState.ProgramCounter == 0);

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu.Clock());

            Debug.Assert(cpu.ROState.cycleCounter == 0);
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
        }

        private static void CPUTestBasicInstructionNop()
        {
            //just make sure NOP takes 0 operands
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.nop,
                                                                  OperandLiteral.u32(0)));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.nop,
                                                                  OperandLiteral.u32(0), 32));


            var prog = PFCParser.ParseAssembly("nop");
            var cpu = new CPU_VM(0, 32, prog);

            Debug.Assert(cpu.ROState.cycleCounter == 0);
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
            cpu.Clock();
            Debug.Assert(cpu.ROState.cycleCounter == 1);
            Debug.Assert(cpu.ROState.ProgramCounter == 1);

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu.Clock());


            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("nop!"));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.nop,
                                                                  writesCPUFlags: true));
        }

        private static void CPUTestBasicInstructionHalt()
        {
            string asm = @"
                            nop                 #0
                            nop                 #1
                            nop                 #2
                            mov r0, 0x1         #3
                            halt                #4
                            mov r0, 0x2         #5                    
            ";
            var cpu = new CPU_VM(0, 32, PFCParser.ParseAssembly(asm));

            Debug.Assert(cpu.ROState.cycleCounter == 0);
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 0);

            cpu.Clock(); cpu.Clock(); cpu.Clock();

            Debug.Assert(cpu.ROState.ProgramCounter == 3);
            cpu.Clock();
            Debug.Assert(cpu.ROState.RegisterBank[0] == 1);

            Debug.Assert(cpu.ROState.ProgramCounter == 4);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 4);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 1);

            Debug.Assert(cpu.ROState.ProgramCounter == 4);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 4);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 1);

            Debug.Assert(cpu.ROState.ProgramCounter == 4);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 4);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 1);


            var results = new[] { Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0),
                                               0x1UL), };
            RunUntilHalted(asm, results, maxCycleCount: 4);


            //just make sure halt takes 0 operands
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.halt,
                                                                  OperandLiteral.u32(0)));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.halt,
                                                                  OperandLiteral.u32(0), 32));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("halt r0"));

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("halt!"));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.halt,
                                                                  writesCPUFlags: true));

        }
        private static void CPUTestBasicInstructionAssert()
        {
            string assert_pos = @"
                mov r0, 0x1
                assert_eq r0, 0x1

                mov r0, 0x1
                assert_neq r0, 0x47873

                assert_eq  0b1000, 0b1000
                assert_neq 0b1000, 0b1110

                mov r0, 0x1
                assert_true r0
                assert_true 0xFFFFFFFF

                mov r1, 0x0
                assert_false r1
                assert_false 0x0
                halt
            ";

            //individual error cases
            string eq_neg = @"
                mov r0, 0x1
                assert_eq r0, 0x47873
                halt
            ";

            string ne_neg = @"
                mov r0, 0x1
                assert_neq r0, 0x1
                halt
            ";

            string eq_lit_neg = @"
                assert_eq 0b1000, 0b1001
                halt
            ";

            string ne_lit_neg = @"
                assert_neq 0b1000, 0b1000
                halt
            ";

            string assert_true_neg = @"
                mov r0, 0x0
                assert_true r0
                halt
            ";

            string assert_true_lit_neg = @"
                assert_true 0x0
                halt
            ";

            string assert_false_neg = @"
                mov r0, 0xFF
                assert_false r0
                halt
            ";

            string assert_false_lit_neg = @"
                assert_false 0x132
                halt
            ";

            RunUntilHalted(assert_pos);

            //assert throws:something. AssertionError? PFCAssertionError? Interrupt?

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_eq r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_neq r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_true"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_false"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_true r0, r1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_false r1, r2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_eq8 0x0, 0x0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_neq32 0x1, 0x2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_true8 0x0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_false8 0x0"));

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_eq! 0x1, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_neq! 0x1, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_true! 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("assert_false! 0x1"));



            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(eq_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(ne_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(eq_lit_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(ne_lit_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(assert_true_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(assert_true_lit_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(assert_false_neg));
            AssertThrows<PFC_CPUAssertionException>(() => RunUntilHalted(assert_false_lit_neg));
        }

        private static void CPUTestBasicInstrucions()
        {
            CPUTestBasicInstructionEmpty();
            CPUTestBasicInstructionHalt();
            CPUTestBasicInstructionNop();
            CPUTestBasicInstructionAssert();
        }
        #endregion

        #region CPUTestDatamov
        private static void CPUTestDatamovInstructionMov32()
        {
            //just make sure MOV takes 2 operands
            var dummyOperand = OperandLiteral.u32(0x0);
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.mov));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.mov, dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.mov, dummyOperand,
                                                                  dummyOperand, dummyOperand));

            //make a program with MOV in them
            string prog = @"mov r0, 1
                            mov r1, 1.0f";
            var cpu = new CPU_VM(0, 32, PFCParser.ParseAssembly(prog));

            Debug.Assert(cpu.ROState.cycleCounter == 0);
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 0);
            Debug.Assert(cpu.ROState.RegisterBank[1] == 0);
            cpu.Clock();
            Debug.Assert(cpu.ROState.cycleCounter == 1);
            Debug.Assert(cpu.ROState.ProgramCounter == 1);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 0x1);
            Debug.Assert(cpu.ROState.RegisterBank[1] == 0);
            cpu.Clock();
            Debug.Assert(cpu.ROState.cycleCounter == 2);
            Debug.Assert(cpu.ROState.ProgramCounter == 2);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 0x1);
            Debug.Assert(cpu.ROState.RegisterBank[1] == (ulong)new Float_Union(1.0f).i);

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu.Clock());
        }

        private static void CPUTestDatamovInstructionMovSignExt()
        {
            RunTestFile(@"Datamov\mov_sign_ext.asm", maxCycleCount: 441);
        }

        private static void CPUTestDatamovInstructionMovC()
        {
            RunTestFile(@"Datamov\movc.asm", maxCycleCount: 12);
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("movc 0, 0, 0, 0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("movc r0, 0, 0"));
        }

        private static void CPUTestDatamovInstructions()
        {
            CPUTestDatamovInstructionMov32();
            CPUTestDatamovInstructionMovSignExt();
            CPUTestDatamovInstructionMovC();
        }
        #endregion

        #region CPUTestArith
        private static void CPUTestArithInstructionIAdd()
        {
            //just make sure iadd takes 3 operands
            var dummyOperand = OperandLiteral.u32(0x0);
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.iadd));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.iadd,
                                                                  dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.iadd,
                                                                  dummyOperand, dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.iadd,
                                                                  dummyOperand, dummyOperand,
                                                                  dummyOperand, dummyOperand));

            //can't iadd to a literal dest?
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.iadd,
                                                                  OperandLiteral.u32(18),
                                                                  OperandLiteral.u32(0),
                                                                  OperandLiteral.u32(4)));

            RunTestFile(@"ALU\iadd32.asm");

            RunTestFile(@"ALU\iadd_mixed_a.asm", maxCycleCount: 22);
            RunTestFile(@"ALU\iadd_mixed_b.asm", maxCycleCount: 10);

            RunTestFile(@"ALU\iadd32_flags_update.asm", maxCycleCount: 144);
            RunTestFile(@"ALU\iadd32_flags_dont_update.asm", maxCycleCount: 9);
            RunTestFile(@"ALU\iadd8_flags_update.asm", maxCycleCount: 144);
            RunTestFile(@"ALU\iadd_mixed_flags_update.asm", maxCycleCount: 38);
            RunTestFile(@"ALU\iadd_untyped_mixed_flags_update.asm", maxCycleCount: 18);

            //The above mixed tests didn't hit zero or V :)
            //It's hard to hit a V overflow if you're using small operands in big operations
            RunTestFile(@"ALU\iadd_flags_mixed_overflow.asm");
        }

        private static void CPUTestArithInstructionIAddC()
        {
            RunTestFile(@"ALU\iaddc.asm");
        }

        private static void CPUTestArithInstructionISub()
        {
            RunTestFile(@"ALU\isub8_flags.asm", maxCycleCount:144);
            RunTestFile(@"ALU\isub32_flags.asm", maxCycleCount: 144);
            RunTestFile(@"ALU\iadd_isub_equivilence.asm");
            //todo: do isub mixed
            //todo: isub with jumps?
            //todo: make cmp use isub?
        }

        private static void CPUTestArithInstructionISubC()
        {
            RunTestFile(@"ALU\isubc.asm");
        }

        private static void CPUTestArithInstructionUMax32()
        {
            //just make sure umax takes 3 operands
            var dummyOperand = OperandLiteral.u32(0x0);
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umax));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umax,
                                                                  dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umax,
                                                                  dummyOperand, dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umax,
                                                                  dummyOperand, dummyOperand,
                                                                  dummyOperand, dummyOperand));

            //umax -- can't have literal dest
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umax,
                                                                  OperandLiteral.u32(0),
                                                                  OperandLiteral.u32(0),
                                                                  OperandLiteral.u32(0)));
            RunTestFile(@"ALU\umax32.asm", maxCycleCount: 14);
        }

        private static void CPUTestArithInstructionUMin32()
        {
            //just make sure umin takes 3 operands
            var dummyOperand = OperandLiteral.u32(0x0);
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umin));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umin,
                                                                  dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umin,
                                                                  dummyOperand, dummyOperand));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umin,
                                                                  dummyOperand, dummyOperand,
                                                                  dummyOperand, dummyOperand));

            //umin -- can't have literal dest
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.umin,
                                                                  OperandLiteral.u32(0),
                                                                  OperandLiteral.u32(0),
                                                                  OperandLiteral.u32(0)));
            RunTestFile(@"ALU\umin32.asm", maxCycleCount: 14);
        }

        private static void CPUTestArithInstructions()
        {
            CPUTestArithInstructionIAdd();
            CPUTestArithInstructionIAddC();
            CPUTestArithInstructionISub();
            CPUTestArithInstructionISubC();
            CPUTestArithInstructionUMax32();
            CPUTestArithInstructionUMin32();
        }
        #endregion

        #region CPUTestLoadStore
        private static void CPUTestLoadStoreInstructionSt32()
        {
            //need explicit size to know how much to load.
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.st,
                                                                 OperandLiteral.u32(0x00000000),
                                                                 OperandLiteral.u32(0xCAFEBABE)));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.st,
                                                                 OperandLiteral.u32(0x00000000),
                                                                 OperandLiteral.u32(0xCAFEBABE),
                                                                 0));

            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.st,
                                                                  OperandLiteral.u32(0x00000000),
                                                                  OperandLiteral.u32(0xCAFEBABE),
                                                                  32, writesCPUFlags: true));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("st32! 0x0000, 0xCAFEBABE "));


            #region big-asm
            string asm = @"
                        
                    # r0 = 0x00000314             # 4
                    # r1 = 0x00001000             #
                    # r2 = 0x00002200             #
                    # r3 = 0x00003330             # 8
                    # r4 = 0x0000C0CC             #
                    #                             #
                    #               start         #   final
                    # MEM 0x0000 = 0xDEADBEEF     # 0xCAFEBABE
                    # MEM 0x0004 = 0xDEADBEEF     # 0x00000314
                    # MEM 0x0008 = 0xDEADBEEF     # 0x0000000F
                    # MEM 0x000C = 0xDEADBEEF     # 0xDEEDEED0
                    # MEM 0x0010 = 0000000000     # 0x00000314
                    # MEM 0xC0CC = 0x00000010     # 
                    #                             # 
                    # MEM 0x1000 = 0xDEEDEED0     # 0x0000777C
                    # MEM 0x2200 = 0xFFFFFFFF     # 0x00001000
                    # MEM 0x3330 = 0xDEADBEEF     # 0xA5A5A5A5
                    #                             # 
                    # MEM 0x0314 = 0xA5A5A5A5     # 0x44444444
                    # MEM 0x111C = 0x0000222C     # 
                    # MEM 0x222C = 0xFFFFFFFF     # 0x0000222C
                    # MEM 0x333C = 0x44444444     # 
                    # MEM 0x777C = 0xDEADBEEF     # 0x0000C0CC
                    #                             #
                    # MEM 0x4014 = 0x00000000     # 0xFeFeFeFe
                    # MEM 0x4018 = 0x00000000     # 0x04
                    # MEM 0x401C = 0x00000000     # 0xFFFFFFFF
                    # MEM 0x4020 = 0x00000000     # 0xDEEDEED0
                    # MEM 0x8100 = 0x00004014     #
                    # MEM 0x8104 = 0x00004018     #
                    # MEM 0x8108 = 0x0000401C     #
                    # MEM 0x810C = 0x00004020     #
                    #-----------------------------#------------------------------------------
                    st32 0x0000, 0xCAFEBABE       #01.  0x0000 = 0xCAFEBABE      //lit_addr, lit_val       
                    st32 0x0004, r0               #02.  0x0004 = 0x00000314      //lit_addr, reg_val       
                    st32 0x0008, @(0x111C)        #03.  0x0008 = 0x0000222C      //lit_addr, @(lit_val)    
                    st32 0x000C, @(r1)            #04.  0x000C = 0xDEEDEED0      //lit_addr, @(reg_val)    
                                                  #                                                                                
                    st32 r1, 0xFFFFFFFF           #05.  0x1000 = 0xFFFFFFFF      // reg_addr, lit_val      
                    st32 r2, r1                   #06.  0x2200 = 0x00001000      // reg_addr, reg_val      
                    st32 r1, @(0x333C)            #07.  0x1000 = 0x44444444      // reg_addr, @(lit_val)   
                    st32 r3, @(r0)                #08.  0x3330 = 0xA5A5A5A5      // reg_addr, @(reg_val)   
                    st32 r2, @(r2)                #09.  0x2200 = 0x00001000      // reg_addr, @(reg_val)   
                                                  #     (which it already was!)
                    mov r0, 0x04                  #10. 
                    mov r3, 0x08                  #11.    
                    st32 @(r2), 0x777C            #12.  0x1000 = 0x0000777C      // @(reg_addr), lit_val   
                    st32 @(r1), r4                #13.  0x777C = 0x0000C0CC      // @(reg_addr), reg_val
                    st32 @(r0), @(0x333C)         #14.  0x0314 = 0x44444444      // @(reg_addr), @(lit_val)
                    st32 @(r4), @(r0)             #15.  0x0010 = 0x00000314      // @(reg_addr), @(reg_val)
                    st32 @(r3), @(r3)             #16.  0x222C = 0x0000222C      // @(reg_addr), @(reg_val)
                                                  #     (which it already was!)
                                                  #   
                    st32 @(0x8100), 0xFeFeFeFe    #17.  0x4014 = 0xFeFeFeFe      // @(lit_addr), lit_val   
                    st32 @(0x8104), r0            #18.  0x4018 = 0x04            // @(lit_addr), reg_val
                    st32 @(0x8108), @(0x0010)     #19.  0x401C = 0x00000314      // @(lit_addr), @(lit_val)
                    st32 @(0x810C), @(r4)         #20.  0x4020 = 0x00000010      // @(lit_addr), @(reg_val)
                                                  #   
                    st32 0xFFFF0008, 0xF          #21.  0x0008 = 0x0000000F     
                        
            ";
            #endregion

            CPUState state32 = new CPUState(0, 32, PFCParser.ParseAssembly(asm));
            var changes = new List<CPURegMemStateChange>
            {
                #region initial-state
                CPURegMemStateChange.RegChange(0, 0x00000314),
                CPURegMemStateChange.RegChange(1, 0x00001000),
                CPURegMemStateChange.RegChange(2, 0x00002200),
                CPURegMemStateChange.RegChange(3, 0x00003330),
                CPURegMemStateChange.RegChange(4, 0x0000C0CC),

                CPURegMemStateChange.MemChange32(0x0000, 0xDEADBEEF),
                CPURegMemStateChange.MemChange32(0x0004, 0xDEADBEEF),
                CPURegMemStateChange.MemChange32(0x0008, 0xDEADBEEF),
                CPURegMemStateChange.MemChange32(0x000C, 0xDEADBEEF),
                CPURegMemStateChange.MemChange32(0x0010, 0000000000),
                CPURegMemStateChange.MemChange32(0xC0CC, 0x00000010),

                CPURegMemStateChange.MemChange32(0x1000, 0xDEEDEED0),
                CPURegMemStateChange.MemChange32(0x2200, 0xFFFFFFFF),
                CPURegMemStateChange.MemChange32(0x3330, 0xDEADBEEF),

                CPURegMemStateChange.MemChange32(0x0314, 0xA5A5A5A5),
                CPURegMemStateChange.MemChange32(0x111C, 0x0000222C),
                CPURegMemStateChange.MemChange32(0x222C, 0xFFFFFFFF),
                CPURegMemStateChange.MemChange32(0x333C, 0x44444444),
                CPURegMemStateChange.MemChange32(0x777C, 0xDEADBEEF),

                CPURegMemStateChange.MemChange32(0x4014, 0x00000000),
                CPURegMemStateChange.MemChange32(0x4018, 0x00000000),
                CPURegMemStateChange.MemChange32(0x401C, 0x00000000),
                CPURegMemStateChange.MemChange32(0x4020, 0x00000000),
                CPURegMemStateChange.MemChange32(0x8100, 0x00004014),
                CPURegMemStateChange.MemChange32(0x8104, 0x00004018),
                CPURegMemStateChange.MemChange32(0x8108, 0x0000401C),
                CPURegMemStateChange.MemChange32(0x810C, 0x00004020),
                #endregion
            };
            state32.TESTONLY_ApplyStateChanges(changes);

            var cpu32 = new CPU_VM(0, 32, state32);

            Action<ushort, uint, uint> memCheck = (addr, numBytes, val) =>
            {
                var memval = state32.ReadAlignedMemory(addr, numBytes);
                Debug.Assert(memval == val);
            };

            #region initial-state-check
            Debug.Assert(cpu32.ROState.RegisterBank[0] == 0x00000314);
            Debug.Assert(cpu32.ROState.RegisterBank[1] == 0x00001000);
            Debug.Assert(cpu32.ROState.RegisterBank[2] == 0x00002200);
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00003330);
            Debug.Assert(cpu32.ROState.RegisterBank[4] == 0x0000C0CC);
            memCheck(0x0000, 4, 0xDEADBEEF);
            memCheck(0x0004, 4, 0xDEADBEEF);
            memCheck(0x0008, 4, 0xDEADBEEF);
            memCheck(0x000C, 4, 0xDEADBEEF);
            memCheck(0x0010, 4, 0000000000);
            memCheck(0xC0CC, 4, 0x00000010);
            memCheck(0x1000, 4, 0xDEEDEED0);
            memCheck(0x2200, 4, 0xFFFFFFFF);
            memCheck(0x3330, 4, 0xDEADBEEF);
            memCheck(0x0314, 4, 0xA5A5A5A5);
            memCheck(0x111C, 4, 0x0000222C);
            memCheck(0x222C, 4, 0xFFFFFFFF);
            memCheck(0x333C, 4, 0x44444444);
            memCheck(0x777C, 4, 0xDEADBEEF);
            memCheck(0x4014, 4, 0x00000000);
            memCheck(0x4018, 4, 0x00000000);
            memCheck(0x401C, 4, 0x00000000);
            memCheck(0x4020, 4, 0x00000000);
            memCheck(0x8100, 4, 0x00004014);
            memCheck(0x8104, 4, 0x00004018);
            memCheck(0x8108, 4, 0x0000401C);
            memCheck(0x810C, 4, 0x00004020);
            #endregion

            // 01. st32 0x0000, 0xCAFEBABE   
            // 02. st32 0x0004, r0           
            // 03. st32 0x0008, @(0x111C)    
            // 04. st32 0x000C, @(r1)        
            cpu32.Clock();
            memCheck(0x0000, 4, 0xCAFEBABE);
            cpu32.Clock();
            memCheck(0x0004, 4, 0x00000314);
            cpu32.Clock();
            memCheck(0x0008, 4, 0x0000222C);
            cpu32.Clock();
            memCheck(0x000C, 4, 0xDEEDEED0);

            // 05. st32 r1, 0xFFFFFFFF       
            // 06. st32 r2, r1               
            // 07. st32 r1, @(0x333C)        
            // 08. st32 r3, @(r0)            
            // 09. st32 r2, @(r2)            
            cpu32.Clock();
            memCheck(0x1000, 4, 0xFFFFFFFF);
            cpu32.Clock();
            memCheck(0x2200, 4, 0x00001000);
            cpu32.Clock();
            memCheck(0x1000, 4, 0x44444444);
            cpu32.Clock();
            memCheck(0x3330, 4, 0xA5A5A5A5);
            cpu32.Clock();
            memCheck(0x2200, 4, 0x00001000);

            // 10. mov r0, 0x04 
            // 11. mov r3, 0x08              
            // 12. st32 @(r2), 0x777C        
            // 13. st32 @(r1), r4            
            // 14. st32 @(r0), @(0x333C)     
            // 15. st32 @(r4), @(r0)         
            // 16. st32 @(r3), @(r3)         
            cpu32.Clock();
            cpu32.Clock();
            cpu32.Clock();
            memCheck(0x1000, 4, 0x0000777C);
            cpu32.Clock();
            memCheck(0x777C, 4, 0x0000C0CC);
            cpu32.Clock();
            memCheck(0x0314, 4, 0x44444444);
            cpu32.Clock();
            memCheck(0x0010, 4, 0x00000314);
            cpu32.Clock();
            memCheck(0x222C, 4, 0x0000222C);

            // 17. st32 @(0x8100), 0xFeFeFeFe
            // 18. st32 @(0x8104), r0        
            // 19. st32 @(0x8108), @(0x0010) 
            // 20. st32 @(0x810C), @(r4)     
            cpu32.Clock();
            memCheck(0x4014, 4, 0xFeFeFeFe);
            cpu32.Clock();
            memCheck(0x4018, 4, 0x00000004);
            cpu32.Clock();
            memCheck(0x401C, 4, 0x00000314);
            cpu32.Clock();
            memCheck(0x4020, 4, 0x00000010);

            // 21. st32 0xFFFF0008, 0xF
            cpu32.Clock();
            memCheck(0x0008, 4, 0x0000000F);

            #region end-state-check
            Debug.Assert(cpu32.ROState.RegisterBank[0] == 0x00000004);
            Debug.Assert(cpu32.ROState.RegisterBank[1] == 0x00001000);
            Debug.Assert(cpu32.ROState.RegisterBank[2] == 0x00002200);
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000008);
            Debug.Assert(cpu32.ROState.RegisterBank[4] == 0x0000C0CC);
            memCheck(0x0000, 4, 0xCAFEBABE);
            memCheck(0x0004, 4, 0x00000314);
            memCheck(0x0008, 4, 0x0000000F);
            memCheck(0x000C, 4, 0xDEEDEED0);
            memCheck(0x0010, 4, 0x00000314);
            memCheck(0xC0CC, 4, 0x00000010);
            memCheck(0x1000, 4, 0x0000777C);
            memCheck(0x2200, 4, 0x00001000);
            memCheck(0x3330, 4, 0xA5A5A5A5);
            memCheck(0x0314, 4, 0x44444444);
            memCheck(0x111C, 4, 0x0000222C);
            memCheck(0x222C, 4, 0x0000222C);
            memCheck(0x333C, 4, 0x44444444);
            memCheck(0x777C, 4, 0x0000C0CC);
            memCheck(0x4014, 4, 0xFeFeFeFe);
            memCheck(0x4018, 4, 0x00000004);
            memCheck(0x401C, 4, 0x00000314);
            memCheck(0x4020, 4, 0x00000010);
            memCheck(0x8100, 4, 0x00004014);
            memCheck(0x8104, 4, 0x00004018);
            memCheck(0x8108, 4, 0x0000401C);
            memCheck(0x810C, 4, 0x00004020);
            #endregion

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu32.Clock());

        }

        private static void CPUTestLoadStoreInstructionStMixed()
        {
            #region giant-asm-string
            string asm = @"
                         #--addrs--
                         #r0 = 0x1080
                         #r1 = 0x1081
                         #r2 = 0x1082
                         #r3 = 0x1083
                         #
                         #--vals--
                         #r4 = 0xFEDC5678
                         #r5 = 0x87654321
                         #
                         #
                         #on a 32bit cpu
                         #-----------------------------|-----------------------------+
                         #st memaddr, value            |
                         #-----------------------------+-----------------------------+
                            st8    r0,       r4       #|01.    MEM 0x0080:0 = 0x78
                            st16   r0,       r4       #|02.    MEM 0x1080:1 = 0x5678
                           #st24   r0,       r4       #|03.    MEM 0x1080:2 = 0xDC5678
                            nop
                            st32   r0,       r4       #|04.    MEM 0x1080:3 = 0xFEDC5678
                                                      #|
                            nop                       #|05.
                            nop                       #|06.
                            nop                       #|07.
                         #-----------------------------+-----------------------------+
                            st32   u32{r0},  u32{r4}  #|08.    MEM 0x1080:3 = 0xFEDC5678
                                                      #|
                            st32   u32{r0},  u16{r4}  #|09.    MEM 0x1080:3 = 0x00005678
                            st32   u16{r0},  u32{r4}  #|10.    MEM 0x1080:3 = 0xFEDC5678
                            st32   u16{r0},  u16{r4}  #|11.    MEM 0x1080:3 = 0x00005678
                                                      #|
                            st32   u32{r0},  u8{r4}   #|12.    MEM 0x1080:3 = 0x00000078
                            st32   u8{r0},   u32{r4}  #|13.    MEM 0x0080:3 = 0xFEDC5678
                            st32   u8{r0},   u8{r4}   #|14.    MEM 0x0080:3 = 0x00000078
                                                      #|
                            st32   u8{r0},   u16{r4}  #|15.    MEM 0x0080:3 = 0x00005678
                            st32   u16{r0},  u8{r4}   #|16.    MEM 0x1080:3 = 0x00000078
                         #-----------------------------+-----------------------------+
                                                      #|
                            st8    u8{r1},   u8{r4}   #|17.    MEM 0x0081:0 = 0x78
                                                      #|
                            st8    u8{r1},   u16{r4}  #|18.    MEM 0x0081:0 = 0x78
                            st8    u16{r1},  u8{r4}   #|19.    MEM 0x1081:0 = 0x78
                            st8    u16{r1},  u16{r4}  #|20.    MEM 0x1081:0 = 0x78
                                                      #|
                            st8    u8{r1},   u32{r4}  #|21.    MEM 0x0081:0 = 0x78
                            st8    u32{r1},  u8{r4}   #|22.    MEM 0x1081:0 = 0x78
                            st8    u32{r1},  u32{r4}  #|23.    MEM 0x1081:0 = 0x78
                                                      #|
                            st8    u32{r1},  u16{r4}  #|24.    MEM 0x1081:0 = 0x78
                            st8    u16{r1},  u32{r4}  #|25.    MEM 0x1081:0 = 0x78
                                                      #|
                         #-----------------------------+-----------------------------+
                                                      #|
                            st16   u16{r2},  u16{r4}  #|26.    MEM 0x1082:1 = 0x5678
                                                      #|
                            st16   u16{r2},  u8{r4}   #|27.    MEM 0x1082:1 = 0x0078
                            st16   u8{r2},   u16{r4}  #|28.    MEM 0x0082:1 = 0x5678
                            st16   u8{r2},   u8{r4}   #|29.    MEM 0x0082:1 = 0x0078
                                                      #|
                            st16   u16{r2},  u32{r4}  #|30.    MEM 0x1082:1 = 0x5678
                            st16   u32{r2},  u16{r4}  #|31.    MEM 0x1082:1 = 0x5678
                            st16   u32{r2},  u32{r4}  #|32.    MEM 0x1082:1 = 0x5678
                                                      #|
                            st16   u32{r2},  u8{r4}   #|33.    MEM 0x1082:1 = 0x0078
                            st16   u8{r2},   u32{r4}  #|34.    MEM 0x0082:1 = 0x5678
                                                      #|
                         #-------------- undefs--------+-----------------------------+
                            st32   r0,       r4       #|35.    MEM 0x1080:3 = 0xFEDC5678
                            st32   r0,       u16{r4}  #|36.    MEM 0x1080:3 = 0x00005678
                            st32   u16{r0},  r4       #|37.    MEM 0x1080:3 = 0xFEDC5678
                            st32   r0,       u8{r4}   #|38.    MEM 0x1080:3 = 0x00000078
                            st32   u8{r0},   r4       #|39.    MEM 0x0080:3 = 0xFEDC5678
                                                      #|
                            st8    r0,       r4       #|40.    MEM 0x0080:0 = 0x78
                            st8    r0,       u16{r4}  #|41.    MEM 0x0080:0 = 0x78
                            st8    u16{r0},  r4       #|42.    MEM 0x1080:0 = 0x78
                            st8    r0,       u32{r4}  #|43.    MEM 0x0080:0 = 0x78
                            st8    u32{r0},  r4       #|44.    MEM 0x1080:0 = 0x78
                                                      #|
                            st8    u16{r0},  r4       #|45.    MEM 0x1080:3 = 0x00000078
                            st16   u16{r2},  r4       #|46.   +MEM 0x1080:3 = 0x56780078
                            st8    u16{r1},  r5       #|47.   +MEM 0x1080:3 = 0x56782178
            ";
            #endregion

            CPUState state32 = new CPUState(0, 32, PFCParser.ParseAssembly(asm));
            #region initial-state-plan
            /*
             *  r0 = 0x1080
             *  r1 = 0x1081
             *  r2 = 0x1082
             *  r3 = 0x1083
             *  
             *  --vals--
             *  r4 = 0xFEDC5678
             *  r5 = 0x87654321
             */
            #endregion
            var changes = new List<CPURegMemStateChange>
            {
                #region initial-state
                CPURegMemStateChange.RegChange(0, 0x1080),
                CPURegMemStateChange.RegChange(1, 0x1081),
                CPURegMemStateChange.RegChange(2, 0x1082),
                CPURegMemStateChange.RegChange(3, 0x1083),
                CPURegMemStateChange.RegChange(4, 0xFEDC5678),
                CPURegMemStateChange.RegChange(5, 0x87654321),
                CPURegMemStateChange.MemChange32(0x1080, 0x00000000),
                #endregion
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            var cpu32 = new CPU_VM(0, 32, state32);

            var resetData = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange32(0x1080, 0x00000000),
                CPURegMemStateChange.MemChange32(0x0080, 0x00000000),
            };
            Action resetMem = () => state32.TESTONLY_ApplyStateChanges(resetData);

            Action<ushort, uint, uint> memCheck = (addr, to, val) =>
            {
                uint numBytes = (to + 1);
                var memval = state32.ReadAlignedMemory(addr, numBytes);
                Debug.Assert(memval == val);
            };
            


            //  01. st8    r0,       r4          |   MEM 0x0080:0 = 0x78
            //  02. st16   r0,       r4          |   MEM 0x1080:1 = 0x5678
            //  03. nop
            //  04. st32   r0,       r4          |   MEM 0x1080:3 = 0xFEDC5678
            resetMem(); cpu32.Clock(); memCheck(0x0080, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 1, 0x5678);
            cpu32.Clock();
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0xFEDC5678);

            //  05. nop
            //  06. nop
            //  07. nop
            cpu32.Clock();
            cpu32.Clock();
            cpu32.Clock();
            
            //  08. st32   u32{r0},  u32{r4}     |   MEM 0x1080:3 = 0xFEDC5678
            //  09. st32   u32{r0},  u16{r4}     |   MEM 0x1080:3 = 0x00005678
            //  10. st32   u16{r0},  u32{r4}     |   MEM 0x1080:3 = 0xFEDC5678
            //  11. st32   u16{r0},  u16{r4}     |   MEM 0x1080:3 = 0x00005678
            //  12. st32   u32{r0},  u8{r4}      |   MEM 0x1080:3 = 0x00000078
            //  13. st32   u8{r0},   u32{r4}     |   MEM 0x0080:3 = 0xFEDC5678
            //  14. st32   u8{r0},   u8{r4}      |   MEM 0x0080:3 = 0x00000078
            //  15. st32   u8{r0},   u16{r4}     |   MEM 0x0080:3 = 0x00005678
            //  16. st32   u16{r0},  u8{r4}      |   MEM 0x1080:3 = 0x00000078
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0xFEDC5678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0x00005678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0xFEDC5678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0x00005678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0x00000078);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 3, 0xFEDC5678);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 3, 0x00000078);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 3, 0x00005678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0x00000078);

            //  17. st8    u8{r1},   u8{r4}      |   MEM 0x0081:0 = 0x78
            //  18. st8    u8{r1},   u16{r4}     |   MEM 0x0081:0 = 0x78
            //  19. st8    u16{r1},  u8{r4}      |   MEM 0x1081:0 = 0x78
            //  20. st8    u16{r1},  u16{r4}     |   MEM 0x1081:0 = 0x78
            //  21. st8    u8{r1},   u32{r4}     |   MEM 0x0081:0 = 0x78
            //  22. st8    u32{r1},  u8{r4}      |   MEM 0x1081:0 = 0x78
            //  23. st8    u32{r1},  u32{r4}     |   MEM 0x1081:0 = 0x78
            //  24. st8    u32{r1},  u16{r4}     |   MEM 0x1081:0 = 0x78
            //  25. st8    u16{r1},  u32{r4}     |   MEM 0x1081:0 = 0x78
            resetMem(); cpu32.Clock(); memCheck(0x0081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x0081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x0081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1081, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1081, 0, 0x78);

            //  26. st16   u16{r2},  u16{r4}     |   MEM 0x1082:1 = 0x5678
            //  27. st16   u16{r2},  u8{r4}      |   MEM 0x1082:1 = 0x0078
            //  28. st16   u8{r2},   u16{r4}     |   MEM 0x0082:1 = 0x5678
            //  29. st16   u8{r2},   u8{r4}      |   MEM 0x0082:1 = 0x0078
            //  30. st16   u16{r2},  u32{r4}     |   MEM 0x1082:1 = 0x5678
            //  31. st16   u32{r2},  u16{r4}     |   MEM 0x1082:1 = 0x5678
            //  32. st16   u32{r2},  u32{r4}     |   MEM 0x1082:1 = 0x5678
            //  33. st16   u32{r2},  u8{r4}      |   MEM 0x1082:1 = 0x0078
            //  34. st16   u8{r2},   u32{r4}     |   MEM 0x0082:1 = 0x5678
            resetMem(); cpu32.Clock(); memCheck(0x1082, 1, 0x5678);
            resetMem(); cpu32.Clock(); memCheck(0x1082, 1, 0x0078);
            resetMem(); cpu32.Clock(); memCheck(0x0082, 1, 0x5678);
            resetMem(); cpu32.Clock(); memCheck(0x0082, 1, 0x0078);
            resetMem(); cpu32.Clock(); memCheck(0x1082, 1, 0x5678);
            resetMem(); cpu32.Clock(); memCheck(0x1082, 1, 0x5678);
            resetMem(); cpu32.Clock(); memCheck(0x1082, 1, 0x5678);
            resetMem(); cpu32.Clock(); memCheck(0x1082, 1, 0x0078);
            resetMem(); cpu32.Clock(); memCheck(0x0082, 1, 0x5678);

            //  35. st32   r0,       r4          |   MEM 0x1080:3 = 0xFEDC5678
            //  36. st32   r0,       u16{r4}     |   MEM 0x1080:3 = 0x00005678
            //  37. st32   u16{r0},  r4          |   MEM 0x1080:3 = 0xFEDC5678
            //  38. st32   r0,       u8{r4}      |   MEM 0x1080:3 = 0x00000078
            //  39. st32   u8{r0},   r4          |   MEM 0x0080:3 = 0xFEDC5678
            //  40. st8    r0,       r4          |   MEM 0x0080:0 = 0x78
            //  41. st8    r0,       u16{r4}     |   MEM 0x0080:0 = 0x78
            //  42. st8    u16{r0},  r4          |   MEM 0x1080:0 = 0x78
            //  43. st8    r0,       u32{r4}     |   MEM 0x0080:0 = 0x78
            //  44. st8    u32{r0},  r4          |   MEM 0x1080:0 = 0x78
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0xFEDC5678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0x00005678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0xFEDC5678);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 3, 0x00000078);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 3, 0xFEDC5678);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x0080, 0, 0x78);
            resetMem(); cpu32.Clock(); memCheck(0x1080, 0, 0x78);


            //  45. st8    u16{r0},  r4          |   MEM 0x1080:3 = 0x00000078
            //  46. st16   u16{r2},  r4          |   MEM 0x1080:3 = 0x56780078
            //  47. st8    u16{r1},  r5          |   MEM 0x1080:3 = 0x56782178
            resetMem();
            cpu32.Clock(); memCheck(0x1080, 3, 0x00000078);
            cpu32.Clock(); memCheck(0x1080, 3, 0x56780078);
            cpu32.Clock(); memCheck(0x1080, 3, 0x56782178);


            //todo st24

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu32.Clock());


        }

        private static void CPUTestLoadStoreInstructionLd32()
        {
            //need explicit size to know how much to load.
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.ld,
                                                                 OperandReg.Register(3),
                                                                 OperandLiteral.u32(0x0000)));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.ld,
                                                                 OperandReg.Register(3),
                                                                 OperandLiteral.u32(0x0000),
                                                                 0));

            string asm = @"
	                        # r0 = 0x0000
	                        # r1 = 0x0040
	                        # r2 = 0x0500
	                        # 
	                        # MEM 0x0000  = 0x01020304
	                        # MEM 0x0004  = 0xa5a5a5a5
	                        # MEM 0x0040  = 0xA1B2C3D4
	                        # 
	                        # MEM 0x0080  = 0x00000004
	                        # MEM 0x0304  = 0xFEFEFEFE
	                        # MEM 0x0500  = 0x00000080
	
	                        ld32 r3, 0x0000       #01. = 0x01020304
	                        ld32 r3, 0x0004       #02. = 0xa5a5a5a5
	                        ld32 r3, 0x0040       #03. = 0xA1B2C3D4
	                        ld32 r3, 0x0080       #04. = 0x00000004
	                        ld32 r3, 0x0500       #05. = 0x00000080
						                          #
	                        ld32 r3, r0           #06. = 0x01020304
	                        ld32 r3, r1           #07. = 0xA1B2C3D4
	                        ld32 r3, r2           #08. = 0x00000080
						                          #
	                        ld32 r3, @(r2)        #09. = 0x00000004
	                        ld32 r4, r3           #10. = 0xa5a5a5a5
						                          #
	                        #should read mem 0304? unsure. depends on address bus size
	                        ld32 r4, @(r0)        #11. = 0xFEFEFEFE
                        ";

            CPUState state32 = new CPUState(0, 32, PFCParser.ParseAssembly(asm));
            var changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.RegChange(0, 0x0000),
                CPURegMemStateChange.RegChange(1, 0x0040),
                CPURegMemStateChange.RegChange(2, 0x0500),
                CPURegMemStateChange.MemChange32(0x0000, 0x01020304),
                CPURegMemStateChange.MemChange32(0x0004, 0xa5a5a5a5),
                CPURegMemStateChange.MemChange32(0x0040, 0xA1B2C3D4),
                CPURegMemStateChange.MemChange32(0x0080, 0x00000004),
                CPURegMemStateChange.MemChange32(0x0304, 0xFEFEFEFE),
                CPURegMemStateChange.MemChange32(0x0500, 0x00000080),
            };
            state32.TESTONLY_ApplyStateChanges(changes);

            var cpu32 = new CPU_VM(0, 32, state32);

            Action<ushort, uint, uint> memCheck = (addr, numBytes, val) =>
            {
                var memval = state32.ReadAlignedMemory(addr, numBytes);
                Debug.Assert(memval == val);
            };

            #region initial-state-check
            Debug.Assert(cpu32.ROState.cycleCounter == 0);
            Debug.Assert(cpu32.ROState.RegisterBank[0] == 0x0000);
            Debug.Assert(cpu32.ROState.RegisterBank[1] == 0x0040);
            Debug.Assert(cpu32.ROState.RegisterBank[2] == 0x0500);
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);
            Debug.Assert(cpu32.ROState.RegisterBank[4] == 0x00000000);
            Debug.Assert(cpu32.ROState.RAM[0x0000] == 0x04);
            Debug.Assert(cpu32.ROState.RAM[0x0001] == 0x03);
            Debug.Assert(cpu32.ROState.RAM[0x0002] == 0x02);
            Debug.Assert(cpu32.ROState.RAM[0x0003] == 0x01);
            Debug.Assert(cpu32.ROState.RAM[0x0004] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0005] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0006] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0007] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0040] == 0xD4);
            Debug.Assert(cpu32.ROState.RAM[0x0041] == 0xC3);
            Debug.Assert(cpu32.ROState.RAM[0x0042] == 0xB2);
            Debug.Assert(cpu32.ROState.RAM[0x0043] == 0xA1);
            Debug.Assert(cpu32.ROState.RAM[0x0080] == 0x04);
            Debug.Assert(cpu32.ROState.RAM[0x0081] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0082] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0083] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0304] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0305] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0306] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0307] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0500] == 0x80);
            Debug.Assert(cpu32.ROState.RAM[0x0501] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0502] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0503] == 0x00);
            memCheck(0x0000, 4, 0x01020304);
            memCheck(0x0004, 4, 0xa5a5a5a5);
            memCheck(0x0040, 4, 0xA1B2C3D4);
            memCheck(0x0080, 4, 0x00000004);
            memCheck(0x0304, 4, 0xFEFEFEFE);
            memCheck(0x0500, 4, 0x00000080);
            #endregion


            // 01. ld32 r3, 0x0000       = 0x01020304
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x01020304);
            // 02. ld32 r3, 0x0004       = 0xa5a5a5a5
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xa5a5a5a5);
            // 03. ld32 r3, 0x0040       = 0xA1B2C3D4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xA1B2C3D4);
            // 04. ld32 r3, 0x0080       = 0x00000004
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000004);
            // 05. ld32 r3, 0x0500       = 0x00000080
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000080);

            // 06. ld32 r3, r0           = 0x01020304
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x01020304);
            // 07. ld32 r3, r1           = 0xA1B2C3D4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xA1B2C3D4);
            // 08. ld32 r3, r2           = 0x00000080
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000080);


            // 09. ld32 r3, @(r2)        = 0x00000004
            // 10. ld32 r4, r3           = 0xa5a5a5a5
            cpu32.Clock();
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000004);
            Debug.Assert(cpu32.ROState.RegisterBank[4] == 0xa5a5a5a5);

            // //should read mem 0304? unsure. depends on address bus size
            // 11. ld32 r4, @(r0)        = 0xFEFEFEFE
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[4] == 0xFEFEFEFE);

            #region end-state-check
            Debug.Assert(cpu32.ROState.cycleCounter == 11);
            Debug.Assert(cpu32.ROState.RegisterBank[0] == 0x0000);
            Debug.Assert(cpu32.ROState.RegisterBank[1] == 0x0040);
            Debug.Assert(cpu32.ROState.RegisterBank[2] == 0x0500);
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000004);
            Debug.Assert(cpu32.ROState.RegisterBank[4] == 0xFEFEFEFE);
            Debug.Assert(cpu32.ROState.RAM[0x0000] == 0x04);
            Debug.Assert(cpu32.ROState.RAM[0x0001] == 0x03);
            Debug.Assert(cpu32.ROState.RAM[0x0002] == 0x02);
            Debug.Assert(cpu32.ROState.RAM[0x0003] == 0x01);
            Debug.Assert(cpu32.ROState.RAM[0x0004] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0005] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0006] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0007] == 0xa5);
            Debug.Assert(cpu32.ROState.RAM[0x0040] == 0xD4);
            Debug.Assert(cpu32.ROState.RAM[0x0041] == 0xC3);
            Debug.Assert(cpu32.ROState.RAM[0x0042] == 0xB2);
            Debug.Assert(cpu32.ROState.RAM[0x0043] == 0xA1);
            Debug.Assert(cpu32.ROState.RAM[0x0080] == 0x04);
            Debug.Assert(cpu32.ROState.RAM[0x0081] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0082] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0083] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0304] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0305] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0306] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0307] == 0xFE);
            Debug.Assert(cpu32.ROState.RAM[0x0500] == 0x80);
            Debug.Assert(cpu32.ROState.RAM[0x0501] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0502] == 0x00);
            Debug.Assert(cpu32.ROState.RAM[0x0503] == 0x00);
            memCheck(0x0000, 4, 0x01020304);
            memCheck(0x0004, 4, 0xa5a5a5a5);
            memCheck(0x0040, 4, 0xA1B2C3D4);
            memCheck(0x0080, 4, 0x00000004);
            memCheck(0x0304, 4, 0xFEFEFEFE);
            memCheck(0x0500, 4, 0x00000080);
            #endregion

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu32.Clock());


        }

        private static void CPUTestLoadStoreInstructionLdMixed()
        {
            string asm = @"
                  # r0 = 0x12345678
                  # r1 = 0x20FEDCBC
                  # r2 = 0xFFFF0002
                  # MEM 0x00BC  = 0xA1B2C3D4
                  # MEM 0xB8C8  = 0x88800880
                  # MEM 0xBEEC  = 0x70707070
                  # MEM 0xDCBC  = 0xDEADBEEC
                  # MEM 0xDCBE  = 0xF6A7B8C8
                  ld32   r3,       r1               #  1.   = 0xDEADBEEC
                  ld32   r3,       u16{r1}          #  2.   = 0xDEADBEEC
                  ld32   r3,       @(r1)            #  3.   = 0x70707070
                  ld32   r3,       u16{@(r1)}       #  4.   = 0x70707070
                  ld8    r3,       r1               #  5.   = 0xD4
                  ld8    r3,       u16{r1}          #  6.   = 0xEC
                  ld8    i32{r3},  r1               #  7.   = 0xFFFFFFD4
                  ld32   r3,       u8{r1}           #  8.   = 0xA1B2C3D4
                  ld32   i16{r3},  r1               #  9.   = 0xBEEC
                  ld16   i32{r3},  r1               # 10.   = 0xFFFFBEEC
                  ld32   i8{r3},   u8{r1}           # 11.   = 0xD4
                 #ld32   r3,       @(r1 + r2)       # 12.   = 0x88800880
                 #ld32   r3,       i16{@(r1 + r2)}  # 13.   = 0x88800880
                  ld8    r3,       r1               # 14:   = 0xD4
                  ld8    r3,       u16{r1}          # 15:   = 0xEC
                  ld8    u32{r3},  r1               # 16:   = 0x000000D4
                  ld8    i32{r3},  r1               # 17:   = 0xFFFFFFD4
            ";


            //todo 12, 13


            CPUState state32 = new CPUState(0, 32, PFCParser.ParseAssembly(asm));
            #region initial-state-plan
            /*
	         *       r0 = 0x12345678
	         *       r1 = 0x20FEDCBC
	         *       r2 = 0xFFFF0002
	         *       MEM 0x00BC  = 0xA1B2C3D4
	         *       MEM 0xB8C8  = 0x88800880
	         *       MEM 0xBEEC  = 0x70707070
	         *       MEM 0xDCBC  = 0xDEADBEEC
	         *       MEM 0xDCC0  = 0xF6A7B8C8
             */
            #endregion
            var changes = new List<CPURegMemStateChange>
            {
                #region initial-state
                CPURegMemStateChange.RegChange(0, 0x12345678),
                CPURegMemStateChange.RegChange(1, 0x20FEDCBC),
                CPURegMemStateChange.RegChange(2, 0xFFFF0002),
                CPURegMemStateChange.RegChange(3, 0x00000000),
                CPURegMemStateChange.MemChange32(0x00BC, 0xA1B2C3D4),
                CPURegMemStateChange.MemChange32(0xB8C8, 0x88800880),
                CPURegMemStateChange.MemChange32(0xBEEC, 0x70707070),
                CPURegMemStateChange.MemChange32(0xDCBC, 0xDEADBEEC),
                CPURegMemStateChange.MemChange32(0xDCC0, 0xF6A7B8C8),
                #endregion
            };
            state32.TESTONLY_ApplyStateChanges(changes);



            var cpu32 = new CPU_VM(0, 32, state32);
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  1. ld32 rN, r1               = 0xDEADBEEC
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xDEADBEEC);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  2. ld32 rN, u16{r1}          = 0xDEADBEEC
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xDEADBEEC);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  3. ld32 rN, @(r1)            = 0x70707070
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x70707070);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  4. ld32 rN, u16{@(r1)}       = 0x70707070
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x70707070);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  5. ld8  rN, r1               = 0xD4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x000000D4);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  6. ld8  rN, u16{r1}          = 0xEC
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x000000EC);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  7. ld8  i32{rN}, r1          = 0xFFFFFFD4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xFFFFFFD4);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  8. ld32 rN, u8{r1}           = 0xA1B2C3D4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xA1B2C3D4);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            //  9. ld32 i16{rN}, r1          = 0xBEEC
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x0000BEEC);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            // 10. ld16 i32{rN}, r1          = 0xFFFFBEEC
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xFFFFBEEC);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            // 11. ld32 i8{rN}, r8{r1}       = 0xD4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x000000D4);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            // 12. ld32 rN, @(r1 + r2)       = 0x88800880
            //cpu32.Clock();
            //Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x88800880);
            //state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            //Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);

            // 13. ld32 rN, u16{@(r1 + r2)}  = 0x88800880
            //cpu32.Clock();
            //Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x88800880);
            //state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0x00000000));
            //Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x00000000);


            //repeat 5, 6, 7 to show that they affect only the correct bits
            //Fill with rubbish that shouldn't change
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, 0xAAAAAAAA));
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xAAAAAAAA);

            // 14:5. ld8  rN, r1               = 0xD4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xAAAAAAD4);
            // 15:6. ld8  rN, u16{r1}          = 0xEC
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xAAAAAAEC);
            // 16:7. ld8  i32{rN}, r1          = 0xFFFFFFD4
            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0x000000D4);

            cpu32.Clock();
            Debug.Assert(cpu32.ROState.RegisterBank[3] == 0xFFFFFFD4);



            /*
             * todo
             *  ld r0, 01234
             *  ld r0, type{01234}
             *  ld r0, @(01234)
             *  ld r0, type{@(01234)}
             *  
             * base + index + scale
             * 
             */

            AssertThrows<PFC_CPUNoInstructionException>(() => cpu32.Clock());

        }

        private static void CPUTestLoadStoreInstructions()
        {
            CPUTestLoadStoreInstructionLd32();
            CPUTestLoadStoreInstructionLdMixed();
            CPUTestLoadStoreInstructionSt32();
            CPUTestLoadStoreInstructionStMixed();
        }
        #endregion

        #region CPUTestJump

        private static void CPUTestJumpInstructions()
        {
            CPUTestJumpInstructionBasic();
            CPUTestJumpInstructionInfinite();
            CPUTestJumpInstructionInfiniteCounter();
            CPUTestJumpInstructionBasicbackwards();
            CPUTestJumpInstructionBasicbackwardsInfinite();
            CPUTestJumpInstructionNolabelLiteral();
            CPUTestJumpInstructionInitialSpaghetti();
            CPUTestJumpInstructionIndirectAndComputed();
            //todo CPUTestJumpInstructionComputedLiteralLabelOffset();
            CPUTestJumpInstructionJumptableBasic();
            CPUTestJumpInstructionJumptableDoublejump();
            CPUTestJumpInstructionJumptableCounter();
            //jump8? jump16? jump32?!
        }

        private static void CPUTestJumpInstructionJumptableCounter()
        {
            string asm = @" #do a terrible counter without a icmp instruction
                    mov r0, 0          #0
            :jump-table:
                    jump :loop-start:  #1
                    jump :loop-start:  #2
                    jump :loop-start:  #3
                    jump :loop-start:  #4
                    jump :loop-start:  #5
                    jump :loop-start:  #6
                    jump :loop-finish: #7
                    jump :loop-start:  #8
                    jump :loop-start:  #9
                    jump :loop-start:  #10
                    jump :loop-start:  #11
            :loop-start:
                    iadd r0, r0, 0x1
                    iadd r1, r0, :jump-table:
                    jump r1
            :loop-finish:
                    halt
                    ";
            var results = new[] { Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:1),
                                               7UL), };
            RunUntilHalted(asm, results, maxCycleCount: 26);
        }

        private static void CPUTestJumpInstructionJumptableDoublejump()
        {
            string asm = @"
                    mov r0, 0x8
                    iadd r1, :jump-start:, r0
                    jump r1
            :jump-start:
                    jump :bad:              #0
                    jump :bad:              #1
                    jump :bad:              #2
                    jump :bad:              #3
                    jump :bad:              #4
                    jump :bad:              #5
                    jump :bad:              #6
                    jump :bad:              #7
                    jump :good:             #8
                    jump :bad:              #9
                    jump :bad:              #10
            :bad:
                    mov r0, 0x0
                    halt
            :good:
                    mov r0, 0x1
                    halt
                    ";
            var results = new[] { Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0),
                                               1UL), };
            RunUntilHalted(asm, results, maxCycleCount: 5);
        }

        private static void CPUTestJumpInstructionJumptableBasic()
        {
            string asm = @"
                    mov r0, 0x6
                    iadd r1, :jump-start:, r0
                    jump r1
       
            :jump-start:
                    mov   r0, 0x1           #0
                    jump  :end:             #1
                    mov   r0, 0x2           #2
                    jump  :end:             #3
                    mov   r0, 0x3           #4
                    jump  :end:             #5
                    mov   r0, 0x4           #6 <---
                    jump  :end:             #7
                    mov   r0, 0x5           #8
                    jump  :end:             #9
                    mov   r0, 0x6           #10
                    jump  :end:             #11
            :end:
                    halt
                    ";
            var results = new[] { Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0),
                                               4UL), };
            RunUntilHalted(asm, results, maxCycleCount: 6);
        }

        private static void CPUTestJumpInstructionComputedLiteralLabelOffset()
        {
            string asm = @"
                    mov r0, :first:
                    jump r0
                    jump :wrong:
                    halt
            :first:    
                    mov r0, :wrong: - 1 #??!
                    jump r0
                    jump :wrong:
                    jump :wrong:
                    jump :wrong:
                    jump :good:
            :wrong:
                    mov r1, 0x0
                    halt
            :good:
                    mov r1, 0x1
                    halt
                    ";
            var results = new[] { Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:1),
                                               1UL), };
            RunUntilHalted(asm, results, maxCycleCount: 7);
        }

        private static void CPUTestJumpInstructionIndirectAndComputed()
        {
            string asm = @"
                    mov r0, :first:
                    jump r0
                    jump :wrong:
                    halt
            :first:  
                    iadd r0, r0, 6
                    mov r1, 0x1
                    jump r0
                    jump :wrong:
                    jump :wrong:
                    jump :wrong:
                    jump :good:
            :wrong:
                    mov r1, 0x0
                    halt
            :good:
                    iadd r1, r1, 0x1
                    halt
                    ";
            var results = new[] { Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:1),
                                               2UL), };
            RunUntilHalted(asm, results, maxCycleCount: 8);
        }

        private static void CPUTestJumpInstructionInitialSpaghetti()
        {
            string asm = @"
                    mov r1, 0x0
                    jump :stuff:
                    mov r0, 0x1
                    jump :wrong-end:
            :back:
                    mov r0, 0x3
                    jump :right-end:
            :stuff:
                    mov r0, 0x2
                    jump :back:
            :wrong-end:
                    halt
            :right-end:
                    mov r1, r0
                    halt
                    ";
            var results = new[] {
                                  Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0),
                                               3UL),
                                  Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:1),
                                               3UL),
                                 };
            RunUntilHalted(asm, results, maxCycleCount: 8);
        }

        private static void CPUTestJumpInstructionNolabelLiteral()
        {
            string asm = @"
                    mov r0, 0x1           #0
                    jump 3                #1
                    halt                  #2
                    mov r0, 0x2           #3
                    halt                  #4
                    ";
            var results = new[] {
                                  Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0),
                                               2UL),
                                 };
            RunUntilHalted(asm, results, maxCycleCount: 4);
        }

        private static void CPUTestJumpInstructionBasicbackwards()
        {
            string asm = @"
                    jump :here:
            :back_again:
                    halt
            :here:
                    jump :back_again:
                    ";
            RunUntilHalted(asm, maxCycleCount: 3);
        }

        private static void CPUTestJumpInstructionBasicbackwardsInfinite()
        {
            string asm = @"
                    jump :here:        #0
            :back_again:               #
                    nop                #1
            :here:                     #
                    jump :back_again:  #2
                    ";
            var cpu = new CPU_VM(0, 32, PFCParser.ParseAssembly(asm));
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 2);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 1);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 2);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 1);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 2);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 1);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 2);
        }

        private static void CPUTestJumpInstructionInfiniteCounter()
        {
            string asm = @"
                    mov r0, 0x0
            :here:
                    iadd r0, r0, 0x1
                    jump :here:
                    ";

            var cpu = new CPU_VM(0, 32, PFCParser.ParseAssembly(asm));
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
            Debug.Assert(cpu.ROState.RegisterBank[0] == 0);

            cpu.Clock(); //0
            cpu.Clock(); //1
            cpu.Clock(); cpu.Clock(); //2
            cpu.Clock(); cpu.Clock(); //3
            cpu.Clock(); cpu.Clock(); //4
            cpu.Clock(); cpu.Clock(); //5
            cpu.Clock(); cpu.Clock(); //6
            cpu.Clock(); cpu.Clock(); //7
            Debug.Assert(cpu.ROState.RegisterBank[0] == 7);
        }

        private static void CPUTestJumpInstructionInfinite()
        {
            string asm = @"
                    mov r0, 0x1     #0
                    jump :here:     #1
                    mov r0, 0x2     #2
            :here:                  #
                    jump :here:     #3
                    ";
            var cpu = new CPU_VM(0, 32, PFCParser.ParseAssembly(asm));
            Debug.Assert(cpu.ROState.ProgramCounter == 0);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 1);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 3);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 3);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 3);
            cpu.Clock();
            Debug.Assert(cpu.ROState.ProgramCounter == 3);
            cpu.Clock(); cpu.Clock(); cpu.Clock(); cpu.Clock(); cpu.Clock(); cpu.Clock(); cpu.Clock(); cpu.Clock(); 
            Debug.Assert(cpu.ROState.ProgramCounter == 3);
        }

        private static void CPUTestJumpInstructionBasic()
        {
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.jump,
                                                      OperandLiteral.u32(0), 32));
            AssertThrows<ArgumentException>(() => new Instruction(OpcodeType.jump,
                                                                  OperandLiteral.u32(0),
                                                                  writesCPUFlags: true ));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("jump! 0x0"));



            string asm = @"
                    mov r0, 0x1
                    nop
                    nop
                    nop
                    jump :end:
                    nop
                    nop
                    mov r0, 0x2
                    nop
                    nop
            :end:
                    halt
            ";
            var results = new[] {
                                  Tuple.Create(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0),
                                               1UL),
                                 };
            RunUntilHalted(asm, results, maxCycleCount: 6);
        }

        #endregion


        private static void CPUTest_Instruction_icmp_condjump()
        {
            //todo: this should really be a 'warning', and it shoulnd't halt parsing.
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("icmp r0, r1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read! r0"));

            RunTestFile(@"Cmp\cmp_flagcheck.asm", maxCycleCount: 151);

            //AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("cmp32 0x1, 0x2"));
            //AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("cmp8 0x1, 0x2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("icmp!0x1, 0x2, 0x3"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("icmp!0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read r0, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read 0x1, r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read8 r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("flags_read32 r0"));


            RunTestFile(@"Cmp\condjump_pos.asm", maxCycleCount: 1000);
            RunTestFile(@"Cmp\condjump_neg.asm", maxCycleCount: 1000);
            RunTestFile(@"Cmp\tfcondjump.asm");
        }

        private static void CPUTest_Instruction_cmpops()
        {
            RunTestFile(@"Cmp\3op_cmpop.asm", maxCycleCount: 490 * 3);
            RunTestFile(@"Cmp\2op_cmpop.asm", maxCycleCount: 28 * 3);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ieq 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ieq r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ieq! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ineq 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ineq r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ineq! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iugt 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iugt r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iugt! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iule 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iule r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iule! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iuge 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iuge r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iuge! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iult 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iult r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iult! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isgt 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isgt r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isgt! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isle 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isle r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isle! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isge 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isge r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_isge! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_islt 0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_islt r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_islt! r0, 1, 2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iz 0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iz r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_iz! r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_inz 0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_inz r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_inz! r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ineg 0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ineg r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ineg! r0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ipos 0, 1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ipos r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("is_ipos! r0, 1"));
        }



        private static void CPUTestCallRetInstructions()
        {
            RunTestFile(@"Call\call_ret_basic.asm");
            RunTestFile(@"Call\fib.asm", maxCycleCount: 24781);
        }

        private static void CPUTestStackInstructions()
        {
            RunTestFile(@"Stack\stack_alloc_free_basic.asm");
            RunTestFile(@"Stack\push_data_basic.asm");
            RunTestFile(@"Stack\pop_data_basic.asm");

            RunTestFile(@"Stack\ctrl_stack_alloc_free.asm");
            AssertThrows<PFC_StackException>(() => RunTestFile(@"Stack\ctrl_stack_alloc_free_underflow.asm"));
            AssertThrows<PFC_StackException>(() => RunTestFile(@"Stack\ctrl_stack_alloc_free_overflow.asm"));

            RunTestFile(@"Stack\ctrl_push_pop.asm", maxCycleCount: 266);
            AssertThrows<PFC_StackException>(() => RunTestFile(@"Stack\ctrl_push_overflow.asm"));
            AssertThrows<PFC_StackException>(() => RunTestFile(@"Stack\ctrl_pop_underflow.asm"));


            RunTestFile(@"Stack\frame_enter.asm",
                        maxCycleCount: 1065);
            RunTestFile(@"Stack\ret_frame.asm",
                        maxCycleCount: 1064);


        }

        private static void CPUTestLogicInstructions()
        {
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("not r0, r1, r2"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("not 0, r1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("not r0"));

            RunTestFile(@"Logicops\simple_not.asm");
            RunTestFile(@"Logicops\simple_and.asm");
            RunTestFile(@"Logicops\simple_or.asm");
            RunTestFile(@"Logicops\simple_xor.asm");
            RunTestFile(@"Logicops\simple_nand.asm");
            RunTestFile(@"Logicops\simple_nor.asm");
            RunTestFile(@"Logicops\simple_xnor.asm");
            RunTestFile(@"Logicops\simple_flagtest.asm");
            //nand? nor?
            //bic/orn  (a op ~b)
        }

        private static void CPUTestSpecialRegisters()
        {
            RunTestFile(@"Registers\Basic_PC.asm");
            RunTestFile(@"Registers\Basic_SP.asm");
            RunTestFile(@"Registers\Basic_LR.asm");
            RunTestFile(@"Registers\Basic_SPLR.asm");
        }

        public static bool RunAllCPUTests()
        {
            CPUStateTests.RunAllCPUStateTests();

            CPUTestBasicInstrucions();

            CPUTestDatamovInstructions();
            CPUTestLogicInstructions();
            CPUTestArithInstructions();
            CPUTestLoadStoreInstructions();
            CPUTestJumpInstructions();

            CPUTest_Instruction_icmp_condjump();
            CPUTest_Instruction_cmpops();


            CPUTestSpecialRegisters();

            CPUTestStackInstructions();
            CPUTestCallRetInstructions();




            // TODO: fully test push_data[width] oper
            // TODO: fully test pop_data[width] oper
            // todo: have tests that fail if csp or [csp] is written to?
            // todo: test push_data SP and pop_data SP
            // TODO: frame register fp
            // TODO: frame_enter nBytes
            // TODO: frame_leave
            // TODO: call_i addr
            // TODO: call_leaf addr
            // TODO: call addr
            // TODO: call_frame addr
            // TODO: ret_i
            // TODO: ret
            // TODO: ret_leaf
            // TODO: ret_free nBytes
            // TODO: ret_frame
            // TODO: 
            // TODO: + exceptions?
            // TODO: + argN, varN, dcl var %x%, 0 ?
            // TODO: OR have an ARG<width> dst, arg opcode that reads arg N from the stack ?
            // TODO: make push_fulldescending etc variants e.g. popfd, popib?
            // TODO: make stm, 
            // TODO: for SP operand (and others) : post/pre increment/decrement
            // todo: have all of these tests just be files on disk?
            // todo st32 0x1, 0xdeadbeef -- - writing to 0x1 didn't cause error?

            // need a test that does '!' for all instructions that don't have FLAGSMOD_DISALLOWED set
            /*
             * reljump
             * maybe compop that takes a comparison operation and returns -1 or 0? (icmpop)
             * dynamic registers
             * mov c for flags? movc_ge  etc?
             */

            // todo: revsub irsub  irevsub? isub_r isubc_r?
            // todo: call/ return. ability to use data in asm. ability to refer to that data in opcodes/get it's address
            //       this will improve the isubc sub24 test program
            // need to test instructions other than iadd with !
            // and then do iadd32!<CZ>  to only update CZ, or something
            // todo: do ![flags] and b{} b32{} etc

            // need to test : jumping to addresses further away / bigger than 256 on an 8bit cpu..
            // need to test ld with literal dests etc?
            // need to test: ld/st/other ops with labels as dests... should that even be allowed?
            // labels are code labels.. need to wait until we have a data section to distinguish between code and
            // data labels?


            // todo: add 3 8 bits to do a 24 but number (signed and unsigned)
            // todo: sub 3 8 bits to do a 24 but number (signed and unsigned)


            // todo: st mixed
            // todo: non-32bit cpus

            // call all functions via reflection?

            //// could have a carry bit for each operand, if it gets shifted?


            return true;
        }



        //need to test MOV PC, 0x4 etc

        //ensure, via tests, that mov r0, *(*(r0)) doesn't do a double dereference.
        //need a test that just does too many Clock()
        //need to test writing to a literal without a store or m[literal] etc
        //need to test really big uints/floats not corrupting causing overflow exceptions etc
        //writing outside of register bank/memory ranges etc (e.g. r500)
        //need to test mismatching bit widths etc (i.e. iadd32 i8 dst, i16 src0, i24 src1)
        //need to test iadd32 r0, r0, 1.0f fails? (though that's a parser issue?)
        //                                         tag each operand with float/int/reg and debug check?

        //need to have a test that uses a value too big for the address bus?
    }
}
