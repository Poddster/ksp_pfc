﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFC.CPU
{
    using System.Runtime.Serialization;

    //todo should probably refactor this so that the exceptions have a mask
    // and the base exception is responsible for log printing

    [Serializable]
    public class PFCBaseException : Exception
    {
        public PFCBaseException()
        {
        }

        public PFCBaseException(string message)
            : base(message)
        {
        }

        public PFCBaseException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected PFCBaseException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class PFC_CPUNoInstructionException : PFCBaseException
    {
        public PFC_CPUNoInstructionException()
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, "");
        }

        public PFC_CPUNoInstructionException(string message)
            : base(message)
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, message);
        }

        public PFC_CPUNoInstructionException(string message, Exception inner)
            : base(message, inner)
        {
            string err = message + inner.ToString();
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, err);
        }

        protected PFC_CPUNoInstructionException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, "");
        }
    }

    [Serializable]
    public class PFC_CPUAssertionException : PFCBaseException
    {
        public PFC_CPUAssertionException()
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.ASSERT, "");
        }

        public PFC_CPUAssertionException(string message)
            : base(message)
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.ASSERT, message);
        }

        public PFC_CPUAssertionException(string message, Exception inner)
            : base(message, inner)
        {
            string err = message + inner.ToString();
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.ASSERT, err);
        }

        protected PFC_CPUAssertionException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.ASSERT, "");
        }
    }

    [Serializable]
    public class PFC_StackException : Exception
    {
        public PFC_StackException()
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, "");
        }

        public PFC_StackException(string message) : base(message)
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, message);
        }

        public PFC_StackException(string message, Exception inner) : base(message, inner)
        {
            string err = message + inner.ToString();
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, err);
        }

        protected PFC_StackException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.INVALID_INSTRUCTION, "");
        }
    }
}
