
		#0x301234 - 0x300233 = 0x001001
		mov8      r0,     0x34
		mov8      r1,     0x12
		mov8      r2,     0x30

		mov8      r3,     0x33
		mov8      r4,     0x02
		mov8      r5,     0x30

		isub8!    r6,     r0,     r3
		isubc8!   r7,     r1,     r4
		isubc8    r8,     r2,     r5

		assert_eq r6,     0x01
		assert_eq r7,     0x10
		assert_eq r8,     0x00


		# 0x000002 - 0x123456  =  2 - 1193046 = -1193044 = 0xedcbac
		mov8      r0,     0x02
		mov8      r1,     0x00
		mov8      r2,     0x00

		mov8      r3,     0x56
		mov8      r4,     0x34
		mov8      r5,     0x12

		isub8!    r6,     r0,     r3
		isubc8!   r7,     r1,     r4
		isubc8    r8,     r2,     r5

		assert_eq r6,     0xac
		assert_eq r7,     0xcb
		assert_eq r8,     0xed

		halt
