                xnor        r0,     0xFFFFFFFF, 0x00000000
                assert_eq   r0,     0x00000000
                xnor        r0,     0xAAAAAAAA, 0x55555555
                assert_eq   r0,     0x00000000

                xnor32      r0,     0x0F0F0F0F, 0x55555555
                assert_eq   r0,     0xA5A5A5A5
                xnor32      r0,     0xFFFFFFFF, 0x55555555
                assert_eq   r0,     0x55555555

                xnor8       r1,     0x13, 0xED
                assert_eq   r1,     0x01
                xnor8       r1,     0x12, 0x80
                assert_eq   r1,     0x6d

                mov32       r1,     0x76543210
                mov32       r2,     0xFEDBCA98
                xnor32      r3,     r2,         r1
                xnor32      r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0x77700777

                mov32       r3,     0x00000000
                mov32       r4,     0x00000000
                xnor16      r3,     r2,         r1
                xnor16      r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0x0777

                halt
