




		icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
		flags_read  r0
		assert_eq   r0,         0b0101

		icmp!       0x00000000, 0x00000001  #          +0 -          +1 = ffffffff = N   
		flags_read  r1
		assert_eq   r1,         0b1000

		icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
		flags_read  r2
		assert_eq   r2,         0b1000

		icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
		flags_read  r0
		assert_eq   r0,         0b1010

		icmp!       0x00000000, 0x80000001  #          +0 - -2147483647 = 7fffffff =     
		flags_read  r1
		assert_eq   r1,         0b0000

		icmp!       0x00000000, 0xFFFFFFFE  #          +0 -          -2 = 00000002 =     
		flags_read  r2
		assert_eq   r2,         0b0000

		icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
		flags_read  r0
		assert_eq   r0,         0b0000


		icmp!       0x00000001, 0x00000000  #          +1 -          +0 = 00000001 =    C
		flags_read  r1
		assert_eq   r1,         0b0001

		icmp!       0x00000001, 0x00000001  #          +1 -          +1 = 00000000 =  Z C
		flags_read  r2
		assert_eq   r2,         0b0101

		icmp!       0x00000001, 0x7FFFFFFF  #          +1 - +2147483647 = 80000002 = N   
		flags_read  r0
		assert_eq   r0,         0b1000

		icmp!       0x00000001, 0x80000000  #          +1 - -2147483648 = 80000001 = N V 
		flags_read  r1
		assert_eq   r1,         0b1010

		icmp!       0x00000001, 0x80000001  #          +1 - -2147483647 = 80000000 = N V 
		flags_read  r2
		assert_eq   r2,         0b1010

		icmp!       0x00000001, 0xFFFFFFFE  #          +1 -          -2 = 00000003 =     
		flags_read  r0
		assert_eq   r0,         0b0000

		icmp!       0x00000001, 0xFFFFFFFF  #          +1 -          -1 = 00000002 =     
		flags_read  r1
		assert_eq   r1,         0b0000


		icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
		flags_read  r2
		assert_eq   r2,         0b0001

		icmp!       0x7FFFFFFF, 0x00000001  # +2147483647 -          +1 = 7ffffffe =    C
		flags_read  r0
		assert_eq   r0,         0b0001

		icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
		flags_read  r1
		assert_eq   r1,         0b0101

		icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
		flags_read  r2
		assert_eq   r2,         0b1010

		icmp!       0x7FFFFFFF, 0x80000001  # +2147483647 - -2147483647 = fffffffe = N V 
		flags_read  r0
		assert_eq   r0,         0b1010

		icmp!       0x7FFFFFFF, 0xFFFFFFFE  # +2147483647 -          -2 = 80000001 = N V 
		flags_read  r1
		assert_eq   r1,         0b1010

		icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
		flags_read  r2
		assert_eq   r2,         0b1010


		icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
		flags_read  r0
		assert_eq   r0,         0b1001

		icmp!       0x80000000, 0x00000001  # -2147483648 -          +1 = 7fffffff =   VC
		flags_read  r1
		assert_eq   r1,         0b0011

		icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
		flags_read  r2
		assert_eq   r2,         0b0011

		icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
		flags_read  r0
		assert_eq   r0,         0b0101

		icmp!       0x80000000, 0x80000001  # -2147483648 - -2147483647 = ffffffff = N   
		flags_read  r1
		assert_eq   r1,         0b1000

		icmp!       0x80000000, 0xFFFFFFFE  # -2147483648 -          -2 = 80000002 = N   
		flags_read  r2
		assert_eq   r2,         0b1000

		icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
		flags_read  r0
		assert_eq   r0,         0b1000


		icmp!       0x80000001, 0x00000000  # -2147483647 -          +0 = 80000001 = N  C
		flags_read  r1
		assert_eq   r1,         0b1001

		icmp!       0x80000001, 0x00000001  # -2147483647 -          +1 = 80000000 = N  C
		flags_read  r2
		assert_eq   r2,         0b1001

		icmp!       0x80000001, 0x7FFFFFFF  # -2147483647 - +2147483647 = 00000002 =   VC
		flags_read  r0
		assert_eq   r0,         0b0011

		icmp!       0x80000001, 0x80000000  # -2147483647 - -2147483648 = 00000001 =    C
		flags_read  r1
		assert_eq   r1,         0b0001

		icmp!       0x80000001, 0x80000001  # -2147483647 - -2147483647 = 00000000 =  Z C
		flags_read  r2
		assert_eq   r2,         0b0101

		icmp!       0x80000001, 0xFFFFFFFE  # -2147483647 -          -2 = 80000003 = N   
		flags_read  r0
		assert_eq   r0,         0b1000

		icmp!       0x80000001, 0xFFFFFFFF  # -2147483647 -          -1 = 80000002 = N   
		flags_read  r1
		assert_eq   r1,         0b1000


		icmp!       0xFFFFFFFE, 0x00000000  #          -2 -          +0 = fffffffe = N  C
		flags_read  r2
		assert_eq   r2,         0b1001

		icmp!       0xFFFFFFFE, 0x00000001  #          -2 -          +1 = fffffffd = N  C
		flags_read  r0
		assert_eq   r0,         0b1001

		icmp!       0xFFFFFFFE, 0x7FFFFFFF  #          -2 - +2147483647 = 7fffffff =   VC
		flags_read  r1
		assert_eq   r1,         0b0011

		icmp!       0xFFFFFFFE, 0x80000000  #          -2 - -2147483648 = 7ffffffe =    C
		flags_read  r2
		assert_eq   r2,         0b0001

		icmp!       0xFFFFFFFE, 0x80000001  #          -2 - -2147483647 = 7ffffffd =    C
		flags_read  r0
		assert_eq   r0,         0b0001

		icmp!       0xFFFFFFFE, 0xFFFFFFFE  #          -2 -          -2 = 00000000 =  Z C
		flags_read  r1
		assert_eq   r1,         0b0101

		icmp!       0xFFFFFFFE, 0xFFFFFFFF  #          -2 -          -1 = ffffffff = N   
		flags_read  r2
		assert_eq   r2,         0b1000


		icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
		flags_read  r0
		assert_eq   r0,         0b1001

		icmp!       0xFFFFFFFF, 0x00000001  #          -1 -          +1 = fffffffe = N  C
		flags_read  r1
		assert_eq   r1,         0b1001

		icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
		flags_read  r2
		assert_eq   r2,         0b1001

		icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
		flags_read  r0
		assert_eq   r0,         0b0001

		icmp!       0xFFFFFFFF, 0x80000001  #          -1 - -2147483647 = 7ffffffe =    C
		flags_read  r1
		assert_eq   r1,         0b0001

		icmp!       0xFFFFFFFF, 0xFFFFFFFE  #          -1 -          -2 = 00000001 =    C
		flags_read  r2
		assert_eq   r2,         0b0001

		icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
		flags_read  r0
		assert_eq   r0,         0b0101


		halt