﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFC;
using PFC.ASMParser;

using PFC.CPU;

namespace PFC.Tests.CPU
{
    public static class ParserTest
    {

        /// <summary>
        /// Checks to make sure a test throws an exception.
        /// </summary>
        /// <typeparam name="exceptionType"></typeparam>
        /// <param name="func"></param>
        private static void AssertThrows<exceptionType>(Action func) where exceptionType : Exception
        {
            bool bPassed = false;
            try
            {
                func();
            }
            catch (exceptionType e)
            {
                Debug.Assert(e.Message != null); //just to stop compiler complaining e is not used
                bPassed = true;
            }
            Debug.Assert(bPassed);
        }


        private static void AssertAssemblyEqualsProgram(string asm, Instruction[] expectedProg)
        {
            var asmInsts = PFCParser.ParseAssembly(asm);

            //printInstructionList(expectedProg);
            //printInstructionList(asmInsts);

            Debug.Assert(expectedProg.Length == asmInsts.Length);
            Debug.Assert(asmInsts.SequenceEqual(expectedProg));
        }

        private static void printInstructionList(Instruction[] insts)
        {
            int i = 0;
            var a = insts.Map((inst) => String.Format("\t{0,-4}{1}", i++, inst));
            Log.Print("\n" + String.Join("\n", a));
        }


        private static void ParserTestNop()
        {
            string asm = @"
                nop
                nop
                nop
            ";
            var prog = new[]
            {
                new Instruction(OpcodeType.nop),
                new Instruction(OpcodeType.nop),
                new Instruction(OpcodeType.nop),
            };
            AssertAssemblyEqualsProgram(asm, prog);

            AssertAssemblyEqualsProgram("nop", new[] { new Instruction(OpcodeType.nop) });
        }

        private static void ParserTest0()
        {
            string asm = @"
                mov r0, 0x5
                iadd32 r0, r0, 0x1
            ";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(5)),
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                32),
            };

            AssertAssemblyEqualsProgram(asm, prog);
        }

        private static void ParserTest1()
        {
            //  notice other instruction names in the comments, shouldn't confuse the Parser.
            string asm = @"
                mov r0, 0x5        #iadd32
                iadd32 r0, r0, 0x1 #mov
            ";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(5)),
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                32),
            };
            AssertAssemblyEqualsProgram(asm, prog);
        }

        private static void ParserTest2()
        {
            string asm = @" #nothing
#comments
#
#\|<,.>/?;:'@#~[{]}-_=+!""£$%^&*()
 
   
                mov r0, 0x5   # \|<,.>/?;:'@#~[{]}-_=+!""£$%^&*()
                iadd16 r0, r0, 0x1#

            ";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(5)),
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                16),
            };
            AssertAssemblyEqualsProgram(asm, prog);
        }
        private static void ParserTest3()
        {
            string asm = "mov r0, 0.0f";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f)),
            };
            AssertAssemblyEqualsProgram(asm, prog);
        }

        private static void ParserTest4()
        {
            string test4 = "wobbles wibble boo boo mov r0, 0.0f";
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(test4));
        }

        private static void ParserTest5()
        {
            string test5 = @"
    mov r0, 0.0f
    #mov r0, 0.5f
    mov r12, 1.0f # dashd akdsh ksah
    iadd32 r0, 0x3, 1.0zzsdasd
";
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(test5));

        }

        private static void ParserTest6()
        {
            string testa = "mov r0,1";
            var proga = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                0),
            };
            AssertAssemblyEqualsProgram(testa, proga);

            string testb = "mov r0,     1";
            var progb = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                0),
            };
            AssertAssemblyEqualsProgram(testb, progb);

            string testc = "  mov r0, 1  ";
            var progc = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                0),
            };
            AssertAssemblyEqualsProgram(testc, progc);

            string testd = "  mov r0,1  ";
            var progd = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(1),
                                0),
            };
            AssertAssemblyEqualsProgram(testd, progd);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("movr0,1"));

            string teste = "iadd r13, 0x1234, r6 #mov r0, r0";
            var proge = new[]
            {
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(13),
                                OperandLiteral.undef(0x1234),
                                OperandReg.Register(6),
                                0),
            };
            AssertAssemblyEqualsProgram(teste, proge);

            string testf = "iadd32 r13, 0x1234, r6 #mov r0, r0";
            var progf = new[]
            {
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(13),
                                OperandLiteral.undef(0x1234),
                                OperandReg.Register(6),
                                32),
            };
            AssertAssemblyEqualsProgram(testf, progf);


            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iaddr0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd.r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("slurp r0, 0x8"));
        }

        private static void ParserTestValidOpcode()
        {
            string test1 = @"mov r0, 0.0f";
            var prog1 = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f)),
            };
            AssertAssemblyEqualsProgram(test1, prog1);

            string test2 = @"mov8 r0, 0.0f";
            var prog2 = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f),
                                8),
            };
            AssertAssemblyEqualsProgram(test2, prog2);

            string test3 = @"mov16 r0, 0.0f";
            var prog3 = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f),
                                16),
            };
            AssertAssemblyEqualsProgram(test3, prog3);

            string test4 = @"mov24 r0, 0.0f";
            var prog4 = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f),
                                24),
            };
            AssertAssemblyEqualsProgram(test4, prog4);

            string test5 = @"mov32 r0, 0.0f";
            var prog5 = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f),
                                32),
            };
            AssertAssemblyEqualsProgram(test5, prog5);

            string test6 = @"mov64 r0, 0.0f";
            var prog6 = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f),
                                64),
            };
            AssertAssemblyEqualsProgram(test6, prog6);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("xmov r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("xmov32 r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov32x r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("movx32 r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov3 r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov0 r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(" mov r0, r8888888"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(" mov r0 r1 r2 r3"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov mov r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r00x100"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov 0x100r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov 0x100,r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r0x100"));
        }

        private static void ParserTestOpcodePosition()
        {
            string a = "mov r0, 0.0f";
            string b = "  mov r0, 0.0f  ";
            string c = "#  mov r0, 0.0f  ";
            var ab_prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f)),
            };
            var c_prog = new Instruction[0];
            AssertAssemblyEqualsProgram(a, ab_prog);
            AssertAssemblyEqualsProgram(b, ab_prog);
            AssertAssemblyEqualsProgram(c, c_prog);


            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("r0 mov r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(",mov r0, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iaddr0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd.r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("slurp r0, 0x8"));
        }

        private static void ParserTestOperandCount()
        {
            string asm = @"mov r0, 0.0f
                           iadd32 r0, r1, 1.0f";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.f32(0.0f)),
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                OperandLiteral.f32(1.0f),
                                32),
            };
            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r0, 0.0f, 1.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32 r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32 r0, r1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32 r0, r1, 1.0f, r2"));
        }

        private static void ParserTestCommas()
        {
            string asm = "mov r0, r1\niadd32 r0, r1, r2\n\tiadd32 r0,\tr1,     r2\n";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1)),
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                OperandReg.Register(2),
                                32),
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                OperandReg.Register(2),
                                32),
            };
            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, ,0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,, 0.0f"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, 0.0f,"));
        }

        private static void ParserTestWhitespace()
        {
            string asm = @"mov r0, r1
                          mov r0,r1
                          mov r0,              r1";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1)),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1)),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1)),
            };
            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iaddr0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("movr0,1"));
        }

        private static void ParserTestBitWidth()
        {
            string asm = @"mov r0, r1
                           mov8 r0, r1
                           mov16 r0, r1
                           mov24 r0, r1
                           mov32 r0, r1
                           mov64 r0, r1";
            var prog = new[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1)),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1), 8),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1), 16),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1), 24),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1), 32),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.Register(1), 64),
            };
            AssertAssemblyEqualsProgram(asm, prog);


            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov11.5 r0, r1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov9 r0, r1"));


            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("nop32"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("jump32 0x500"));


        }

        private static void ParserTestDodgySymbols()
        {
            var prog = new[]
            {
                new Instruction(OpcodeType.nop),
            };
            AssertAssemblyEqualsProgram(@"nop", prog);
            AssertAssemblyEqualsProgram(@"nop#", prog);
            AssertAssemblyEqualsProgram(@"nop##", prog);
            AssertAssemblyEqualsProgram(@"nop#\", prog);
            AssertAssemblyEqualsProgram(@"nop#|", prog);
            AssertAssemblyEqualsProgram(@"nop#<", prog);
            AssertAssemblyEqualsProgram(@"nop#,", prog);
            AssertAssemblyEqualsProgram(@"nop#.", prog);
            AssertAssemblyEqualsProgram(@"nop#>", prog);
            AssertAssemblyEqualsProgram(@"nop#/", prog);
            AssertAssemblyEqualsProgram(@"nop#?", prog);
            AssertAssemblyEqualsProgram(@"nop#;", prog);
            AssertAssemblyEqualsProgram(@"nop#:", prog);
            AssertAssemblyEqualsProgram(@"nop#'", prog);
            AssertAssemblyEqualsProgram(@"nop#@", prog);
            AssertAssemblyEqualsProgram(@"nop#~", prog);
            AssertAssemblyEqualsProgram(@"nop#[", prog);
            AssertAssemblyEqualsProgram(@"nop#{", prog);
            AssertAssemblyEqualsProgram(@"nop#]", prog);
            AssertAssemblyEqualsProgram(@"nop#}", prog);
            AssertAssemblyEqualsProgram(@"nop#-", prog);
            AssertAssemblyEqualsProgram(@"nop#_", prog);
            AssertAssemblyEqualsProgram(@"nop#=", prog);
            AssertAssemblyEqualsProgram(@"nop#+", prog);
            AssertAssemblyEqualsProgram(@"nop#!", prog);
            AssertAssemblyEqualsProgram(@"nop#""", prog);
            AssertAssemblyEqualsProgram(@"nop#£", prog);
            AssertAssemblyEqualsProgram(@"nop#$", prog);
            AssertAssemblyEqualsProgram(@"nop#%", prog);
            AssertAssemblyEqualsProgram(@"nop#^", prog);
            AssertAssemblyEqualsProgram(@"nop#&", prog);
            AssertAssemblyEqualsProgram(@"nop#*", prog);
            AssertAssemblyEqualsProgram(@"nop#(", prog);
            AssertAssemblyEqualsProgram(@"nop#)", prog);
            AssertAssemblyEqualsProgram(@"nop#`", prog);
            AssertAssemblyEqualsProgram(@"nop#¬", prog);
            AssertAssemblyEqualsProgram(@"nop#€", prog);
            AssertAssemblyEqualsProgram(@"nop#¦", prog);
            AssertAssemblyEqualsProgram(@"nop#é", prog);
            AssertAssemblyEqualsProgram(@"nop#ú", prog);
            AssertAssemblyEqualsProgram(@"nop#í", prog);
            AssertAssemblyEqualsProgram(@"nop#ó", prog);
            AssertAssemblyEqualsProgram(@"nop#á", prog);
                                             
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop \"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop |"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop <"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ,"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ."));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop >"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop /"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ?"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ;"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop :"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop '"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop @"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ~"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ["));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop {"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ]"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop }"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop -"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop _"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ="));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop +"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop !"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop """));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop £"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop $"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop %"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ^"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop &"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop *"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ("));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop )"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop `"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ¬"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop €"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ¦"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop é"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ú"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop í"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop ó"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"nop á"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("nop \x14"));


            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iaddr0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd32r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("iadd.r0, 0x8, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("slurp r0, 0x8"));
        }

        private static void ParserTestTypeCasts()
        {
            string asm = @"
                mov r0, 0x12345678
	            mov r1, 0x20FEDCBA
	            iadd32 r2, r0, r1                         #1# = 0x33333332
	            iadd8  r3, r0, r1                         #2# = 0x32
	            iadd32 r4, r0, i8{r1}                     #3# = 0x12345632
	            iadd32 i16{r5}, r0, r1                    #4# = 0x3332
	            iadd16 i32{r6}, r0, r1                    #5# = 0x00003332
	            iadd32 i8{r7}, i8{r0}, i8{r1}             #6# = 0x32
                iadd16 i32{r8}, r0, 0xeeee5432            #7# = 0xFFFFaaaa  (sign extended)
                iadd8  i32{r8}, i32{r0}, i32{0xeeee5432}  #8# = 0xFFFFFFAA
            ";
            var prog = new Instruction[]
            {
                /*
                 * 	r0 = 0x12345678
	             *  r1 = 0x20FEDCBA
                 */
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(0x12345678)),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(1),
                                OperandLiteral.undef(0x20FEDCBA)),
                //  1. iadd32 rN, r0, r1               = 0x33333332
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(2),
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                32),
	            //  2. iadd8  rN, r0, r1               = 0x32
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(3),
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                8),
	            //  3. iadd32 rN, r0, i8{r1}           = 0x12345632
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(4),
                                OperandReg.Register(0),
                                OperandReg.Register(1, DataType.i8),
                                32),
	            //  4. iadd32 i16{rN}, r0, r1          = 0x3332
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(5, DataType.i16),
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                32),
	            //  5. iadd16 i32{rN}, r0, r1          = 0x00003332
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(6, DataType.i32),
                                OperandReg.Register(0),
                                OperandReg.Register(1),
                                16),
	            //  6. iadd32 i8{rN}, i8{r0}, i8{r1}   = 0x32
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(7, DataType.i8),
                                OperandReg.Register(0, DataType.i8),
                                OperandReg.Register(1, DataType.i8),
                                32),
                //  7. iadd16 i32{r8}, r0, 0xeeee5432  = 0xFFFFaaaa  (sign extended)
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(8, DataType.i32),
                                OperandReg.Register(0),
                                OperandLiteral.undef(0xeeee5432),
                                16),
                //  8. iadd8  i32{r8}, i32{r0}, i32{0xeeee5432}= 0xFFFFFFAA
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(8, DataType.i32),
                                OperandReg.Register(0, DataType.i32),
                                OperandLiteral.i32(unchecked((int)0xeeee5432)),
                                8),
            };

            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u16{u8{r1}}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u8{u8{r1}}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  @(u16{r1})"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  -u16{r1}"));


            // can't use a data type with a special register, even if it's u16 (which matches current implementation)
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u16{pc}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u16{sp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u16{lr}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u16{fp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  u16{csp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  i32{pc}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  i32{sp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  i32{lr}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  i32{fp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  i32{csp}"));
        }

        private static void ParserTestDeref()
        {
            #region big-string
            string asm = @"
                        
                    # r0 = 0x00000314             # 4
                    # r1 = 0x00001000             #
                    # r2 = 0x00002200             #
                    # r3 = 0x00003330             # 8
                    # r4 = 0x0000C0CC             #
                    #                             #
                    #               start         #   final
                    # MEM 0x0000 = 0xDEADBEEF     # 0xCAFEBABE
                    # MEM 0x0004 = 0xDEADBEEF     # 0x00000314
                    # MEM 0x0008 = 0xDEADBEEF     # 0x0000000F
                    # MEM 0x000C = 0xDEADBEEF     # 0xDEEDEED0
                    # MEM 0x0010 = 0000000000     # 0x00000314
                    # MEM 0xC0CC = 0x00000010     # 
                    #                             # 
                    # MEM 0x1000 = 0xDEEDEED0     # 0x0000777C
                    # MEM 0x2200 = 0xFFFFFFFF     # 0x00001000
                    # MEM 0x3330 = 0xDEADBEEF     # 0xA5A5A5A5
                    #                             # 
                    # MEM 0x0314 = 0xA5A5A5A5     # 0x44444444
                    # MEM 0x111C = 0x0000222C     # 
                    # MEM 0x222C = 0xFFFFFFFF     # 0x0000222C
                    # MEM 0x333C = 0x44444444     # 
                    # MEM 0x777C = 0xDEADBEEF     # 0x0000C0CC
                    #                             #
                    # MEM 0x4014 = 0x00000000     # 0xFeFeFeFe
                    # MEM 0x4018 = 0x00000000     # 0x04
                    # MEM 0x401C = 0x00000000     # 0xFFFFFFFF
                    # MEM 0x4020 = 0x00000000     # 0xDEEDEED0
                    # MEM 0x8100 = 0x00004014     #
                    # MEM 0x8104 = 0x00004018     #
                    # MEM 0x8108 = 0x0000401C     #
                    # MEM 0x810C = 0x00004020     #
                    #-----------------------------#------------------------------------------
                    st32 0x0000, 0xCAFEBABE       #01.  0x0000 = 0xCAFEBABE      //lit_addr, lit_val       
                    st32 0x0004, r0               #02.  0x0004 = 0x00000314      //lit_addr, reg_val       
                    st32 0x0008, @(0x111C)        #03.  0x0008 = 0x0000222C      //lit_addr, @(lit_val)    
                    st32 0x000C, @(r1)            #04.  0x000C = 0xDEEDEED0      //lit_addr, @(reg_val)    
                                                  #                                                                                
                    st32 r1, 0xFFFFFFFF           #05.  0x1000 = 0xFFFFFFFF      // reg_addr, lit_val      
                    st32 r2, r1                   #06.  0x2200 = 0x00001000      // reg_addr, reg_val      
                    st32 r1, @(0x333C)            #07.  0x1000 = 0x44444444      // reg_addr, @(lit_val)   
                    st32 r3, @(r0)                #08.  0x3330 = 0xA5A5A5A5      // reg_addr, @(reg_val)   
                    st32 r2, @(r2)                #09.  0x2200 = 0x00001000      // reg_addr, @(reg_val)   
                                                  #     (which it already was!)
                    mov r0, 0x04                  #10. 
                    mov r3, 0x08                  #11.    
                    st32 @(r2), 0x777C            #12.  0x1000 = 0x0000777C      // @(reg_addr), lit_val   
                    st32 @(r1), r4                #13.  0x777C = 0x0000C0CC      // @(reg_addr), reg_val
                    st32 @(r0), @(0x333C)         #14.  0x0314 = 0x44444444      // @(reg_addr), @(lit_val)
                    st32 @(r4), @(r0)             #15.  0x0010 = 0x00000314      // @(reg_addr), @(reg_val)
                    st32 @(r3), @(r3)             #16.  0x222C = 0x0000222C      // @(reg_addr), @(reg_val)
                                                  #     (which it already was!)
                                                  #   
                    st32 @(0x8100), 0xFeFeFeFe    #17.  0x4014 = 0xFeFeFeFe      // @(lit_addr), lit_val   
                    st32 @(0x8104), r0            #18.  0x4018 = 0x04            // @(lit_addr), reg_val
                    st32 @(0x8108), @(0x0010)     #19.  0x401C = 0x00000314      // @(lit_addr), @(lit_val)
                    st32 @(0x810C), @(r4)         #20.  0x4020 = 0x00000010      // @(lit_addr), @(reg_val)
                                                  #   
                    st32 0xFFFF0008, 0xF          #21.  0x0008 = 0x0000000F     
                        
            ";
            #endregion
            #region big-program
            var prog = new Instruction[]
            {
                // 01. st32 0x0000, 0xCAFEBABE   | 0x0000 = 0xCAFEBABE      //lit_addr, lit_val   
                // 02. st32 0x0004, r0           | 0x0004 = 0x00000314      //lit_addr, reg_val   
                // 03. st32 0x0008, @(0x111C)    | 0x0008 = 0x0000222C      //lit_addr, @(lit_val)
                // 04. st32 0x000C, @(r1)        | 0x000C = 0xDEEDEED0      //lit_addr, @(reg_val)
                new Instruction(OpcodeType.st,
                                OperandLiteral.undef(0x00000000),
                                OperandLiteral.undef(0xCAFEBABE),
                                32),
                new Instruction(OpcodeType.st,
                                OperandLiteral.undef(0x00000004),
                                OperandReg.Register(0),
                                32),
                new Instruction(OpcodeType.st,
                                OperandLiteral.undef(0x00000008),
                                OperandReg.LiteralAddress(0x111C,
                                                       DataType.undef),
                                32),
                new Instruction(OpcodeType.st,
                                OperandLiteral.undef(0x0000000C),
                                OperandReg.RegisterDeref(1),
                                32),


                // 05. st32 r1, 0xFFFFFFFF       | 0x1000 = 0xFFFFFFFF      // reg_addr, lit_val        
                // 06. st32 r2, r1               | 0x2200 = 0x00001000      // reg_addr, reg_val        
                // 07. st32 r1, @(0x333C)        | 0x1000 = 0x44444444      // reg_addr, @(lit_val)     
                // 08. st32 r3, @(r0)            | 0x3330 = 0xA5A5A5A5      // reg_addr, @(reg_val)     
                // 09. st32 r2, @(r2)            | 0x2200 = 0x00001000      // reg_addr, @(reg_val)     
                new Instruction(OpcodeType.st,
                                OperandReg.Register(1),
                                OperandLiteral.undef(0xFFFFFFFF),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.Register(2),
                                OperandReg.Register(1),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.Register(1),
                                OperandReg.LiteralAddress(0x333C,
                                                       DataType.undef),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.Register(3),
                                OperandReg.RegisterDeref(0),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.Register(2),
                                OperandReg.RegisterDeref(2),
                                32),

                // 10. mov r0, 0x04 
                // 11. mov r3, 0x08                  
                // 12. st32 @(r2), 0x777C        | 0x1000 = 0x0000777C      // @(reg_addr), lit_val   
                // 13. st32 @(r1), r4            | 0x777C = 0x0000C0CC      // @(reg_addr), reg_val
                // 14. st32 @(r0), @(0x333C)     | 0x0314 = 0x44444444      // @(reg_addr), @(lit_val)
                // 15. st32 @(r4), @(r0)         | 0x0010 = 0x00000314      // @(reg_addr), @(reg_val)
                // 16. st32 @(r3), @(r3)         | 0x222C = 0x0000222C      // @(reg_addr), @(reg_val)
                //                             (which it already was!)
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandLiteral.undef(0x04)),
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(3),
                                OperandLiteral.undef(0x08)),
                new Instruction(OpcodeType.st,
                                OperandReg.RegisterDeref(2),
                                OperandLiteral.undef(0x777C),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.RegisterDeref(1),
                                OperandReg.Register(4),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.RegisterDeref(0),
                                OperandReg.LiteralAddress(0x333C,
                                                       DataType.undef),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.RegisterDeref(4),
                                OperandReg.RegisterDeref(0),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.RegisterDeref(3),
                                OperandReg.RegisterDeref(3),
                                32),


                // 17. st32 @(0x8100), 0xFeFeFeFe| 0x4014 = 0xFeFeFeFe      // @(lit_addr), lit_val   
                // 18. st32 @(0x8104), r0        | 0x4018 = 0x04            // @(lit_addr), reg_val
                // 19. st32 @(0x8108), @(0x0010) | 0x401C = 0x00000314      // @(lit_addr), @(lit_val)
                // 20. st32 @(0x810C), @(r4)     | 0x4020 = 0x00000010      // @(lit_addr), @(reg_val)
                // 
                // 21. st32 0xFFFF0008, 0xF      | 0x0008 = 0x0000000F   
                new Instruction(OpcodeType.st,
                                OperandReg.LiteralAddress(0x8100,
                                                       DataType.undef),
                                OperandLiteral.undef(0xFeFeFeFe),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.LiteralAddress(0x8104,
                                                       DataType.undef),
                                OperandReg.Register(0),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.LiteralAddress(0x8108,
                                                       DataType.undef),
                                OperandReg.LiteralAddress(0x0010,
                                                       DataType.undef),
                                32),
                new Instruction(OpcodeType.st,
                                OperandReg.LiteralAddress(0x810C,
                                                       DataType.undef),
                                OperandReg.RegisterDeref(4),
                                32),
                new Instruction(OpcodeType.st,
                                OperandLiteral.undef(0xFFFF0008),
                                OperandLiteral.undef(0xF),
                                32),
            };
            #endregion


            AssertAssemblyEqualsProgram(asm, prog);


            // These special registers can be dereferenced
            const string SpecialsAsm = @"
                    mov r0, @(sp)
                    mov r1, @(fp)
            ";
            var specialsProg = new Instruction[]
            {
                new Instruction(OpcodeType.mov, 
                                OperandReg.Register(0),
                                OperandReg.StackPointer(deref: true)),
                new Instruction(OpcodeType.mov, 
                                OperandReg.Register(1),
                                OperandReg.FramePointer(deref: true)),
            };
            AssertAssemblyEqualsProgram(SpecialsAsm, specialsProg);

            // These special registers can't be dereferenced
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r0, @(pc)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r0, @(lr)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("mov r0, @(csp)"));







            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("st 0, 0xCAFEBABE"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("st u32{0}, u32{0xCAFEBABE}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("st0 0, 0xCAFEBABE"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("st 32, 0xCAFEBABE, 0"));




            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("ld r3, 0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly("ld0 r3, 0"));

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  @(@(r1))"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  -@(r1)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  @(u8{r1})"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  -@(fp)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0,  -@(sp)"));


        }

        private static void ParserTestLabel()
        {
            #region asm-string
            string asm = @"
                    mov8 r0, 0x1                         #0
            :label:                                      #
                    mov8 r1, 0x2                         #1
            :label0: mov8 r2, 0x3                        #2
            :label1: mov8 r3, 0x4                        #3
                                                         #
            :label2:                                     #
                    mov16 r0, :label:                    #4 :label: -> 1
            :label3:                                     #
                    mov16 r0, :label3:                   #5 :label3: -> 5
                    mov16 r0, :label4:                   #6 :label4: -> 7
            :label4:                                     #
                    mov8 r0, u8{:label3:}                #7 :label3: -> 5
                    mov8 r0, u16{:label3:}               #8 :label3: -> 5   ? this ok.
                                                         #          it's ok on a 32 bit cpu, though it's useless as
                                                         #          it won't stop truncation..
                    jump :label0:                        #9 :label: -> 2
            :TEST:                                       #
            :another-test:                               #
                    iadd16 r1, :another-TEST:, r0        #10 :another-test: -> 10 //case insensitive
                    iadd16 r1, u8{:another-test:}, r0    #11 :another-test: -> 10
                    iadd32 r1, :another-test:, r0        #12 :another-test: -> 10
                    iadd32 r1, u8{:another-test:}, r0    #13 :another-test: -> 10
                    iadd32 r1, u16{:another-test:}, r0   #14 :another-test: -> 10
                    iadd32 r1, u32{:another-test:}, r0   #15 :another-test: -> 10
                    iadd   r1, :another-test:, r0        #16 :another-test: -> 10
            ";
            #endregion
            #region program
            //ideally the parser would return a Program, containing a symbol table.
            var prog = new Instruction[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandLiteral.undef(0x1),
                                8), 
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(1), OperandLiteral.undef(0x2),
                                8), 
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(2), OperandLiteral.undef(0x3),
                                8), 
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(3), OperandLiteral.undef(0x4),
                                8), 

                //mov16 r0, :label:                    #4 :label: -> 1
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandLiteral.undef(0x1),
                                16), 
                //mov16 r0, :label3:                   #5 :label3: -> 5
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandLiteral.undef(0x5),
                                16), 
                //mov16 r0, :label4:                   #6 :label4: -> 7
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandLiteral.undef(0x7),
                                16), 
                //mov8 r0, u8{:label3:}                #7 :label3: -> 5
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandLiteral.u8(0x5),
                                8),
                //mov8 r0, u16{:label3:}               #8 :label3: -> 5 
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandLiteral.u16(0x5),
                                8),
                //jump :label0:                        #9 :label: -> 2
                new Instruction(OpcodeType.jump,
                                OperandLiteral.undef(2),
                                0),

                //iadd16 r1, :another-test:, r0        #10 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.undef(10) ,OperandReg.Register(0), 
                                16),
                //iadd16 r1, u8{:another-test:}, r0    #11 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.u8(10) ,OperandReg.Register(0), 
                                16),
                //iadd32 r1, :another-test:, r0        #12 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.undef(10) ,OperandReg.Register(0), 
                                32),
                //iadd32 r1, u8{:another-test:}, r0    #13 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.u8(10) ,OperandReg.Register(0), 
                                32),
                //iadd32 r1, u16{:another-test:}, r0   #14 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.u16(10) ,OperandReg.Register(0), 
                                32),
                //iadd32 r1, u32{:another-test:}, r0   #15 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.u32(10) ,OperandReg.Register(0), 
                                32),
                //iadd   r1, :another-test:, r0        #16 :another-test: -> 10
                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(1), OperandLiteral.undef(10) ,OperandReg.Register(0), 
                                0),
            };
            #endregion

            AssertAssemblyEqualsProgram(asm, prog);
            //iadd8 is too small for an implicit label
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label:
                                                                                iadd8 r1, r0, :label:"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label:
                                                                                iadd8 r1, :label:, r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":labelX:
                                                                                iadd32 r1, :labelY:, r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label:
                                                                           :LABEL:
                                                                                iadd32 r1, r0, r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label:
                                                                                mov r1, r0
                                                                           :label:
                                                                                mov r2, r1
                                                                                halt
                                                                          "));
            //label can't be dest
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label:
                                                                            mov32 :label:, r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label: :label1:
                                                                                iadd32 r1, r0, r1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label: :label1: iadd8 r1, r0, :label:"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"iadd8 :label: r1, r0, :label:"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov32 :label:, r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@":label: mov32 r0, i16{:label:)"));


            //todo test using labels in 8bit operations without cast
            //todo test using labels in untyped operations on an 8bt cpu without cast. e.g. iadd r0,r0, :label:

        }

        private static void ParserTestPC()
        {
            #region asm-string
            string asm = @"
                    assert_eq  pc,  0x0
                    assert_eq  pc,  0x1
                    assert_eq  0x2, pc
                    assert_eq  PC,  0x3

                    iadd8      r0,  8,   PC
                    assert_eq  r0,  12

                    mov        r0,  PC
                    assert_eq  r0,  6
            ";
            #endregion
            #region program
            //ideally the parser would return a Program, containing a symbol table.
            var prog = new Instruction[]
            {
                new Instruction(OpcodeType.assert_eq,
                                OperandReg.ProgramCounter(), OperandLiteral.undef(0x0)), 
                new Instruction(OpcodeType.assert_eq,
                                OperandReg.ProgramCounter(), OperandLiteral.undef(0x1)), 
                new Instruction(OpcodeType.assert_eq,
                                OperandLiteral.undef(0x2), OperandReg.ProgramCounter()), 
                new Instruction(OpcodeType.assert_eq,
                                OperandReg.ProgramCounter(), OperandLiteral.undef(0x3)), 

                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandLiteral.undef(8), OperandReg.ProgramCounter(),
                                8),
                new Instruction(OpcodeType.assert_eq,
                                OperandReg.Register(0), OperandLiteral.undef(12)),                 

                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0), OperandReg.ProgramCounter()),
                new Instruction(OpcodeType.assert_eq,
                                OperandReg.Register(0), OperandLiteral.undef(6)),   
            };
            #endregion

            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"iadd8  pc, 0x0, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"iadd16 pc, 0x0, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, u16{pc}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, @(PC)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov @(PC), r0"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, -PC"));
        }

        private static void ParserTestLR()
        {
            //todo more tests?
            #region asm-string
            string asm = @"
                    mov         r0,  lr
                    iadd8   lr, 0x0, 0x1   # ??? 8bit lr access? allowed?
                    iadd16  lr, 0x0, 0x1

            ";

            #endregion
            #region program

            var prog = new Instruction[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.LinkRegister()),
                new Instruction(OpcodeType.iadd,
                                OperandReg.LinkRegister(),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 8),
                new Instruction(OpcodeType.iadd,
                                OperandReg.LinkRegister(),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 16),
            };
            #endregion

            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, u16{lr}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, i8{lr}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, @(lr)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, -lr"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov @(lr), r0"));

        }

        private static void ParserTestSP()
        {
            //todo more tests?
            #region asm-string
            string asm = @"
                    mov        r0,    sp
                    mov        r1,    @(sp)
                    iadd8      sp,    0x0,   0x1
                    iadd16     sp,    0x0,   0x1
                    iadd32     @(sp), 0x0,   0x1
                    iadd       r0,    @(sp), 0x4
            ";
            #endregion
            #region program

            var prog = new Instruction[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.StackPointer()),

                new Instruction(OpcodeType.mov,
                                OperandReg.Register(1),
                                OperandReg.StackPointer(deref: true)),

                new Instruction(OpcodeType.iadd,
                                OperandReg.StackPointer(),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 8),

                new Instruction(OpcodeType.iadd,
                                OperandReg.StackPointer(),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 16),

                new Instruction(OpcodeType.iadd,
                                OperandReg.StackPointer(deref: true),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 32),

                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.StackPointer(deref: true),
                                OperandLiteral.undef(4)),
            };
            #endregion

            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, u16{sp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, -sp"));
        }

        private static void ParserTestFP()
        {
            //todo more tests?
            #region asm-string
            string asm = @"
                    mov        r0,    fp
                    mov        r1,    @(fp)
                    iadd8      fp,    0x0,   0x1
                    iadd16     fp,    0x0,   0x1
                    iadd32     @(fp), 0x0,   0x1
                    iadd       r0,    @(fp), 0x4
            ";
            #endregion
            #region program

            var prog = new Instruction[]
            {
                new Instruction(OpcodeType.mov,
                                OperandReg.Register(0),
                                OperandReg.FramePointer()),

                new Instruction(OpcodeType.mov,
                                OperandReg.Register(1),
                                OperandReg.FramePointer(deref: true)),

                new Instruction(OpcodeType.iadd,
                                OperandReg.FramePointer(),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 8),

                new Instruction(OpcodeType.iadd,
                                OperandReg.FramePointer(),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 16),

                new Instruction(OpcodeType.iadd,
                                OperandReg.FramePointer(deref: true),
                                OperandLiteral.undef(0),
                                OperandLiteral.undef(1),
                                bitWidth: 32),

                new Instruction(OpcodeType.iadd,
                                OperandReg.Register(0),
                                OperandReg.FramePointer(deref: true),
                                OperandLiteral.undef(4)),
            };
            #endregion

            AssertAssemblyEqualsProgram(asm, prog);

            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, u16{FP}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov r0, -FP"));
        }

        private static void ParserTestCSP()
        {
            //todo more tests?

            // User can't use CSP
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov     r0,  csp"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"iadd8   csp, 0x0, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"iadd16  csp, 0x0, 0x1"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov     r0,  u16{csp}"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov     r0,  @(csp)"));
            AssertThrows<PFCAsmInputError>(() => PFCParser.ParseAssembly(@"mov     r0,  -csp"));
        }

        public static bool RunAllParserTests()
        {
            ParserTest0();
            ParserTest1();
            ParserTest2();
            ParserTest3();
            ParserTest4();
            ParserTest5();
            ParserTest6();
            ParserTestBitWidth();
            ParserTestCommas();
            ParserTestDodgySymbols();
            ParserTestNop();
            ParserTestOpcodePosition();
            ParserTestOperandCount();
            ParserTestValidOpcode();
            ParserTestWhitespace();
            ParserTestTypeCasts();
            ParserTestDeref();

            ParserTestLabel();
            ParserTestPC();
            ParserTestLR();
            ParserTestSP();
            ParserTestFP();
            ParserTestCSP();




            //ILLEGAL ld32 r0, @(i8{r1} + i16{r[r8 + 4]} * 4)
            //ld32 r0, i8{@(r1 + r[r8 + 4] * 4)}
            //ld32 r0, i8 { @(r1 + r[r8 + 4] * 4) }
            //iadd32 r0, r0, i8 { r1.2 }
            //printLexedStuff("i8{r0}, i8{r0}, i8{r1}");

            //todo base index scale
            //todo dynamic register
            //Debug.Assert(false);
            //todo proper - + handling for non-literals.

            return true;
        }

    }
}

