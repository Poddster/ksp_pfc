                 :test:
                        mov        SP,  0x8000            #0
                        push_data  0xDEADBEEF             #1
                        push_data  0x0F0F0F0F             #2
                        call       :func:                 #3 
                        assert_eq  r0,   0xedbccdfe       #4
                        iadd       sp,   sp,  0x8         #5

                        push_data  0x00000001             #6
                        push_data  0x00000002             #7
                        call       :func:                 #8 
                        assert_eq  r0,   0x00000003       #9 
                        
                        #add because default is a full descending stack
                        iadd       sp,   sp,  0x8         #10


                        assert_eq  SP,   0x8000           #11

                        halt                              #12

                 :func:
                        #add because default is a full descending stack
                        ld32       r0,    SP
                        iadd       r5,    sp,   0x4       
                        ld32       r1,    r5
                        iadd32     r0,    r0,   r1
                        ret   