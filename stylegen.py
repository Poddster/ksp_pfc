import random

opcodes = [
            ("iadd32", 3),
            ("iadd", 3),
            ("mov", 2),
            ("mov32", 2),
            ("b", "L"),
            ("jmp", "L"),
            ("br", "L"),
            ("iadd.8", 3),
            ("flags_read", 1),
            ("iadd8", 3),
            ("halt", 0),
    ]

condcodes = [
            "al", #always
            "nv", #never
            "true",   #nz?
            "false",  #z?
            "z",
            "nz",
            "v",
            "nv",
            "c",
            "nc",
            "neg",
            "pos",
            "eq",
            "neq",
            "ugt",
            "ule",
            "uge",
            "ult",
            "sgt",
            "sle",
            "sge",
            "slt",
            ]

options = [
            "{op}.{cond}",
            "{op}.{cond}?",
            #"{op}{cond}?",
            #"{cond}? {op}",
            #"{op} if {cond}?",
            #"if {cond}? {op}",
            #"if ({cond}?) {op}",
            #"{op}{cond}",
            #"{op} {cond}",
            #"{cond}.{op}",
            #"{op} {cond}?",
            #".{cond}.{op}",
            #".{cond}{op}",
            #".{cond} {op}",
            #"{op}?{cond}",
            #"{op} ?{cond}",
            #"{op} ?{cond}?",
            #"?{cond} {op}",
            #"{cond}?{op}",
            #"?{cond}{op}",
            #"{cond}?.{op}",
            #"?{cond}.{op}",
            #"cc:?{cond} {op}",
            #"cc:{cond}? {op}",
            ]

options2 = [
            "{op}.{cond}!",
            "{op}!.{cond}",
            "{op}.{cond}?!",
            "{op}!.{cond}?",
            "{op}!{cond}?",
        ]

options3 = [
            "{op}.{cond}!<ncv>",
            "{op}.{cond}?!<ncv>",
            "{op}!<ncv>.{cond}?",
            "{op}!<ncv>{cond}?",
        ]

options4 = [
            "{op}.{cond}!ncv",
            "{op}.{cond}?!ncv",
            "{op}!ncv.{cond}?",
            "{op}!ncv{cond}?",
        ]

options5 = [
            "{op}.{cond}!(ncv)",
            "{op}.{cond}?!(ncv)",
            "{op}!(ncv).{cond}?",
            "{op}!(ncv){cond}?",
        ]

options6 = [
            "{op}.{cond}.ncv!",
            "{op}.{cond}?.ncv!",
            "{op}.ncv!.{cond}?",
            "{op}.ncv!{cond}?",
        ]

options7 = [
            "{op}.{cond}.!ncv",
            "{op}.{cond}?.!ncv",
            "{op}.?{cond}.!ncv",
            "{op}.!ncv.{cond}?",
            "{op}.!ncv.?{cond}",
            "{op}.ncv!.{cond}?",
            "{op}.ncv!.?{cond}",
        ]

options8 = [
            "{op}.{cond}.!ncv",
            "{op}.{cond}?.!ncv",
            "{op}.?{cond}.!ncv",
            "{op}!ncv.{cond}?",
            "{op}!ncv.?{cond}",
            "{op}.ncv!.{cond}?",
            "{op}.ncv!.?{cond}",
        ]

options9 = [
            "{op}!v.{cond}",
            "{op}!v.{cond}?",
            "{op}!v.?{cond}",
        ]

options10 = [
            #"{op}!.{cond}",
            #"{op}!.{cond}?",
            #"{op}.!.{cond}",
            #"{op}.!.{cond}?",
            "{op}!nv.{cond}",
            "{op}!nv.{cond}?",
            #"{op}.!nv.{cond}",
            #"{op}.!nv.{cond}?",
            #"{op}.nv!.{cond}",
            #"{op}.nv!.{cond}?",
        ]

options = options10

rand = random.Random()
#rand.seed(21)

full_collection = []
cond = condcodes[rand.randrange(len(condcodes))]

for (op, optype) in opcodes:
    if type(optype) is int:
        if optype == 0:
            opers = ""
        elif optype == 1:
            opers = "r0"
        elif optype == 2:
            opers = "r0, r1"
        elif optype == 3:
            opers = "r0, r1, 0x1"
        else:
            raise "wrong optype:", optype
    else:
        assert type(optype) == str
        if optype == "L":
            opers = ":loop-start:"
        else:
            raise "wrong optype:", optype

    op_collection = []
    for (i, opt) in enumerate(options):
        #cond = condcodes[rand.randrange(len(condcodes))]
        opcode_str = str.format(opt, op=op, cond=cond)
        out_str = str.format("{2:3}:   :label: {0: <13}  {1}", opcode_str, opers, i)
        op_collection.append(out_str)
        print out_str

    full_collection.append(op_collection)
    print

print

#for x in zip(l_collection):
#    for op in x:
#        print op
#    print