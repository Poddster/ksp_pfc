﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PFC.CPU
{
    using JetBrains.Annotations;

    /// <summary>
    /// To allow multi-core, super-scalar, threading (as in c#, not PFC) etc
    /// in the future, then all instructions execute and produce some results
    /// that the CPU then applies in a write back phase.
    /// </summary>
    class CPURegMemStateChange
    {
        [NotNull]
        readonly public SimpleRegister Reg;
        readonly public ulong Value;
        readonly public DataType DataType;

        public override string ToString()
        {
            // using {3} and {4} beacuse 0x{1:X}}} prints "X" instead of
            // a number formatted by X
            // http://blogs.msdn.com/b/brada/archive/2003/12/26/50978.aspx
            return string.Format("{2}{3}{0} = 0x{1:X}{4}",
                this.Reg,
                this.Value,
                this.DataType,
                "{", "}");
        }

        //should probably make this private and just use factories?
        public CPURegMemStateChange(DataType dataType, [NotNull] SimpleRegister register, ulong value)
        {
            if (register == null)
            {
                throw new ArgumentNullException("register");
            }
            ulong mask = dataType.GetDataTypeMask();
            ulong maskedValue = (mask & value);

            // Ensures that a masked version of 'value' still represents the value when it's
            // considered as type 'dataType'
            if ((mask & value) != value)
            {
                string err = string.Format("value `{0:X}` too big for data type `{1}`",
                                            value, dataType);
                throw new ArgumentException(err, "value");
            }

            this.DataType = dataType;
            this.Reg = register;
            this.Value = maskedValue;
        }

        /// <summary>
        /// Create a 'Change' object to alter any 0d register (e.g. PC, SP)
        /// </summary>
        /// <param name="value">New register value</param>
        /// <param name="regType"></param>
        /// <returns>Change that can be applied to a CPUState</returns>
        public static CPURegMemStateChange RegChange0D(RegisterType regType, ulong value)
        {
            //DataType.u16.ThrowIfInvalidAlignment(value);
            return new CPURegMemStateChange(DataType.u16,
                                            SimpleRegister.Reg0D(regType),
                                            value);
        }

        public static CPURegMemStateChange RegChange(uint index, ulong value)
        {
            return new CPURegMemStateChange(DataType.undef,
                                      SimpleRegister.Reg1D(RegisterType.scalar_reg, index: index),
                                      value);
        }

        /// <summary>
        /// Create a change object to alter a value on the control stack
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="address"></param>
        /// <param name="value"></param>
        /// <returns>Change that can be applied to a CPUState</returns>
        public static CPURegMemStateChange CtrlStackChange(DataType dataType, uint address, ulong value)
        {
            Debug.Assert(dataType == DataType.undef);
            return new CPURegMemStateChange(dataType,
                                            SimpleRegister.Reg1D(RegisterType.control_stack_val,
                                                                 address),
                                            value);
        }



        // todo: address should be uint16?
        public static CPURegMemStateChange MemChange(DataType dataType, uint address, ulong value)
        {
            int alignment = dataType.GetAddressAlignment();
            if ((address % alignment) != 0)
            {
                string err = string.Format("for a {0} bit write address must be a multiple of {1}. Address given {2}",
                                            dataType.GetBitWidth(0), alignment, address);
                throw new ArgumentException(err, "address");
            }

            return new CPURegMemStateChange(dataType,
                                      SimpleRegister.Reg1D(RegisterType.memory, index:address),
                                      value);
        }

        public static CPURegMemStateChange MemChangeUnaligned(DataType dataType, uint address, ulong value)
        {
            return new CPURegMemStateChange(dataType,
                                      SimpleRegister.Reg1D(RegisterType.memory, index: address),
                                      value);
        }

        public static CPURegMemStateChange MemChange8(uint address, byte value)
        {
            return new CPURegMemStateChange(DataType.u8,
                                      SimpleRegister.Reg1D(RegisterType.memory, index: address),
                                      value);
        }

        public static CPURegMemStateChange MemChange16(uint address, ushort value)
        {
            if ((address % 2) != 0)
            {
                string err = String.Format("unaligned 16bit address `{0}`, must be multiple of 2",
                                            address.ToHex());
                throw new ArgumentException(err, "address");
            }
            return new CPURegMemStateChange(DataType.u16,
                                      SimpleRegister.Reg1D(RegisterType.memory, index: address),
                                      value);
        }

        public static CPURegMemStateChange MemChange32(uint address, ulong value)
        {
            if ((address % 4) != 0)
            {
                string err = String.Format("unaligned 32bit address `{0}`, must be multiple of 4",
                                            address.ToHex());
                throw new ArgumentException(err, "address");
            }
            return new CPURegMemStateChange(DataType.u32,
                                      SimpleRegister.Reg1D(RegisterType.memory, index:address),
                                      value);
        }

    }
}

