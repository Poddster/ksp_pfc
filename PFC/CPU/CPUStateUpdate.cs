using System.Collections.Generic;
namespace PFC.CPU
{
    using System;
    using System.Collections.Specialized;

    using JetBrains.Annotations;

    //We could also implement this by:
    // interface ICPURegMemStateChange
    // class CPUStateUpdate: ICPURegMemStateChange
    // class CPURegMemStateChange : ICPURegMemStateChange
    //
    // Execute/Decode returns a list of ICPURegMemStateChange.
    //
    // and then doing a dispatch-on-type in ApplyStateChange, or 
    // having all of the functions that change state be named the same thing and doing
    // ApplyStateChangeImpl((dynamic)change); ? slow on mono?
    //
    // but then how would we supress the auto PC increment? We'd need to check the list of 
    // state changes to find one that updated the PC, which doesn't sound to speedy.

    [Flags]
    public enum CPUFlags : uint
    {
        None = 0x0,
        N = 8,
        Z = 4,
        V = 2,
        C = 1,

        NZ = N | Z, //instructions will either do NZVC, NZ or None
        All = N | Z | V | C,
    }


    internal class CPUStateUpdate
    {
        //todo: should these just be InstructionFlags?

        /// <summary>
        /// Put the CPU into halt mode.
        /// </summary>
        public bool Halt;

        /// <summary>
        /// Supress the automatic PC++
        /// 
        /// An instruction would set this if it altered the PC
        /// </summary>
        public bool SupressPCIncrement;

        public uint CycleCost;

        [NotNull]
        public IList<CPURegMemStateChange> RegMemChanges;

        public bool UpdateFlags;
        //BitVector32
        public CPUFlags Flags;

        public CPUStateUpdate()
        {
            this.Halt = false;
            this.SupressPCIncrement = false;
            this.CycleCost = 0;
            this.RegMemChanges = new List<CPURegMemStateChange>(2);
            this.Flags = CPUFlags.None;
            this.UpdateFlags = false;
        }

        public override string ToString()
        {
            string changes = string.Join(", ", this.RegMemChanges);
            return string.Format(
                "StateUpdate:|[{0}] Cost:{1} Flags({2}):{3} AutoPC:{4} Halt:{5}|",
                changes,
                this.CycleCost,
                this.UpdateFlags ? "change" : "No change",
                this.Flags,
                !this.SupressPCIncrement,
                this.Halt);
        }
    }
}