		######
		iadd32!     r0, 0XFFFFFFFF, 0XFFFFFFFF          #  0          -1 +          -1 =          -2 | n  c
		assert_eq   r0, 0XFFFFFFFE                      #  1
		flags_read  r1                                  #  2
		assert_eq   r1, 0b1001                          #  3

		iadd32!     r0, 0XFFFFFFFF, 0X00000000          #  4          -1 +          +0 =          -1 | n   
		assert_eq   r0, 0XFFFFFFFF                      #  5
		flags_read  r1                                  #  6
		assert_eq   r1, 0b1000                          #  7

		iadd32!     r0, 0XFFFFFFFF, 0X00000001          #  8          -1 +          +1 =          +0 |  z c
		assert_eq   r0, 0X00000000                      #  9
		flags_read  r1                                  # 10
		assert_eq   r1, 0b0101                          # 11

		iadd32!     r0, 0XFFFFFFFF, 0X7FFFFFFF          # 12          -1 + +2147483647 = +2147483646 |    c
		assert_eq   r0, 0X7FFFFFFE                      # 13
		flags_read  r1                                  # 14
		assert_eq   r1, 0b0001                          # 15

		iadd32!     r0, 0XFFFFFFFF, 0X80000000          # 16          -1 + -2147483648 = +2147483647 |   vc
		assert_eq   r0, 0X7FFFFFFF                      # 17
		flags_read  r1                                  # 18
		assert_eq   r1, 0b0011                          # 19

		iadd32!     r0, 0XFFFFFFFF, 0X80000001          # 20          -1 + -2147483647 = -2147483648 | n  c
		assert_eq   r0, 0X80000000                      # 21
		flags_read  r1                                  # 22
		assert_eq   r1, 0b1001                          # 23

		######
		iadd32!     r0, 0X00000000, 0XFFFFFFFF          # 24          +0 +          -1 =          -1 | n   
		assert_eq   r0, 0XFFFFFFFF                      # 25
		flags_read  r1                                  # 26
		assert_eq   r1, 0b1000                          # 27

		iadd32!     r0, 0X00000000, 0X00000000          # 28          +0 +          +0 =          +0 |  z  
		assert_eq   r0, 0X00000000                      # 29
		flags_read  r1                                  # 30
		assert_eq   r1, 0b0100                          # 31

		iadd32!     r0, 0X00000000, 0X00000001          # 32          +0 +          +1 =          +1 |     
		assert_eq   r0, 0X00000001                      # 33
		flags_read  r1                                  # 34
		assert_eq   r1, 0b0000                          # 35

		iadd32!     r0, 0X00000000, 0X7FFFFFFF          # 36          +0 + +2147483647 = +2147483647 |     
		assert_eq   r0, 0X7FFFFFFF                      # 37
		flags_read  r1                                  # 38
		assert_eq   r1, 0b0000                          # 39

		iadd32!     r0, 0X00000000, 0X80000000          # 40          +0 + -2147483648 = -2147483648 | n   
		assert_eq   r0, 0X80000000                      # 41
		flags_read  r1                                  # 42
		assert_eq   r1, 0b1000                          # 43

		iadd32!     r0, 0X00000000, 0X80000001          # 44          +0 + -2147483647 = -2147483647 | n   
		assert_eq   r0, 0X80000001                      # 45
		flags_read  r1                                  # 46
		assert_eq   r1, 0b1000                          # 47

		######
		iadd32!     r0, 0X00000001, 0XFFFFFFFF          # 48          +1 +          -1 =          +0 |  z c
		assert_eq   r0, 0X00000000                      # 49
		flags_read  r1                                  # 50
		assert_eq   r1, 0b0101                          # 51

		iadd32!     r0, 0X00000001, 0X00000000          # 52          +1 +          +0 =          +1 |     
		assert_eq   r0, 0X00000001                      # 53
		flags_read  r1                                  # 54
		assert_eq   r1, 0b0000                          # 55

		iadd32!     r0, 0X00000001, 0X00000001          # 56          +1 +          +1 =          +2 |     
		assert_eq   r0, 0X00000002                      # 57
		flags_read  r1                                  # 58
		assert_eq   r1, 0b0000                          # 59

		iadd32!     r0, 0X00000001, 0X7FFFFFFF          # 60          +1 + +2147483647 = -2147483648 | n v 
		assert_eq   r0, 0X80000000                      # 61
		flags_read  r1                                  # 62
		assert_eq   r1, 0b1010                          # 63

		iadd32!     r0, 0X00000001, 0X80000000          # 64          +1 + -2147483648 = -2147483647 | n   
		assert_eq   r0, 0X80000001                      # 65
		flags_read  r1                                  # 66
		assert_eq   r1, 0b1000                          # 67

		iadd32!     r0, 0X00000001, 0X80000001          # 68          +1 + -2147483647 = -2147483646 | n   
		assert_eq   r0, 0X80000002                      # 69
		flags_read  r1                                  # 70
		assert_eq   r1, 0b1000                          # 71

		######
		iadd32!     r0, 0X7FFFFFFF, 0XFFFFFFFF          # 72 +2147483647 +          -1 = +2147483646 |    c
		assert_eq   r0, 0X7FFFFFFE                      # 73
		flags_read  r1                                  # 74
		assert_eq   r1, 0b0001                          # 75

		iadd32!     r0, 0X7FFFFFFF, 0X00000000          # 76 +2147483647 +          +0 = +2147483647 |     
		assert_eq   r0, 0X7FFFFFFF                      # 77
		flags_read  r1                                  # 78
		assert_eq   r1, 0b0000                          # 79

		iadd32!     r0, 0X7FFFFFFF, 0X00000001          # 80 +2147483647 +          +1 = -2147483648 | n v 
		assert_eq   r0, 0X80000000                      # 81
		flags_read  r1                                  # 82
		assert_eq   r1, 0b1010                          # 83

		iadd32!     r0, 0X7FFFFFFF, 0X7FFFFFFF          # 84 +2147483647 + +2147483647 =          -2 | n v 
		assert_eq   r0, 0XFFFFFFFE                      # 85
		flags_read  r1                                  # 86
		assert_eq   r1, 0b1010                          # 87

		iadd32!     r0, 0X7FFFFFFF, 0X80000000          # 88 +2147483647 + -2147483648 =          -1 | n   
		assert_eq   r0, 0XFFFFFFFF                      # 89
		flags_read  r1                                  # 90
		assert_eq   r1, 0b1000                          # 91

		iadd32!     r0, 0X7FFFFFFF, 0X80000001          # 92 +2147483647 + -2147483647 =          +0 |  z c
		assert_eq   r0, 0X00000000                      # 93
		flags_read  r1                                  # 94
		assert_eq   r1, 0b0101                          # 95

		######
		iadd32!     r0, 0X80000000, 0XFFFFFFFF          # 96 -2147483648 +          -1 = +2147483647 |   vc
		assert_eq   r0, 0X7FFFFFFF                      # 97
		flags_read  r1                                  # 98
		assert_eq   r1, 0b0011                          # 99

		iadd32!     r0, 0X80000000, 0X00000000          #100 -2147483648 +          +0 = -2147483648 | n   
		assert_eq   r0, 0X80000000                      #101
		flags_read  r1                                  #102
		assert_eq   r1, 0b1000                          #103

		iadd32!     r0, 0X80000000, 0X00000001          #104 -2147483648 +          +1 = -2147483647 | n   
		assert_eq   r0, 0X80000001                      #105
		flags_read  r1                                  #106
		assert_eq   r1, 0b1000                          #107

		iadd32!     r0, 0X80000000, 0X7FFFFFFF          #108 -2147483648 + +2147483647 =          -1 | n   
		assert_eq   r0, 0XFFFFFFFF                      #109
		flags_read  r1                                  #110
		assert_eq   r1, 0b1000                          #111

		iadd32!     r0, 0X80000000, 0X80000000          #112 -2147483648 + -2147483648 =          +0 |  zvc
		assert_eq   r0, 0X00000000                      #113
		flags_read  r1                                  #114
		assert_eq   r1, 0b0111                          #115

		iadd32!     r0, 0X80000000, 0X80000001          #116 -2147483648 + -2147483647 =          +1 |   vc
		assert_eq   r0, 0X00000001                      #117
		flags_read  r1                                  #118
		assert_eq   r1, 0b0011                          #119

		######
		iadd32!     r0, 0X80000001, 0XFFFFFFFF          #120 -2147483647 +          -1 = -2147483648 | n  c
		assert_eq   r0, 0X80000000                      #121
		flags_read  r1                                  #122
		assert_eq   r1, 0b1001                          #123

		iadd32!     r0, 0X80000001, 0X00000000          #124 -2147483647 +          +0 = -2147483647 | n   
		assert_eq   r0, 0X80000001                      #125
		flags_read  r1                                  #126
		assert_eq   r1, 0b1000                          #127

		iadd32!     r0, 0X80000001, 0X00000001          #128 -2147483647 +          +1 = -2147483646 | n   
		assert_eq   r0, 0X80000002                      #129
		flags_read  r1                                  #130
		assert_eq   r1, 0b1000                          #131

		iadd32!     r0, 0X80000001, 0X7FFFFFFF          #132 -2147483647 + +2147483647 =          +0 |  z c
		assert_eq   r0, 0X00000000                      #133
		flags_read  r1                                  #134
		assert_eq   r1, 0b0101                          #135

		iadd32!     r0, 0X80000001, 0X80000000          #136 -2147483647 + -2147483648 =          +1 |   vc
		assert_eq   r0, 0X00000001                      #137
		flags_read  r1                                  #138
		assert_eq   r1, 0b0011                          #139

		iadd32!     r0, 0X80000001, 0X80000001          #140 -2147483647 + -2147483647 =          +2 |   vc
		assert_eq   r0, 0X00000002                      #141
		flags_read  r1                                  #142
		assert_eq   r1, 0b0011                          #143

		halt
