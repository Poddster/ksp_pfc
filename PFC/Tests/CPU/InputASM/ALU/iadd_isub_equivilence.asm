
		mov         r0,    0x1234
		mov         r1,    0x0233

		isub        r2,    r0,     r1
		flags_read  r3

		not         r1,    r1
		iadd        r1,    r1,     0x1
		iadd        r4,    r0,     r1
		flags_read  r5

		assert_eq   r2,    r4
		assert_eq   r3,    r5
		halt
