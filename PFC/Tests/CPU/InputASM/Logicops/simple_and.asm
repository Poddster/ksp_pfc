                and         r0,     0xFFFFFFFF, 0x00000000
                assert_eq   r0,     0x00000000
                and         r0,     0xAAAAAAAA, 0x55555555
                assert_eq   r0,     0x00000000

                and32       r0,     0x0F0F0F0F, 0x55555555
                assert_eq   r0,     0x05050505
                and32       r0,     0xFFFFFFFF, 0x55555555
                assert_eq   r0,     0x55555555

                and8        r1,     0x13, 0xED
                assert_eq   r1,     0x01
                and8        r1,     0x12, 0x80
                assert_eq   r1,     0x00

                mov32       r1,     0x76543210
                mov32       r2,     0xFEDBCA98
                and32       r3,     r2,         r1
                and32       r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0x76500210

                mov32       r3,     0x00000000
                mov32       r4,     0x00000000
                and16       r3,     r2,         r1
                and16       r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0x0210

                halt
