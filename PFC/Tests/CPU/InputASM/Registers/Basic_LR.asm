             :test:
                    mov       r0,   0xDEADBEEF       #0
                    mov       r1,   0x0F0F0F0F       #1
                    iadd      LR,   PC,   2          #2
                    jump      :func:                 #3 
                    assert_eq  r0,  0xedbccdfe       #4

                    mov       r0,   0x00000001       #5
                    mov       r1,   0x00000002       #6
                    iadd      LR,   PC,   2          #7 
                    jump      :func:                 #8 
                    assert_eq  r0,  0x00000003       #9 

                    halt                             #10

             :func:
                    iadd32     r0,  r0,  r1          #11
                    jump       LR                    #12