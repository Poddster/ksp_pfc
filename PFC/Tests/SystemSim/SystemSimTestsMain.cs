﻿namespace PFC.Tests.SystemSim
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Management.Instrumentation;

    using JetBrains.Annotations;

    public static class SystemSimTestsMain
    {

        public enum PortIODir
        {
            Input,
            Output,
        }

        public enum PortIOType
        {
            Input,
            Output,
            InputOutput, //implicitly tri-state?
        }

        public class ComponentPort
        {
            //todo: a port is owned by one component (that reads input and makes output) and connected to one or more components/busses?

            public readonly uint NumPins;

            [NotNull]
            public readonly string Name;

            public readonly PortIOType Type;

            public readonly PortIODir Direction;

            public readonly bool Enabled;

            public ComponentPort(
                uint numPins,
                [NotNull] string name,
                PortIOType portIOType,
                PortIODir portIODir,
                bool enabled)
            {
                this.NumPins = numPins;
                this.Name = name;
                this.Enabled = enabled;
                this.Type = portIOType;
                this.Direction = portIODir;
            }

        }

        public class StubCPUComponent
        {
            // todo: do we care about the number of pins? or just 'ports'?
            /* todo: this is nothing like a real GPIO.
             *      no latching
             *      no masking/enable for individual pins
             * however it's meant to capture the idea of what the individual parts of a GPIO are used for
             * but would we want individual pin on/off?
             */

            private static readonly Dictionary<string, ComponentPort> ports;

            static StubCPUComponent()
            {
                ComponentPort[] portsList = 
                { 
                    new ComponentPort(8, "Address", PortIOType.Output, PortIODir.Output, false),
                    new ComponentPort(8, "Data", PortIOType.InputOutput, PortIODir.Input, false),
                    new ComponentPort(8, "Inst/!Data", PortIOType.InputOutput, PortIODir.Input, false),
                };

                StubCPUComponent.ports = portsList.ToDictionary(
                port =>
                {
                    Debug.Assert(port != null);
                    return port.Name.ToLower();
                });
            }

            [NotNull]
            public ComponentPort this[[NotNull] string name]
            {
                get
                {
                    Debug.Assert(StubCPUComponent.ports != null);
                    return StubCPUComponent.ports[name.ToLower()];
                }
            }
        }


        public static bool RunAllSystemSimTests()
        {
            /*
             *  make a stub CPU component
             *      it just outputs a different addresses (from a set) every time it's clocked?
             *      alternating between instruction and data
             *  make a bus component?
             *  make RAM
             *  make ROM
             *  make a crystal/clock
             *  make arbiter/memory address range decoder 
             *  make a pull up?
             *  link them all togther
             *  run
             *  ???
             *  profit
             * */

            //todo: how does the port work re: it's "current" value and the things it is connected to?

            var stubCPU = new StubCPUComponent();

            // todo: is the indexer a useful thing to do? only useful for latched GPIO?
            // todo: should the indexer return the port, or simply the value of the port?
            Debug.Assert(stubCPU["Address"].Name == "Address");

            return true;
        }
    }
}
