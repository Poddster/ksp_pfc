﻿namespace PFC.CPU
{
    using System;
    using System.Diagnostics;

    using JetBrains.Annotations;

    using PFC.CPU;

    public class StackType
    {
        public enum StackTypeDir
        {
            /// <summary>
            /// Stack starts at a high address, grows downwards, shrinks to higher addresses
            /// </summary>
            Descending,

            /// <summary>
            /// Stack starts at a low address, grows upwards, shrinks to lower addresses
            /// </summary>
            Ascending,
        }

        public enum StackTypeMode
        {
            /// <summary>
            /// Stack pointer points to the last valid data on the stack
            /// </summary>
            Full,

            /// <summary>
            /// Stack pointer points to the first free space on the stack
            /// </summary>
            Empty,
        }

        public readonly StackTypeDir Dir;

        public readonly StackTypeMode Mode;

        public StackType(StackTypeMode mode, StackTypeDir dir)
        {
            this.Dir = dir;
            this.Mode = mode;
        }
    }


    class CPU_VM
    {
        public const ulong TrueBits = 0xFFFFFFFFFFFFFFFF;
        public const ulong FalseBits = 0x0;

        /// <summary>
        /// in Khz?
        /// </summary>
        public readonly uint ClockFreq;
        public readonly uint ISAVersion;

        /// <summary>
        /// In bits.
        /// </summary>
        public readonly uint WordSize;

        /// <summary>
        /// In bits;
        /// </summary>
        public readonly uint AddressBusSize;
        /// <summary>
        ///  in ms
        /// </summary>
        public readonly uint RAMLatency;

        [NotNull]
        public readonly StackType DefaultStackType;

        [NotNull]
        public readonly StackType ControlStackType;

        public bool TraceInst { get; set; }

        [NotNull]
        private CPUState State;

        [NotNull]
        public ICPUStateRO ROState;

        public CPU_VM(uint version, uint wordSizeInBits, Instruction[] program) :
            this(version, wordSizeInBits, new CPUState(version, wordSizeInBits, program))
        {
        }

        public CPU_VM(uint version, uint wordSizeInBits, [NotNull] CPUState state)
        {
            #region arg-check

            if (version != 0)
            {
                throw new NotImplementedException(String.Format("Version {0} not implemented yet", version));
            }
            if (state == null)
            {
                throw new ArgumentNullException("state", "state can't be null");
            }
            if (state.WordSizeInBits != wordSizeInBits)
            {
                string err = String.Format(
                    "state wordsize `{0}` doesn't match CPU wordsize `{1}`",
                    state.WordSizeInBits,
                    wordSizeInBits);
                throw new ArgumentException(err, "wordSizeInBits");
            }

            #endregion

            this.ISAVersion = version;
            this.WordSize = wordSizeInBits;
            this.State = state;

            this.ClockFreq = 1;
            this.AddressBusSize = 16;
            this.RAMLatency = 1;
            this.ROState = this.State;

            this.TraceInst = false;

            this.DefaultStackType = new StackType(StackType.StackTypeMode.Full, StackType.StackTypeDir.Descending);

            //should this be const? Why would it ever change?
            this.ControlStackType = new StackType(StackType.StackTypeMode.Empty, StackType.StackTypeDir.Ascending);
        }

        private void ThrowCPUAssertion([NotNull] Instruction inst, string vals)
        {
            ulong PCVal = this.State.ProgramCounter;
            string err = String.Format("Assert failed. PC = {0}. Inst: {1}. {2}", PCVal, inst, vals);
            throw new PFC_CPUAssertionException(err);
        }

        [NotNull]
        private CPURegMemStateChange GenerateDestStateChange(
                      DataType   instType,
            [NotNull] OperandReg operand,
                      ulong      dstVal)
        {
            var dest0Reg = this.ROState.ReduceRegOperand(operand, instType);
            Debug.Assert(dest0Reg.RegType == RegisterType.scalar_reg ||
                         dest0Reg.RegType == RegisterType.stack_pointer ||
                         dest0Reg.RegType == RegisterType.frame_pointer ||
                         dest0Reg.RegType == RegisterType.link_register);

            var evalType = CPUState.CalculateEvaluationDataType(operand, instType);

            return new CPURegMemStateChange(evalType, dest0Reg, dstVal);
        }

        private void GenerateGenericDestStateChanges(
            [NotNull] CPUStateUpdate  stateUpdate,
            [NotNull] Instruction     inst,
            [NotNull] InstructionInfo info,
            [NotNull] ulong[]         dstVals,
                      CPUFlags        flags)
        {
            var instWidth = inst.GetBitWidth(this.WordSize);
            var instType = instWidth.DataTypeFromWidth();
            Debug.Assert(instType != DataType.undef);

            if (info.NumDests > 0)
            {
                Debug.Assert(info.NumDests == 1);
                var reg = inst.Operands[0] as OperandReg;
                if (reg == null)
                {
                    var message = string.Format("Instruction has non-register dest? : `{0}`", inst);
                    throw new ArgumentException(message, "inst");
                }
                var newState = this.GenerateDestStateChange(instType, reg, dstVals[0]);
                stateUpdate.RegMemChanges.Add(newState);


                /*
                 * Update common flags.
                 */
                if (inst.WritesCPUFlags)
                {
                    CPUFlags flagsModified = info.CpuFlagsModified;
                    if (flagsModified.HasFlag(CPUFlags.NZ))
                    {
                        /* Does it make sense to update N but not Z?
                         * It's usually NZCV, NZ or none
                         */
                        Debug.Assert(flagsModified.HasFlag(CPUFlags.N) &&
                                     flagsModified.HasFlag(CPUFlags.Z));

                        ulong instSignBit = 0x1UL << ((int)instWidth - 1);
                        ulong instValMasked = newState.Value;
                        bool sRes = (instValMasked & instSignBit) != 0x0;

                        if (instValMasked == 0)
                        {
                            flags |= CPUFlags.Z;
                        }
                        if (sRes)
                        {
                            flags |= CPUFlags.N;
                        }
                    }

                    if ((flagsModified & flags) != flags)
                    {
                        string err = String.Format("Instruction has updated flags it shouldn't have." +
                                                       " Inst `{0}`, stateupdate `{1}` allowed `{2}`",
                                                   inst,
                                                   stateUpdate.Flags,
                                                   info.CpuFlagsModified);
                        throw new ArgumentException(err, "flags");
                    }

                    Debug.Assert(stateUpdate.Flags == CPUFlags.None);
                    stateUpdate.Flags = flags | (this.State.Flags & ~flagsModified);
                    stateUpdate.UpdateFlags = true;
                }
            }
        }




        [Pure]
        private static ulong CastValue(ulong srcVal, uint srcWidth,
                                       uint dstWidth, bool signExt)
        {
            //Don't do this check, as e.g. iadd r0, 12, -2  will result in overflow into the top bit.
            //Debug.Assert((srcVal & srcWidth.GetMask()) == srcVal);

            ulong dstMask = dstWidth.GetMask();
            if (signExt && srcWidth < dstWidth)
            {
                ulong srcValExt = srcVal.SignExtendArb(srcWidth);
                return srcValExt & dstMask;
            }
            else
            {
                return srcVal & dstMask;
            }
        }

        [Pure]
        private ulong EvaluateSingleDestValue(ulong rawDstVal, DataType dstType, uint instWidth)
        {
            uint dstWidth = dstType.GetBitWidth(instWidth);

            //cast from 'undef/raw' to 'inst width'
            ulong castInstVal = CastValue(rawDstVal, this.WordSize, instWidth,
                                          signExt: false);

            //cast from inst width to operand type
            ulong castDstVal = CastValue(castInstVal, instWidth, dstWidth,
                                         dstType.NeedsSignExtending());
            return castDstVal;
        }

        [Pure]
        [NotNull]
        private ulong[] EvaluateDestValues(
            [NotNull] Instruction inst,
            [NotNull] ulong[]     rawDstVals)
        {
            var instWidth = inst.GetBitWidth(this.WordSize);
            var dstVals = new ulong[rawDstVals.Length];
            for (var i = 0; i < rawDstVals.Length; i++)
            {
                var operand = inst.Operands[i];
                Debug.Assert(operand != null, "operand != null");
                var dstValTyped = this.EvaluateSingleDestValue(rawDstVals[i], operand.DataType, instWidth);
                dstVals[i] = dstValTyped;
            }
            return dstVals;
        }

        private class GenericInstOutput
        {
            public readonly CPUFlags Flags;
            [NotNull] public readonly ulong[] DstVals;
            
            public GenericInstOutput(CPUFlags flags = CPUFlags.None)
            {
                this.DstVals = new ulong[0];
                this.Flags = flags;
            }

            public GenericInstOutput(ulong d0, CPUFlags flags = CPUFlags.None)
            {
                this.DstVals = new[] { d0 };
                this.Flags = flags;
            }
        }




        [NotNull]
        private delegate GenericInstOutput Execute_generic_func(
            [NotNull] Instruction inst,
            [NotNull] ulong[]     srcVals);

        [Pure]
        [NotNull]
        private GenericInstOutput Execute_generic_is_ieq(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);

            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong result = (a == b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [Pure]
        [NotNull]
        private GenericInstOutput Execute_generic_is_ineq(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);

            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong result = (a != b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [Pure]
        [NotNull]
        private GenericInstOutput Execute_generic_is_iugt(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);

            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong result = (a > b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [Pure]
        [NotNull]
        private GenericInstOutput Execute_generic_is_iule(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);

            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong result = (a <= b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [Pure]
        [NotNull]
        private GenericInstOutput Execute_generic_is_iuge(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);

            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong result = (a >= b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_iult(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);

            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong result = (a < b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_isgt(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);
            
            long a = (long) srcVals[0];
            long b = (long) srcVals[1];
            ulong result = (a > b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_isle(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);
            
            long a = (long) srcVals[0];
            long b = (long) srcVals[1];
            ulong result = (a <= b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_isge(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);
            
            long a = (long) srcVals[0];
            long b = (long) srcVals[1];
            ulong result = (a >= b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_islt(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);
            
            long a = (long) srcVals[0];
            long b = (long) srcVals[1];
            ulong result = (a < b) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_iz(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 1);

            ulong a = srcVals[0];
            ulong result = (a == 0x0) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_inz(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 1);
            ulong a = srcVals[0];
            ulong result = (a != 0x0) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_ineg(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 1);
            
            long a = (long) srcVals[0];
            ulong result = (a < 0x0) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_is_ipos(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 1);
            
            long a = (long) srcVals[0];
            ulong result = (a >= 0x0) ? TrueBits : FalseBits;

            return new GenericInstOutput(result);
        }



        [NotNull]
        private GenericInstOutput Execute_generic_not(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 1);
            ulong result = ~srcVals[0];
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_and(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong result = srcVals[0] & srcVals[1];
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_or(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong result = srcVals[0] | srcVals[1];
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_xor(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong result = srcVals[0] ^ srcVals[1];
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_nand(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong result = ~(srcVals[0] & srcVals[1]);
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_nor(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong result = ~(srcVals[0] | srcVals[1]);
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_xnor(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong result = ~(srcVals[0] ^ srcVals[1]);
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_flags_read(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            uint flags = (uint)this.State.Flags;
            return new GenericInstOutput(flags);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_breakpoint(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 0);
            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
            return new GenericInstOutput();
        }

        [NotNull]
        private GenericInstOutput Execute_generic_nop(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 0);

            // Do nothing
            return new GenericInstOutput();
        }

        [NotNull]
        private GenericInstOutput Execute_generic_umax(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);
            ulong result = Math.Max(srcVals[0], srcVals[1]);
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_umin(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 2);
            ulong result = Math.Min(srcVals[0], srcVals[1]);
            return new GenericInstOutput(result);
        }

        [NotNull]
        private static GenericInstOutput internal_sub(
            [NotNull] ulong[] srcVals,
                      bool    writesCpuFlags,
                      uint    instWidth,
                      ulong   carryIn,
                      bool    calcNZ)
        {
            var flags = CPUFlags.None;

            Debug.Assert(srcVals.Length == 2);
            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong instValRaw = unchecked(a + (~b) + carryIn);
            ulong result = instValRaw;

            if (writesCpuFlags)
            {
                ulong instSignBit = 0x1UL << ((int)instWidth - 1);
                ulong carryBit = 0x1UL << (int)instWidth;
                bool sA = (a & instSignBit) != 0x0;
                bool sB = (b & instSignBit) != 0x0;
                ulong instValMasked = instValRaw & instWidth.GetMask();
                bool sRes = (instValMasked & instSignBit) != 0x0;

                if (calcNZ)
                {
                    if (instValMasked == 0)
                    {
                        flags |= CPUFlags.Z;
                    }
                    if (sRes)
                    {
                        flags |= CPUFlags.N;
                    }
                }

                //using arm method that does it via (a + ~b + 1)
                //so it's usual for a subtract to produce a carry flag.
                //therefore flag is set to 0 when there's an unsigned underflow (b>a), 1 otherwise.
                //c is really "not borrow"
                bool c = (instValRaw & carryBit) != 0;
                if (!c)
                {
                    flags |= CPUFlags.C;
                }

                //specific to subtract?
                if ((sA != sB) && (sB == sRes))
                {
                    flags |= CPUFlags.V;
                }
            }
            return new GenericInstOutput(result, flags);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_isub(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            uint instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(srcVals.Length == 2);
            return internal_sub(srcVals, inst.WritesCPUFlags, instWidth, carryIn: 1, calcNZ: false);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_isubc(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            uint instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(srcVals.Length == 2);
            ulong carryIn = this.State.Flags.HasFlag(CPUFlags.C) ? 1UL : 0UL;
            return internal_sub(srcVals, inst.WritesCPUFlags, instWidth, carryIn, calcNZ: false);
        }

        [NotNull]
        private static GenericInstOutput internal_add(
            [NotNull] ulong[] srcVals,
                      bool    writesCpuFlags,
                      uint    instWidth,
                      ulong   carryIn)
        {
            Debug.Assert(srcVals.Length == 2);
            ulong a = srcVals[0];
            ulong b = srcVals[1];
            ulong instValRaw = unchecked(a + b + carryIn);
            ulong result = instValRaw;

            CPUFlags flags = CPUFlags.None;
            if (writesCpuFlags)
            {
                ulong instSignBit = 0x1UL << ((int)instWidth - 1);
                ulong carryBit = 0x1UL << (int)instWidth;
                bool sA = (a & instSignBit) != 0x0;
                bool sB = (b & instSignBit) != 0x0;
                ulong instValMasked = instValRaw & instWidth.GetMask();
                bool sRes = (instValRaw & instSignBit) != 0x0;

                //set carry and overflow flags...
                //todo: if doing a 64 bit add, how do we do carry?
                Debug.Assert(instWidth <= 32);
                if ((instValRaw & carryBit) != 0)
                {
                    //unsigned overflow, aka carry flag
                    flags |= CPUFlags.C;
                }

                if (sA == sB && sB != sRes)
                {
                    flags |= CPUFlags.V;
                }
            }
            return new GenericInstOutput(result, flags);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_iadd(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            uint instWidth = inst.GetBitWidth(this.WordSize);
            bool writesCpuFlags = inst.WritesCPUFlags;
            return internal_add(srcVals, writesCpuFlags, instWidth, carryIn: 0);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_iaddc(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(srcVals.Length == 2);
            uint instWidth = inst.GetBitWidth(this.WordSize);
            bool writesCpuFlags = inst.WritesCPUFlags;
            ulong carryIn = this.State.Flags.HasFlag(CPUFlags.C) ? 1UL : 0UL;
            return internal_add(srcVals, writesCpuFlags, instWidth, carryIn);
        }


        [NotNull]
        private GenericInstOutput Execute_generic_movc(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 3);
            ulong result = (srcVals[0] != FalseBits) ? srcVals[1] : srcVals[2];
            return new GenericInstOutput(result);
        }

        [NotNull]
        private GenericInstOutput Execute_generic_mov(
            [NotNull] Instruction inst,
            [NotNull] ulong[] srcVals)
        {
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(srcVals.Length == 1);
            ulong result = srcVals[0];
            return new GenericInstOutput(result);
        }

        [NotNull]
        private Execute_generic_func SelectGenericExecuteFunction([NotNull] Instruction inst)
        {
            switch (inst.Opcode)
            {
                case OpcodeType.umax:
                    return this.Execute_generic_umax;
                case OpcodeType.umin:
                    return this.Execute_generic_umin;
                case OpcodeType.iadd:
                    return this.Execute_generic_iadd;
                case OpcodeType.iaddc:
                    return this.Execute_generic_iaddc;
                case OpcodeType.isub:
                    return this.Execute_generic_isub;
                case OpcodeType.isubc:
                    return this.Execute_generic_isubc;
                case OpcodeType.mov:
                    return this.Execute_generic_mov;
                case OpcodeType.movc:
                    return this.Execute_generic_movc;
                case OpcodeType.nop:
                    return this.Execute_generic_nop;
                case OpcodeType.flags_read:
                    return this.Execute_generic_flags_read;
                case OpcodeType.is_ieq:
                    return this.Execute_generic_is_ieq;
                case OpcodeType.is_ineq:
                    return this.Execute_generic_is_ineq;
                case OpcodeType.is_iugt:
                    return this.Execute_generic_is_iugt;
                case OpcodeType.is_iule:
                    return this.Execute_generic_is_iule;
                case OpcodeType.is_iuge:
                    return this.Execute_generic_is_iuge;
                case OpcodeType.is_iult:
                    return this.Execute_generic_is_iult;
                case OpcodeType.is_isgt:
                    return this.Execute_generic_is_isgt;
                case OpcodeType.is_isle:
                    return this.Execute_generic_is_isle;
                case OpcodeType.is_isge:
                    return this.Execute_generic_is_isge;
                case OpcodeType.is_islt:
                    return this.Execute_generic_is_islt;
                case OpcodeType.is_iz:
                    return this.Execute_generic_is_iz;
                case OpcodeType.is_inz:
                    return this.Execute_generic_is_inz;
                case OpcodeType.is_ineg:
                    return this.Execute_generic_is_ineg;
                case OpcodeType.is_ipos:
                    return this.Execute_generic_is_ipos;
                case OpcodeType.breakpoint:
                    return this.Execute_generic_breakpoint;
                case OpcodeType.and:
                    return this.Execute_generic_and;
                case OpcodeType.or:
                    return this.Execute_generic_or;
                case OpcodeType.xor:
                    return this.Execute_generic_xor;
                case OpcodeType.nand:
                    return this.Execute_generic_nand;
                case OpcodeType.nor:
                    return this.Execute_generic_nor;
                case OpcodeType.xnor:
                    return this.Execute_generic_xnor;
                case OpcodeType.not:
                    return this.Execute_generic_not;
                default:
                    string errStr = String.Format("Instruction ({0}):{1} not implemented",
                        (int)inst.Opcode /*Is this correct way to get enum value?*/,
                        inst.Opcode);
                    throw new NotImplementedException(errStr);
            }
        }

        [NotNull]
        private ulong[] EvaluateAllSourceOperands([NotNull] Instruction inst, [NotNull] InstructionInfo info)
        {
            var instBitWidth = inst.GetBitWidth(this.WordSize);

            Debug.Assert(info.NumOperands >= info.NumDests);
            uint numSrc = info.NumOperands - info.NumDests;

            ulong[] srcVals = new ulong[numSrc];
            for (uint i = 0; i < numSrc; i++)
            {
                Operand oper = inst.Operands[(int)(info.NumDests + i)];
                ulong valueBx = this.ROState.GetValueBX(oper, instBitWidth);

                if (info.InstFlags.HasFlag(InstructionFlags.SIGNED))
                {
                    //Passing in a signed version of the operand's datatype to GetValueBX
                    //would not work, as is_sle32 u8{0xFF}, u8{0x1} would give wrong result.
                    srcVals[i] = valueBx.SignExtendArb(instBitWidth);
                }
                else
                {
                    srcVals[i] = valueBx;
                }
            }
            return srcVals;
        }

        private void Execute_Generic(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            Debug.Assert(stateUpdate.Flags == CPUFlags.None);

            /*
             * evaluate sources
             */
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);

            /*
             * execute operation
             */
            Execute_generic_func genericInstFunc = this.SelectGenericExecuteFunction(inst);
            var result = genericInstFunc(inst, srcVals);

            Debug.Assert(result != null, "result != null");
            Debug.Assert(result.DstVals.Length == info.NumDests);

            /*
             * Deal with inst/dest type's effect on dest values.
             */
            ulong[] dstVals = this.EvaluateDestValues(inst, result.DstVals);

            // todo: could move EvaluateDestValues into GenericInstOuput's constructor
            // and also deal with generic flag setting there?
            // GenerateGenericDestStateChanges can then be the filter/updater.


            /*
             * Make state change to write back the dest values
             */
            this.GenerateGenericDestStateChanges(stateUpdate, inst, info, dstVals, result.Flags);
        }



        private void Execute_Special_icmp(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            bool writesCpuFlags = inst.WritesCPUFlags;
            if (writesCpuFlags)
            {
                ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
                uint instWidth = inst.GetBitWidth(this.WordSize);
                var result = internal_sub(srcVals, writesCpuFlags, instWidth, carryIn: 1, calcNZ: true);

                Debug.Assert(stateUpdate.Flags == CPUFlags.None);
                stateUpdate.Flags = result.Flags;
                stateUpdate.UpdateFlags = true;
            }
        }

        private void Execute_Special_assert_true(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);

            if (srcVals[0] == 0x0)
            {
                string vals = string.Format("src0:{0} != 0 failed", srcVals[0]);
                this.ThrowCPUAssertion(inst, vals);
            }
        }

        private void Execute_Special_assert_false(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);

            if (srcVals[0] != 0x0)
            {
                string vals = string.Format("src0:{0} == 0 failed", srcVals[0]);
                this.ThrowCPUAssertion(inst, vals);
            }
        }
        
        private void Execute_Special_assert_neq(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 2);
            if (srcVals[0] == srcVals[1])
            {
                string vals = string.Format("src0:{0} != src1:{1} failed", srcVals[0], srcVals[1]);
                this.ThrowCPUAssertion(inst, vals);
            }
        }
       
        private void Execute_Special_assert_eq(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 2);
            if (srcVals[0] != srcVals[1])
            {
                string vals = string.Format("src0:{0} == src1:{1} failed", srcVals[0], srcVals[1]);
                this.ThrowCPUAssertion(inst, vals);
            }
        }

        private void Execute_Special_branch_flags(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            bool jump;
            CPUFlags flags = this.State.Flags;
            bool n = (flags & CPUFlags.N) == CPUFlags.N;
            bool z = (flags & CPUFlags.Z) == CPUFlags.Z;
            bool c = (flags & CPUFlags.C) == CPUFlags.C;
            bool v = (flags & CPUFlags.V) == CPUFlags.V;

            Debug.Assert(!inst.WritesCPUFlags);

            switch (inst.Opcode)
            {
                default:
                    throw new ArgumentException("Execute_Special_branch_flags called on a non-branch op?!", "inst.Opcode");
                case OpcodeType.bz:
                    jump = z;
                    break;
                case OpcodeType.bnz:
                    jump = !z;
                    break;
                case OpcodeType.bv:
                    jump = v;
                    break;
                case OpcodeType.bnv:
                    jump = !v;
                    break;
                case OpcodeType.bc:
                    jump = c;
                    break;
                case OpcodeType.bnc:
                    jump = !c;
                    break;
                case OpcodeType.bneg:
                    jump = n;
                    break;
                case OpcodeType.bpos:
                    jump = !n;
                    break;
                case OpcodeType.beq:
                    jump = z;
                    break;
                case OpcodeType.bneq:
                    jump = !z;
                    break;
                case OpcodeType.bugt:
                    jump = c && !z;
                    break;
                case OpcodeType.bule:
                    jump = !c || z;
                    break;
                case OpcodeType.buge:
                    jump = c;
                    break;
                case OpcodeType.bult:
                    jump = !c;
                    break;
                case OpcodeType.bsgt:
                    jump = !z && (n == v);
                    break;
                case OpcodeType.bsle:
                    jump = z || (n != v);
                    break;
                case OpcodeType.bsge:
                    jump = n == v;
                    break;
                case OpcodeType.bslt:
                    jump = n != v;
                    break;
            }

            if (jump)
            {
                this.Execute_Special_jump(stateUpdate, inst, info);
            }
        }

        private void Execute_Special_branch_args(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 2);
            Debug.Assert(!inst.WritesCPUFlags);

            bool jump;
            switch (inst.Opcode)
            {
                default:
                    throw new ArgumentException(
                        "Execute_Special_branch_args called on a non-branch op?!",
                        "inst.Opcode");
                case OpcodeType.btrue:
                    jump = srcVals[0] != FalseBits;
                    break;
                case OpcodeType.bfalse:
                    jump = srcVals[0] == FalseBits;
                    break;
            }

            if (jump)
            {
                this.JumpToOperandLabel(stateUpdate, inst, uLabelOperandNum: 1);
            }   
        }

        private void Execute_Special_jump(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            Debug.Assert(inst.GetBitWidth(0) == 0);
            this.JumpToOperandLabel(stateUpdate, inst, uLabelOperandNum: 0);
        }

        private void JumpToOperandLabel(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            int uLabelOperandNum)
        {
            /*
             * evaluate source operand
             */
            Operand src0Oper = inst.Operands[uLabelOperandNum];
            Debug.Assert(src0Oper != null, "src0Oper != null");
            src0Oper.DataType.ThrowIfInvalidMemType(); //todo : this should be part of the instruction constructor

            //TODO : 16 should come from CPU address bus size.
            ushort newPCAddr = (ushort)this.ROState.GetValueBX(src0Oper, 16, DataType.u16);

            /*
             * execute operation
             * write the new PC address
             */
            var PCReg = SimpleRegister.Reg0D(RegisterType.program_counter);
            var newPC = new CPURegMemStateChange(DataType.u16, PCReg, newPCAddr);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(newPC);
            stateUpdate.SupressPCIncrement = true;
        }

        /*
         * Funtion call / return
         */

        private void Execute_Special_call_leaf(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            /*
             *  call_leaf addr
             *      LR = PC + 1
             *      jump addr
             */

            /*
             * evaluate source operand
             */
            Operand src0Oper = inst.Operands[0];
            Debug.Assert(src0Oper != null, "src0Oper != null");
            src0Oper.DataType.ThrowIfInvalidMemType(); // todo : this should be part of the instruction constructor?

            // TODO : 16 should come from CPU address bus size.
            ushort jumpAddr = (ushort)this.ROState.GetValueBX(src0Oper, 16, DataType.u16);


            //      LR = PC + 1
            var oldPcAddr = this.State.ProgramCounter;
            var linkReg = SimpleRegister.Reg0D(RegisterType.link_register);
            var newLinkReg = new CPURegMemStateChange(DataType.u16, linkReg, oldPcAddr + 1);


            //      mov PC, addr   (aka jump addr)
            var progCounter = SimpleRegister.Reg0D(RegisterType.program_counter);
            var newPc = new CPURegMemStateChange(DataType.u16, progCounter, jumpAddr);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(newLinkReg);
            stateUpdate.RegMemChanges.Add(newPc);
            stateUpdate.SupressPCIncrement = true;
        }

        private void Execute_Special_call(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            /*
             *  call addr
             *      push_ctrl LR
             *      LR = PC + 1
             *      jump addr
             */
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);

            /*
             * evaluate source operand
             */
            Operand src0Oper = inst.Operands[0];
            Debug.Assert(src0Oper != null, "src0Oper != null");
            src0Oper.DataType.ThrowIfInvalidMemType(); // todo : this should be part of the instruction constructor?
            // TODO : 16 should come from CPU address bus size.
            ushort jumpAddr = (ushort)this.ROState.GetValueBX(src0Oper, 16, DataType.u16);


             //      push_ctrl LR
            ulong oldLrVal = this.State.LinkRegister;
            this.PushCtrl(stateUpdate, oldLrVal, instWidth);

            //      LR = PC + 1
            var oldPcAddr = this.State.ProgramCounter;
            var linkReg = SimpleRegister.Reg0D(RegisterType.link_register);
            var newLinkReg = new CPURegMemStateChange(DataType.u16, linkReg, oldPcAddr + 1);


            //      mov PC, addr   (aka jump addr)
            var progCounter = SimpleRegister.Reg0D(RegisterType.program_counter);
            var newPc = new CPURegMemStateChange(DataType.u16, progCounter, jumpAddr);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(newLinkReg);
            stateUpdate.RegMemChanges.Add(newPc);
            stateUpdate.SupressPCIncrement = true;
        }


        /// <summary>
        /// PC = ret_addr
        /// </summary>
        /// <param name="stateUpdate"></param>
        /// <param name="retAddr"></param>
        private static void RetAddr(
            [NotNull] CPUStateUpdate stateUpdate,
                      ulong retAddr)
        {
            //  PC = ret_addr
            var pcReg = SimpleRegister.Reg0D(RegisterType.program_counter);
            var newPc = new CPURegMemStateChange(
                DataType.u16,
                pcReg,
                retAddr);
            stateUpdate.RegMemChanges.Add(newPc);
            stateUpdate.SupressPCIncrement = true;
        }

        private void Execute_Special_ret_leaf(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            /*
             * execute operation:
             *      mov PC, LR
             */
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);

            RetAddr(stateUpdate, this.State.LinkRegister);
        }


        /// <summary>
        /// ret_addr = LR
        ///     pop_ctrl LR
        ///     PC = ret_addr
        /// </summary>
        /// <param name="stateUpdate"></param>
        /// <param name="instWidth"></param>
        private void RetPopLink(
            [NotNull] CPUStateUpdate stateUpdate,
                      uint           instWidth)
        {
            //  ret_addr = LR
            var retAddr = this.State.LinkRegister;

            // pop_ctrl LR
            var lrDestReg = OperandReg.LinkRegister();
            this.PopCtrl(stateUpdate, lrDestReg, instWidth);

            //  PC = ret_addr
            RetAddr(stateUpdate, retAddr);
        }

        private void Execute_Special_ret(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            /*
             * execute operation:
             *      ret_addr = LR
             *      pop_ctrl LR
             *      PC = ret_addr
             */
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);

            this.RetPopLink(stateUpdate, instWidth);
        }

        private void Execute_Special_ret_frame(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            /*
             *  ret_frame
             *      frame_leave
             *      ret
             */
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);

            this.FrameLeave(stateUpdate, instWidth);
            this.RetPopLink(stateUpdate, instWidth);
        }

        /*
         * Stack ops
         */

        private static ulong GrowShrinkStackPointer(
            bool add,//else sub
            ulong stackLowLimit,
            ulong currentStackVal,
            ulong stackHighLimit,
            ulong adjustVal,
            out bool limitExceeded)
        {
            ulong newSpValue;
            if (stackHighLimit <= stackLowLimit)
            {
                throw new PFC_StackException("Stack not properly sized");
            }
            Debug.Assert(currentStackVal <= stackHighLimit);
            Debug.Assert(currentStackVal >= stackLowLimit);

            if (add)
            {
                if ((stackHighLimit - currentStackVal) < adjustVal)
                {
                    limitExceeded = true;
                    newSpValue = stackHighLimit;
                }
                else
                {
                    newSpValue = currentStackVal + adjustVal;
                    limitExceeded = false;
                }
            }
            else
            {
                if ((currentStackVal - stackLowLimit) < adjustVal)
                {
                    limitExceeded = true;
                    newSpValue = stackLowLimit;
                }
                else
                {
                    newSpValue = currentStackVal - adjustVal;
                    limitExceeded = false;
                }
            }

            Debug.Assert(newSpValue <= stackHighLimit);
            Debug.Assert(newSpValue >= stackLowLimit);

            return newSpValue;
        }

        private static ulong GrowStackPointer(
            [NotNull] StackType stackType,
            ulong stackLowLimit,
            ulong currentStackVal,
            ulong stackHighLimit,
            ulong adjustVal)
        {
            bool add;
            bool overflow;
            
            switch (stackType.Dir)
            {
                case StackType.StackTypeDir.Descending:
                    add = false;
                    break;
                case StackType.StackTypeDir.Ascending:
                    add = true;
                    break;
                default:
                    throw new NotImplementedException("Stack direction not handled:" + stackType.Dir);
            }

            var newSpValue = GrowShrinkStackPointer(add,
                stackLowLimit,
                currentStackVal,
                stackHighLimit,
                adjustVal,
                out overflow);
            if (overflow)
            {
                throw new PFC_StackException("Stack overflow");
            }

            return newSpValue;
        }

        private static ulong ShrinkStackPointer(
            [NotNull] StackType stackType,
            ulong stackLowLimit,
            ulong currentStackVal,
            ulong stackHighLimit,
            ulong adjustVal)
        {
            bool add;
            bool underflow;

            switch (stackType.Dir)
            {
                case StackType.StackTypeDir.Descending:
                    add = true;
                    break;
                case StackType.StackTypeDir.Ascending:
                    add = false;
                    break;
                default:
                    throw new NotImplementedException("Stack direction not handled:" + stackType.Dir);
            }

            var newSpValue = GrowShrinkStackPointer(add,
                stackLowLimit,
                currentStackVal,
                stackHighLimit,
                adjustVal,
                out underflow);
            if (underflow)
            {
                throw new PFC_StackException("Stack underflow");
            }

            return newSpValue;
        }

        private void Execute_Special_stack_alloc(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //stack_alloc nBytes
            //	SP = SP - nBytes
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);

            //todo stack limit and stack base
            var newSpValue = GrowStackPointer(
                this.DefaultStackType,
                0,
                this.State.StackPointer,  //todo: valid to just read current state?
                0xFFFF,
                srcVals[0]);
            var changeSp = CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, newSpValue);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(changeSp);
        }


        private void Execute_Special_stack_free(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //stack_free nBytes
            //	SP = SP + nBytes
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);

            //todo stack limit and stack base
            var newSpValue = ShrinkStackPointer(
                this.DefaultStackType,
                0,
                this.State.StackPointer,  //todo: valid to just read current state?
                0xFFFF,
                srcVals[0]);
            var changeSp = CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, newSpValue);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(changeSp);
        }

        private void Execute_Special_push_data(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //push_data[width] oper
            //	depends on cpu setting for F/E D/A
            //	pushes data onto data stack
            //	data value is always evaluated first before sp change and write to sp
            //	default is fd.
            //	
            //	stack_alloc width/8
            //	[SP] = oper
            //
            //  push_data8   i16{r0}  # [sp] = r0 & 0xFFFF
            //  push_data16  i16{r0}  # [sp] = r0
            //  push_data32  i16{r0}  # [sp] = sign_ext(r0)

            /*
             * evaluate source
             */
            var srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            Debug.Assert(!inst.WritesCPUFlags);

            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);
            var numBytesToWrite = instWidth / 8;


            /* 
             * Sort out stack growth direction
             */
            var oldSpValue = this.State.StackPointer;  //todo: valid to just read current state?
            var newSpValue = GrowStackPointer(
                this.DefaultStackType,
                0,
                oldSpValue,
                0xFFFF,
                numBytesToWrite);
            var stackMode = this.DefaultStackType.Mode;

            /*
             * Sort out stack mode and therefore write address
             */
            uint writeAddress;
            switch (stackMode)
            {
                case StackType.StackTypeMode.Full:
                    writeAddress = (uint)(newSpValue & 0xFFFFFF);
                    Debug.Assert(writeAddress == newSpValue);
                    break;
                case StackType.StackTypeMode.Empty:
                    writeAddress = (uint)(oldSpValue & 0xFFFFFF);
                    Debug.Assert(writeAddress == oldSpValue);
                    break;
                default:
                    throw new PFC_StackException("Invalid Stack Mode");
            }

            /*
             * Massage the operand to the stack write-size and write-type.
             */
            var stackWriteType = instWidth.DataTypeFromWidth();
            ulong dstValTyped = this.EvaluateSingleDestValue(srcVals[0], stackWriteType, instWidth);


            /*
             * State changes for stack push_data
             * The order in RegMemChanges doesn't matter.
             */
            var changeMem = CPURegMemStateChange.MemChange(stackWriteType, writeAddress, dstValTyped);
            var changeSp = CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, newSpValue);

            stateUpdate.RegMemChanges.Add(changeSp);
            stateUpdate.RegMemChanges.Add(changeMem);
        }

        private void Execute_Special_pop_data(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //pop_data[width] oper
            //	depends on cpu setting for F/E D/A
            //	pops data off of data stack
            //	data value is always evaluated first before sp change and read from sp
            //	default is fd.
            //	
            //	oper = [SP]
            //	stack_free width/8
            //
            //  pop_data8   i16{r0}  # sign_ext(r0) = [SP]
            //  pop_data16  i16{r0}  # r0           = [SP]
            //  pop_data32  i16{r0}  # r0 & 0xFFFF  = [SP]
            //
            //
            Debug.Assert(!inst.WritesCPUFlags);

            // inst type
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);
            var instType = instWidth.DataTypeFromWidth();
            Debug.Assert(instType != DataType.undef);
            uint numBytesToRead = instWidth / 8;



            /*
             * Sort out stack growth direction
             */
            var oldSpValue = this.State.StackPointer; //todo: valid to just read current state?
            var newSpValue = ShrinkStackPointer(
                this.DefaultStackType,
                0,
                oldSpValue,
                0xFFFF,
                numBytesToRead);
            var stackMode = this.DefaultStackType.Mode;

            /*
             * Sort out stack mode and therefore read address
             */
            uint readAddress;
            switch (stackMode)
            {
                case StackType.StackTypeMode.Full:
                    readAddress = (uint)(oldSpValue& 0xFFFFFF);
                    Debug.Assert(readAddress == oldSpValue);
                    break;
                case StackType.StackTypeMode.Empty:
                    readAddress = (uint)(newSpValue & 0xFFFFFF);
                    Debug.Assert(readAddress == newSpValue);
                    break;
                default:
                    throw new PFC_StackException("Invalid Stack Mode");
            }

            /*
             * execute operation
             * read the data from memory at SP as inst width type
             */
            ulong memVal = this.State.ReadAlignedMemory(readAddress, numBytesToRead);

            /*
             * Write the memory value to operand
             */
            var destreg = inst.Operands[0] as OperandReg;
            if (destreg == null)
            {
                //todo is this checked at compile time?
                var message = string.Format("pop_data instruction has non-register dest? : `{0}`", inst);
                throw new ArgumentException(message, "inst");
            }
            var typedDstVal = this.EvaluateSingleDestValue(memVal, destreg.DataType, instWidth);


            // State changes for stack pop_data
            // The order in RegMemChanges doesn't matter.
            var changeOper = this.GenerateDestStateChange(instType, destreg, typedDstVal);
            var changeSp = CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, newSpValue);

            stateUpdate.RegMemChanges.Add(changeSp);
            stateUpdate.RegMemChanges.Add(changeOper);


        }


        private void Execute_Special_ctrl_stack_alloc(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //ctrl_stack_alloc n (units of size_t type thing)
            //	CSP = CSP + n
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);

            var newCspValue = GrowStackPointer(
                this.ControlStackType,
                0,
                this.State.ControlStackPointer,
                (ulong)this.State.ControlStack.Length,
                srcVals[0]);
            var changeCsp = CPURegMemStateChange.RegChange0D(RegisterType.control_stack_pointer, newCspValue);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(changeCsp);
        }

        private void Execute_Special_ctrl_stack_free(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //ctrl_stack_free n (units of size_t type thing)
            //	CSP = CSP - n
            ulong[] srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);

            // todo how does this check work if the cpu is pipelined?
            // todo a previously instruction that frees could not yet have written back CSP value?
            // todo this should be an PFC_StackUnderflowException?
            // todo this should give useful information, like PC, inst, etc?
            var newCspValue = ShrinkStackPointer(
                this.ControlStackType,
                0,
                this.State.ControlStackPointer,
                (ulong) this.State.ControlStack.Length,
                srcVals[0]);
            var changeCsp = CPURegMemStateChange.RegChange0D(RegisterType.control_stack_pointer, newCspValue);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(changeCsp);
        }

        private void Execute_Special_push_ctrl(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //push_ctrl oper
            //	push item onto control stack
            //	data value is always evaluated first
            //	
            //	if csp < frame_stack_size:
            //		[csp] = oper
            //		csp++
            //	else:
            //		explode
            //

            /*
             * evaluate source
             */
            var srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            var data = srcVals[0];

            Debug.Assert(!inst.WritesCPUFlags);

            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);

            //uint instWidth = sizeof(ulong) * 8; //todo need a constant for this. Is this even correct?

            this.PushCtrl(stateUpdate, data, instWidth);
        }

        private void PushCtrl(
            [NotNull] CPUStateUpdate stateUpdate,
            ulong data,
            uint instWidth)
        {
            /* 
             * Sort out stack growth direction
             */
            var oldCspValue = this.State.ControlStackPointer;
            var newCspValue = GrowStackPointer(this.ControlStackType, 0, oldCspValue, (ulong)this.State.ControlStack.Length, 1);

            /*
             * Sort out stack mode and therefore write address
             */
            var stackMode = this.ControlStackType.Mode;
            Debug.Assert(stackMode == StackType.StackTypeMode.Empty);
            uint writeAddress;
            switch (stackMode)
            {
                case StackType.StackTypeMode.Full:
                    writeAddress = (uint)(newCspValue & 0xFFFFFF);
                    Debug.Assert(writeAddress == newCspValue);
                    break;
                case StackType.StackTypeMode.Empty:
                    writeAddress = (uint)(oldCspValue & 0xFFFFFF);
                    Debug.Assert(writeAddress == oldCspValue);
                    break;
                default:
                    throw new PFC_StackException("Invalid Stack Mode");
            }

            /*
             * Massage the operand to the stack write-size and write-type.
             */
            var stackWriteType = DataType.undef; //instWidth.DataTypeFromWidth(); ?
            ulong dstValTyped = this.EvaluateSingleDestValue(data, stackWriteType, instWidth);

            /*
             * State changes for stack push_ctrl
             * The order in RegMemChanges doesn't matter.
             */
            var changeCtrlStack = CPURegMemStateChange.CtrlStackChange(stackWriteType, writeAddress, dstValTyped);
            var changeCsp = CPURegMemStateChange.RegChange0D(RegisterType.control_stack_pointer, newCspValue);

            stateUpdate.RegMemChanges.Add(changeCsp);
            stateUpdate.RegMemChanges.Add(changeCtrlStack);
        }

        private void Execute_Special_pop_ctrl(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //pop_ctrl oper
            //	pops item off of control stack
            //	data value is always evaluated first
            //	
            //	if csp != 0 && framestacksize != 0
            //		csp--
            //		oper = [csp]
            //	else:
            //		explode

            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);


            /*
             * Write the value to operand
             */
            var destreg = inst.Operands[0] as OperandReg;
            if (destreg == null)
            {
                //todo is this checked at compile time?
                var message = string.Format("pop_data instruction has non-register dest? : `{0}`", inst);
                throw new ArgumentException(message, "inst");
            }

            this.PopCtrl(stateUpdate, destreg, instWidth);
        }

        private void PopCtrl(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] OperandReg destreg,
            uint instWidth)
        {
            /*
             * Sort out stack growth direction
             */
            var oldCspValue = this.State.ControlStackPointer;
            var newCspValue = ShrinkStackPointer(
                this.ControlStackType,
                0,
                oldCspValue,
                (ulong)this.State.ControlStack.Length,
                1);

            /*
             * Sort out stack mode and therefore read address
             */
            var stackMode = this.ControlStackType.Mode;
            Debug.Assert(stackMode == StackType.StackTypeMode.Empty);
            uint readAddress;
            switch (stackMode)
            {
                case StackType.StackTypeMode.Full:
                    readAddress = (uint)(oldCspValue & 0xFFFFFF);
                    Debug.Assert(readAddress == oldCspValue);
                    break;
                case StackType.StackTypeMode.Empty:
                    readAddress = (uint)(newCspValue & 0xFFFFFF);
                    Debug.Assert(readAddress == newCspValue);
                    break;
                default:
                    throw new PFC_StackException("Invalid Stack Mode");
            }

            /*
             * execute operation
             * read the data on the control stack to by CSP
             */
            Debug.Assert(readAddress < this.State.ControlStack.Length);
            ulong memVal = this.State.ControlStack[readAddress]; //todo how does this work with transactions?

            //todo will this write correct size?
            var typedDstVal = this.EvaluateSingleDestValue(memVal, destreg.DataType, instWidth);

            // State changes for stack pop_data
            // The order in RegMemChanges doesn't matter.

            var instType = instWidth.DataTypeFromWidth();
            Debug.Assert(instType != DataType.undef);
            var changeOper = this.GenerateDestStateChange(instType, destreg, typedDstVal);
            var changeCsp = CPURegMemStateChange.RegChange0D(RegisterType.control_stack_pointer, newCspValue);

            stateUpdate.RegMemChanges.Add(changeCsp);
            stateUpdate.RegMemChanges.Add(changeOper);
        }

        private void Execute_Special_frame_enter(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //frame_enter nBytes
            //  #aka 'alloca'
            //	push_ctrl fp
            //	fp = sp
            //		
            //	stack_alloc nBytes

            //todo: valid to just read current FP and SP state?
            //


            /*
             * evaluate sources
             */
            var srcVals = this.EvaluateAllSourceOperands(inst, info);
            Debug.Assert(srcVals.Length == 1);
            var nBytes = srcVals[0];

            Debug.Assert(!inst.WritesCPUFlags);

            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);

            //uint instWidth = sizeof(ulong) * 8; //todo need a constant for this. Is this even correct?


            /*
             * push_ctrl fp
             */
            this.PushCtrl(stateUpdate, this.State.FramePointer, instWidth);

            /*
             * fp = sp
             */
            var spVal = this.State.StackPointer;
            var changeFp = CPURegMemStateChange.RegChange0D(RegisterType.frame_pointer, spVal);
            stateUpdate.RegMemChanges.Add(changeFp);

            /*
             * stack_alloc nBytes
             */
            //todo stack limit and stack base
            var newSpValue = GrowStackPointer(
                this.DefaultStackType,
                0,
                this.State.StackPointer,
                0xFFFF,
                nBytes);
            var changeSp = CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, newSpValue);
            stateUpdate.RegMemChanges.Add(changeSp);
        }


        /// <summary>
        /// frame_leave
        ///     sp = fp
        ///     pop_ctrl fp
        /// </summary>
        /// <param name="stateUpdate"></param>
        /// <param name="instWidth"></param>
        private void FrameLeave(
            [NotNull] CPUStateUpdate stateUpdate,
            uint                     instWidth)
        {
            /*
             * sp = fp
             */
            var fpVal = this.State.FramePointer;
            var changeSp = CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, fpVal);
            stateUpdate.RegMemChanges.Add(changeSp);

            /*
             * pop_ctrl fp
             */
            var destreg = OperandReg.FramePointer();
            this.PopCtrl(stateUpdate, destreg, instWidth);
        }

        private void Execute_Special_frame_leave(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            //frame_leave
            //	sp = fp
            //	pop_ctrl fp

            Debug.Assert(!inst.WritesCPUFlags);
            Debug.Assert(inst.GetBitWidth(0) == 0);
            var instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);

            this.FrameLeave(stateUpdate, instWidth);
        }


        private void Execute_Special_halt(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            stateUpdate.Halt = true;
            stateUpdate.SupressPCIncrement = true;
        }

        private void Execute_Special_st(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            // st memaddr, value
            // remember for ST the width is the number of bits to write.
            // The `system address size` numbits will be read from the dest address
            // if no type is given for an operand, then it's type is inferred from the inst width
            // st32  r0, r1       is the same as    st32  u32{r0}, u32{r1}
            // st8   r0, r1       is the same as    st8   u8{r0},  u8{r1}
            // st8   u16{r1}, u8{r0},  stores an 8bit value from whereever r1 points to

            uint instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);
            DataType instType = OperandEnumHelpers.DataTypeFromWidth(instWidth);
            Debug.Assert(instType != DataType.undef);

            DataType memWriteType = instType;

            /*
             * evaluate all operands. This will give us a memory address and a value.
             */
            Operand addressOper = inst.Operands[0];
            Debug.Assert(addressOper != null, "addressOper != null");
            addressOper.DataType.ThrowIfInvalidMemType();

            //TODO : 16 should come from CPU address bus size.
            uint address = (ushort)this.ROState.GetValueBX(addressOper, 16, instType);

            Operand valueOper = inst.Operands[1];
            ulong value = this.ROState.GetValueBX(valueOper, instWidth);

            /*
             * execute operation
             * write 'value' to memory address
             */
            var memAddr = SimpleRegister.Reg1D(RegisterType.memory, index: address & 0xFFFF);
            var newState = new CPURegMemStateChange(memWriteType, memAddr, value);

            /*
             * Add state change to write back the value
             */
            stateUpdate.RegMemChanges.Add(newState);
        }

        private void Execute_Special_ld(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            // remember for LD the width is the number of bits to load.
            // The `system address size` numbits will be read from the source address
            // if no type is given for an operand, then it's type is inferred from the inst width
            // ld32  r0, r1       is the same as    ld32  u32{r0}, u32{r1}
            // ld8   r0, r1       is the same as    ld8   u8{r0},  u8{r1}
            // ld8   u8{r0}, u16{r1} -- loads an 8bit value from whereever r1 points to.
            uint instWidth = inst.GetBitWidth(this.WordSize);
            Debug.Assert(instWidth != 0);
            DataType instType = instWidth.DataTypeFromWidth();
            Debug.Assert(instType != DataType.undef);

            uint numBytesToRead = instWidth / 8;

            /*
             * evaluate sources
             */
            Operand src0Oper = inst.Operands[1];
            Debug.Assert(src0Oper != null, "src0Oper != null");
            src0Oper.DataType.ThrowIfInvalidMemType();

            //TODO : 16 should come from CPU address bus size.
            ushort address = (ushort)this.ROState.GetValueBX(src0Oper, 16, instType);

            /*
             * execute operation
             * read value at memory address in src0
             */
            ulong memVal = this.State.ReadAlignedMemory(address, numBytesToRead);

            /*
             * Deal with inst/dest type's effect on dest value.
             */
            OperandReg destreg = inst.Operands[0] as OperandReg;
            if (destreg == null)
            {
                //todo is this checked at compile time?
                var message = string.Format("ld instruction has non-register dest? : `{0}`", inst);
                throw new ArgumentException(message, "inst");
            }
            var typedDstVal = this.EvaluateSingleDestValue(memVal, destreg.DataType, instWidth);

            /*
             * Make state change to write back the dest value
             */
            var newState = this.GenerateDestStateChange(instType, destreg, typedDstVal);
            stateUpdate.RegMemChanges.Add(newState);
        }

        private void Execute_Special(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            switch (inst.Opcode)
            {
                case OpcodeType.st:
                    this.Execute_Special_st(stateUpdate, inst, info);
                    break;
                case OpcodeType.ld:
                    this.Execute_Special_ld(stateUpdate, inst, info);
                    break;
                case OpcodeType.halt:
                    this.Execute_Special_halt(stateUpdate, inst, info);
                    break;
                case OpcodeType.jump:
                    this.Execute_Special_jump(stateUpdate, inst, info);
                    break;
                case OpcodeType.assert_eq:
                    this.Execute_Special_assert_eq(stateUpdate, inst, info);
                    break;
                case OpcodeType.assert_neq:
                    this.Execute_Special_assert_neq(stateUpdate, inst, info);
                    break;
                case OpcodeType.assert_true:
                    this.Execute_Special_assert_true(stateUpdate, inst, info);
                    break;
                case OpcodeType.assert_false:
                    this.Execute_Special_assert_false(stateUpdate, inst, info);
                    break;
                case OpcodeType.icmp:
                    this.Execute_Special_icmp(stateUpdate, inst, info);
                    break;

                /*
                 * Funtion call / return
                 */
                case OpcodeType.call_leaf:
                    this.Execute_Special_call_leaf(stateUpdate, inst, info);
                    break;
                case OpcodeType.call:
                    this.Execute_Special_call(stateUpdate, inst, info);
                    break;
                case OpcodeType.ret_leaf:
                    this.Execute_Special_ret_leaf(stateUpdate, inst, info);
                    break;
                case OpcodeType.ret:
                    this.Execute_Special_ret(stateUpdate, inst, info);
                    break;
                case OpcodeType.ret_frame:
                    this.Execute_Special_ret_frame(stateUpdate, inst, info);
                    break;

                /*
                 * Stack ops
                 */
                case OpcodeType.stack_alloc:
                    this.Execute_Special_stack_alloc(stateUpdate, inst, info);
                    break;
                case OpcodeType.stack_free:
                    this.Execute_Special_stack_free(stateUpdate, inst, info);
                    break;
                case OpcodeType.push_data:
                    this.Execute_Special_push_data(stateUpdate, inst, info);
                    break;
                case OpcodeType.pop_data:
                    this.Execute_Special_pop_data(stateUpdate, inst, info);
                    break;
                case OpcodeType.ctrl_stack_alloc:
                    this.Execute_Special_ctrl_stack_alloc(stateUpdate, inst, info);
                    break;
                case OpcodeType.ctrl_stack_free:
                    this.Execute_Special_ctrl_stack_free(stateUpdate, inst, info);
                    break;
                case OpcodeType.push_ctrl:
                    this.Execute_Special_push_ctrl(stateUpdate, inst, info);
                    break;
                case OpcodeType.pop_ctrl:
                    this.Execute_Special_pop_ctrl(stateUpdate, inst, info);
                    break;
                case OpcodeType.frame_enter:
                    this.Execute_Special_frame_enter(stateUpdate, inst, info);
                    break;
                case OpcodeType.frame_leave:
                    this.Execute_Special_frame_leave(stateUpdate, inst, info);
                    break;

                case OpcodeType.bz:
                case OpcodeType.bnz:
                case OpcodeType.bv:
                case OpcodeType.bnv:
                case OpcodeType.bc:
                case OpcodeType.bnc:
                case OpcodeType.bneg:
                case OpcodeType.bpos:
                case OpcodeType.beq:
                case OpcodeType.bneq:
                case OpcodeType.bugt:
                case OpcodeType.bule:
                case OpcodeType.buge:
                case OpcodeType.bult:
                case OpcodeType.bsgt:
                case OpcodeType.bsle:
                case OpcodeType.bsge:
                case OpcodeType.bslt:
                    this.Execute_Special_branch_flags(stateUpdate, inst, info);
                    break;
                case OpcodeType.btrue:
                case OpcodeType.bfalse:
                    this.Execute_Special_branch_args(stateUpdate, inst, info);
                    break;
                default:
                    string errStr = String.Format("Instruction ({0}):{1} not implemented",
                        (int)inst.Opcode /*Is this correct way to get enum value?*/,
                        inst.Opcode);
                    throw new NotImplementedException(errStr);
            }
        }


        private void DbgTraceInst(
            [NotNull] Instruction inst,
            [NotNull] InstructionInfo info)
        {
            // todo: operations that check flags should print flags and if they passed/failed etc
            if (!this.TraceInst)
            {
                return;
            }
            // todo this function and EvaluateAllSourceOperands both
            // iterate over the source variables in the same way... 
            var srcVals = this.EvaluateAllSourceOperands(inst, info);

            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.TRACE_INST,
                      "Cycle:{0} PC: {1}",
                      this.State.cycleCounter,
                      this.State.ProgramCounter);

            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.TRACE_INST,
                      "\t{0}", inst);
            if (srcVals.Length > 0)
            {
                for (var i = 0; i < srcVals.Length; i++)
                {
                    var oper = inst.Operands[(int)info.NumDests + i];
                    Debug.Assert(oper != null, "oper != null");

                    var str = string.Format(
                        "{0}: {1}",
                        oper.ToStringShort(),
                        srcVals[i]);
                    Log.Print(DPFCLASS.CPU, DPFMASK_CPU.TRACE_INST,
                              "\t\t{0}", str);
                }

            }
        }

        private void DbgTraceStateUpdates(
            [NotNull] CPUStateUpdate stateUpdate)
        {
            if (!this.TraceInst)
            {
                return;
            }

            Log.Print(DPFCLASS.CPU, DPFMASK_CPU.TRACE_INST,
                "\t{0}", stateUpdate.ToString());
        }


        private void DecodeAndExecute(
            [NotNull] CPUStateUpdate stateUpdate,
            [NotNull] Instruction inst)
        {
            InstructionInfo info = InstructionInfo.Table[inst.Opcode];
            Debug.Assert(info != null, "info != null");
            Debug.Assert(info.Opcode == inst.Opcode);

            this.DbgTraceInst(inst, info);

            if ((info.InstFlags & InstructionFlags.EXECUTE_NON_TRIVIAL) != 0x0)
            {
                this.Execute_Special(stateUpdate, inst, info);
            }
            else
            {
                this.Execute_Generic(stateUpdate, inst, info);
            }

            this.DbgTraceStateUpdates(stateUpdate);
        }


        /// <summary>
        /// execute a clock's worth of instructions
        /// </summary>
        public void Clock()
        {
            if (this.State.IsHalted)
            {
                return;
            }

            //Fetch instruction
            ulong PC_val = this.State.ProgramCounter;

            if (PC_val >= (ulong) this.State.CodeROM.Count)
            {
                string err = string.Format("PC {0} invalid. CodeROM only {1} in size",
                                            PC_val, this.State.CodeROM.Count);
                throw new PFC_CPUNoInstructionException(err);
            }

            Instruction inst = this.State.CodeROM[(int)PC_val];
            if (inst == null)
            {
                throw new PFC_CPUNoInstructionException();
            }

            var stateUpdate = new CPUStateUpdate();


            //Decode + Execute instruction
            this.DecodeAndExecute(stateUpdate, inst);

            //Increment PC_val
            if (stateUpdate.SupressPCIncrement == false)
            {
                ulong value = PC_val + 1;
                stateUpdate.RegMemChanges.Add(CPURegMemStateChange.RegChange0D(RegisterType.program_counter, value));
            }

            //everything costs 1 cycle right now and none of the instructions set this value
            Debug.Assert(stateUpdate.CycleCost == 0);
            stateUpdate.CycleCost = 1;

            //Write back state changes.
            this.State.ApplyStateUpdate(stateUpdate);
        }


    }
}
