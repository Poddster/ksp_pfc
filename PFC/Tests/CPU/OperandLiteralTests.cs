﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using PFC.CPU;

namespace PFC.Tests.CPU
{
    public static class OperandLiteralTests
    {

        private static void CPUTest_Operand_Literals_f32()
        {
            Func<float, uint, bool> f_lit_f32_u32 = (float f, uint u) =>
             (OperandLiteral.f32(f).LiteralB32 == u);
            Debug.Assert(f_lit_f32_u32(0.0f, 0x00000000U));
            Debug.Assert(f_lit_f32_u32(1.0f, 0x3f800000));
            Debug.Assert(f_lit_f32_u32(2.0f, 0x40000000));
            Debug.Assert(f_lit_f32_u32(3.0f, 0x40400000));
            Debug.Assert(f_lit_f32_u32(4.0f, 0x40800000));
            Debug.Assert(f_lit_f32_u32(5.0f, 0x40a00000));
            Debug.Assert(f_lit_f32_u32(0.1f, 0x3dcccccd));
            Debug.Assert(f_lit_f32_u32(0.5f, 0x3f000000));
            Debug.Assert(f_lit_f32_u32(0.0125f, 0x3c4ccccd));

            //not sure how machine dependent this is. Plus, it relies on c# interpretation of literals
            Debug.Assert(f_lit_f32_u32(340282000000000000000000000000000000000.0f, 0x7f7fffee));
            Debug.Assert(f_lit_f32_u32(340282000000000000000000000000000000000.001f, 0x7f7fffee));
            Debug.Assert(f_lit_f32_u32(3.40282e+38f, 0x7f7fffee));
            Debug.Assert(f_lit_f32_u32(float.MaxValue, 0x7f7fffff));
            Debug.Assert(f_lit_f32_u32(float.MinValue, 0xff7fffff));
            Debug.Assert(f_lit_f32_u32(-3.40282e+38f, 0xff7fffee));
            Debug.Assert(f_lit_f32_u32(0.0000000000000000000000000000000000001f, 0x2081cea));
            Debug.Assert(f_lit_f32_u32(1.401298464324817e-45f, 0x1)); //ulp

            Debug.Assert(f_lit_f32_u32(float.NegativeInfinity, 0xff800000));
            Debug.Assert(f_lit_f32_u32(float.PositiveInfinity, 0x7f800000));
            //technically there are lots of NAN representations. probably 0xffc00000
            //So let's ensure we get the same one back, bits untouched
            Debug.Assert(f_lit_f32_u32(float.NaN, BitConverter.ToUInt32(BitConverter.GetBytes(float.NaN), 0)));
        }

        private static void CPUTest_Operand_Literals_u8()
        {
            Func<byte, uint, bool> f_lit_u8_u32 = (byte b, uint u) =>
                    (OperandLiteral.u8(b).LiteralB32 == u);
            Debug.Assert(f_lit_u8_u32(0, 0U));
            Debug.Assert(f_lit_u8_u32(56, 0x38U));
            Debug.Assert(f_lit_u8_u32(85, 0x55U));
            Debug.Assert(f_lit_u8_u32(86, 0x56U));
            Debug.Assert(f_lit_u8_u32(127, 0x7FU));
            Debug.Assert(f_lit_u8_u32(128, 0x80U));
            Debug.Assert(f_lit_u8_u32(170, 0xAAU));
            Debug.Assert(f_lit_u8_u32(255, 0xFFU));
        }

        private static void CPUTest_Operand_Literals_u16()
        {
            Func<ushort, uint, bool> f_lit_u16_u32 = (ushort s, uint u) =>
                    (OperandLiteral.u16(s).LiteralB32 == u);
            Debug.Assert(f_lit_u16_u32(0, 0x0000U));
            Debug.Assert(f_lit_u16_u32(127, 0x007FU));
            Debug.Assert(f_lit_u16_u32(128, 0x0080U));
            Debug.Assert(f_lit_u16_u32(255, 0x00FFU));
            Debug.Assert(f_lit_u16_u32(256, 0x0100U));
            Debug.Assert(f_lit_u16_u32(21845, 0x5555));
            Debug.Assert(f_lit_u16_u32(32767, 0x7FFFU));
            Debug.Assert(f_lit_u16_u32(32768, 0x8000U));
            Debug.Assert(f_lit_u16_u32(43690, 0xaaaa));
            Debug.Assert(f_lit_u16_u32(65535, 0xFFFFU));
        }

        private static void CPUTest_Operand_Literals_u32()
        {
            Func<uint, uint, bool> f_lit_u32_u32 = (uint ui, uint uo) =>
                         (OperandLiteral.u32(ui).LiteralB32 == uo);
            Debug.Assert(f_lit_u32_u32(0, 0x00000000U));
            Debug.Assert(f_lit_u32_u32(127, 0x0000007FU));
            Debug.Assert(f_lit_u32_u32(128, 0x00000080U));
            Debug.Assert(f_lit_u32_u32(255, 0x000000FFU));
            Debug.Assert(f_lit_u32_u32(256, 0x00000100U));
            Debug.Assert(f_lit_u32_u32(32767, 0x00007FFFU));
            Debug.Assert(f_lit_u32_u32(32768, 0x00008000U));
            Debug.Assert(f_lit_u32_u32(65535, 0x0000FFFFU));
            Debug.Assert(f_lit_u32_u32(65536, 0x00010000U));
            Debug.Assert(f_lit_u32_u32(1431655765, 0x55555555U));
            Debug.Assert(f_lit_u32_u32(0x7FFFFFFF, 0x7FFFFFFFU));
            Debug.Assert(f_lit_u32_u32(0x80000000, 0x80000000U));
            Debug.Assert(f_lit_u32_u32(2863311530, 0xAAAAAAAAU));
            Debug.Assert(f_lit_u32_u32(0xDEADBEEF, 0xDEADBEEF));

            Debug.Assert(f_lit_u32_u32(0xFFFFFFFF, 0xFFFFFFFFU));

            // this is actually a C# test, but it illustrates how 2**32-1 can't be done in floats.
            // along with other numbers, e.g. 2**32-2, 2**31+1 etc, and why it's important to test
            // them, incase the implementation decides to do something insane and store everything
            // as float
            float f = 0xFFFFFFFFU;
            uint i = (uint)f;
            Debug.Assert(i != 0xFFFFFFFFU);

        }

        private static void CPUTest_Operand_Literals_i8()
        {
            Func<sbyte, uint, bool> f_lit_i8_u32 = (sbyte b, uint u) =>
                    (OperandLiteral.i8(b).LiteralB32 == u);
            Debug.Assert(f_lit_i8_u32(0, 0U));
            Debug.Assert(f_lit_i8_u32(56, 0x00000038U));
            Debug.Assert(f_lit_i8_u32(85, 0x00000055U));
            Debug.Assert(f_lit_i8_u32(86, 0x00000056U));
            Debug.Assert(f_lit_i8_u32(127, 0x0000007FU));
            Debug.Assert(f_lit_i8_u32(-128, 0xFFFFFF80U));
            Debug.Assert(f_lit_i8_u32(-127, 0xFFFFFF81U));
            Debug.Assert(f_lit_i8_u32(-86, 0xFFFFFFAAU));
            Debug.Assert(f_lit_i8_u32(-56, 0xFFFFFFC8U));
            Debug.Assert(f_lit_i8_u32(-1, 0xFFFFFFFFU));
        }

        private static void CPUTest_Operand_Literals_i16()
        {
            Func<short, uint, bool> f_lit_i16_u32 = (short s, uint u) =>
                    (OperandLiteral.i16(s).LiteralB32 == u);
            Debug.Assert(f_lit_i16_u32(0, 0x00000000U));
            Debug.Assert(f_lit_i16_u32(85, 0x00000055U));
            Debug.Assert(f_lit_i16_u32(86, 0x00000056U));
            Debug.Assert(f_lit_i16_u32(127, 0x0000007FU));
            Debug.Assert(f_lit_i16_u32(128, 0x00000080U));
            Debug.Assert(f_lit_i16_u32(255, 0x000000FFU));
            Debug.Assert(f_lit_i16_u32(256, 0x00000100U));
            Debug.Assert(f_lit_i16_u32(21845, 0x00005555U));
            Debug.Assert(f_lit_i16_u32(32767, 0x00007FFFU));
            Debug.Assert(f_lit_i16_u32(-32768, 0xFFFF8000U));
            Debug.Assert(f_lit_i16_u32(-32767, 0xFFFF8001U));
            Debug.Assert(f_lit_i16_u32(-21846, 0xFFFFAAAAU));
            Debug.Assert(f_lit_i16_u32(-128, 0xFFFFff80U));
            Debug.Assert(f_lit_i16_u32(-127, 0xFFFFFF81U));
            Debug.Assert(f_lit_i16_u32(-86, 0xFFFFFFAAU));
            Debug.Assert(f_lit_i16_u32(-56, 0xFFFFFFC8U));
            Debug.Assert(f_lit_i16_u32(-1, 0xFFFFFFFFU));
        }

        private static void CPUTest_Operand_Literals_i32()
        {
            Func<int, uint, bool> f_lit_i32_u32 = (int i, uint u) =>
                         (OperandLiteral.i32(i).LiteralB32 == u);
            Debug.Assert(f_lit_i32_u32(0, 0x00000000U));
            Debug.Assert(f_lit_i32_u32(85, 0x00000055U));
            Debug.Assert(f_lit_i32_u32(86, 0x00000056U));
            Debug.Assert(f_lit_i32_u32(127, 0x0000007FU));
            Debug.Assert(f_lit_i32_u32(128, 0x00000080U));
            Debug.Assert(f_lit_i32_u32(255, 0x000000FFU));
            Debug.Assert(f_lit_i32_u32(256, 0x00000100U));
            Debug.Assert(f_lit_i32_u32(32767, 0x00007FFFU));
            Debug.Assert(f_lit_i32_u32(32768, 0x00008000U));
            Debug.Assert(f_lit_i32_u32(65535, 0x0000FFFFU));
            Debug.Assert(f_lit_i32_u32(65536, 0x00010000U));
            Debug.Assert(f_lit_i32_u32(1431655765, 0x55555555U));
            Debug.Assert(f_lit_i32_u32(2147483647, 0x7FFFFFFFU));
            Debug.Assert(f_lit_i32_u32(-2147483648, 0x80000000U));
            Debug.Assert(f_lit_i32_u32(-2147483647, 0x80000001U));
            Debug.Assert(f_lit_i32_u32(-1431655766, 0xAAAAAAAA));
            Debug.Assert(f_lit_i32_u32(-32768, 0xFFFF8000U));
            Debug.Assert(f_lit_i32_u32(-32767, 0xFFFF8001U));
            Debug.Assert(f_lit_i32_u32(-21846, 0xFFFFAAAAU));
            Debug.Assert(f_lit_i32_u32(-128, 0xFFFFff80U));
            Debug.Assert(f_lit_i32_u32(-127, 0xFFFFFF81U));
            Debug.Assert(f_lit_i32_u32(-86, 0xFFFFFFAAU));
            Debug.Assert(f_lit_i32_u32(-56, 0xFFFFFFC8U));
            Debug.Assert(f_lit_i32_u32(-1, 0xFFFFFFFFU));
        }


        public static void CPUTest_Operand_Literals()
        {
            CPUTest_Operand_Literals_u8();
            CPUTest_Operand_Literals_u16();
            CPUTest_Operand_Literals_u32();

            CPUTest_Operand_Literals_i8();
            CPUTest_Operand_Literals_i16();
            CPUTest_Operand_Literals_i32();

            CPUTest_Operand_Literals_f32();
        }


    }
}
