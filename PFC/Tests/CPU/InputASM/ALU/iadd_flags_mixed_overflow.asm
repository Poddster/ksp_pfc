
    #Other iadd mixed tests didn't hit zero or V :)
    #It's hard to hit a V overflow if you're using small operands in big operations

                                                    #   NZVC
    nop
    mov32            r1,      0x7FF                 #1

    mov32            r0,      0x0                   #2
    iadd8!           r0,      r1,      0x1          #3  -Z-C
    flags_read       r2                             #4
    assert_eq        r2,      0b0101                #5
    assert_eq        r0,      0x0                   #6


    mov32            r0,      0x0                   #7
    iadd8!       u32{r0}, u32{r1}, u32{0x1}         #8  -Z-C
    flags_read       r2                             #9
    assert_eq        r2,      0b0101                #10
    assert_eq        r0,      0x0                   #11

    mov32            r0,      0x0                   #12
    mov16            r1,      0x7FFF                #13
    iadd16!      i32{r0},  i32{r1},    r1           #14  N-V-
    flags_read       r2                             #15
    assert_eq        r2,      0b1010                #16
    assert_eq        r0,      0xfffffffe            #17

    iadd16!      i16{r1},     r1,      0x1          #18  N-V-
    flags_read       r2                             #19
    assert_eq        r2,      0b1010                #20
    nop                                             #21 r1 is now 0x8000

    mov32            r0,      0x0                   #22
    iadd32!      i32{r0},  i32{r1},    r1           #23  ----
    flags_read       r2                             #24
    assert_eq        r2,      0b0000                #25
    assert_eq        r0,      0x10000               #26

    mov32            r0,      0x0                   #27
    iadd16!          r0,      r1,      r1           #28 -ZVC
    flags_read       r2                             #29
    assert_eq        r2,      0b0111                #30
    assert_eq        r0,      0x0000                #31

    mov32            r0,      0x0                   #32
    iadd8!           r0,      r1,      r1           #33 -Z--
    flags_read       r2                             #34
    assert_eq        r2,      0b0100                #35
    assert_eq        r0,      0x00                  #36

    # note how the next two both do 0x80+0xf2 and get the same results
    # But note that 32 doesn't set V and but the unsigned one doesn't set the carry flag
    mov              r0,      0x80                  #37
    mov              r1,      0xF2                  #38
    iadd32!       i8{r2},  i8{r1},  i8{r0}          #39 ---C
    flags_read       r3                             #40
    assert_eq        r3,      0b0001                #41
    assert_eq        r2,      0x72                  #42

    iadd8!        i8{r2},  i8{r1},  i8{r0}          #43 --VC
    flags_read       r3                             #44
    assert_eq        r3,      0b0011                #45
    assert_eq        r2,      0x72                  #46

    iadd32!       i8{r2},  u8{r1},  u8{r0}          #47 ----
    flags_read       r3                             #48
    assert_eq        r3,      0b0000                #49
    assert_eq        r2,      0x72                  #50

    halt
