		mov           r0,      0x12345678
		mov           r1,      0x20FEDCBA

		iadd          r2,      r0,      r1           #2 r2 = 0x33333332
		assert_eq     r2,      0x33333332
		iadd          r3,      r0,   i8{r1}          #4 r3 = 0x12345632
		assert_eq     r3,      0x12345632
		iadd      i16{r4},     r0,      r1           #6 r4 = 0x3332
		assert_eq     r4,      0x3332
		iadd       i8{r5},  i8{r0},  i8{r1}          #8 r5 = 0x32
		assert_eq     r5,      0x32

		halt
