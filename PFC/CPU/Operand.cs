﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PFC.CPU
{
    using JetBrains.Annotations;

    using PFC.ASMParser;

    /// <summary>
    /// Operand address mode.
    /// </summary>
    enum AddressMode
    {
        /// <summary>
        /// a literal operand e.g. 0x500
        /// </summary>
        literal,

        /// <summary>
        /// direct register use e.g. r0
        /// </summary>
        reg_direct,

        /// <summary>
        /// use register as an address, e.g. @(r0)
        /// </summary>
        reg_deref,

        /// <summary>
        /// base offset scale (BOS) dereference  @(r0 + r1 * 4)  
        /// </summary>
        reg_deref_offset_scale,

        /// <summary>
        /// a program label, e.g. :funcExit:
        /// </summary>
        label,

        //simple dereference + dynamic register?
        //BOS + dynamic register?
    }

    /// <summary>
    /// Data type to use when evaluating the value of an operand
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// Undefined data type. The operand uses the word size/default data type of the CPU.
        /// kind of like size_t
        /// </summary>
        undef,

        /// <summary>
        /// evaluate operand in an 8bit signed context
        /// </summary>
        i8,

        /// <summary>
        /// evaluate operand in an 16bit signed context
        /// </summary>
        i16,

        /// <summary>
        /// evaluate operand in an 32bit signed context
        /// </summary>
        i32,

        /// <summary>
        /// evaluate operand in an 8bit unsigned context
        /// </summary>
        u8,

        /// <summary>
        /// evaluate operand in an 16bit unsigned context
        /// </summary>
        u16,

        /// <summary>
        /// evaluate operand in an 24bit unsigned context
        /// </summary>
        u24,

        /// <summary>
        /// evaluate operand in an 32bit unsigned context
        /// </summary>
        u32,

        /// <summary>
        /// evaluate operand in an 64bit unsigned context
        /// </summary>
        u64,

        /// <summary>
        /// evaluate operand in an F16 float context
        /// </summary>
        f16,

        /// <summary>
        /// evaluate operand in an F32 float context
        /// </summary>
        f32,
    }

    /// <summary>
    /// The scale op in a Base|Offset|Scale e.g. MUL = @(r0 + r1 * x)  
    /// </summary>
    enum ScaleType
    {
        /// <summary>
        /// No Scale. @(r0 + r1)  
        /// </summary>
        none,

        /// <summary>
        /// Multiply. @(r0 + r1 * x)  
        /// </summary>
        mul,

        /// <summary>
        /// Left shift. @(r0 + r1 &lt;&lt; x)  
        /// </summary>
        shl,

        /// <summary>
        /// Arithmetic right shift. @(r0 + r1 ASR &gt;&gt; x)  
        /// </summary>
        ishr,

        /// <summary>
        /// logical right shift. @(r0 + r1 &gt;&gt; x)  
        /// </summary>
        ushr,

        /// <summary>
        /// Rotate right @(r0 + r1 ROR x)  
        /// </summary>
        ror,

        /// <summary>
        /// Rotate right. @(r0 + r1 ROL X)  
        /// </summary>
        rol,
    }

    /// <summary>
    /// Extensions for enums: DataType, RegisterType
    /// </summary>
    internal static class OperandEnumHelpers
    {
        [NotNull]
        internal static string RegisterTypeToString(this RegisterType regType)
        {
            switch (regType)
            {
                case RegisterType.memory:
                    return "RAM ";
                case RegisterType.scalar_reg:
                case RegisterType.scalar_reg_base:
                    return "r";
                case RegisterType.vector_reg:
                    return "v";
                case RegisterType.program_counter:
                    return "PC";
                case RegisterType.stack_pointer:
                    return "SP";
                case RegisterType.frame_pointer:
                    return "FP";
                case RegisterType.link_register:
                    return "LR";
                case RegisterType.control_stack_pointer:
                    return "CSP";
                case RegisterType.control_stack_val:
                    return "ctrl_stack";
                default:
                    throw new NotImplementedException();
            }
        }

        internal static uint GetBitWidth(this DataType dataType,
                                         uint undefinedSize)
        {
            switch (dataType)
            {
                case DataType.i8:
                case DataType.u8:
                    return 8;
                case DataType.f16:
                case DataType.u16:
                case DataType.i16:
                    return 16;
                case DataType.u24:
                    return 24;
                case DataType.f32:
                case DataType.u32:
                case DataType.i32:
                    return 32;
                case DataType.u64:
                    return 64;
                case DataType.undef:
                    return undefinedSize;
                default:
                    string error = String.Format("Unknown OperandDataType {0}", dataType.ToString());
                    throw new ArgumentException(error, "datatype");
            }
        }

        internal static ulong GetDataTypeMask(this DataType dataType)
        {
            return dataType.GetBitWidth(64).GetMask();
        }

        internal static DataType DataTypeFromWidth(this uint size)
        {
            return DataTypeFromWidth((ulong) size);
        }

        internal static DataType DataTypeFromWidth(this ulong size)
        {
            DataType type;
            switch (size)
            {
                case 8: type = DataType.u8; break;
                case 16: type = DataType.u16; break;
                case 24: type = DataType.u24; break;
                case 32: type = DataType.u32; break;
                case 64: type = DataType.u64; break;
                default: throw new NotImplementedException();
            }
            return type;
        }

        internal static bool NeedsSignExtending(this DataType dataType)
        {
            switch (dataType)
            {
                case DataType.i8:
                case DataType.i16:
                case DataType.i32:
                    return true;
                case DataType.f32:
                case DataType.f16:
                case DataType.u8:
                case DataType.u16:
                case DataType.u24:
                case DataType.u32:
                case DataType.u64:
                    return false;
                case DataType.undef:
                    return true;
                default:
                    string error = String.Format("Unknown OperandDataType {0}", dataType.ToString());
                    throw new ArgumentException(error, "datatype");
            }
        }

        internal static bool IsValidMemType(this DataType dataType)
        {
            switch (dataType)
            {
                default:
                case DataType.f16:
                case DataType.f32:
                case DataType.i32:
                case DataType.i16:
                case DataType.i8:
                    return false;
                case DataType.u32:
                case DataType.u16:
                case DataType.undef:
                case DataType.u8:
                    return true;
            }
        }

        internal static void ThrowIfInvalidMemType(this DataType dataType)
        {
            //maybe http://stackoverflow.com/questions/4411109/how-to-create-an-instance-of-a-generic-type-argument-using-a-parameterized-const
            if (!dataType.IsValidMemType())
            {
                string err = string.Format("Address must be an unsigned or undef type. {0} given",
                                            dataType.ToString());
                throw new ArgumentException(err, "dataType");
            }
        }

        internal static int GetAddressAlignment(this DataType dataType)
        {
            ThrowIfInvalidMemType(dataType);

            switch (dataType)
            {
                case DataType.u32:
                    return 4;
                case DataType.u16:
                    return 2;
                //don't know if undef should be 'unaligned allowed' (aka 1),
                //datapath width (e.g. 4 on a 32bit cpu), 8 (to match ulong),
                default:
                case DataType.undef:
                case DataType.u8:
                    return 1;
            }
        }

        internal static int ThrowIfInvalidAlignment(this DataType dataType, ulong literal)
        {
            int alignment = dataType.GetAddressAlignment();
            if ((literal % (ulong)alignment) != 0)
            {
                string err = string.Format("for a {0} bit write address must be a multiple of {1}. Address given {2}",
                            dataType.GetBitWidth(0), alignment, literal);
                throw new ArgumentException(err, "address");
            }
            return alignment;
        }

        internal static int GetBankDimension(this RegisterType regType)
        {
            switch (regType)
            {
                case RegisterType.control_stack_pointer:
                case RegisterType.link_register:
                case RegisterType.stack_pointer:
                case RegisterType.frame_pointer:
                case RegisterType.program_counter:
                    return 0;
                case RegisterType.control_stack_val:
                case RegisterType.memory:
                case RegisterType.scalar_reg:
                    return 1;
                default:
                    throw new NotImplementedException();
            }
        }

    }

    abstract class Operand : IEquatable<Operand>
    {
        //literal : this.Literal
        //register direct : this.BaseReg
        //simple dereference : @(this.BaseReg) or @(literal)
        //base offset scale (BOS) dereference @(this.BaseReg + index OP scale)  (or literals in there)
        readonly public AddressMode AddressMode;

        //i8, u8, i16, u16 etc etc
        readonly public DataType DataType;

        protected Operand(AddressMode addressMode, DataType dataType)
        {
            this.AddressMode = addressMode;
            this.DataType = dataType;
        }


        #region to-string

        public override string ToString()
        {
            return this.ToString(DataType.undef);
        }

        [NotNull]
        public string ToString(DataType defaultType)
        {
            return "Operand: " + this.ToStringShort(defaultType);
        }

        public string ToStringShort()
        {
            return this.ToStringShort(DataType.undef);
        }

        public string ToStringShort(DataType defaultType)
        {
            string regString = this.ToStringShortDerived(defaultType);
            if (this.DataType != defaultType)
            {
                return String.Format("{0}{1}{2}{3}",
                                 this.DataType.ToString(), "{",
                                 regString, "}");
            }
            else
            {
                return regString;
            }
        }

        internal abstract string ToStringShortDerived(DataType defaultType);


        #endregion

        #region equality
        public abstract override bool Equals(object obj);

        public abstract bool Equals(Operand other);

        public abstract override int GetHashCode();
        #endregion
    }

    class OperandReg : Operand
    {
        readonly public bool DoesWriteBack; //one of the registers does writeback?
        readonly public bool IsDynamic;     //one of the registers is dynamic?

        //reg_direct, reg_deref, reg_deref_offset_scale
        readonly public ComplexRegister BaseReg;

        //reg_deref_offset_scale
        readonly public ComplexRegister Offset;

        readonly public ScaleType ScaleOp; //none, mul, shl, ishr, ushr, ror, rol,

        readonly public SimpleRegister Scale;


        public OperandReg(AddressMode mode, ComplexRegister baseReg, DataType dataType):
            base(mode, dataType)
        {
            Debug.Assert(baseReg != null);
            
            this.BaseReg = baseReg;

            this.DoesWriteBack = baseReg.DoesWriteback();
            this.IsDynamic = baseReg.IsDynamic();

            this.Offset = null;
            this.ScaleOp = ScaleType.none;
            this.Scale = null;
        }


        #region to-string
        internal override string ToStringShortDerived(DataType defaultType)
        {
            Debug.Assert(this.DoesWriteBack == false);
            Debug.Assert(this.IsDynamic == false);

            if (this.AddressMode == AddressMode.reg_direct)
            {
                return this.BaseReg.ToString();
            }
            else if (this.AddressMode == AddressMode.reg_deref)
            {
                return String.Format("@({0})", this.BaseReg.ToString());
            }
            else if (this.AddressMode == AddressMode.reg_deref_offset_scale)
            {
                throw new NotImplementedException();
            }
            else
            {
                throw new NotImplementedException();
            }

        }
        #endregion

        #region equality
        public override bool Equals(object obj)
        {
            return this.Equals(obj as OperandReg);
        }

        public override bool Equals(Operand other)
        {
            return this.Equals(other as OperandReg);
        }

        public bool Equals(OperandReg other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            Debug.Assert(this.Offset == null);
            Debug.Assert(this.ScaleOp == ScaleType.none);
            Debug.Assert(this.Scale == null);
            Debug.Assert(other.AddressMode == AddressMode.reg_deref ||
                            other.AddressMode == AddressMode.reg_direct);
            Debug.Assert(this.AddressMode == AddressMode.reg_deref ||
                            this.AddressMode == AddressMode.reg_direct);
            Debug.Assert(this.BaseReg != null);

            bool bEqual = this.AddressMode == other.AddressMode &&
                            this.DataType == other.DataType &&
                            this.DoesWriteBack == other.DoesWriteBack &&
                            this.IsDynamic == other.IsDynamic &&
                            this.BaseReg.Equals(other.BaseReg);
            return bEqual;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                Debug.Assert(this.BaseReg != null);
                Debug.Assert(this.Offset == null);
                Debug.Assert(this.ScaleOp == ScaleType.none);
                Debug.Assert(this.Scale == null);
                int hash = 17;
                hash = hash * 23 + this.AddressMode.GetHashCode();
                hash = hash * 23 + this.DataType.GetHashCode();
                hash = hash * 23 + this.DoesWriteBack.GetHashCode();
                hash = hash * 23 + this.IsDynamic.GetHashCode();
                hash = hash * 23 + this.BaseReg.GetHashCode();
                return hash;
            }
        }
        #endregion

        #region reg-factories

        /// <summary>
        /// Make `@(literal)`, `type{@(rN)}`
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg LiteralAddress(ulong literal,
                                                DataType dataType)
        {
            ulong masked = literal & dataType.GetDataTypeMask();
            if (masked != literal)
            {
                string err = String.Format("{0} too large to fit in type {1} : {2}",
                    literal,
                    dataType.ToString(),
                    dataType.GetDataTypeMask());
                throw new ArgumentException(err, "literal_address");
            }
            OperandEnumHelpers.ThrowIfInvalidAlignment(dataType, masked);
            OperandEnumHelpers.ThrowIfInvalidMemType(dataType);




            ulong signExt;
            if (dataType.NeedsSignExtending())
            {
                signExt = masked.SignExtendArb(dataType.GetBitWidth(64));
            }
            else
            {
                signExt = masked;
            }
            Debug.Assert((signExt & 0xFFFFFFFF) == signExt);
            var base_complex = new ComplexRegister((uint)signExt);

            OperandReg operand = new OperandReg(AddressMode.reg_deref, base_complex, dataType);
            return operand;
        }

        //todo: tidy up these functions? Just have one for temps, one for SP etc. Combine normal/deref etc.

        /// <summary>
        /// Make `rN` or `type{rN}`
        /// </summary>
        /// <param name="index"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        [NotNull]
        public static OperandReg Register(uint index,
                                          DataType dataType = DataType.undef)
        {
            var simple = SimpleRegister.Reg1D(RegisterType.scalar_reg, index: index);
            var complex = new ComplexRegister(simple);
            OperandReg operand = new OperandReg(AddressMode.reg_direct, complex, dataType);
            return operand;
        }

        /// <summary>
        /// Make `rN.i` or `type{rN.i}`
        /// </summary>
        /// <param name="index"></param>
        /// <param name="byteOffset"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        [NotNull]
        public static OperandReg Register(uint index,
                                          uint byteOffset,
                                          DataType dataType = DataType.undef)
        {
            var simple = SimpleRegister.Reg1D(RegisterType.scalar_reg, index, byteOffset);
            var complex = new ComplexRegister(simple);

            OperandReg operand = new OperandReg(AddressMode.reg_direct, complex, dataType);
            return operand;
        }

        /// <summary>
        /// Make `@(rN)`, `@(rN.i)`, `type{@(rN)}`, `type{@(rN.i)}`
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg RegisterDeref(uint index,
                                               uint byteOffset = 0,
                                               DataType dataType = DataType.undef)
        {
            OperandEnumHelpers.ThrowIfInvalidMemType(dataType);
            var base_simple = SimpleRegister.Reg1D(RegisterType.scalar_reg, index, byteOffset);
            var base_complex = new ComplexRegister(base_simple);
            OperandReg operand = new OperandReg(AddressMode.reg_deref, base_complex, dataType);
            return operand;
        }

        /// <summary>
        /// Make `PC`, `PC.i`, `@(PC)`, `@(PC.i)`
        /// 
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg ProgramCounter(uint byteOffset = 0, bool deref = false)
        {
            var base_simple = SimpleRegister.Reg0D(RegisterType.program_counter, offset: byteOffset);
            var base_complex = new ComplexRegister(base_simple);
            var addressMode = (deref) ? AddressMode.reg_deref : AddressMode.reg_direct;
            var operand = new OperandReg(addressMode, base_complex, DataType.u16);
            return operand;
        }

        /// <summary>
        /// Make `FP`, `FP.i`, `@(FP)`, `@(FP.i)`
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg FramePointer(uint byteOffset = 0, bool deref = false)
        {
            var base_simple = SimpleRegister.Reg0D(RegisterType.frame_pointer, offset: byteOffset);
            var base_complex = new ComplexRegister(base_simple);
            var addressMode = (deref) ? AddressMode.reg_deref : AddressMode.reg_direct;
            var operand = new OperandReg(addressMode, base_complex, DataType.u16);
            return operand;
        }

        /// <summary>
        /// Make `SP`, `SP.i`, `@(SP)`, `@(SP.i)`
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg StackPointer(uint byteOffset = 0, bool deref = false)
        {
            var base_simple = SimpleRegister.Reg0D(RegisterType.stack_pointer, offset: byteOffset);
            var base_complex = new ComplexRegister(base_simple);
            var addressMode = (deref) ? AddressMode.reg_deref : AddressMode.reg_direct;
            var operand = new OperandReg(addressMode, base_complex, DataType.u16);
            return operand;
        }

        /// <summary>
        /// Make `LR`, `LR.i`, `@(LR)`, `@(LR.i)`
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg LinkRegister(uint byteOffset = 0, bool deref = false)
        {
            var base_simple = SimpleRegister.Reg0D(RegisterType.link_register, offset: byteOffset);
            var base_complex = new ComplexRegister(base_simple);
            var addressMode = (deref) ? AddressMode.reg_deref : AddressMode.reg_direct;
            var operand = new OperandReg(addressMode, base_complex, DataType.u16);
            return operand;
        }

        /// <summary>
        /// Make `CSP`, `CSP.i`, `@(CSP)`, `@(CSP.i)`
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public static OperandReg ControlStackPointer(uint byteOffset = 0, bool deref = false)
        {
            var base_simple = SimpleRegister.Reg0D(RegisterType.control_stack_pointer, offset: byteOffset);
            var base_complex = new ComplexRegister(base_simple);
            var addressMode = (deref) ? AddressMode.reg_deref : AddressMode.reg_direct;
            var operand = new OperandReg(addressMode, base_complex, DataType.u16);
            return operand;
        }

        #endregion
    }

    class OperandLiteral : Operand
    {
        //for AddressMode.literal
        readonly private Float_Union Literal;

        /// <summary>
        /// as a 32 bit value
        /// </summary>
        /// <returns></returns>
        internal uint LiteralB32
        {
            get
            {
                Debug.Assert(this.AddressMode == AddressMode.literal);
                return this.Literal.u;
            }
        }

        public OperandLiteral(Float_Union literal, DataType dataType):
            base(AddressMode.literal, dataType)
        {
            this.Literal = literal;
        }

        #region to-string
        internal override string ToStringShortDerived(DataType defaultType)
        {
            Debug.Assert(this.AddressMode == AddressMode.literal);
            return String.Format("literal {0}", this.Literal.ToString());
        }
        #endregion

        #region equality
        public override bool Equals(object obj)
        {
            return this.Equals(obj as OperandLiteral);
        }

        public override bool Equals(Operand other)
        {
            return this.Equals(other as OperandLiteral);
        }

        public bool Equals(OperandLiteral other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            Debug.Assert(this.AddressMode == AddressMode.literal);
            Debug.Assert(other.AddressMode == AddressMode.literal);
            return this.Literal == other.Literal &&
                    this.DataType == other.DataType;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + this.AddressMode.GetHashCode();
                hash = hash * 23 + this.DataType.GetHashCode();
                hash = hash * 23 + this.Literal.GetHashCode();
                return hash;
            }
        }
        #endregion

        #region literal_factories
        // ----------- uint factory 

        [NotNull]
        internal static OperandLiteral u64(ulong val)
        {
            throw new NotImplementedException();
        }

        [NotNull]
        public static OperandLiteral undef(ulong val)
        {
            unchecked
            {
                uint val2 = (uint)val;

                if (val2 != val)
                {
                    if ((long)val < 0)
                    {
                        if (val2.SignExtend() != val)
                        {
                            throw new NotImplementedException();
                        }
                        else
                        {
                            //ok.
                        }
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }

                return new OperandLiteral(new Float_Union(val2), DataType.undef);
            }
        }

        [NotNull]
        public static OperandLiteral u32(uint val)
        {
            return new OperandLiteral(new Float_Union(val), DataType.u32);
        }

        [NotNull]
        public static OperandLiteral u16(ushort val)
        {
            uint u32 = val;
            return new OperandLiteral(new Float_Union(u32), DataType.u16);
        }

        [NotNull]
        public static OperandLiteral u8(byte val)
        {
            uint u32 = val;
            return new OperandLiteral(new Float_Union(u32), DataType.u8);
        }

        // ----------- int factory

        [NotNull]
        internal static OperandLiteral i64(long val)
        {
            throw new NotImplementedException();
        }

        [NotNull]
        public static OperandLiteral i32(int val)
        {
            return new OperandLiteral(new Float_Union(val), DataType.i32);
        }

        [NotNull]
        public static OperandLiteral i16(short val)
        {
            int i32 = val;
            return new OperandLiteral(new Float_Union(i32), DataType.i16);
        }

        [NotNull]
        public static OperandLiteral i8(sbyte val)
        {
            int i32 = val;
            return new OperandLiteral(new Float_Union(i32), DataType.i8);
        }

        // ----------- float factory

        [NotNull]
        public static OperandLiteral f32(float val)
        {
            return new OperandLiteral(new Float_Union(val), DataType.f32);
        }

        [NotNull]
        public static OperandLiteral d64(double val)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    class OperandLabel : Operand
    {
        [NotNull]
        public readonly string LabelText;

        public OperandLabel([NotNull] string labelText, DataType dataType = DataType.u16):
            base(AddressMode.label, dataType)
        {
            this.LabelText = labelText;
        }

        #region to-string
        internal override string ToStringShortDerived(DataType defaultType)
        {
            Debug.Assert(this.AddressMode == AddressMode.label);
            return String.Format("label {0}", this.LabelText.ToString());
        }
        #endregion

        #region equality
        public override bool Equals(object obj)
        {
            return this.Equals(obj as OperandLabel);
        }

        public override bool Equals(Operand other)
        {
            return this.Equals(other as OperandLabel);
        }

        public bool Equals(OperandLabel other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }

            Debug.Assert(this.AddressMode == AddressMode.label);
            Debug.Assert(other.AddressMode == AddressMode.label);
            return this.LabelText == other.LabelText;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + this.AddressMode.GetHashCode();
                hash = hash * 23 + this.DataType.GetHashCode();
                hash = hash * 23 + this.LabelText.GetHashCode();
                return hash;
            }
        }
        #endregion

    }

}