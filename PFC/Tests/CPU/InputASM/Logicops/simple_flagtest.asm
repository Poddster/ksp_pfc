                mov          r0, 0xFFB0125A           #0

                and!         r1, r0,  0xFFFFFFFF      #1   N---
                flags_read   r2                       #2
                assert_eq    r2, 0b1000               #3

                and32!       r1, r0,  0x000F0000      #4   -Z--
                flags_read   r2                       #5
                assert_eq    r2, 0b0100               #6

                and8!        r1, r0,  0xFFF8          #7   ----
                flags_read   r2                       #8
                assert_eq    r2, 0b0000               #9

                #notice z is set, even though u32{r1} is non zero
                mov32        r1, 0xAAAAAAAA           #10
                and8!        r1, r0, 0x00             #11  -Z--
                flags_read   r2                       #12
                assert_eq    r2, 0b0100               #13
                assert_eq    r1, 0xAAAAAA00           #14

                #set the vc flags
                mov          r0, 0x80                 #15
                mov32        r1, 0x000000000          #16
                iadd8!       r1, r0,  r0              #17  -zvc
                assert_eq    r1, 0x000                #18
                flags_read   r2                       #19
                assert_eq    r2, 0b0111               #20

                #now don't touch the vc flags
                and8!        r1, r0,  0xF0            #21  n-??
                assert_eq    r1, 0x80                 #22
                flags_read   r2                       #23
                assert_eq    r2, 0b1011               #24

                halt
