        mov         fp,     0x400
        mov         sp,     0x800

        
        
        push_data   5
        push_data   19
        push_data   4120
        call        :func1:
        stack_free  12
        assert_eq   fp, 0x400
        assert_eq   sp, 0x800
        halt
        
    :func1:
        # function doesn't really do anything useful, just checks 
        # local store works fine.
        #for every odd i to 80
        #    local_stack[i] = input0 + i
        #
        #for every even i to 80
        #    local_stack[i] == input1 + i
        #
        #accum = 0
        #for i 0 to 80
        #    accum += local_stack[i]
        #
        #assert input2 == accum
        
        frame_enter     320 #80*4

        ld32            r2,     fp              # r2 = arg2

        iadd            r4,     fp,     0x4     # r4 = &arg1
        ld32            r1,     r4              # r1 = arg1
        
        iadd            r4,     fp,     0x8     # r4 = &arg0
        ld32            r0,     r4              # r0 = arg0

        # use local_stack in a empty_descending style
        isub            r3,     fp,     0x4     # r3 = &local_stack
        
        
        #for every odd i to 80
        #    local_stack[i] = input0 + i
        mov             r4,     1               # r4: i = 1
        mov             r7,     4               # r7: i*4
:repeat-odd:
        iadd            r5,     r0,     r4      # r5 = input0 + i
        isub            r6,     r3,     r7      # r6 = local_stack + i*4
        st32            r6,     r5              # *r6 = r5
        iadd            r7,     r7,     8
        iadd            r4,     r4,     2
        icmp!           r4,     80
        bult            :repeat-odd:
        
        
        #for every even i to 80
        #    local_stack[i] == input1 + i
        mov             r4,     0               # r4: i = 0
        mov             r7,     0               # r7: i*4
:repeat-even:
        iadd            r5,     r1,     r4      # r5 = input1 + i
        isub            r6,     r3,     r7      # r6 = local_stack + i*4
        st32            r6,     r5              # *r6 = r5
        iadd            r7,     r7,     8
        iadd            r4,     r4,     2
        icmp!           r4,     80
        bult            :repeat-even:

        #accum = 0
        #for i 0 to 80
        #    accum += local_stack[i]
        mov             r0,     0               # r0: accum = 0
        mov             r4,     0               # r4: i*4     = 0
        
:repeat-accum:
        isub            r6,     r3,     r4      # r6 = local_stack + i*4
        ld32            r5,     r6              # r5 = *r6
        iadd            r0,     r0,     r5
        iadd            r4,     r4,     4
        icmp!           r4,     320             # 80*4 = 320
        bult            :repeat-accum:
        
        assert_eq       r0,     r2

        frame_leave
        ret

