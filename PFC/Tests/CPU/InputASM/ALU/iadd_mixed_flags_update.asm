	mov           r0,      0x12345678
	mov           r1,      0x20FEDCBA
												 #                       | NZVC
	iadd32!       r2,      r0,      r1           #2  r2 = 0x33333332     |-----
	assert_eq     r2,      0x33333332            #3                      |
	flags_read    r2                             #4                      |
	assert_eq     r2,      0b0000                #5                      |

	iadd8!        r3,      r0,      r1           #6  r3 = 0x32           |----C
	assert_eq     r3,      0x32                  #7                      |
	flags_read    r3                             #8                      |
	assert_eq     r3,      0b0001                #9                      |

	iadd32!       r4,      r0,   i8{r1}          #10 r4 = 0x12345632     |----C
	assert_eq     r4,      0x12345632            #11                     |
	flags_read    r4                             #12                     |
	assert_eq     r4,      0b0001                #13                     |

	iadd32!   i16{r5},     r0,      r1           #14 r5 = 0x3332         |-----
	assert_eq     r5,      0x3332                #15                     |
	flags_read    r5                             #16                     |
	assert_eq     r5,      0b0000                #17                     |

	iadd16!   i32{r6},     r0,      r1           #18  r6 = 0x00003332    |----C
	assert_eq     r6,      0x00003332            #19                     |
	flags_read    r6                             #20                     |
	assert_eq     r6,      0b0001                #21                     |

	iadd32!    i8{r7},  i8{r0},  i8{r1}          #22  r7 = 0x32          |----C
	assert_eq     r7,      0x32                  #23                     |
	flags_read    r7                             #24                     |
	assert_eq     r7,      0b0001                #25                     |

	iadd16!   i32{r8},     r0,      0xeeee5432   #26  r8 = 0xFFFFaaaa    |-N-V-
	assert_eq     r8,      0xFFFFaaaa            #27     (sign extended) |
	flags_read    r8                             #28                     |
	assert_eq     r8,      0b1010                #29                     |

	iadd16!   u32{r8},     r0,      0xeeee5432   #30  r8 = 0x0000aaaa    |-N-V-
	assert_eq     r8,      0x0000aaaa            #31  (NOT sign extended)|
	flags_read    r8                             #32                     |
	assert_eq     r8,      0b1010                #33                     |

	iadd8!    i32{r8}, i32{r0}, i32{0xeeee5432}  #34  r8 = 0xFFFFFFAA    |-N-V-
	assert_eq     r8,      0xFFFFFFAA            #35                     |
	flags_read    r8                             #36                     |
	assert_eq     r8,      0b1010                #37                     |

	halt                                         #38                     |
