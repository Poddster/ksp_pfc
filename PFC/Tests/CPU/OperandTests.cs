﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using PFC.CPU;

namespace PFC.Tests.CPU
{
    public static class OperandTests
    {
        /// <summary>
        /// Checks to make sure a test throws an exception.
        /// </summary>
        /// <typeparam name="exceptionType"></typeparam>
        /// <param name="func"></param>
        private static void AssertThrows<exceptionType>(Action func) where exceptionType : Exception
        {
            bool bPassed = false;
            try
            {
                func();
            }
            catch (exceptionType e)
            {
                Debug.Assert(e.Message != null); //just to stop compiler complaining e is not used
                bPassed = true;
            }
            Debug.Assert(bPassed);
        }



        private static void CPUTest_Operand_Reg_Everything()
        {
            //rN
            //rN.i
            //@(rN)
            //@(rN.i)

            //type{rN}
            //type{rN.i}
            //type{@(rN)}
            //type{@(rN.i)}
        }

        private static void CPUTest_Operand_Reg_Index_Dynamic()
        {
            //does dynamic/dereference, dynamic/indexing, dynamic/writeback
            throw new NotImplementedException();
        }

        private static void CPUTest_Operand_Reg_Dynamic()
        {
            //r[base + index]
            throw new NotImplementedException();
        }

        private static void CPUTest_Operand_Reg_Writeback()
        {
            //++,--,!
            throw new NotImplementedException();
        }

        private static void CPUTest_Operand_Reg_Indexing()
        {
            //@(base + index * scale)
            //@(base + index << scale)  shl, ishr, ushr, ror, rol
            throw new NotImplementedException();
        }

        private static void CPUTest_Operand_Reg_Dereference()
        {
            //@(rN)
            //@(rN.i)

            {
                CPUState state = new CPUState(0, 32, new Instruction[0]);
                List<CPURegMemStateChange> changes = new List<CPURegMemStateChange>
                {
                    CPURegMemStateChange.MemChange32(0x0, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x4, 0x12345678), // the value we're after
                    CPURegMemStateChange.MemChange32(0x8, 0xBADDF00D),

                    CPURegMemStateChange.RegChange(4, 0x04),
                    CPURegMemStateChange.RegChange(5, 0x400),
                };
                state.TESTONLY_ApplyStateChanges(changes);

                var d_r4 = OperandReg.RegisterDeref(4);
                //shoud probably overload == for SimpleRegister
                var d_r4_eff = state.ReduceRegOperand(d_r4, DataType.undef);
                Debug.Assert(d_r4_eff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index:0x4)));
                Debug.Assert(state.GetValueB32(d_r4) == 0x12345678);
            }
            {
                CPUState state = new CPUState(0, 32, new Instruction[0]);
                List<CPURegMemStateChange> changes = new List<CPURegMemStateChange>
                {
                    CPURegMemStateChange.MemChange32(0x0, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x4, 0x12345678),
                    CPURegMemStateChange.MemChange32(0x8, 0xBADDF00D), // the value we're after

                    CPURegMemStateChange.RegChange(4, 0x0800),
                    CPURegMemStateChange.RegChange(5, 0x1),
                };
                state.TESTONLY_ApplyStateChanges(changes);

                //can't read r6.1 as it's 32bit
                //can read u16{@(r6.1)}, so let's do that. That should give us 0x08 from "r4 >> 8"
                //the idea is that r6.1 is evaluated at 16 bit, as is @(val). Should return a 16 bit address.

                var d_r4_1_u16 = OperandReg.RegisterDeref(4, 1, DataType.u16);
                var d_r4_1_u16_eff = state.ReduceRegOperand(d_r4_1_u16, DataType.undef);
                Debug.Assert(d_r4_1_u16_eff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index:0x8)));
                Debug.Assert(state.GetValueB16(d_r4_1_u16) == 0x0000F00D);
                Debug.Assert(state.GetValueB32(d_r4_1_u16) == 0x0000F00D);
            }

            {
                //test @(literal)
                CPUState state = new CPUState(0, 32, new Instruction[0]);
                List<CPURegMemStateChange> changes = new List<CPURegMemStateChange>
                {
                    CPURegMemStateChange.MemChange32(0x0000, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x0004, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x0008, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x000C, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x0010, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x0014, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x0018, 0xDEADBEEF),
                    CPURegMemStateChange.MemChange32(0x001C, 0xC3C2C1C0),
                    CPURegMemStateChange.MemChange32(0x0020, 0xDEADBEEF),

                    CPURegMemStateChange.MemChange32(0x0080, 0x04030201),
                    CPURegMemStateChange.MemChange32(0x1504, 0xD3D2D1D0),
                    CPURegMemStateChange.MemChange32(0x8080, 0xE3E2E1E0),
                    CPURegMemStateChange.MemChange32(0x8E00, 0xF3F2F1F0),
                };
                state.TESTONLY_ApplyStateChanges(changes);


                //too big
                AssertThrows<ArgumentException>(() => OperandReg.LiteralAddress(0x200, DataType.u8));

                //i8{@(0x1)}, f32{@(1.0)}
                //When the first operand is evaluated, it's going to try and pull
                //1 byte over the bus from a signed address, which is not allowed,
                //even though the address if positive. f32 is definitely not
                //allowed as a mem address :)
                AssertThrows<ArgumentException>(() => OperandReg.LiteralAddress(0x01, DataType.i8));
                AssertThrows<ArgumentException>(() => OperandReg.LiteralAddress(0x3f800000, DataType.f32));

                //u32{@(0x1)}. When this operand is evaluated, it's going to try and pull
                //4 bytes over the buss, which is an unaligned access.
                AssertThrows<ArgumentException>(() => OperandReg.LiteralAddress(0x01, DataType.u32));
                //i16{@(0x1501)}
                AssertThrows<ArgumentException>(() => OperandReg.LiteralAddress(0x1501, DataType.i16));


                var lit_undef = OperandReg.LiteralAddress(0x8E00, DataType.undef); //    @(0x8E00)
                var lit_u8 = OperandReg.LiteralAddress(0x1C, DataType.u8);         // u8{@(0x1c)}
                var lit_u16 = OperandReg.LiteralAddress(0x1504, DataType.u16);     //u16{@(0x1504)}
                var lit_u32 = OperandReg.LiteralAddress(0x8080, DataType.u32);     //u32{@(0x8080)}

                //check that the literal addresses decay into memory registers
                Debug.Assert(state.ReduceRegOperand(lit_u8, DataType.undef).Equals(
                                    SimpleRegister.Reg1D(RegisterType.memory, index:0x1C)));
                Debug.Assert(state.ReduceRegOperand(lit_u16, DataType.undef).Equals(
                                    SimpleRegister.Reg1D(RegisterType.memory, index:0x1504)));
                Debug.Assert(state.ReduceRegOperand(lit_u32, DataType.undef).Equals(
                                    SimpleRegister.Reg1D(RegisterType.memory, index:0x8080)));
                Debug.Assert(state.ReduceRegOperand(lit_undef, DataType.undef).Equals(
                                    SimpleRegister.Reg1D(RegisterType.memory, index:0x8E00)));

                // u8{@(0x1c)} => u8{MEM 0x1C} => 0xC0
                Debug.Assert(state.EvaluateOperandValue(lit_u8) == 0xC0);

                Debug.Assert(state.GetValueB32(lit_u8) == 0xC0);
                Debug.Assert(state.GetValueB32(lit_u16) == 0xD1D0);
                Debug.Assert(state.GetValueB32(lit_u32) == 0xE3E2E1E0);
                Debug.Assert(state.GetValueB32(lit_undef) == 0xF3F2F1F0);

                //that this just gets the lower 8bits of of 0x8080,
                //and does not get the memory at address 0x0080 :)
                //Remember, the operand is evaluated in it's context,
                //which would be u32{0x8080}, and then put onto the 8 bit bus.
                Debug.Assert(state.GetValueB8(lit_u32) == 0xE0);

            }


        }

        private static void CPUTest_Operand_Reg_DataTypes()
        {
            //type {rN} e.g. i8 {r0}
            //I don't really know the full scope of what it means to have
            //a data type on an operand. So not many tests yet.

            //I don't think this tests anything that CPUTest_Operand_Reg_ByteOffset doesn't also test.

            CPUState state32 = new CPUState(0, 32, new Instruction[0]);

            //test u16
            {
                var dest_r4_u16 = OperandReg.Register(4, DataType.u16);
                var dest_r4_u16_eff = state32.ReduceRegOperand(dest_r4_u16, DataType.undef);

                //value is too big for u16
                AssertThrows<ArgumentException>(() => new CPURegMemStateChange(dest_r4_u16.DataType,
                                                                         dest_r4_u16_eff,
                                                                         0xEDEDEDED));

                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0x0));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0x0);
                //0xEDED needs to be cast to i16, otherwise c# makes it a i32 of value 0x0000EDED
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_u16.DataType,
                                                            dest_r4_u16_eff,
                                                            0xEDED));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0x0000EDED);



                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xFFFFFFFF));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xFFFFFFFF);
                //0xEDED needs to be cast to i16, otherwise c# makes it a i32 of value 0x0000EDED
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_u16.DataType,
                                                            dest_r4_u16_eff,
                                                            0xEDED));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xFFFFEDED);

                //even though it's i16, it should only fill the last 16 bits. The sign extension
                //shouldn't destroy the upper bits
                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xAAAAAAAA));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xAAAAAAAA);
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_u16.DataType,
                                                            dest_r4_u16_eff,
                                                            0xFFFF));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xAAAAFFFF);
            }



            //test i16
            {
                var dest_r4_i16 = OperandReg.Register(4, DataType.i16);
                var dest_r4_i16_eff = state32.ReduceRegOperand(dest_r4_i16, DataType.undef);

                //value is too big for i16
                AssertThrows<ArgumentException>(() => new CPURegMemStateChange(dest_r4_i16.DataType,
                                                                         dest_r4_i16_eff,
                                                                         0xEDEDEDED));

                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0x0));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0x0);
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_i16.DataType,
                                                            dest_r4_i16_eff,
                                                            0xEDED));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0x0000EDED);



                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xFFFFFFFF));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xFFFFFFFF);
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_i16.DataType,
                                                            dest_r4_i16_eff,
                                                            0xEDED));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xFFFFEDED);

                //even though it's i16, it should only fill the last 16 bits. The sign extension
                //shouldn't destroy the upper bits
                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xAAAAAAAA));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xAAAAAAAA);
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_i16.DataType,
                                                            dest_r4_i16_eff,
                                                            unchecked((ulong)-1) & 0xFFFF));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(4)) == 0xAAAAFFFF);
            }

            //TODO: test these things
            var r6_i8 = OperandReg.Register(6, 1, DataType.i8);
            var r9_f32 = OperandReg.Register(9, DataType.f32);
            var r1_undef = OperandReg.Register(1, DataType.undef);




            //EvaluateOperandValue
            {
                var r0_undef = OperandReg.Register(0, DataType.undef);
                var r0_u32 = OperandReg.Register(0, DataType.u32);
                var r0_i16 = OperandReg.Register(0, DataType.i16);
                var r0_u8 = OperandReg.Register(0, DataType.u8);
                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xA1C2D3E4));

                //can't evaluate an undef operand without giving it a context.
                AssertThrows<InvalidOperationException>(() => state32.EvaluateOperandValue(r0_undef));
                AssertThrows<InvalidOperationException>(() => state32.EvaluateOperandValue(r0_undef,
                                                                                           DataType.undef));
                //an undef operand should evaluate in the context of the instruction's data type.
                Debug.Assert(state32.EvaluateOperandValue(r0_undef, DataType.u32) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_undef, DataType.i16) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_undef, DataType.u8) == 0x00000000000000E4);

                //an operand always uses it's own datatype if it has one.
                Debug.Assert(state32.EvaluateOperandValue(r0_u32, DataType.u32) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u32, DataType.f32) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u32, DataType.i16) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u32, DataType.u8) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u32, DataType.undef) == 0x00000000A1C2D3E4);

                Debug.Assert(state32.EvaluateOperandValue(r0_i16, DataType.u32) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_i16, DataType.f32) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_i16, DataType.f16) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_i16, DataType.i16) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_i16, DataType.u8) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_i16, DataType.undef) == 0xFFFFFFFFFFFFD3E4);

                Debug.Assert(state32.EvaluateOperandValue(r0_u8, DataType.u32) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u8, DataType.i16) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u8, DataType.i8) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u8, DataType.u8) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(r0_u8, DataType.undef) == 0x00000000000000E4);
            }

            {
                var lit_undef = OperandLiteral.undef(0xA1C2D3E4);
                var lit_u32 = OperandLiteral.u32(0xA1C2D3E4);
                var lit_i16 = OperandLiteral.i16(unchecked((short)0xD3E4));
                var lit_u8 = OperandLiteral.u8(0xE4);
                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xA1C2D3E4));

                //can't evaluate an undef operand without giving it a context.
                AssertThrows<InvalidOperationException>(() => state32.EvaluateOperandValue(lit_undef));
                AssertThrows<InvalidOperationException>(() => state32.EvaluateOperandValue(lit_undef,
                                                                                           DataType.undef));
                //an undef operand should evaluate in the context of the instruction's data type.
                Debug.Assert(state32.EvaluateOperandValue(lit_undef, DataType.u32) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_undef, DataType.i16) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_undef, DataType.u8) == 0x00000000000000E4);

                //an operand always uses it's own datatype if it has one.
                Debug.Assert(state32.EvaluateOperandValue(lit_u32, DataType.u32) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u32, DataType.f32) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u32, DataType.i16) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u32, DataType.u8) == 0x00000000A1C2D3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u32, DataType.undef) == 0x00000000A1C2D3E4);

                Debug.Assert(state32.EvaluateOperandValue(lit_i16, DataType.u32) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_i16, DataType.f32) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_i16, DataType.f16) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_i16, DataType.i16) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_i16, DataType.u8) == 0xFFFFFFFFFFFFD3E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_i16, DataType.undef) == 0xFFFFFFFFFFFFD3E4);

                Debug.Assert(state32.EvaluateOperandValue(lit_u8, DataType.u32) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u8, DataType.i16) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u8, DataType.i8) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u8, DataType.u8) == 0x00000000000000E4);
                Debug.Assert(state32.EvaluateOperandValue(lit_u8, DataType.undef) == 0x00000000000000E4);
            }

        }

        private static void CPUTest_Operand_Reg_ByteOffset()
        {
            // rN.1
            CPUState state32 = new CPUState(0, 32, new Instruction[0]);
            CPUState state64 = new CPUState(0, 64, new Instruction[0]);

            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(6, 0xDEADBEEF));
            state64.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(6, 0xFEDCBA0987654321));

            //r6.0
            var r6 = OperandReg.Register(6, 0);
            Debug.Assert(state32.GetValueB8(r6) == 0xEF);
            Debug.Assert(state32.GetValueB16(r6) == 0xBEEF);
            Debug.Assert(state32.GetValueB24(r6) == 0xADBEEF);
            Debug.Assert(state32.GetValueB32(r6) == 0xDEADBEEF);
            Debug.Assert(state64.GetValueB8(r6) == 0x21);
            Debug.Assert(state64.GetValueB16(r6) == 0x4321);
            Debug.Assert(state64.GetValueB24(r6) == 0x654321);
            Debug.Assert(state64.GetValueB32(r6) == 0x87654321);
            Debug.Assert(state64.GetValueB64(r6) == 0xFEDCBA0987654321);


            //r6_1.1
            var r6_1 = OperandReg.Register(6, 1, DataType.undef);
            Debug.Assert(state32.GetValueB8(r6_1) == 0xBE);
            Debug.Assert(state32.GetValueB16(r6_1) == 0xADBE);
            Debug.Assert(state32.GetValueB24(r6_1) == 0xDEADBE);
            //can't read r6_1.b1-b4 on a 32bit cpu
            AssertThrows<InvalidOperationException>(() => state32.GetValueB32(r6_1));
            Debug.Assert(state64.GetValueB8(r6_1) == 0x43);
            Debug.Assert(state64.GetValueB16(r6_1) == 0x6543);
            Debug.Assert(state64.GetValueB24(r6_1) == 0x876543);
            Debug.Assert(state64.GetValueB32(r6_1) == 0x09876543);
            //can't read r6_1.b1-b8 on a 64bit cpu
            AssertThrows<InvalidOperationException>(() => state64.GetValueB64(r6_1));


            //undef r6_3.3
            var r6_3 = OperandReg.Register(6, 3, DataType.undef);
            Debug.Assert(state32.GetValueB8(r6_3) == 0xDE);
            AssertThrows<InvalidOperationException>(() => state32.GetValueB16(r6_3));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB24(r6_3));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB32(r6_3));
            Debug.Assert(state64.GetValueB8(r6_3) == 0x87);
            Debug.Assert(state64.GetValueB16(r6_3) == 0x0987);
            Debug.Assert(state64.GetValueB24(r6_3) == 0xBA0987);
            Debug.Assert(state64.GetValueB32(r6_3) == 0xDCBA0987);
            AssertThrows<InvalidOperationException>(() => state64.GetValueB64(r6_3));



            //test operand as a destination.


            {
                //r4.1 should fail on both 32 and 64 bit, as no typesize is given, so the maximum is assumed.
                var dest_r4_4_undef = OperandReg.Register(4, 1, DataType.undef);
                var dest_r4_4_undef_eff_32 = state32.ReduceRegOperand(dest_r4_4_undef, DataType.undef);
                var dest_r4_4_undef_eff_64 = state64.ReduceRegOperand(dest_r4_4_undef, DataType.undef);

                AssertThrows<InvalidOperationException>(() =>
                                                        state32.TESTONLY_ApplyStateChange(
                                                            new CPURegMemStateChange(DataType.undef,
                                                                               dest_r4_4_undef_eff_32,
                                                                               0xA5A5A5A5)));
                AssertThrows<InvalidOperationException>(() =>
                                                        state64.TESTONLY_ApplyStateChange(
                                                            new CPURegMemStateChange(DataType.undef,
                                                                                dest_r4_4_undef_eff_64,
                                                                                0xA5A5A5A5)));
            }

            {
                //test generic r4.0 writing
                var dest_r4_0 = OperandReg.Register(4, 0);

                //setup data
                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xDEADBEEF));
                state64.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xFEDCBA0987654321));
                Debug.Assert(state32.GetValueB32(dest_r4_0) == 0xDEADBEEF);
                Debug.Assert(state64.GetValueB64(dest_r4_0) == 0xFEDCBA0987654321);

                //change r4's state on 32bit cpu
                state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_0.DataType,
                                                            state32.ReduceRegOperand(dest_r4_0, DataType.undef),
                                                            0xA5A5A5A5));
                Debug.Assert(state32.GetValueB32(dest_r4_0) == 0xA5A5A5A5);

                //change r4's state on 64bit cpu
                state64.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_0.DataType,
                                                            state64.ReduceRegOperand(dest_r4_0, DataType.undef),
                                                            0xA5A5A5A5));
                Debug.Assert(state64.GetValueB32(dest_r4_0) == 0xA5A5A5A5);
                Debug.Assert(state64.GetValueB64(dest_r4_0) == 0x00000000A5A5A5A5);
                Debug.Assert(state64.RegisterBank[4] == 0x00000000A5A5A5A5);
            }

            {
                //test writing to lower 32 bits only on 64 bit machine
                var dest_r4_0_u32 = OperandReg.Register(4, 0, DataType.u32);

                //setup data
                state64.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xFEDCBA0987654321));
                Debug.Assert(state64.GetValueB64(dest_r4_0_u32) == 0x0000000087654321);
                Debug.Assert(state64.RegisterBank[4] == 0xFEDCBA0987654321);

                //change u32{r4}'s state on 64bit cpu
                state64.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_0_u32.DataType,
                                                            state64.ReduceRegOperand(dest_r4_0_u32, DataType.undef),
                                                            0xA5A5A5A5));
                Debug.Assert(state64.GetValueB32(dest_r4_0_u32) == 0xA5A5A5A5);
                Debug.Assert(state64.GetValueB64(dest_r4_0_u32) == 0x00000000A5A5A5A5);
                Debug.Assert(state64.RegisterBank[4] == 0xFEDCBA09A5A5A5A5);
            }


            {
                //now try and write to the upper 32 bits
                var dest_r4_4_32b = OperandReg.Register(4, 4, DataType.u32);

                state64.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xFEDCBA0987654321));
                Debug.Assert(state64.GetValueB64(OperandReg.Register(4)) == 0xFEDCBA0987654321);
                Debug.Assert(state64.RegisterBank[4] == 0xFEDCBA0987654321);

                //this should fail on the 32 bit machine.
                AssertThrows<InvalidOperationException>(() =>
                    state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_4_32b.DataType,
                                                                state32.ReduceRegOperand(dest_r4_4_32b, DataType.undef),
                                                                0x73737373)));

                state64.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_4_32b.DataType,
                                                            state64.ReduceRegOperand(dest_r4_4_32b, DataType.undef),
                                                            0x73737373));
                Debug.Assert(state64.GetValueB32(dest_r4_4_32b) == 0x73737373);

                Debug.Assert(state64.RegisterBank[4] == 0x7373737387654321);

            }



            {
                //test an unaligned write.
                var dest_r4_4_32b = OperandReg.Register(4, 2, DataType.u32);

                state64.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(4, 0xFEDCBA0987654321));
                Debug.Assert(state64.GetValueB64(OperandReg.Register(4)) == 0xFEDCBA0987654321);
                Debug.Assert(state64.RegisterBank[4] == 0xFEDCBA0987654321);

                //this should fail on the 32 bit machine.
                AssertThrows<InvalidOperationException>(() =>
                    state32.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_4_32b.DataType,
                                                                state32.ReduceRegOperand(dest_r4_4_32b, DataType.undef),
                                                                0x73737373)));

                state64.TESTONLY_ApplyStateChange(new CPURegMemStateChange(dest_r4_4_32b.DataType,
                                                            state64.ReduceRegOperand(dest_r4_4_32b, DataType.undef),
                                                            0x00000000));
                Debug.Assert(state64.GetValueB32(dest_r4_4_32b) == 0x00000000);

                Debug.Assert(state64.RegisterBank[4] == 0xFEDC000000004321);

            }


            {

                state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xFFFFFFFF));
                Debug.Assert(state32.GetValueB32(OperandReg.Register(0)) == 0xFFFFFFFF);
                Debug.Assert(state32.RegisterBank[0] == 0x00000000FFFFFFFF);


                var r0_0_i8 = OperandReg.Register(0, 0, DataType.i8);
                var r0_1_i16 = OperandReg.Register(0, 1, DataType.i16);
                var r0_3_i8 = OperandReg.Register(0, 3, DataType.i8);

                List<CPURegMemStateChange> changes = new List<CPURegMemStateChange> {
                    new CPURegMemStateChange(r0_0_i8.DataType, state32.ReduceRegOperand(r0_0_i8, DataType.undef), 0x80),
                    new CPURegMemStateChange(r0_1_i16.DataType, state32.ReduceRegOperand(r0_1_i16, DataType.undef), 0x9999),
                    new CPURegMemStateChange(r0_3_i8.DataType, state32.ReduceRegOperand(r0_3_i8, DataType.undef), 0x01),
                };

                state32.TESTONLY_ApplyStateChanges(changes);

                Debug.Assert(state32.GetValueB32(OperandReg.Register(0)) == 0x01999980);
                Debug.Assert(state32.RegisterBank[0] == 0x000000001999980);
            }

        }

        private static void CPUTest_Operand_Reg_Direct()
        {
            unchecked
            {
                //test operands of the form 'rN'
                CPUState state = new CPUState(0, 32, new Instruction[0]);

                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xAABBCCDD));


                //create r0 operand
                var r0 = OperandReg.Register(0);

                //check that it doesn't have any fancy indexing and is in fact r0
                var r0_eff = state.ReduceRegOperand(r0, DataType.undef);
                Debug.Assert(r0_eff.Equals(SimpleRegister.Reg1D(RegisterType.scalar_reg, index:0)));

                //not sure about the usefulness of this check
                //I just want to make sure to string doesn't explode
                //ToString might return all sorts of stuff in future...
                Debug.Assert("r0" == r0_eff.ToString());
                Debug.Assert(!String.IsNullOrEmpty(r0.ToString()));

                Debug.Assert(state.GetValueB32(r0) == 0xAABBCCDD);
                Debug.Assert(state.GetValueB24(r0) == 0xBBCCDD);
                Debug.Assert(state.GetValueB16(r0) == 0xCCDD);
                Debug.Assert(state.GetValueB8(r0) == 0xDD);



                //r1 is i32
                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(1, (uint)-8));
                var r1_i32 = OperandReg.Register(1, DataType.i32);

                Debug.Assert(state.GetValueB32(r1_i32) == (uint)-8);
                Debug.Assert(state.GetValueB24(r1_i32) == ((uint)-8 & 0xFFFFFF));
                Debug.Assert(state.GetValueB16(r1_i32) == (ushort)-8);
                Debug.Assert(state.GetValueB8(r1_i32) == (byte)-8);

                //r2 is i8
                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(2, (uint)-2));
                var r2_i8 = OperandReg.Register(2, DataType.i8);

                Debug.Assert(state.GetValueB32(r2_i8) == 0xFFFFFFFE);
                Debug.Assert(state.GetValueB24(r2_i8) == 0xFFFFFE);
                Debug.Assert(state.GetValueB16(r2_i8) == 0xFFFE);
                Debug.Assert(state.GetValueB8(r2_i8) == 0xFE);



                //r3 is i32
                var fi = new Float_Union(1.0f);
                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(3, fi.u));
                var r3_f32 = OperandReg.Register(3, DataType.f32);

                Debug.Assert(state.GetValueB32(r3_f32) == (uint)fi.u);
                Debug.Assert(state.GetValueB24(r3_f32) == ((uint)fi.u & 0xFFFFFF));
                Debug.Assert(state.GetValueB16(r3_f32) == (ushort)fi.u);
                Debug.Assert(state.GetValueB8(r3_f32) == (byte)fi.u);




                //Tests sizes on different CPU states


            }
        }

        private static void CPUTest_Operand_Registers()
        {
            //need a test to test GetHashCode and Equals
            CPUTest_Operand_Reg_Direct();
            CPUTest_Operand_Reg_DataTypes();
            CPUTest_Operand_Reg_ByteOffset();
            CPUTest_Operand_Reg_Dereference();
            //CPUTest_Operand_Reg_Indexing();
            //CPUTest_Operand_Reg_Writeback();
            //CPUTest_Operand_Reg_Dynamic();
            //CPUTest_Operand_Reg_Index_Dynamic();
            CPUTest_Operand_Reg_Everything();
        }

        private static void CPUTest_Operand_Labels()
        {
            var label = new OperandLabel(":hello:");
        }

        private static void CPUTest_Operand_ProgramCounter()
        {
            unchecked
            {
                CPUState state = new CPUState(0, 32, new Instruction[0]);

                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange0D(RegisterType.program_counter, 0xCCDA));

                //create PC operand
                var PC = OperandReg.ProgramCounter();

                //check that it doesn't have any fancy indexing and is in fact PC
                var PC_eff = state.ReduceRegOperand(PC, DataType.undef);
                Debug.Assert(PC_eff.Equals(SimpleRegister.Reg0D(RegisterType.program_counter)));

                //not sure about the usefulness of this check
                //I just want to make sure to string doesn't explode
                //ToString might return all sorts of stuff in future...
                Debug.Assert("PC" == PC_eff.ToString());
                Debug.Assert(!String.IsNullOrEmpty(PC.ToString()));

                Debug.Assert(state.GetValueB32(PC) == 0x0000CCDA);
                Debug.Assert(state.GetValueB24(PC) == 0x00CCDA);
                Debug.Assert(state.GetValueB16(PC) == 0xCCDA);
                Debug.Assert(state.GetValueB8(PC) == 0xDA);


                //Ensure that PC and r15 are not aliases.
                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(15, 0x12345678));
                var r15 = OperandReg.Register(15);
                Debug.Assert(state.GetValueB32(r15) == 0x12345678);
                Debug.Assert(state.GetValueB32(PC) == 0x0000CCDA);

                var pcDeref = OperandReg.ProgramCounter(deref: true);
                Debug.Assert(pcDeref.AddressMode == AddressMode.reg_deref);
                var pcDerefEff = state.ReduceRegOperand(pcDeref, DataType.undef);
                Debug.Assert(pcDerefEff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index: 0xCCDA)));

                //todo: more tests

            }
        }

        private static void CPUTest_Operand_StackPointer()
        {
            unchecked
            {
                CPUState state = new CPUState(0, 32, new Instruction[0]);

                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, 0x8080));

                //create SP operand
                var SP = OperandReg.StackPointer();

                //check that it doesn't have any fancy indexing and is in fact SP
                var SP_eff = state.ReduceRegOperand(SP, DataType.undef);
                Debug.Assert(SP_eff.Equals(SimpleRegister.Reg0D(RegisterType.stack_pointer)));

                //not sure about the usefulness of this check
                //I just want to make sure to string doesn't explode
                //ToString might return all sorts of stuff in future...
                Debug.Assert("SP" == SP_eff.ToString());
                Debug.Assert(!String.IsNullOrEmpty(SP.ToString()));

                Debug.Assert(state.GetValueB32(SP) == 0x00008080);
                Debug.Assert(state.GetValueB24(SP) == 0x008080);
                Debug.Assert(state.GetValueB16(SP) == 0x8080);
                Debug.Assert(state.GetValueB8(SP) == 0x80);


                //Ensure that SP and r13 are not aliases.
                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(13, 0x12345678));
                var r13 = OperandReg.Register(13);
                Debug.Assert(state.GetValueB32(r13) == 0x12345678);
                Debug.Assert(state.GetValueB32(SP) == 0x00008080);


                var spDeref = OperandReg.StackPointer(deref: true);
                Debug.Assert(spDeref.AddressMode == AddressMode.reg_deref);
                var spDerefEff = state.ReduceRegOperand(spDeref, DataType.undef);
                Debug.Assert(spDerefEff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index: 0x8080)));

                //todo: more tests

            }
        }

        private static void CPUTest_Operand_LinkRegister()
        {
            unchecked
            {
                var state = new CPUState(0, 32, new Instruction[0]);

                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange0D(RegisterType.link_register, 0x8080));

                // create LR operand
                var linkRegister = OperandReg.LinkRegister();

                // check that it doesn't have any fancy indexing and is in fact LR
                var LR_eff = state.ReduceRegOperand(linkRegister, DataType.undef);
                Debug.Assert(LR_eff.Equals(SimpleRegister.Reg0D(RegisterType.link_register)));

                // not sure about the usefulness of this check
                // I just want to make sure to string doesn't explode
                // ToString might return all sorts of stuff in future...
                Debug.Assert("LR" == LR_eff.ToString());
                Debug.Assert(!String.IsNullOrEmpty(linkRegister.ToString()));

                Debug.Assert(state.GetValueB32(linkRegister) == 0x00008080);
                Debug.Assert(state.GetValueB24(linkRegister) == 0x008080);
                Debug.Assert(state.GetValueB16(linkRegister) == 0x8080);
                Debug.Assert(state.GetValueB8(linkRegister) == 0x80);


                // Ensure that LR and r14 are not aliases.
                state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(14, 0x12345678));
                var r14 = OperandReg.Register(14);
                Debug.Assert(state.GetValueB32(r14) == 0x12345678);
                Debug.Assert(state.GetValueB32(linkRegister) == 0x00008080);


                var lrDeref = OperandReg.LinkRegister(deref: true);
                Debug.Assert(lrDeref.AddressMode == AddressMode.reg_deref);
                var lrDerefEff = state.ReduceRegOperand(lrDeref, DataType.undef);
                Debug.Assert(lrDerefEff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index: 0x8080)));


                //todo: more tests


            }
        }

        private static void CPUTest_Operand_ControlStackPointer()
        {
            unchecked
            {
                var state = new CPUState(0, 32, new Instruction[0]);

                state.TESTONLY_ApplyStateChange(
                    CPURegMemStateChange.RegChange0D(RegisterType.control_stack_pointer, 0x8080));

                //create CSP operand
                var CSP = OperandReg.ControlStackPointer();

                //check that it doesn't have any fancy indexing and is in fact CSP
                var CSP_eff = state.ReduceRegOperand(CSP, DataType.undef);
                Debug.Assert(CSP_eff.Equals(SimpleRegister.Reg0D(RegisterType.control_stack_pointer)));

                //not sure about the usefulness of this check
                //I just want to make sure to string doesn't explode
                //ToString might return all sorts of stuff in future...
                Debug.Assert("CSP" == CSP_eff.ToString());
                Debug.Assert(!String.IsNullOrEmpty(CSP.ToString()));

                Debug.Assert(state.GetValueB32(CSP) == 0x00008080);
                Debug.Assert(state.GetValueB24(CSP) == 0x008080);
                Debug.Assert(state.GetValueB16(CSP) == 0x8080);
                Debug.Assert(state.GetValueB8(CSP) == 0x80);


                //Ensure that CSP and SP are not aliases.
                state.TESTONLY_ApplyStateChange(
                    CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, 0x00005678));
                var SP = OperandReg.StackPointer();
                Debug.Assert(state.GetValueB32(SP) == 0x00005678);
                Debug.Assert(state.GetValueB32(CSP) == 0x00008080);

                var cspDeref = OperandReg.ControlStackPointer(deref: true);
                Debug.Assert(cspDeref.AddressMode == AddressMode.reg_deref);
                var cspDerefEff = state.ReduceRegOperand(cspDeref, DataType.undef);
                Debug.Assert(cspDerefEff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index: 0x8080)));


                //todo: more tests


            }
        }

        private static void CPUTest_Operand_FramePointer()
        {
            unchecked
            {
                var state = new CPUState(0, 32, new Instruction[0]);

                state.TESTONLY_ApplyStateChange(
                    CPURegMemStateChange.RegChange0D(RegisterType.frame_pointer, 0x8080));

                //create fp operand
                var fp = OperandReg.FramePointer();

                //check that it doesn't have any fancy indexing and is in fact fp
                var fpEff = state.ReduceRegOperand(fp, DataType.undef);
                Debug.Assert(fpEff.Equals(SimpleRegister.Reg0D(RegisterType.frame_pointer)));

                //not sure about the usefulness of this check
                //I just want to make sure to string doesn't explode
                //ToString might return all sorts of stuff in future...
                Debug.Assert("FP" == fpEff.ToString());
                Debug.Assert(!String.IsNullOrEmpty(fp.ToString()));

                Debug.Assert(state.GetValueB32(fp) == 0x00008080);
                Debug.Assert(state.GetValueB24(fp) == 0x008080);
                Debug.Assert(state.GetValueB16(fp) == 0x8080);
                Debug.Assert(state.GetValueB8(fp) == 0x80);


                //Ensure that fp and SP are not aliases.
                state.TESTONLY_ApplyStateChange(
                    CPURegMemStateChange.RegChange0D(RegisterType.stack_pointer, 0x00005678));
                var SP = OperandReg.StackPointer();
                Debug.Assert(state.GetValueB32(SP) == 0x00005678);
                Debug.Assert(state.GetValueB32(fp) == 0x00008080);

                //Ensure that fp and SP are not aliases.
                state.TESTONLY_ApplyStateChange(
                    CPURegMemStateChange.RegChange0D(RegisterType.control_stack_pointer, 0x00001234));
                var CSP = OperandReg.ControlStackPointer();
                Debug.Assert(state.GetValueB32(CSP) == 0x00001234);
                Debug.Assert(state.GetValueB32(fp) == 0x00008080);

                var fpDeref = OperandReg.FramePointer(deref: true);
                Debug.Assert(fpDeref.AddressMode == AddressMode.reg_deref);
                var fpDerefEff = state.ReduceRegOperand(fpDeref, DataType.undef);
                Debug.Assert(fpDerefEff.Equals(SimpleRegister.Reg1D(RegisterType.memory, index: 0x8080)));

                //todo: more tests


            }
        }


        public static bool RunAllOperandTests()
        {
            //  todo: literal_dereference, e.g. @(literal)
            //  todo: infact literals everywhere in base+index+scale?
            OperandLiteralTests.CPUTest_Operand_Literals();
            CPUTest_Operand_Registers();
            CPUTest_Operand_Labels();
            CPUTest_Operand_ProgramCounter();
            CPUTest_Operand_StackPointer();
            CPUTest_Operand_LinkRegister();
            CPUTest_Operand_ControlStackPointer();
            CPUTest_Operand_FramePointer();

            return true;
        }

  

    }
}
