﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PFC.CPU
{
    using JetBrains.Annotations;

    [Flags]
    public enum InstructionFlags
    {
        DEFAULT = 0x0,

        EXECUTE_NON_TRIVIAL = 0x1,
        /// <summary>
        /// Instruction needs an explicit bitwidth.
        /// e.g. ld not allowed, must be ld8, ld32 etc
        /// Mutually exclusive to BIT_WIDTH_DISALLOWED.
        /// </summary>
        BIT_WIDTH_REQUIRED = 0x2,

        /// <summary>
        /// Instruction must have no bitwidth specified, e.g. jump.
        /// Mutually exclusive to BIT_WIDTH_REQUIRED.
        /// </summary>
        BIT_WIDTH_DISALLOWED = 0x4,

        /// <summary>
        /// Not using the flag control symbol '!' on this instruction is really stupid.
        /// </summary>
        FLAGSMOD_HIGHLYSUGGESTED = 0x8,

        /// <summary>
        /// The instruction arguments will always need to be sign extended.
        /// Probably (?) mutally exclusive to EXECUTE_NON_TRIVIAL.
        /// </summary>
        SIGNED = 0x10,
    }

    public class InstructionInfo
    {
        public readonly OpcodeType Opcode;
        public readonly uint NumOperands;
        public readonly uint NumDests;
        public readonly InstructionFlags InstFlags;
        public readonly CPUFlags CpuFlagsModified;

        public const uint MAX_OPERANDS = 4;
        public const uint MIN_OPERANDS = 0;
        public const uint MAX_DESTS = 1;
        public const uint MIN_DESTS = 0;

        public InstructionInfo(OpcodeType opcode, uint numOperands, uint numDests,
                               InstructionFlags instFlags, CPUFlags cpuFlags)
        {
            #region arg-check
            if (!Enum.IsDefined(typeof(OpcodeType), opcode))
            {
                throw new ArgumentException("Invalid OperandType", "opcode");
            }
            if (numOperands > InstructionInfo.MAX_OPERANDS)
            {
                string message = String.Format("Invalid number of operands. Valid range: [{0}, {1}]",
                                                InstructionInfo.MIN_OPERANDS,
                                                InstructionInfo.MAX_OPERANDS);
                throw new ArgumentOutOfRangeException("numOperands", numOperands, message);
            }

            if (numDests > InstructionInfo.MAX_DESTS)
            {
                string message = String.Format("Invalid number of dests. Valid range: [{0}, {1}]",
                                                InstructionInfo.MIN_DESTS,
                                                InstructionInfo.MAX_DESTS);
                throw new ArgumentOutOfRangeException("numDests", numDests, message);
            }

            if (numDests > numOperands)
            {
                string message = String.Format("Invalid number of dests/operands. numOperands includes sources AND dests."+
                                                "numOperands:{0} numDests:{1}",
                                                numOperands, numDests);
                throw new ArgumentException(message, "numOperands");
            }
            #endregion

            this.Opcode = opcode;
            this.NumOperands = numOperands;
            this.NumDests = numDests;
            this.InstFlags = instFlags;
            this.CpuFlagsModified = cpuFlags;
        }

        // this would be good as a static initialiser if c# supported them.
        //Is it a good idea to have the static table of type X created in a static constructor of
        //type X?! I guess as long as instances don't try to access it in their constructor.
        [NotNull]
        public static readonly Dictionary<OpcodeType, InstructionInfo> Table;

        static InstructionInfo()
        {
            InstructionInfo[] instInfo =
            {
                new InstructionInfo(OpcodeType.nop, 0, 0,
                                    InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.mov, 2, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.movc, 4, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),

                new InstructionInfo(OpcodeType.iadd, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.All),
                new InstructionInfo(OpcodeType.iaddc, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.All),
                new InstructionInfo(OpcodeType.isub, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.All),
                new InstructionInfo(OpcodeType.isubc, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.All),
                new InstructionInfo(OpcodeType.umax, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.umin, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),


                new InstructionInfo(OpcodeType.ld, 2, 1,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_REQUIRED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.st, 2, 1,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_REQUIRED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.halt, 0, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL | 
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.jump, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),

                new InstructionInfo(OpcodeType.assert_eq, 2, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.assert_neq, 2, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.assert_true, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.assert_false, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.breakpoint, 0, 0,
                                    InstructionFlags.BIT_WIDTH_DISALLOWED, CPUFlags.None),
                new InstructionInfo(OpcodeType.icmp, 2, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.FLAGSMOD_HIGHLYSUGGESTED,
                                    CPUFlags.All),
                new InstructionInfo(OpcodeType.flags_read, 1, 1,
                                    InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),


                /*
                 * Funtion call
                 */
                new InstructionInfo(
                    OpcodeType.call_i,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.call_leaf,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.call,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.call_frame,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),

                /*
                 * Funtion return
                 */
                new InstructionInfo(
                    OpcodeType.ret_i,
                    0,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.ret,
                    0,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.ret_leaf,
                    0,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.ret_free,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.ret_frame,
                    0,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),

                /*
                 * Stack ops
                 */
                new InstructionInfo(
                    OpcodeType.stack_alloc,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.stack_free,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.push_data,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.pop_data,
                    1,
                    1,
                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.ctrl_stack_alloc,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.ctrl_stack_free,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL | InstructionFlags.BIT_WIDTH_DISALLOWED,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.push_ctrl,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.pop_ctrl,
                    1,
                    1,
                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.frame_enter,
                    1,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                    CPUFlags.None),
                new InstructionInfo(
                    OpcodeType.frame_leave,
                    0,
                    0,
                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                    CPUFlags.None),



                new InstructionInfo(OpcodeType.bc, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.beq, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bnc, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bneg, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bneq, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bnv, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bnz, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bpos, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bsge, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bsgt, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bsle, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bslt, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.buge, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bugt, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bule, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bult, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bv, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bz, 1, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL |
                                        InstructionFlags.BIT_WIDTH_DISALLOWED,
                                    CPUFlags.None),

                new InstructionInfo(OpcodeType.btrue, 2, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.bfalse, 2, 0,
                                    InstructionFlags.EXECUTE_NON_TRIVIAL,
                                    CPUFlags.None),

                new InstructionInfo(OpcodeType.is_iz,   2, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                    
                new InstructionInfo(OpcodeType.is_inz,  2, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_ineg, 2, 1,
                                    InstructionFlags.SIGNED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_ipos, 2, 1,
                                    InstructionFlags.SIGNED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_ieq,  3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_ineq, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_iugt, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_iule, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_iuge, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_iult, 3, 1,
                                    InstructionFlags.DEFAULT,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_isgt, 3, 1,
                                    InstructionFlags.SIGNED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_isle, 3, 1,
                                    InstructionFlags.SIGNED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_isge, 3, 1,
                                    InstructionFlags.SIGNED,
                                    CPUFlags.None),
                new InstructionInfo(OpcodeType.is_islt, 3, 1,
                                    InstructionFlags.SIGNED,
                                    CPUFlags.None),

                new InstructionInfo(OpcodeType.not, 2, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
                new InstructionInfo(OpcodeType.and, 3, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
                new InstructionInfo(OpcodeType.or, 3, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
                new InstructionInfo(OpcodeType.xor, 3, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
                new InstructionInfo(OpcodeType.nand, 3, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
                new InstructionInfo(OpcodeType.nor, 3, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
                new InstructionInfo(OpcodeType.xnor, 3, 1, InstructionFlags.DEFAULT, CPUFlags.NZ), 
            };
            InstructionInfo.Table = instInfo.ToDictionary(op => op.Opcode);

#if DEBUG
            foreach (var opcodeVal in Enum.GetValues(typeof(OpcodeType)))
            {
                OpcodeType opcode = (OpcodeType)opcodeVal;
                Debug.Assert(InstructionInfo.Table.ContainsKey(opcode));
            }
#endif
        }
    }
}
