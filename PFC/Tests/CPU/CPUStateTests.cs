﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using PFC.CPU;


namespace PFC.Tests.CPU
{
    public static class CPUStateTests
    {
        /// <summary>
        /// Checks to make sure a test throws an exception.
        /// </summary>
        /// <typeparam name="exceptionType"></typeparam>
        /// <param name="func"></param>
        private static void AssertThrows<exceptionType>(Action func) where exceptionType : Exception
        {
            bool bPassed = false;
            try
            {
                func();
            }
            catch (exceptionType e)
            {
                Debug.Assert(e.Message != null); //just to stop compiler complaining e is not used
                bPassed = true;
            }
            Debug.Assert(bPassed);
        }


        private static void CPUTest_State_Sizes_32bitCPU()
        {
            //32 bit machine can read 8,16,24,32 bits from register and literals,
            //and can accept 8, 16, 24, 32 bit literals
            CPUState state32 = new CPUState(0, 32, new Instruction[0]);
            state32.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xAABBCCDDABCDEF98));

            {
                var r0 = OperandReg.Register(0);
                Debug.Assert(state32.GetValueB8(r0) == 0x98);
                Debug.Assert(state32.GetValueB16(r0) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0) == 0xCDEF98);
                Debug.Assert(state32.GetValueB32(r0) == 0xABCDEF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0));
            }

            {
                var r0_u8 = OperandReg.Register(0, DataType.u8);
                Debug.Assert(state32.GetValueB8(r0_u8) == 0x98);
                Debug.Assert(state32.GetValueB16(r0_u8) == 0x0098);
                Debug.Assert(state32.GetValueB24(r0_u8) == 0x000098);
                Debug.Assert(state32.GetValueB32(r0_u8) == 0x00000098);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_u8));
            }

            {
                var r0_i8 = OperandReg.Register(0, DataType.i8);
                Debug.Assert(state32.GetValueB8(r0_i8) == 0x98);
                Debug.Assert(state32.GetValueB16(r0_i8) == 0xFF98);
                Debug.Assert(state32.GetValueB24(r0_i8) == 0xFFFF98);
                Debug.Assert(state32.GetValueB32(r0_i8) == 0xFFFFFF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_i8));
            }

            {
                //note that this case sign extends as you'd expect, but the i32 one doesn't.
                var r0_i16 = OperandReg.Register(0, DataType.i16);
                Debug.Assert(state32.GetValueB8(r0_i16) == 0x98);
                Debug.Assert(state32.GetValueB16(r0_i16) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0_i16) == 0xFFEF98);
                Debug.Assert(state32.GetValueB32(r0_i16) == 0xFFFFEF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_i16));
            }

            {
                var r0_u16 = OperandReg.Register(0, DataType.u16);
                Debug.Assert(state32.GetValueB8(r0_u16) == 0x0098);
                Debug.Assert(state32.GetValueB16(r0_u16) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0_u16) == 0x00EF98);
                Debug.Assert(state32.GetValueB32(r0_u16) == 0x0000EF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_u16));
            }

            {
                //note, not sign extended, as it was i32. the reg->i32 value is signed extended, but then we've
                //asked for 8/16/24 bits of that.
                //note that the results match the u32 case.
                var r0_i32 = OperandReg.Register(0, DataType.i32);
                Debug.Assert(state32.GetValueB8(r0_i32) == 0x98);
                Debug.Assert(state32.GetValueB16(r0_i32) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0_i32) == 0xCDEF98);
                Debug.Assert(state32.GetValueB32(r0_i32) == 0xABCDEF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_i32));
            }

            {
                var r0_u32 = OperandReg.Register(0, DataType.u32);
                Debug.Assert(state32.GetValueB8(r0_u32) == 0x98);
                Debug.Assert(state32.GetValueB16(r0_u32) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0_u32) == 0xCDEF98);
                Debug.Assert(state32.GetValueB32(r0_u32) == 0xABCDEF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_u32));
            }

            {
                var r0_f16 = OperandReg.Register(0, DataType.f16);
                Debug.Assert(state32.GetValueB8(r0_f16) == 0x0098);
                Debug.Assert(state32.GetValueB16(r0_f16) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0_f16) == 0x00EF98);
                Debug.Assert(state32.GetValueB32(r0_f16) == 0x0000EF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_f16));
            }

            {
                var r0_f32 = OperandReg.Register(0, DataType.f32);
                Debug.Assert(state32.GetValueB8(r0_f32) == 0x0098);
                Debug.Assert(state32.GetValueB16(r0_f32) == 0xEF98);
                Debug.Assert(state32.GetValueB24(r0_f32) == 0xCDEF98);
                Debug.Assert(state32.GetValueB32(r0_f32) == 0xABCDEF98);
                AssertThrows<InvalidOperationException>(() => state32.GetValueB64(r0_f32));
            }

            var literal_u32 = OperandLiteral.u32(0xDEADBEEF);
            var literal_u16 = OperandLiteral.u16(0xBEEF);
            var literal_u8 = OperandLiteral.u8(0xEF);
            var literal_i32 = OperandLiteral.i32(unchecked((int)0xDEADBEEF));
            var literal_i16 = OperandLiteral.i16(unchecked((short)0xBEEF));
            var literal_i8 = OperandLiteral.i8(unchecked((sbyte)0xEF));

            Debug.Assert(state32.GetValueB8(literal_u8) == 0xEF);
            Debug.Assert(state32.GetValueB8(literal_u16) == 0xEF);
            Debug.Assert(state32.GetValueB8(literal_u32) == 0xEF);
            Debug.Assert(state32.GetValueB8(literal_i8) == 0xEF);
            Debug.Assert(state32.GetValueB8(literal_i16) == 0xEF);
            Debug.Assert(state32.GetValueB8(literal_i32) == 0xEF);

            Debug.Assert(state32.GetValueB16(literal_u8) == 0x00EF);
            Debug.Assert(state32.GetValueB16(literal_u16) == 0xBEEF);
            Debug.Assert(state32.GetValueB16(literal_u32) == 0xBEEF);
            Debug.Assert(state32.GetValueB16(literal_i8) == 0xFFEF);
            Debug.Assert(state32.GetValueB16(literal_i16) == 0xBEEF);
            Debug.Assert(state32.GetValueB16(literal_i32) == 0xBEEF);


            Debug.Assert(state32.GetValueB32(literal_u8) == 0xEF);
            Debug.Assert(state32.GetValueB32(literal_u16) == 0x0000BEEF);
            Debug.Assert(state32.GetValueB32(literal_u32) == 0xDEADBEEF);
            Debug.Assert(state32.GetValueB32(literal_i8) == 0xFFFFFFEF);
            Debug.Assert(state32.GetValueB32(literal_i16) == 0xFFFFBEEF);
            Debug.Assert(state32.GetValueB32(literal_i32) == 0xDEADBEEF);

            AssertThrows<InvalidOperationException>(() => state32.GetValueB64(literal_u8));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB64(literal_u16));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB64(literal_u32));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB64(literal_i8));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB64(literal_i16));
            AssertThrows<InvalidOperationException>(() => state32.GetValueB64(literal_i32));

        }

        private static void CPUTest_State_Sizes_16bitCPU()
        {
            //16 bit machine can read 8 or 16 bits from register and literals, and can accept 8, 16 bit literals
            CPUState state16 = new CPUState(0, 16, new Instruction[0]);
            state16.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xAABBCCDDABCDEF98));

            {
                var r0 = OperandReg.Register(0);
                Debug.Assert(state16.GetValueB8(r0) == 0x98);
                Debug.Assert(state16.GetValueB16(r0) == 0xEF98);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0));
            }

            {
                var r0_u8 = OperandReg.Register(0, DataType.u8);
                Debug.Assert(state16.GetValueB8(r0_u8) == 0x98);
                Debug.Assert(state16.GetValueB16(r0_u8) == 0x0098);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_u8));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_u8));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_u8));
            }

            {
                var r0_i8 = OperandReg.Register(0, DataType.i8);
                Debug.Assert(state16.GetValueB8(r0_i8) == 0x98);
                Debug.Assert(state16.GetValueB16(r0_i8) == 0xFF98);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_i8));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_i8));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_i8));
            }

            {
                var r0_i16 = OperandReg.Register(0, DataType.i16);
                //note, not sign extended, as it was i16. the reg->i16 value is signed extended, but then we've
                //asked for 8 bits of that.
                Debug.Assert(state16.GetValueB8(r0_i16) == 0x0098);
                Debug.Assert(state16.GetValueB16(r0_i16) == 0xEF98);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_i16));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_i16));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_i16));
            }

            {
                var r0_u16 = OperandReg.Register(0, DataType.u16);
                Debug.Assert(state16.GetValueB8(r0_u16) == 0x0098);
                Debug.Assert(state16.GetValueB16(r0_u16) == 0xEF98);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_u16));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_u16));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_u16));
            }

            {
                var r0_i32 = OperandReg.Register(0, DataType.i32);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB8(r0_i32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB16(r0_i32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_i32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_i32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_i32));
            }

            {
                var r0_u32 = OperandReg.Register(0, DataType.u32);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB8(r0_u32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB16(r0_u32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_u32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_u32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_u32));
            }

            {
                var r0_f16 = OperandReg.Register(0, DataType.f16);
                Debug.Assert(state16.GetValueB8(r0_f16) == 0x0098);
                Debug.Assert(state16.GetValueB16(r0_f16) == 0xEF98);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_f16));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_f16));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_f16));
            }

            {
                var r0_f32 = OperandReg.Register(0, DataType.f32);
                AssertThrows<InvalidOperationException>(() => state16.GetValueB8(r0_f32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB16(r0_f32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB24(r0_f32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB32(r0_f32));
                AssertThrows<InvalidOperationException>(() => state16.GetValueB64(r0_f32));
            }

            var literal_u32 = OperandLiteral.u32(0xDEADBEEF);
            var literal_u16 = OperandLiteral.u16(0xBEEF);
            var literal_u8 = OperandLiteral.u8(0xEF);
            var literal_i32 = OperandLiteral.i32(unchecked((int)0xDEADBEEF));
            var literal_i16 = OperandLiteral.i16(unchecked((short)0xBEEF));
            var literal_i8 = OperandLiteral.i8(unchecked((sbyte)0xEF));

            Debug.Assert(state16.GetValueB8(literal_u8) == 0xEF);
            Debug.Assert(state16.GetValueB8(literal_u16) == 0xEF);
            AssertThrows<InvalidOperationException>(() => state16.GetValueB8(literal_u32));
            Debug.Assert(state16.GetValueB8(literal_i8) == 0xEF);
            Debug.Assert(state16.GetValueB8(literal_i16) == 0xEF);
            AssertThrows<InvalidOperationException>(() => state16.GetValueB8(literal_i32));

            Debug.Assert(state16.GetValueB16(literal_u8) == 0x00EF);
            Debug.Assert(state16.GetValueB16(literal_u16) == 0xBEEF);
            AssertThrows<InvalidOperationException>(() => state16.GetValueB16(literal_u32));
            Debug.Assert(state16.GetValueB16(literal_i8) == 0xFFEF);
            Debug.Assert(state16.GetValueB16(literal_i16) == 0xBEEF);
            AssertThrows<InvalidOperationException>(() => state16.GetValueB16(literal_i32));


            AssertThrows<InvalidOperationException>(() => state16.GetValueB32(literal_u8));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB32(literal_u16));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB32(literal_u32));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB32(literal_i8));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB32(literal_i16));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB32(literal_i32));

            AssertThrows<InvalidOperationException>(() => state16.GetValueB64(literal_u8));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB64(literal_u16));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB64(literal_u32));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB64(literal_i8));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB64(literal_i16));
            AssertThrows<InvalidOperationException>(() => state16.GetValueB64(literal_i32));

        }

        private static void CPUTest_State_Sizes_8bitCPU()
        {
            //8 bit machine can only read 8 bits from register and literals, and can only accept 8 bit literals
            CPUState state8 = new CPUState(0, 8, new Instruction[0]);
            state8.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xAABBCCDDABCDEF98));

            {
                var r0 = OperandReg.Register(0);
                Debug.Assert(state8.GetValueB8(r0) == 0x98);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0));
            }

            {
                var r0_i8 = OperandReg.Register(0, DataType.i8);
                Debug.Assert(state8.GetValueB8(r0_i8) == 0x98);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_i8));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_i8));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_i8));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_i8));
            }

            {
                var r0_u8 = OperandReg.Register(0, DataType.u8);
                Debug.Assert(state8.GetValueB8(r0_u8) == 0x98);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_u8));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_u8));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_u8));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_u8));
            }

            {
                var r0_i16 = OperandReg.Register(0, DataType.i16);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB8(r0_i16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_i16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_i16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_i16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_i16));
            }

            {
                var r0_u16 = OperandReg.Register(0, DataType.u16);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB8(r0_u16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_u16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_u16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_u16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_u16));
            }

            {
                var r0_i32 = OperandReg.Register(0, DataType.i32);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB8(r0_i32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_i32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_i32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_i32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_i32));
            }

            {
                var r0_u32 = OperandReg.Register(0, DataType.u32);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB8(r0_u32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_u32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_u32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_u32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_u32));
            }

            {
                var r0_f16 = OperandReg.Register(0, DataType.f16);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB8(r0_f16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_f16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_f16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_f16));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_f16));
            }

            {
                var r0_f32 = OperandReg.Register(0, DataType.f32);
                AssertThrows<InvalidOperationException>(() => state8.GetValueB8(r0_f32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB16(r0_f32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB24(r0_f32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB32(r0_f32));
                AssertThrows<InvalidOperationException>(() => state8.GetValueB64(r0_f32));
            }


            var literal_u32 = OperandLiteral.u32(0xDEADBEEF);
            var literal_u16 = OperandLiteral.u16(0xBEEF);
            var literal_u8 = OperandLiteral.u8(0xEF);
            var literal_i32 = OperandLiteral.i32(unchecked((int)0xDEADBEEF));
            var literal_i16 = OperandLiteral.i16(unchecked((short)0xBEEF));
            var literal_i8 = OperandLiteral.i8(unchecked((sbyte)0xEF));

            Debug.Assert(state8.GetValueB8(literal_u8) == 0xEF);
            AssertThrows<InvalidOperationException>(() => state8.GetValueB8(literal_u16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB8(literal_u32));
            Debug.Assert(state8.GetValueB8(literal_i8) == 0xEF);
            AssertThrows<InvalidOperationException>(() => state8.GetValueB8(literal_i16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB8(literal_i32));

            AssertThrows<InvalidOperationException>(() => state8.GetValueB16(literal_u8));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB16(literal_u16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB16(literal_u32));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB16(literal_i8));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB16(literal_i16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB16(literal_i32));


            AssertThrows<InvalidOperationException>(() => state8.GetValueB32(literal_u8));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB32(literal_u16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB32(literal_u32));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB32(literal_i8));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB32(literal_i16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB32(literal_i32));

            AssertThrows<InvalidOperationException>(() => state8.GetValueB64(literal_u8));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB64(literal_u16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB64(literal_u32));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB64(literal_i8));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB64(literal_i16));
            AssertThrows<InvalidOperationException>(() => state8.GetValueB64(literal_i32));
        }

        //ensure that the RW and RO interfaces interoperate properly
        private static void CPUTest_State_DualInterface()
        {
            CPUState state = new CPUState(0, 32, new Instruction[0]);
            ICPUStateRO roState = state;

            //change something through the 'State' var
            Debug.Assert(state.cycleCounter == 0);
            Debug.Assert(state.cycleCounter == roState.cycleCounter);
            state.cycleCounter++;
            Debug.Assert(1 == state.cycleCounter);
            Debug.Assert(state.cycleCounter == roState.cycleCounter);


            //change a collection's index
            Debug.Assert(state.RegisterBank[0] == 0);
            Debug.Assert(state.RegisterBank[0] == roState.RegisterBank[0]);
            state.TESTONLY_ApplyStateChange(CPURegMemStateChange.RegChange(0, 0xDEADBEEF));
            Debug.Assert(state.RegisterBank[0] == 0xDEADBEEF);
            Debug.Assert(state.RegisterBank[0] == roState.RegisterBank[0]);
            Debug.Assert(state.GetValueB32(OperandReg.Register(0)) == 0xDEADBEEF);
        }


        private static void CPUTest_State_Sizes()
        {
            AssertThrows<ArgumentException>(() => new CPUState(0, 0, new Instruction[0]));
            AssertThrows<ArgumentException>(() => new CPUState(0, 9, new Instruction[0]));
            AssertThrows<ArgumentException>(() => new CPUState(0, 128, new Instruction[0]));

            CPUTest_State_Sizes_8bitCPU();
            CPUTest_State_Sizes_16bitCPU();
            CPUTest_State_Sizes_32bitCPU();
        }


        private static void CPUTest_State_MemAccess()
        {
            CPUState state32 = new CPUState(0, 32, new Instruction[0]);

            //mem change can only be unsigned
            AssertThrows<ArgumentException>(() => CPURegMemStateChange.MemChange(DataType.f32, 0, 0));
            AssertThrows<ArgumentException>(() => CPURegMemStateChange.MemChange(DataType.i8, 0, 0));

            //unaligned?
            AssertThrows<ArgumentException>(() => CPURegMemStateChange.MemChange(DataType.i16, 1, 0));
            AssertThrows<ArgumentException>(() => CPURegMemStateChange.MemChange16(1, 0));

            Action<ushort, uint, uint> memCheck = (ushort addr, uint numBytes, uint val) =>
            {
                var memval = state32.ReadAlignedMemory(addr, numBytes);
                Debug.Assert(memval == val);
            };

            //Write DEADBEEF to position 0
            memCheck(0x0, 4, 0x000000000);
            List<CPURegMemStateChange> changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange32(0x0, 0x77777777),
                CPURegMemStateChange.MemChange32(0x4, 0x66666666),
                CPURegMemStateChange.MemChange32(0x8, 0x55555555),
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x0, 4, 0x77777777);
            memCheck(0x4, 4, 0x66666666);
            memCheck(0x8, 4, 0x55555555);


            //Write DEADBEEF to position 4 as 4 byte writes. Also tests little endian nature.
            //It shoukdn't change the other data already written.
            changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange8(0x4, 0xEF),
                CPURegMemStateChange.MemChange8(0x5, 0xBE),
                CPURegMemStateChange.MemChange8(0x6, 0xAD),
                CPURegMemStateChange.MemChange8(0x7, 0xDE),
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x0, 4, 0x77777777);
            memCheck(0x4, 4, 0xDEADBEEF);
            memCheck(0x8, 4, 0x55555555);


            //Now write them in the other order. Still little endian.
            changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange8(0xF, 0xBA),
                CPURegMemStateChange.MemChange8(0xE, 0xDD),
                CPURegMemStateChange.MemChange8(0xD, 0xF0),
                CPURegMemStateChange.MemChange8(0xC, 0x0D),
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x0, 4, 0x77777777);
            memCheck(0x4, 4, 0xDEADBEEF);
            memCheck(0x8, 4, 0x55555555);
            memCheck(0xC, 4, 0xBADDF00D);


            //Write 0x12345678 to position 4 as 2*16. Also tests little endian nature.
            changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange16(0x10, 0x5678),
                CPURegMemStateChange.MemChange16(0x12, 0x1234),
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x00, 4, 0x77777777);
            memCheck(0x04, 4, 0xDEADBEEF);
            memCheck(0x08, 4, 0x55555555);
            memCheck(0x0C, 4, 0xBADDF00D);
            memCheck(0x10, 4, 0x12345678);


            //Write 0x12345678 to position 4
            changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange32(0x18, 0xCAFEF00D),
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x00, 4, 0x77777777);
            memCheck(0x04, 4, 0xDEADBEEF);
            memCheck(0x08, 4, 0x55555555);
            memCheck(0x0C, 4, 0xBADDF00D);
            memCheck(0x10, 4, 0x12345678);
            memCheck(0x18, 4, 0xCAFEF00D);



            //Change a single byte in the middle of a 32bit word
            changes = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange8(0x0B, 0xFF), //changes the top byte of 0x55555555
                CPURegMemStateChange.MemChange8(0x12, 0x00), //changes the '34' of 0x12345678

            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x00, 4, 0x77777777);
            memCheck(0x04, 4, 0xDEADBEEF);
            memCheck(0x08, 4, 0xFF555555); //change
            memCheck(0x0C, 4, 0xBADDF00D);
            memCheck(0x10, 4, 0x12005678); //change
            memCheck(0x18, 4, 0xCAFEF00D);


            changes = new List<CPURegMemStateChange>
            {
                //Remove the 'ADBE' from 0xDEADBEEF.
                CPURegMemStateChange.MemChangeUnaligned(DataType.u16, 0x5, 0x1234),
            };
            state32.TESTONLY_ApplyStateChanges(changes);
            memCheck(0x00, 4, 0x77777777);
            memCheck(0x04, 4, 0xDE1234EF);
            memCheck(0x08, 4, 0xFF555555); //change
            memCheck(0x0C, 4, 0xBADDF00D);
            memCheck(0x10, 4, 0x12005678); //change
            memCheck(0x18, 4, 0xCAFEF00D);

            //this _is_ unaligned, as we're trying to read 4 bytes
            //TODO: rather than except, it should probably issue a warning
            AssertThrows<ArgumentException>(() => memCheck(0x19, 4, 0x00000000));


            //should test large addresses etc
        }


        private static void CPUTest_State_StateUpdate()
        {
            CPUState state32 = new CPUState(0, 32, new Instruction[0]);
            ICPUStateRO roState = state32;

            CPUStateUpdate update;

            Debug.Assert(state32.cycleCounter == 0);
            Debug.Assert(state32.cycleCounter == roState.cycleCounter);

            update = new CPUStateUpdate();
            update.CycleCost = 1;
            state32.ApplyStateUpdate(update);
            
            Debug.Assert(state32.cycleCounter == 1);
            Debug.Assert(!state32.IsHalted);
            Debug.Assert(state32.cycleCounter == roState.cycleCounter);
            Debug.Assert(state32.IsHalted == roState.IsHalted);

            update.CycleCost = 18;
            state32.ApplyStateUpdate(update);

            Debug.Assert(state32.cycleCounter == 19);
            Debug.Assert(!state32.IsHalted);
            Debug.Assert(state32.cycleCounter == roState.cycleCounter);
            Debug.Assert(state32.IsHalted == roState.IsHalted);


            update = new CPUStateUpdate();
            //change a collection's index
            Debug.Assert(state32.RegisterBank[0] == 0);
            Debug.Assert(state32.RegisterBank[0] == roState.RegisterBank[0]);
            update.RegMemChanges = new List<CPURegMemStateChange>() { CPURegMemStateChange.RegChange(0, 0xDEADBEEF)};
            state32.ApplyStateUpdate(update);
            Debug.Assert(state32.RegisterBank[0] == 0xDEADBEEF);
            Debug.Assert(state32.RegisterBank[0] == roState.RegisterBank[0]);
            Debug.Assert(state32.GetValueB32(OperandReg.Register(0)) == 0xDEADBEEF);



            update = new CPUStateUpdate();
            Action<ushort, uint, uint> memCheck = (ushort addr, uint numBytes, uint val) =>
            {
                var memval = state32.ReadAlignedMemory(addr, numBytes);
                Debug.Assert(memval == val);
            };

            //Write DEADBEEF to position 0
            memCheck(0x0, 4, 0x000000000);
            update.RegMemChanges = new List<CPURegMemStateChange>
            {
                CPURegMemStateChange.MemChange32(0x0, 0x77777777),
                CPURegMemStateChange.MemChange32(0x4, 0x66666666),
                CPURegMemStateChange.MemChange32(0x8, 0x55555555),
            };
            state32.ApplyStateUpdate(update);
            memCheck(0x0, 4, 0x77777777);
            memCheck(0x4, 4, 0x66666666);
            memCheck(0x8, 4, 0x55555555);



            update = new CPUStateUpdate();
            update.Halt = true;

            Debug.Assert(state32.cycleCounter == 19);
            Debug.Assert(!state32.IsHalted);
            Debug.Assert(state32.cycleCounter == roState.cycleCounter);
            Debug.Assert(state32.IsHalted == roState.IsHalted);

            update.CycleCost = 2;
            state32.ApplyStateUpdate(update);

            Debug.Assert(state32.cycleCounter == 21);
            Debug.Assert(state32.IsHalted);
            Debug.Assert(state32.cycleCounter == roState.cycleCounter);
            Debug.Assert(state32.IsHalted == roState.IsHalted);




        }

        public static bool RunAllCPUStateTests()
        {
            CPUTest_State_DualInterface();
            CPUTest_State_Sizes();
            CPUTest_State_MemAccess();
            CPUTest_State_StateUpdate();

            //need to test cycle counter nad instruction cycle counts?
            //need to test halted/state update

            return true;
        }

    }
}
