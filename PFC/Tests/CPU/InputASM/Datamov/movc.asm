        movc         r0, 1,          0xAAAAAAAA, 0xCCCCCCCC
        assert_eq    r0,             0xAAAAAAAA

        movc         r0, 0,          0xAAAAAAAA, 0xCCCCCCCC
        assert_eq    r0,                         0xCCCCCCCC

        movc         r0, 0x80000000, 0xCCCCCCCC, 0xAAAAAAAA
        assert_eq    r0,             0xCCCCCCCC

        mov          r1, -1
        movc         r0, r1,         0xAAAAAAAA, 0xCCCCCCCC
        assert_eq    r0,             0xAAAAAAAA

        mov          r1, 0
        movc         r0, r1,         0xAAAAAAAA, 0xCCCCCCCC
        assert_eq    r0,                         0xCCCCCCCC
        halt
