			mov          r1, 0
			is_iz        r0, 0X00000000
			assert_true  r0

			mov          r1, 1
			is_iz        r0, 0X00000001
			assert_false r0

			mov          r1, 2
			is_iz        r0, 0X7FFFFFFF
			assert_false r0

			mov          r1, 3
			is_iz        r0, 0X80000000
			assert_false r0

			mov          r1, 4
			is_iz        r0, 0X80000001
			assert_false r0

			mov          r1, 5
			is_iz        r0, 0XFFFFFFFE
			assert_false r0

			mov          r1, 6
			is_iz        r0, 0XFFFFFFFF
			assert_false r0


			######
			mov          r1, 7
			is_inz       r0, 0X00000000
			assert_false r0

			mov          r1, 8
			is_inz       r0, 0X00000001
			assert_true  r0

			mov          r1, 9
			is_inz       r0, 0X7FFFFFFF
			assert_true  r0

			mov          r1, 10
			is_inz       r0, 0X80000000
			assert_true  r0

			mov          r1, 11
			is_inz       r0, 0X80000001
			assert_true  r0

			mov          r1, 12
			is_inz       r0, 0XFFFFFFFE
			assert_true  r0

			mov          r1, 13
			is_inz       r0, 0XFFFFFFFF
			assert_true  r0


			######
			mov          r1, 14
			is_ineg      r0, 0X00000000
			assert_false r0

			mov          r1, 15
			is_ineg      r0, 0X00000001
			assert_false r0

			mov          r1, 16
			is_ineg      r0, 0X7FFFFFFF
			assert_false r0

			mov          r1, 17
			is_ineg      r0, 0X80000000
			assert_true  r0

			mov          r1, 18
			is_ineg      r0, 0X80000001
			assert_true  r0

			mov          r1, 19
			is_ineg      r0, 0XFFFFFFFE
			assert_true  r0

			mov          r1, 20
			is_ineg      r0, 0XFFFFFFFF
			assert_true  r0


			######
			mov          r1, 21
			is_ipos      r0, 0X00000000
			assert_true  r0

			mov          r1, 22
			is_ipos      r0, 0X00000001
			assert_true  r0

			mov          r1, 23
			is_ipos      r0, 0X7FFFFFFF
			assert_true  r0

			mov          r1, 24
			is_ipos      r0, 0X80000000
			assert_false r0

			mov          r1, 25
			is_ipos      r0, 0X80000001
			assert_false r0

			mov          r1, 26
			is_ipos      r0, 0XFFFFFFFE
			assert_false r0

			mov          r1, 27
			is_ipos      r0, 0XFFFFFFFF
			assert_false r0


			######

			halt
