                    assert_eq  pc,  0x0
                    assert_eq  pc,  0x1
                    assert_eq  0x2, pc
                    assert_eq  PC,  0x3

                    iadd8      r0,  8,   PC
                    assert_eq  r0,  12

                    mov        r0,  PC
                    assert_eq  r0,  6

                    mov        r15, 0x0
                    halt