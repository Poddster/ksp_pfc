﻿namespace ConsoleTest
{
    using System;
    using System.Diagnostics;

    public static class Program
    {
        public static void Main(string[] args)
        {
            RunAllCPUTests();
            RunAllSystemSimTests();

            // need to check unix, windows, mac files?
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(@"all tests passed.");
            Console.WriteLine(@"smash keyboard to exit");
            Console.ReadKey();
        }

        private static void RunAllCPUTests()
        {
            bool passed = PFC.Tests.CPU.LexAndParserOperandTests.RunAllParserOperandTests();
            Debug.Assert(passed, "RunAllParserOperandTests failed");

            passed = PFC.Tests.CPU.ParserTest.RunAllParserTests();
            Debug.Assert(passed, "RunAllParserTests failed");

            passed = PFC.Tests.CPU.OperandTests.RunAllOperandTests();
            Debug.Assert(passed, "RunAllOperandTests failed");

            passed = PFC.Tests.CPU.CPUTests.RunAllCPUTests();
            Debug.Assert(passed, "RunAllCPUTests failed");
        }

        private static void RunAllSystemSimTests()
        {
            bool passed = PFC.Tests.SystemSim.SystemSimTestsMain.RunAllSystemSimTests();
            Debug.Assert(passed, "RunAllSystemSimTests failed");
        }
    }
}
