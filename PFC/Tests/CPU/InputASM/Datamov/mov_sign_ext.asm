        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8       r0 ,        0xF1E2C3D4      # 0   r0 = 0x______D4
        mov8       r1 ,     u8{0x______D4}     # 1   r1 = 0x______D4
        mov8       r2 ,     i8{0x______D4}     # 2   r2 = 0x______D4
        mov8       r3 ,    u16{0x____C3D4}     # 3   r3 = 0x______D4
        mov8       r4 ,    i16{0x____C3D4}     # 4   r4 = 0x______D4
        mov8       r5 ,    u32{0xF1E2C3D4}     # 5   r5 = 0x______D4
        mov8       r6 ,    i32{0xF1E2C3D4}     # 6   r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8    u8{r0},        0xF1E2C3D4      # 7   r0 = 0x______D4
        mov8    u8{r1},     u8{0x______D4}     # 8   r1 = 0x______D4
        mov8    u8{r2},     i8{0x______D4}     # 9   r2 = 0x______D4
        mov8    u8{r3},    u16{0x____C3D4}     # 10  r3 = 0x______D4
        mov8    u8{r4},    i16{0x____C3D4}     # 11  r4 = 0x______D4
        mov8    u8{r5},    u32{0xF1E2C3D4}     # 12  r5 = 0x______D4
        mov8    u8{r6},    i32{0xF1E2C3D4}     # 13  r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8    i8{r0},        0xF1E2C3D4      # 14  r0 = 0x______D4
        mov8    i8{r1},     u8{0x______D4}     # 15  r1 = 0x______D4
        mov8    i8{r2},     i8{0x______D4}     # 16  r2 = 0x______D4
        mov8    i8{r3},    u16{0x____C3D4}     # 17  r3 = 0x______D4
        mov8    i8{r4},    i16{0x____C3D4}     # 18  r4 = 0x______D4
        mov8    i8{r5},    u32{0xF1E2C3D4}     # 19  r5 = 0x______D4
        mov8    i8{r6},    i32{0xF1E2C3D4}     # 20  r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8   u16{r0},        0xF1E2C3D4      # 21  r0 = 0x____00D4
        mov8   u16{r1},     u8{0x______D4}     # 22  r1 = 0x____00D4
        mov8   u16{r2},     i8{0x______D4}     # 23  r2 = 0x____00D4
        mov8   u16{r3},    u16{0x____C3D4}     # 24  r3 = 0x____00D4
        mov8   u16{r4},    i16{0x____C3D4}     # 25  r4 = 0x____00D4
        mov8   u16{r5},    u32{0xF1E2C3D4}     # 26  r5 = 0x____00D4
        mov8   u16{r6},    i32{0xF1E2C3D4}     # 27  r6 = 0x____00D4
        assert_eq  r0,         0xAAAA00D4
        assert_eq  r1,         0xAAAA00D4
        assert_eq  r2,         0xAAAA00D4
        assert_eq  r3,         0xAAAA00D4
        assert_eq  r4,         0xAAAA00D4
        assert_eq  r5,         0xAAAA00D4
        assert_eq  r6,         0xAAAA00D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8   i16{r0},        0xF1E2C3D4      # 28  r0 = 0x____FFD4
        mov8   i16{r1},     u8{0x______D4}     # 29  r1 = 0x____FFD4
        mov8   i16{r2},     i8{0x______D4}     # 30  r2 = 0x____FFD4
        mov8   i16{r3},    u16{0x____C3D4}     # 31  r3 = 0x____FFD4
        mov8   i16{r4},    i16{0x____C3D4}     # 32  r4 = 0x____FFD4
        mov8   i16{r5},    u32{0xF1E2C3D4}     # 33  r5 = 0x____FFD4
        mov8   i16{r6},    i32{0xF1E2C3D4}     # 34  r6 = 0x____FFD4
        assert_eq  r0,         0xAAAAFFD4
        assert_eq  r1,         0xAAAAFFD4
        assert_eq  r2,         0xAAAAFFD4
        assert_eq  r3,         0xAAAAFFD4
        assert_eq  r4,         0xAAAAFFD4
        assert_eq  r5,         0xAAAAFFD4
        assert_eq  r6,         0xAAAAFFD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8   u32{r0},        0xF1E2C3D4      # 35  r0 = 0x000000D4
        mov8   u32{r1},     u8{0x______D4}     # 36  r1 = 0x000000D4
        mov8   u32{r2},     i8{0x______D4}     # 37  r2 = 0x000000D4
        mov8   u32{r3},    u16{0x____C3D4}     # 38  r3 = 0x000000D4
        mov8   u32{r4},    i16{0x____C3D4}     # 39  r4 = 0x000000D4
        mov8   u32{r5},    u32{0xF1E2C3D4}     # 40  r5 = 0x000000D4
        mov8   u32{r6},    i32{0xF1E2C3D4}     # 41  r6 = 0x000000D4
        assert_eq  r0,         0x000000D4
        assert_eq  r1,         0x000000D4
        assert_eq  r2,         0x000000D4
        assert_eq  r3,         0x000000D4
        assert_eq  r4,         0x000000D4
        assert_eq  r5,         0x000000D4
        assert_eq  r6,         0x000000D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov8   i32{r0},        0xF1E2C3D4      # 42  r0 = 0xFFFFFFD4
        mov8   i32{r1},     u8{0x______D4}     # 43  r1 = 0xFFFFFFD4
        mov8   i32{r2},     i8{0x______D4}     # 44  r2 = 0xFFFFFFD4
        mov8   i32{r3},    u16{0x____C3D4}     # 45  r3 = 0xFFFFFFD4
        mov8   i32{r4},    i16{0x____C3D4}     # 46  r4 = 0xFFFFFFD4
        mov8   i32{r5},    u32{0xF1E2C3D4}     # 47  r5 = 0xFFFFFFD4
        mov8   i32{r6},    i32{0xF1E2C3D4}     # 48  r6 = 0xFFFFFFD4
        assert_eq  r0,         0xFFFFFFD4
        assert_eq  r1,         0xFFFFFFD4
        assert_eq  r2,         0xFFFFFFD4
        assert_eq  r3,         0xFFFFFFD4
        assert_eq  r4,         0xFFFFFFD4
        assert_eq  r5,         0xFFFFFFD4
        assert_eq  r6,         0xFFFFFFD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16      r0 ,        0xF1E2C3D4      # 49  r0 = 0x____C3D4
        mov16      r1 ,     u8{0x______D4}     # 50  r1 = 0x____00D4
        mov16      r2 ,     i8{0x______D4}     # 51  r2 = 0x____FFD4
        mov16      r3 ,    u16{0x____C3D4}     # 52  r3 = 0x____C3D4
        mov16      r4 ,    i16{0x____C3D4}     # 53  r4 = 0x____C3D4
        mov16      r5 ,    u32{0xF1E2C3D4}     # 54  r5 = 0x____C3D4
        mov16      r6 ,    i32{0xF1E2C3D4}     # 55  r6 = 0x____C3D4
        assert_eq  r0,         0xAAAAC3D4
        assert_eq  r1,         0xAAAA00D4
        assert_eq  r2,         0xAAAAFFD4
        assert_eq  r3,         0xAAAAC3D4
        assert_eq  r4,         0xAAAAC3D4
        assert_eq  r5,         0xAAAAC3D4
        assert_eq  r6,         0xAAAAC3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16   u8{r0},        0xF1E2C3D4      # 56  r0 = 0x______D4
        mov16   u8{r1},     u8{0x______D4}     # 57  r1 = 0x______D4
        mov16   u8{r2},     i8{0x______D4}     # 58  r2 = 0x______D4
        mov16   u8{r3},    u16{0x____C3D4}     # 59  r3 = 0x______D4
        mov16   u8{r4},    i16{0x____C3D4}     # 60  r4 = 0x______D4
        mov16   u8{r5},    u32{0xF1E2C3D4}     # 61  r5 = 0x______D4
        mov16   u8{r6},    i32{0xF1E2C3D4}     # 62  r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16   i8{r0},        0xF1E2C3D4      # 63  r0 = 0x______D4
        mov16   i8{r1},     u8{0x______D4}     # 64  r1 = 0x______D4
        mov16   i8{r2},     i8{0x______D4}     # 65  r2 = 0x______D4
        mov16   i8{r3},    u16{0x____C3D4}     # 66  r3 = 0x______D4
        mov16   i8{r4},    i16{0x____C3D4}     # 67  r4 = 0x______D4
        mov16   i8{r5},    u32{0xF1E2C3D4}     # 68  r5 = 0x______D4
        mov16   i8{r6},    i32{0xF1E2C3D4}     # 69  r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16  u16{r0},        0xF1E2C3D4      # 70  r0 = 0x____C3D4
        mov16  u16{r1},     u8{0x______D4}     # 71  r1 = 0x____00D4
        mov16  u16{r2},     i8{0x______D4}     # 72  r2 = 0x____FFD4
        mov16  u16{r3},    u16{0x____C3D4}     # 73  r3 = 0x____C3D4
        mov16  u16{r4},    i16{0x____C3D4}     # 74  r4 = 0x____C3D4
        mov16  u16{r5},    u32{0xF1E2C3D4}     # 75  r5 = 0x____C3D4
        mov16  u16{r6},    i32{0xF1E2C3D4}     # 76  r6 = 0x____C3D4
        assert_eq  r0,         0xAAAAC3D4
        assert_eq  r1,         0xAAAA00D4
        assert_eq  r2,         0xAAAAFFD4
        assert_eq  r3,         0xAAAAC3D4
        assert_eq  r4,         0xAAAAC3D4
        assert_eq  r5,         0xAAAAC3D4
        assert_eq  r6,         0xAAAAC3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16  i16{r0},        0xF1E2C3D4      # 77  r0 = 0x____C3D4
        mov16  i16{r1},     u8{0x______D4}     # 78  r1 = 0x____00D4
        mov16  i16{r2},     i8{0x______D4}     # 79  r2 = 0x____FFD4
        mov16  i16{r3},    u16{0x____C3D4}     # 80  r3 = 0x____C3D4
        mov16  i16{r4},    i16{0x____C3D4}     # 81  r4 = 0x____C3D4
        mov16  i16{r5},    u32{0xF1E2C3D4}     # 82  r5 = 0x____C3D4
        mov16  i16{r6},    i32{0xF1E2C3D4}     # 83  r6 = 0x____C3D4
        assert_eq  r0,         0xAAAAC3D4
        assert_eq  r1,         0xAAAA00D4
        assert_eq  r2,         0xAAAAFFD4
        assert_eq  r3,         0xAAAAC3D4
        assert_eq  r4,         0xAAAAC3D4
        assert_eq  r5,         0xAAAAC3D4
        assert_eq  r6,         0xAAAAC3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16  u32{r0},        0xF1E2C3D4      # 84  r0 = 0x0000C3D4
        mov16  u32{r1},     u8{0x______D4}     # 85  r1 = 0x000000D4
        mov16  u32{r2},     i8{0x______D4}     # 86  r2 = 0x0000FFD4
        mov16  u32{r3},    u16{0x____C3D4}     # 87  r3 = 0x0000C3D4
        mov16  u32{r4},    i16{0x____C3D4}     # 88  r4 = 0x0000C3D4
        mov16  u32{r5},    u32{0xF1E2C3D4}     # 89  r5 = 0x0000C3D4
        mov16  u32{r6},    i32{0xF1E2C3D4}     # 90  r6 = 0x0000C3D4
        assert_eq  r0,         0x0000C3D4
        assert_eq  r1,         0x000000D4
        assert_eq  r2,         0x0000FFD4
        assert_eq  r3,         0x0000C3D4
        assert_eq  r4,         0x0000C3D4
        assert_eq  r5,         0x0000C3D4
        assert_eq  r6,         0x0000C3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov16  i32{r0},        0xF1E2C3D4      # 91  r0 = 0xFFFFC3D4
        mov16  i32{r1},     u8{0x______D4}     # 92  r1 = 0x000000D4
        mov16  i32{r2},     i8{0x______D4}     # 93  r2 = 0xFFFFFFD4
        mov16  i32{r3},    u16{0x____C3D4}     # 94  r3 = 0xFFFFC3D4
        mov16  i32{r4},    i16{0x____C3D4}     # 95  r4 = 0xFFFFC3D4
        mov16  i32{r5},    u32{0xF1E2C3D4}     # 96  r5 = 0xFFFFC3D4
        mov16  i32{r6},    i32{0xF1E2C3D4}     # 97  r6 = 0xFFFFC3D4
        assert_eq  r0,         0xFFFFC3D4
        assert_eq  r1,         0x000000D4
        assert_eq  r2,         0xFFFFFFD4
        assert_eq  r3,         0xFFFFC3D4
        assert_eq  r4,         0xFFFFC3D4
        assert_eq  r5,         0xFFFFC3D4
        assert_eq  r6,         0xFFFFC3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32      r0 ,        0xF1E2C3D4      # 98  r0 = 0xF1E2C3D4
        mov32      r1 ,     u8{0x______D4}     # 99  r1 = 0x000000D4
        mov32      r2 ,     i8{0x______D4}     # 100 r2 = 0xFFFFFFD4
        mov32      r3 ,    u16{0x____C3D4}     # 101 r3 = 0x0000C3D4
        mov32      r4 ,    i16{0x____C3D4}     # 102 r4 = 0xFFFFC3D4
        mov32      r5 ,    u32{0xF1E2C3D4}     # 103 r5 = 0xF1E2C3D4
        mov32      r6 ,    i32{0xF1E2C3D4}     # 104 r6 = 0xF1E2C3D4
        assert_eq  r0,         0xF1E2C3D4
        assert_eq  r1,         0x000000D4
        assert_eq  r2,         0xFFFFFFD4
        assert_eq  r3,         0x0000C3D4
        assert_eq  r4,         0xFFFFC3D4
        assert_eq  r5,         0xF1E2C3D4
        assert_eq  r6,         0xF1E2C3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32   u8{r0},        0xF1E2C3D4      # 105 r0 = 0x______D4
        mov32   u8{r1},     u8{0x______D4}     # 106 r1 = 0x______D4
        mov32   u8{r2},     i8{0x______D4}     # 107 r2 = 0x______D4
        mov32   u8{r3},    u16{0x____C3D4}     # 108 r3 = 0x______D4
        mov32   u8{r4},    i16{0x____C3D4}     # 109 r4 = 0x______D4
        mov32   u8{r5},    u32{0xF1E2C3D4}     # 110 r5 = 0x______D4
        mov32   u8{r6},    i32{0xF1E2C3D4}     # 111 r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32   i8{r0},        0xF1E2C3D4      # 112 r0 = 0x______D4
        mov32   i8{r1},     u8{0x______D4}     # 113 r1 = 0x______D4
        mov32   i8{r2},     i8{0x______D4}     # 114 r2 = 0x______D4
        mov32   i8{r3},    u16{0x____C3D4}     # 115 r3 = 0x______D4
        mov32   i8{r4},    i16{0x____C3D4}     # 116 r4 = 0x______D4
        mov32   i8{r5},    u32{0xF1E2C3D4}     # 117 r5 = 0x______D4
        mov32   i8{r6},    i32{0xF1E2C3D4}     # 118 r6 = 0x______D4
        assert_eq  r0,         0xAAAAAAD4
        assert_eq  r1,         0xAAAAAAD4
        assert_eq  r2,         0xAAAAAAD4
        assert_eq  r3,         0xAAAAAAD4
        assert_eq  r4,         0xAAAAAAD4
        assert_eq  r5,         0xAAAAAAD4
        assert_eq  r6,         0xAAAAAAD4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32  u16{r0},        0xF1E2C3D4      # 119 r0 = 0x____C3D4
        mov32  u16{r1},     u8{0x______D4}     # 120 r1 = 0x____00D4
        mov32  u16{r2},     i8{0x______D4}     # 121 r2 = 0x____FFD4
        mov32  u16{r3},    u16{0x____C3D4}     # 122 r3 = 0x____C3D4
        mov32  u16{r4},    i16{0x____C3D4}     # 123 r4 = 0x____C3D4
        mov32  u16{r5},    u32{0xF1E2C3D4}     # 124 r5 = 0x____C3D4
        mov32  u16{r6},    i32{0xF1E2C3D4}     # 125 r6 = 0x____C3D4
        assert_eq  r0,         0xAAAAC3D4
        assert_eq  r1,         0xAAAA00D4
        assert_eq  r2,         0xAAAAFFD4
        assert_eq  r3,         0xAAAAC3D4
        assert_eq  r4,         0xAAAAC3D4
        assert_eq  r5,         0xAAAAC3D4
        assert_eq  r6,         0xAAAAC3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32  i16{r0},        0xF1E2C3D4      # 126 r0 = 0x____C3D4
        mov32  i16{r1},     u8{0x______D4}     # 127 r1 = 0x____00D4
        mov32  i16{r2},     i8{0x______D4}     # 128 r2 = 0x____FFD4
        mov32  i16{r3},    u16{0x____C3D4}     # 129 r3 = 0x____C3D4
        mov32  i16{r4},    i16{0x____C3D4}     # 130 r4 = 0x____C3D4
        mov32  i16{r5},    u32{0xF1E2C3D4}     # 131 r5 = 0x____C3D4
        mov32  i16{r6},    i32{0xF1E2C3D4}     # 132 r6 = 0x____C3D4
        assert_eq  r0,         0xAAAAC3D4
        assert_eq  r1,         0xAAAA00D4
        assert_eq  r2,         0xAAAAFFD4
        assert_eq  r3,         0xAAAAC3D4
        assert_eq  r4,         0xAAAAC3D4
        assert_eq  r5,         0xAAAAC3D4
        assert_eq  r6,         0xAAAAC3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32  u32{r0},        0xF1E2C3D4      # 133 r0 = 0xF1E2C3D4
        mov32  u32{r1},     u8{0x______D4}     # 134 r1 = 0x000000D4
        mov32  u32{r2},     i8{0x______D4}     # 135 r2 = 0xFFFFFFD4
        mov32  u32{r3},    u16{0x____C3D4}     # 136 r3 = 0x0000C3D4
        mov32  u32{r4},    i16{0x____C3D4}     # 137 r4 = 0xFFFFC3D4
        mov32  u32{r5},    u32{0xF1E2C3D4}     # 138 r5 = 0xF1E2C3D4
        mov32  u32{r6},    i32{0xF1E2C3D4}     # 139 r6 = 0xF1E2C3D4
        assert_eq  r0,         0xF1E2C3D4
        assert_eq  r1,         0x000000D4
        assert_eq  r2,         0xFFFFFFD4
        assert_eq  r3,         0x0000C3D4
        assert_eq  r4,         0xFFFFC3D4
        assert_eq  r5,         0xF1E2C3D4
        assert_eq  r6,         0xF1E2C3D4

        mov        r0,         0xAAAAAAAA
        mov        r1,         0xAAAAAAAA
        mov        r2,         0xAAAAAAAA
        mov        r3,         0xAAAAAAAA
        mov        r4,         0xAAAAAAAA
        mov        r5,         0xAAAAAAAA
        mov        r6,         0xAAAAAAAA
        mov32  i32{r0},        0xF1E2C3D4      # 140 r0 = 0xF1E2C3D4
        mov32  i32{r1},     u8{0x______D4}     # 141 r1 = 0x000000D4
        mov32  i32{r2},     i8{0x______D4}     # 142 r2 = 0xFFFFFFD4
        mov32  i32{r3},    u16{0x____C3D4}     # 143 r3 = 0x0000C3D4
        mov32  i32{r4},    i16{0x____C3D4}     # 144 r4 = 0xFFFFC3D4
        mov32  i32{r5},    u32{0xF1E2C3D4}     # 145 r5 = 0xF1E2C3D4
        mov32  i32{r6},    i32{0xF1E2C3D4}     # 146 r6 = 0xF1E2C3D4
        assert_eq  r0,         0xF1E2C3D4
        assert_eq  r1,         0x000000D4
        assert_eq  r2,         0xFFFFFFD4
        assert_eq  r3,         0x0000C3D4
        assert_eq  r4,         0xFFFFC3D4
        assert_eq  r5,         0xF1E2C3D4
        assert_eq  r6,         0xF1E2C3D4

        halt
