



                not         r0, 0x0
                assert_eq   r0, 0xFFFFFFFF
                not32       r0, 0x0
                assert_eq   r0, 0xFFFFFFFF

                not8        r1, 0x0
                assert_eq   r1, 0xFF
                not8        r1, 0xFF
                assert_eq   r1, 0x00

                not32       r0, 0xAAAAAAAA
                assert_eq   r0, 0x55555555
                not32       r0, 0x00000001
                assert_eq   r0, 0xFFFFFFFE

                mov         r0, 0xFEDBCA98
                not32       r1, r0
                assert_eq   r1, 0x01243567
                not32       r2, r1
                assert_eq   r2, r0

                halt




