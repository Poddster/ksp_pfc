		assert_eq  SP,  0x0
		mov        SP,  0x8080

		jump        :test:


		# manually read 3 things off of the stack
:func:
		ld32       r0,   SP

		iadd       r13,  SP,  4
		ld32       r1,   r13

		iadd32     r0,  r0,  r1
		assert_eq  r0,  0xedbccdfe

		ret

		# manually push 3 things onto the stack
:test:

		# push_data 0xDEADBEEF
		stack_alloc 0x4
		st32        SP,  0xDEADBEEF

		# push_data 0x0F0F0F0F
		stack_alloc 0x4
		st32        SP,  0x0F0F0F0F

		call        :func:
		stack_free  0x8
		assert_eq   SP,  0x8080
		halt