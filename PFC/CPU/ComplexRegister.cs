﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PFC.CPU
{

    enum DynamicType
    {
        none,
        index,
        index_offset_scale,
    }

    enum WritebackType
    {
        none,         //   r0
        post_inc,     //   r0++
        pre_inc,      // ++r0
        post_dec,     //   r0--
        pre_dec,      // --r0
        ea_writeback, //   r0!  effective address write back
    }

    class ComplexRegister : IEquatable<ComplexRegister>
    {
        /// <summary>
        /// The Complex Register can either be a literal, or a (potentially dynamic) register (with writeback)
        /// </summary>
        readonly private bool bLiteral;
        readonly private uint Literal;
        public uint LiteralB32
        {
            get
            {
                Debug.Assert(this.bLiteral == true);
                return this.Literal;
            }
        }

        //              none : r0        // r,0: (from BaseReg)
        //             Index : r[r1]     // r is from BaseReg, r1 is from DynIndex. Same as [r1 + 0 * 1]
        //      Index/offset : r[r1 + 5] // same as [r1 + 5 * 1]
        //Index/offset/scale : r[r1 + 5 * 4]
        readonly private DynamicType DynamicMode;
        readonly private WritebackType Writeback;

        readonly private SimpleRegister BaseReg;
        readonly private SimpleRegister DynIndex;
        readonly private SimpleRegister DynOffset;
        readonly private SimpleRegister DynScale;

        public ComplexRegister(uint literal)
        {
            this.bLiteral = true;
            this.Literal = literal;

            this.DynamicMode = DynamicType.none;
            this.Writeback = WritebackType.none;
            this.BaseReg = null;
            this.DynIndex = null;
            this.DynOffset = null;
            this.DynScale = null;
        }

        public ComplexRegister(SimpleRegister baseReg)
        {
            this.bLiteral = false;

            Debug.Assert(baseReg != null);
            Debug.Assert(baseReg.RegType != RegisterType.scalar_reg_base);

            this.DynamicMode = DynamicType.none;
            this.Writeback = WritebackType.none;

            this.BaseReg = baseReg;

            this.DynIndex = null;
            this.DynOffset = null;
            this.DynScale = null;
        }

        internal SimpleRegister ReduceComplexRegister(DataType dataType)
        {
            Debug.Assert(dataType != DataType.undef);
            Debug.Assert(this.bLiteral == false);
            Debug.Assert(this.DynamicMode == DynamicType.none);
            Debug.Assert(this.Writeback == WritebackType.none);
            //TODO: we should copy this.
            return this.BaseReg;
        }

        //todo: delete me
        internal SimpleRegister GetEffectiveRegister()
        {
            Debug.Assert(this.bLiteral == false);
            Debug.Assert(this.DynamicMode == DynamicType.none);
            Debug.Assert(this.Writeback == WritebackType.none);
            //TODO: we should copy this.
            return this.BaseReg;
        }

        internal bool IsLiteral()
        {
            return this.bLiteral;
        }

        public bool IsDynamic()
        {
            return this.DynamicMode != DynamicType.none;
        }

        public bool DoesWriteback()
        {
            return this.Writeback != WritebackType.none;
        }

        public override string ToString()
        {
            Debug.Assert(this.DynamicMode == DynamicType.none);
            Debug.Assert(this.Writeback == WritebackType.none);
            if (this.bLiteral)
            {
                return this.Literal.ToHex();
            }
            else
            {
                return this.BaseReg.ToString();
            }
        }


        #region equality
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            ComplexRegister other = obj as ComplexRegister;
            return this.Equals(other);
        }

        public bool Equals(ComplexRegister other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (this.bLiteral)
            {
                Debug.Assert(this.DynamicMode == DynamicType.none);
                Debug.Assert(this.Writeback == WritebackType.none);
                Debug.Assert(this.BaseReg == null);
                Debug.Assert(this.DynIndex == null);
                Debug.Assert(this.DynOffset == null);
                Debug.Assert(this.DynScale == null);

                return this.Literal == other.Literal;
            }
            else
            {
                Debug.Assert(this.BaseReg != null);
                Debug.Assert(this.DynIndex == null);
                Debug.Assert(this.DynOffset == null);
                Debug.Assert(this.DynScale == null);

                return this.BaseReg.Equals(other.BaseReg) &&
                       this.DynamicMode == other.DynamicMode &&
                       this.Writeback == other.Writeback;
            }
        }

        public override int GetHashCode()
        {
            if (this.bLiteral)
            {
                Debug.Assert(this.DynamicMode == DynamicType.none);
                Debug.Assert(this.Writeback == WritebackType.none);
                Debug.Assert(this.BaseReg == null);
                Debug.Assert(this.DynIndex == null);
                Debug.Assert(this.DynOffset == null);
                Debug.Assert(this.DynScale == null);

                return this.Literal.GetHashCode();
            }
            else
            {
                Debug.Assert(this.BaseReg != null);
                Debug.Assert(this.DynIndex == null);
                Debug.Assert(this.DynOffset == null);
                Debug.Assert(this.DynScale == null);

                int hash = 17;
                hash = hash * 23 + this.BaseReg.GetHashCode();
                hash = hash * 23 + this.DynamicMode.GetHashCode();
                hash = hash * 23 + this.Writeback.GetHashCode();
                return hash;
            }
        }
        #endregion
    }
}
