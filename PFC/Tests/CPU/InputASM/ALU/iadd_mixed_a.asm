		mov           r0,      0x12345678
		mov           r1,      0x20FEDCBA

		iadd32        r2,      r0,      r1           #2 r2 = 0x33333332
		assert_eq     r2,      0x33333332
		iadd8         r3,      r0,      r1           #4 r3 = 0x32
		assert_eq     r3,      0x32
		iadd32        r4,      r0,   i8{r1}          #6 r4 = 0x12345632
		assert_eq     r4,      0x12345632
		iadd32    i16{r5},     r0,      r1           #8 r5 = 0x3332
		assert_eq     r5,      0x3332
		iadd16    i32{r6},     r0,      r1           #10 r6 = 0x00003332
		assert_eq     r6,      0x00003332
		iadd32     i8{r7},  i8{r0},  i8{r1}          #12 r7 = 0x32
		assert_eq     r7,      0x32
		iadd16    i32{r8},     r0,      0xeeee5432   #14 r8 = 0xFFFFaaaa (sign extended)
		assert_eq     r8,      0xFFFFaaaa
		iadd16    u32{r8},     r0,      0xeeee5432   #16 r8 = 0x0000aaaa (NOT sign extended)
		assert_eq     r8,      0x0000aaaa
		iadd8     i32{r8}, i32{r0}, i32{0xeeee5432}  #18 r8 = 0xFFFFFFAA
		assert_eq     r8,      0xFFFFFFAA

		iadd8     u32{r0},     0xff,    0x1          #20
		assert_eq     r0,      0x0

		halt
