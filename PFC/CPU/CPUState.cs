﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace PFC.CPU
{
    using JetBrains.Annotations;

    /// <summary>
    /// C#'s lack of immutable method parameters means I've made a read-only interface for the CPU State.
    /// having to use the heavy-weight ReadOnlyCollection here. If it's a performance problem, just switch back to
    /// returning an array.
    /// </summary>
    internal interface ICPUStateRO
    {
        ulong ProgramCounter { get; }
        ulong StackPointer { get; }
        ulong LinkRegister { get; }

        [NotNull]
        ReadOnlyCollection<ulong> RegisterBank { get; }

        [NotNull]
        ReadOnlyCollection<Instruction> CodeROM { get; }

        [NotNull]
        ReadOnlyCollection<byte> RAM { get; }
        uint cycleCounter { get; }
        bool IsHalted { get; }

        ulong ReadAlignedMemory(uint address, uint numBytes);

        [NotNull]
        SimpleRegister ReduceRegOperand([NotNull] OperandReg operand, DataType evalType);
        ulong GetValueB64(Operand operand,
                        DataType instructionType = DataType.u64);
        uint GetValueB32(Operand operand,
                        DataType instructionType = DataType.u32);
        uint GetValueB24(Operand operand,
                        DataType instructionType = DataType.u24);
        ushort GetValueB16(Operand operand,
                        DataType instructionType = DataType.u16);
        byte GetValueB8(Operand operand,
                        DataType instructionType = DataType.u8);
        ulong GetValueBX(Operand operand, uint instWidth,
                         DataType instType = DataType.undef);
    }

    internal class CPUState : ICPUStateRO
    {
        private ulong[] RW_RegisterBank { get; set; }
        /// <summary>
        /// Note that code rom is in terms of Instruction, rather than bytes.
        /// Currently there'stateChanges no explicit machine-code bitcode representation
        /// therefore no self modifying code or codegen. That also means PC_val goes 0,1,2,3,4 rather than 0,4,8,C or
        /// 0,2,4,8,10,C,E,18 etc etc
        /// </summary>
        private Instruction[] RW_CodeROM { get; set; }
        /// <summary>
        /// Ram. Can have more ram than address bus can specify using crazy things like segmentation, etc.
        /// </summary>
        private byte[] RW_RAM { get; set; }
        public uint cycleCounter { get; set; }

        private readonly ReadOnlyCollection<ulong> RO_RegisterBank;
        private readonly ReadOnlyCollection<Instruction> RO_CodeROM;
        private readonly ReadOnlyCollection<byte> RO_RAM;

        public ulong ProgramCounter { get; private set; }
        public ulong StackPointer { get; private set; }
        public ulong LinkRegister { get; private set; }
        public ulong ControlStackPointer { get; private set; }
        public ulong FramePointer { get; private set; }


        [NotNull]
        public ulong[] ControlStack { get; private set; }


        public ReadOnlyCollection<ulong> RegisterBank { get { return this.RO_RegisterBank; } }
        public ReadOnlyCollection<Instruction> CodeROM { get { return this.RO_CodeROM; } }
        public ReadOnlyCollection<byte> RAM { get { return this.RO_RAM; } }

        public readonly uint WordSizeInBits;

        public bool IsHalted
        {
            get
            {
                return this.isHalted;
            }
        }

        private bool isHalted;


        public CPUFlags Flags
        {
            get
            {
                return this._flags;
            }
        }
        private CPUFlags _flags;

        //Make a new new constructor that allows the initial RAM to be given? Useful for tests.
        public CPUState(uint version, uint wordSizeInBits, Instruction[] program)
        {
            var validSizes = new uint[] { 8, 16, 24, 32, 64 };
            if (!validSizes.Contains(wordSizeInBits))
            {
                string error = String.Format("Should be one of [{0}]", string.Join(",", validSizes));
                throw new ArgumentException(error, "wordSizeInBits");
            }
            if (program == null)
            {
                throw new ArgumentNullException("program", "program can't be null");
            }

            this.WordSizeInBits = wordSizeInBits;
            this.cycleCounter = 0;
            this.ProgramCounter = 0;
            this.LinkRegister = 0;
            this.RW_RegisterBank = new ulong[16];
            this.RW_CodeROM = program;
            this.RW_RAM = new byte[0x1 << 16];

            // todo: should probaby just use Stack<ulong> and have both these thiings map onto it?;
            this.StackPointer = 0;
            this.ControlStack = new ulong[30];
            this.FramePointer = 0;
            

            //set up readonly wrappers. The collections they reference should change values when
            // the non-readonly ones change.
            this.RO_RegisterBank = Array.AsReadOnly<ulong>(this.RW_RegisterBank);
            this.RO_CodeROM = Array.AsReadOnly<Instruction>(this.RW_CodeROM);
            this.RO_RAM = Array.AsReadOnly<byte>(this.RW_RAM);


            var defaultUpdate = new CPUStateUpdate();
            this.ApplyStateUpdate(defaultUpdate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessSize"></param>
        /// <param name="byteOffset"></param>
        /// <param name="thing">string representation of the thing accessing data</param>
        private void ValidateSize(uint returnSize, uint accessSize, uint byteOffset, string thing)
        {
            //TODO is ValidateSize still needed?
            ValidateReturnSize(returnSize, thing);
            ValidateAccessSize(accessSize, byteOffset, thing);
        }

        private void ValidateAccessSize(uint accessSize, uint byteOffset, string thing)
        {

            uint maxBitAccessed = (accessSize - 1) + (byteOffset * 8);
            if (maxBitAccessed >= this.WordSizeInBits)
            {
                string error = string.Format("Can't access [byte{0}:byte{1}] ({2}bits {3}Bytes) on a {4}bit CPU. {5}",
                                             byteOffset,
                                             maxBitAccessed / 8,
                                             accessSize,
                                             accessSize / 8,
                                             this.WordSizeInBits,
                                             thing);
                throw new InvalidOperationException(error);
            }
        }

        private void ValidateReturnSize(uint returnSize, string thing)
        {
            if (returnSize > this.WordSizeInBits)
            {
                string error = string.Format("Can't read {0} bits on a {1}bit CPU. {2}",
                                             returnSize,
                                             this.WordSizeInBits,
                                             thing);
                throw new InvalidOperationException(error);
            }
        }

        private void ValidateRegisterByteOffset(SimpleRegister reg, DataType evalType)
        {
            //todo is this adapter needed? will there be one that takes an operand?
            uint width = evalType.GetBitWidth(this.WordSizeInBits);
            ValidateAccessSize(width, reg.ByteOffset, reg.ToString());
        }



        //change all uses of TESTONLY_ApplyStateChange and TESTONLY_ApplyStateChanges to use ApplyStateUpdate
        internal void TESTONLY_ApplyStateChange(CPURegMemStateChange change)
        {
            this.ApplyRegMemStateChange(change);
        }
        internal void TESTONLY_ApplyStateChanges(IList<CPURegMemStateChange> change)
        {
            this.ApplyRegMemStateChanges(change);
        }



        internal void ApplyStateUpdate(CPUStateUpdate update)
        {
            if (update.Halt)
            {
                this.isHalted = true;
            }
            if (update.UpdateFlags)
            {
                Debug.Assert(Utility.IsDefinedEx(update.Flags));
                this._flags = update.Flags;
            }
            this.ApplyRegMemStateChanges(update.RegMemChanges);
            this.cycleCounter += update.CycleCost;
        }
        private void ApplyRegMemStateChanges(IList<CPURegMemStateChange> changes)
        {
            // todo check that there are no write conflicts?
            Debug.Assert(changes != null);
            foreach (var change in changes)
            {
                this.ApplyRegMemStateChange(change);
            }
        }
        private void ApplyRegMemStateChange(CPURegMemStateChange change)
        {
            Debug.Assert(change != null);
            //need to get CPURegMemStateChange size. If it's undef then use CPUState width
            ValidateSize(this.WordSizeInBits, change.DataType.GetBitWidth(this.WordSizeInBits),
                         change.Reg.ByteOffset, change.ToString());

            var reg = change.Reg;
            switch (reg.RegType)
            {
                case RegisterType.memory:
                    Debug.Assert(reg.ByteOffset == 0);
                    Debug.Assert(BitConverter.IsLittleEndian == true);


                    int numBytes = unchecked((int)(change.DataType.GetBitWidth(this.WordSizeInBits) / 8));

                    //Take is important -- it stops Array.Reverse reversing the wrong range
                    byte[] bytes = BitConverter.GetBytes(change.Value).Take(numBytes).ToArray();
                    Debug.Assert(bytes.Length == numBytes);
                    if (BitConverter.IsLittleEndian == false)
                    {
                        Array.Reverse(bytes);
                    }
                    var baseAddr = reg.Index;
                    for (int i = 0; i < numBytes; i++)
                    {
                        this.RW_RAM[baseAddr + i] = bytes[i];
                    }
                    break;
                case RegisterType.scalar_reg:
                    //read in old value, clear out the bytes being written to,
                    //stick in the new ones, write back to register

                    ulong currentVal = this.RW_RegisterBank[reg.Index];
                    unchecked
                    {
                        ulong mask = change.DataType.GetDataTypeMask();
                        //this should have been picked up in the CPURegMemStateChange constructor.
                        Debug.Assert((change.Value & mask) == change.Value);

                        int shiftAmount = (int)(reg.ByteOffset * 8);
                        currentVal &= ~(mask << shiftAmount);
                        currentVal |= (change.Value << shiftAmount);
                    }
                    this.RW_RegisterBank[reg.Index] = currentVal;
                    break;
                case RegisterType.program_counter:
                    Debug.Assert(change.DataType == DataType.u16);
                    Debug.Assert(change.Reg.ByteOffset == 0);
                    Debug.Assert(change.Reg.Index == 0);
                    this.ProgramCounter = change.Value;
                    break;
                case RegisterType.stack_pointer:
                    Debug.Assert(change.DataType == DataType.u16);
                    Debug.Assert(change.Reg.ByteOffset == 0);
                    Debug.Assert(change.Reg.Index == 0);
                    this.StackPointer = change.Value;
                    break;
                case RegisterType.frame_pointer:
                    Debug.Assert(change.DataType == DataType.u16);
                    Debug.Assert(change.Reg.ByteOffset == 0);
                    Debug.Assert(change.Reg.Index == 0);
                    this.FramePointer = change.Value;
                    break;
                case RegisterType.link_register:
                    Debug.Assert(change.DataType == DataType.u16);
                    Debug.Assert(change.Reg.ByteOffset == 0);
                    Debug.Assert(change.Reg.Index == 0);
                    this.LinkRegister = change.Value;
                    break;
                case RegisterType.control_stack_pointer:
                    Debug.Assert(change.DataType == DataType.u16);
                    Debug.Assert(change.Reg.ByteOffset == 0);
                    Debug.Assert(change.Reg.Index == 0);
                    this.ControlStackPointer = change.Value;
                    break;
                case RegisterType.control_stack_val:
                    Debug.Assert(change.DataType == DataType.undef);
                    Debug.Assert(change.Reg.ByteOffset == 0);
                    Debug.Assert(change.Reg.Index < this.ControlStack.Length);
                    this.ControlStack[reg.Index] = change.Value;
                    break;
                default:
                    string error = string.Format("Can't apply changes to Reg {0}", reg.ToString());
                    throw new NotImplementedException(error);
            }
        }



        private ulong ReadMemory(uint address, uint numBytes)
        {
            Debug.Assert(numBytes != 0 && numBytes < 8);

            ulong output = 0;
            for (int i = 0; i < numBytes; i++)
            {
                int mem_index = unchecked((int)(address + i));
                Debug.Assert(mem_index >= 0);
                byte b = this.RAM[mem_index];
                ulong u = b;
                output |= u << (i * 8);
            }
            return output;
        }

        public ulong ReadAlignedMemory(uint address, uint numBytes)
        {
            Debug.Assert(numBytes != 0);
            var evalType = OperandEnumHelpers.DataTypeFromWidth(numBytes * 8);
            //todo should include PC etc.
            OperandEnumHelpers.ThrowIfInvalidAlignment(evalType, address);

            return ReadMemory(address, numBytes);
        }

        private ulong ReadRegister(uint index, uint byteOffset)
        {
            int reg_index = unchecked((int)index);
            Debug.Assert(reg_index >= 0);

            var raw = this.RW_RegisterBank[reg_index];
            int shiftVal = unchecked((int)(byteOffset * 8));
            var shifted = raw >> shiftVal;
            return shifted;
        }


        //---------------
        // Get 'value' that fits on the internal bus
        private ulong EvaluateRegisterValue(SimpleRegister reg, DataType evalType)
        {
            Debug.Assert(evalType != DataType.undef);
            uint dataBitWidth = evalType.GetBitWidth(this.WordSizeInBits);
            ulong dataTypeMask = evalType.GetDataTypeMask();
            ulong maskedVal;
            switch (reg.RegType)
            {
                case RegisterType.memory:
                    //memory address should already have been evaluated in the same
                    //eval data type context, so it should fit...
                    Debug.Assert(reg.ByteOffset == 0);
                    if ((reg.Index & dataTypeMask) != reg.Index)
                    {
                        string err = string.Format("Invalid register `{2}`: " + 
                                                        "Memory index `{0}` too large for type context `{1}`",
                                                    reg.Index.ToHex(),
                                                    evalType.ToString(),
                                                    reg.ToString());
                        throw new ArgumentException(err, "reg");
                    }
                    OperandEnumHelpers.ThrowIfInvalidAlignment(evalType, reg.Index);

                    uint numBytes = dataBitWidth / 8;
                    ulong mem_val = ReadMemory(reg.Index, numBytes);

                    //we should only read evalType number of bytes, so this should fit.
                    Debug.Assert((mem_val & dataTypeMask) == mem_val);
                    maskedVal = mem_val & dataTypeMask;
                    break;
                case RegisterType.scalar_reg:
                    ulong reg_val = ReadRegister(reg.Index, reg.ByteOffset);
                    maskedVal = reg_val & dataTypeMask;
                    break;
                case RegisterType.program_counter:
                    Debug.Assert(reg.Index == 0);
                    Debug.Assert(reg.ByteOffset == 0);
                    return this.ProgramCounter;
                case RegisterType.stack_pointer:
                    Debug.Assert(reg.Index == 0);
                    Debug.Assert(reg.ByteOffset == 0);
                    return this.StackPointer;
                case RegisterType.frame_pointer:
                    Debug.Assert(reg.Index == 0);
                    Debug.Assert(reg.ByteOffset == 0);
                    return this.FramePointer;
                case RegisterType.link_register:
                    Debug.Assert(reg.Index == 0);
                    Debug.Assert(reg.ByteOffset == 0);
                    return this.LinkRegister;
                case RegisterType.control_stack_pointer:
                    Debug.Assert(reg.Index == 0);
                    Debug.Assert(reg.ByteOffset == 0);
                    return this.ControlStackPointer;
                default:
                    string error = string.Format("Can't read raw value for reg {0}", reg.ToString());
                    throw new NotImplementedException(error);
            }
            if (evalType.NeedsSignExtending())
            {
                return maskedVal.SignExtendArb(dataBitWidth);
            }
            else
            {
                return maskedVal;
            }
        }

        [NotNull]
        private SimpleRegister ReduceOperandInContext(
            [NotNull] OperandReg regOperand,
                      DataType evalType)
        {
            Debug.Assert(evalType != DataType.undef);
            Debug.Assert(regOperand.AddressMode != AddressMode.literal);
            Debug.Assert(regOperand.IsDynamic == false); //not implemented yet
            Debug.Assert(regOperand.DoesWriteBack == false); //not implemented yet

            switch (regOperand.AddressMode)
            {
                case AddressMode.reg_direct:
                    return regOperand.BaseReg.ReduceComplexRegister(evalType);
                case AddressMode.reg_deref:
                    ulong address;
                    if (regOperand.BaseReg.IsLiteral())
                    {
                        address = regOperand.BaseReg.LiteralB32;
                    }
                    else
                    {
                        var reg = regOperand.BaseReg.ReduceComplexRegister(evalType);
                        address = EvaluateRegisterValue(reg, evalType);
                    }

                    //todo take this from the address bus size?
                    uint clamped_address = (uint) (address & 0xFFFF);
                    var memreg = SimpleRegister.Reg1D(RegisterType.memory, index:clamped_address);
                    return memreg;
                default:
                    throw new NotImplementedException();
            }

        }

        public static DataType CalculateEvaluationDataType(Operand operand, DataType instructionType)
        {
            DataType evalType;
            DataType operandType = operand.DataType;
            string operandString = operand.ToString();

            evalType = ResolveUndefDataType(instructionType, operandType, operandString);
            return evalType;
        }

        private static DataType ResolveUndefDataType(DataType weakType,
                                                            DataType strongType,
                                                            string operandString)
        {
            DataType evalType;
            if (strongType == DataType.undef)
            {
                evalType = weakType;
                if (evalType == DataType.undef)
                {
                    string err = string.Format("Must provide an evaluation context for operand {0}",
                                                operandString);
                    throw new InvalidOperationException(err);
                }
            }
            else
            {
                evalType = strongType;
            }
            return evalType;
        }

        public ulong EvaluateOperandValue(Operand operand,
                                          DataType instructionType = DataType.undef)
        {
            //todo : should this be here or in all GetValueXXX funcs?
            ValidateReturnSize(instructionType.GetBitWidth(this.WordSizeInBits),
                               operand.ToString());
            ValidateReturnSize(operand.DataType.GetBitWidth(this.WordSizeInBits),
                               operand.ToString());

            DataType evalType = CalculateEvaluationDataType(operand, instructionType);
            Debug.Assert(evalType != DataType.undef);


            switch (operand.AddressMode)
            {
                case AddressMode.literal:
                    OperandLiteral litOp = operand as OperandLiteral;
                    if (litOp == null)
                    {
                        throw new ArgumentException("`AddressMode.literal` but `as OperandLiteral` failed?",
                                                    "operand");
                    }

                    var lit_mask = evalType.GetDataTypeMask();
                    var lit_val = litOp.LiteralB32 & (uint)lit_mask;
                    if (evalType.NeedsSignExtending())
                    {
                        return Utility.SignExtendArb(lit_val, evalType.GetBitWidth(this.WordSizeInBits));
                    }
                    else
                    {
                        return lit_val;
                    }
                case AddressMode.reg_direct:
                case AddressMode.reg_deref:
                    var regOperand = operand as OperandReg;
                    if (regOperand == null)
                    {
                        string err = string.Format("`AddressMode.{0}` but `as OperandReg` failed?",
                                                   operand.AddressMode);
                        throw new ArgumentException(err, "operand");
                    }
                    var reg = ReduceOperandInContext(regOperand, evalType);

                    ValidateRegisterByteOffset(reg, evalType);

                    var reg_val = EvaluateRegisterValue(reg, evalType);
                    return reg_val;
                case AddressMode.reg_deref_offset_scale:
                default:
                    throw new NotImplementedException();
            }
        }


        //---------------



        //should probably change name of GetValueB.. to EvalOperandSIZE. (evaluate operand in SIZE context)


        //Resolve an operand into a simple register by evaluating it in the given data type context.
        [NotNull]
        public SimpleRegister ReduceRegOperand(
            [NotNull] OperandReg operand,
                      DataType instType)
        {
            if (instType == DataType.undef)
            {
                instType = this.WordSizeInBits.DataTypeFromWidth();
            }
            DataType evalType = CalculateEvaluationDataType(operand, instType);
            if (evalType == DataType.undef)
            {
                throw new ArgumentException("Evaluating in an undefined context", "evalType");
            }

            return ReduceOperandInContext(operand, evalType);
        }

        public ulong GetValueB64(Operand operand,
                                DataType instructionType = DataType.u64)
        {
            ulong full_val = EvaluateOperandValue(operand, instructionType);
            return full_val;
        }

        public uint GetValueB32(Operand operand,
                                DataType instructionType = DataType.u32)
        {
            ulong full_val = EvaluateOperandValue(operand, instructionType);
            uint cast_val = unchecked((uint)(full_val & 0xFFFFFFFF));
            return cast_val;
        }


        public uint GetValueB24(Operand operand,
                                DataType instructionType = DataType.u24)
        {
            ulong full_val = EvaluateOperandValue(operand, instructionType);
            uint cast_val = unchecked((uint)(full_val & 0xFFFFFF));
            return cast_val;
        }

        public ushort GetValueB16(Operand operand,
                                DataType instructionType = DataType.u16)
        {
            ulong full_val = EvaluateOperandValue(operand, instructionType);
            ushort cast_val = unchecked((ushort)(full_val & 0xFFFF));
            return cast_val;
        }

        public byte GetValueB8(Operand operand,
                                DataType instructionType = DataType.u8)
        {
            ulong full_val = EvaluateOperandValue(operand, instructionType);
            byte cast_val = unchecked((byte)(full_val & 0xFF));
            return cast_val;
        }

        public ulong GetValueBX(Operand operand, uint readWidth,
                                DataType instType = DataType.undef)
        {
            ulong val;
            Debug.Assert(readWidth != 0);
            if (instType == DataType.undef)
            {
                instType = OperandEnumHelpers.DataTypeFromWidth(readWidth);
            }
            switch (readWidth)
            {
                case 32:
                    val = this.GetValueB32(operand, instType);
                    break;
                case 16:
                    val = this.GetValueB16(operand, instType);
                    break;
                case 8:
                    val = this.GetValueB8(operand, instType);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return val;
        }

    }
}