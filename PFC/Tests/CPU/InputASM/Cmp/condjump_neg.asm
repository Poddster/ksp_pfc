		mov   r0, 0x0
		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0x00000000  #          +0 -          +0 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0x00000001  #          +0 -          +1 = 0xffffffff = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 0x80000001 = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0x80000000  #          +0 - -2147483648 = 0x80000000 = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0x80000001  #          +0 - -2147483647 = 0x7fffffff =     
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0xFFFFFFFE  #          +0 -          -2 = 0x00000002 =     
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 0x00000001 =     
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:


		######
		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0x00000000  #          +1 -          +0 = 0x00000001 =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0x00000001  #          +1 -          +1 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0x7FFFFFFF  #          +1 - +2147483647 = 0x80000002 = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0x80000000  #          +1 - -2147483648 = 0x80000001 = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0x80000001  #          +1 - -2147483647 = 0x80000000 = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0xFFFFFFFE  #          +1 -          -2 = 0x00000003 =     
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x00000001, 0xFFFFFFFF  #          +1 -          -1 = 0x00000002 =     
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:


		######
		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 0x7fffffff =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0x00000001  # +2147483647 -          +1 = 0x7ffffffe =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = 0xffffffff = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0x80000001  # +2147483647 - -2147483647 = 0xfffffffe = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0xFFFFFFFE  # +2147483647 -          -2 = 0x80000001 = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 0x80000000 = N V 
		bz   :fail:
		bpos :fail:
		bnv  :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsle :fail:
		bslt :fail:


		######
		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0x00000000  # -2147483648 -          +0 = 0x80000000 = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0x00000001  # -2147483648 -          +1 = 0x7fffffff =   VC
		bz   :fail:
		bneg :fail:
		bnv  :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 0x00000001 =   VC
		bz   :fail:
		bneg :fail:
		bnv  :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0x80000000  # -2147483648 - -2147483648 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0x80000001  # -2147483648 - -2147483647 = 0xffffffff = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0xFFFFFFFE  # -2147483648 -          -2 = 0x80000002 = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 0x80000001 = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:


		######
		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0x00000000  # -2147483647 -          +0 = 0x80000001 = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0x00000001  # -2147483647 -          +1 = 0x80000000 = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0x7FFFFFFF  # -2147483647 - +2147483647 = 0x00000002 =   VC
		bz   :fail:
		bneg :fail:
		bnv  :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0x80000000  # -2147483647 - -2147483648 = 0x00000001 =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0x80000001  # -2147483647 - -2147483647 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0xFFFFFFFE  # -2147483647 -          -2 = 0x80000003 = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0x80000001, 0xFFFFFFFF  # -2147483647 -          -1 = 0x80000002 = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:


		######
		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0x00000000  #          -2 -          +0 = 0xfffffffe = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0x00000001  #          -2 -          +1 = 0xfffffffd = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0x7FFFFFFF  #          -2 - +2147483647 = 0x7fffffff =   VC
		bz   :fail:
		bneg :fail:
		bnv  :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0x80000000  #          -2 - -2147483648 = 0x7ffffffe =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0x80000001  #          -2 - -2147483647 = 0x7ffffffd =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0xFFFFFFFE  #          -2 -          -2 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFE, 0xFFFFFFFF  #          -2 -          -1 = 0xffffffff = N   
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bc   :fail:
		beq  :fail:
		bugt :fail:
		buge :fail:
		bsgt :fail:
		bsge :fail:


		######
		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0x00000000  #          -1 -          +0 = 0xffffffff = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0x00000001  #          -1 -          +1 = 0xfffffffe = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 0x80000000 = N  C
		bz   :fail:
		bpos :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsgt :fail:
		bsge :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 0x7fffffff =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0x80000001  #          -1 - -2147483647 = 0x7ffffffe =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0xFFFFFFFE  #          -1 -          -2 = 0x00000001 =    C
		bz   :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		beq  :fail:
		bule :fail:
		bult :fail:
		bsle :fail:
		bslt :fail:

		iadd  r0, r0, 0x1
		icmp! 0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 0x00000000 =  Z C
		bnz  :fail:
		bneg :fail:
		bv   :fail:
		bnc  :fail:
		bneq :fail:
		bugt :fail:
		bult :fail:
		bsgt :fail:
		bslt :fail:


		######

		mov r0, 0x0
:fail:
		assert_eq r0, 0x0
		halt

