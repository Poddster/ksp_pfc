﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Diagnostics;
using JetBrains.Annotations;

namespace PFC.CPU
{
    internal sealed class Instruction : IEquatable<Instruction>
    {

        private readonly uint BitWidth;
        public readonly OpcodeType Opcode;
        [NotNull] public readonly ReadOnlyCollection<Operand> Operands;
        public readonly bool WritesCPUFlags;

        public uint GetBitWidth(uint CPUWordSize)
        {
            if (this.BitWidth == 0)
            {
                return CPUWordSize;
            }
            else
            {
                return this.BitWidth;
            }
        }

        public Instruction(OpcodeType opcode, uint bitWidth = 0, bool writesCPUFlags = false) :
            this(opcode, new Operand[0], bitWidth, writesCPUFlags)
        {
        }

        public Instruction(OpcodeType opcode, Operand op0, uint bitWidth = 0, bool writesCPUFlags = false) :
            this(opcode, new Operand[] { op0 }, bitWidth, writesCPUFlags)
        {
        }

        public Instruction(OpcodeType opcode, Operand op0, Operand op1, uint bitWidth = 0, bool writesCPUFlags = false) :
            this(opcode, new Operand[] { op0, op1 }, bitWidth, writesCPUFlags)
        {
        }

        public Instruction(OpcodeType opcode, Operand op0, Operand op1, Operand op2, uint bitWidth = 0, bool writesCPUFlags = false) :
            this(opcode, new Operand[] { op0, op1, op2 }, bitWidth, writesCPUFlags)
        {
        }

        public Instruction(OpcodeType opcode, Operand op0, Operand op1, Operand op2, Operand op3, uint bitWidth = 0, bool writesCPUFlags = false) :
            this(opcode, new Operand[] { op0, op1, op2, op3 }, bitWidth, writesCPUFlags)
        {
        }

        public Instruction(OpcodeType opcode, Operand[] operands,
                           uint bitWidth = 0, bool writesCPUFlags = false)
        {
            #region check_args
            if (operands == null)
            {
                throw new ArgumentNullException("operands");
            }

            if (!Enum.IsDefined(typeof(OpcodeType), opcode))
            {
                string error = String.Format("invalid opcode enum {0}:{1}", (int)opcode, opcode.ToString());
                throw new ArgumentException(error, "opcode");
            }

            var info = InstructionInfo.Table[opcode];

            var numOperands = info.NumOperands;
            if (numOperands != operands.Length)
            {
                string error = String.Format("Opcode ({0}){1} takes {2} operands, {3} given.",
                        (int)opcode, opcode.ToString(), numOperands, operands.Length);

                throw new ArgumentException(error, "operands");
            }

            var numDests = info.NumDests;
            foreach (var dest in operands.Take((int)numDests))
            {
                if (opcode != OpcodeType.st &&
                    (dest.AddressMode == AddressMode.literal ||
                     dest.AddressMode == AddressMode.label))
                {
                    string error = String.Format("Opcode '{0}' can't have literal dest '{1}'",
                            opcode.ToString(), dest.ToString());

                    throw new ArgumentException(error, "operands");
                }
                else if (dest.AddressMode == AddressMode.reg_direct)
                {
                    var destReg = dest as OperandReg;
                    Debug.Assert(destReg != null);

                    //todo: this method of checking if an operand is PC is a massive hack..
                    if (destReg.BaseReg.GetEffectiveRegister().RegType == RegisterType.program_counter)
                    {
                        string error = String.Format(
                            "Opcode '{0}' can't have Program Counter reg as dest '{1}'",
                            opcode.ToString(),
                            dest.ToString());
                        throw new ArgumentException(error, "operands");
                    }
                }

            }

            Debug.Assert(((info.InstFlags & InstructionFlags.BIT_WIDTH_REQUIRED) == 0x0) ||
                         ((info.InstFlags & InstructionFlags.BIT_WIDTH_DISALLOWED) == 0x0));
            if (((info.InstFlags & InstructionFlags.BIT_WIDTH_REQUIRED) != 0x0) && (bitWidth == 0))
            {
                string error = String.Format("Opcode ({0}){1} requires an explicit width, e.g. `{1}32`",
                        (int)opcode, opcode.ToString());
                throw new ArgumentException(error, "bitWidth");
            }
            else if (((info.InstFlags & InstructionFlags.BIT_WIDTH_DISALLOWED) != 0x0) && (bitWidth != 0))
            {
                string error = String.Format("Opcode ({0}){1} cannot have a an explicit width, e.g. `{1}`",
                        (int)opcode, opcode.ToString());
                throw new ArgumentException(error, "bitWidth");
            }

            bool canSetFlags = info.CpuFlagsModified != CPUFlags.None;
            bool mustSetFlags = info.InstFlags.HasFlag(InstructionFlags.FLAGSMOD_HIGHLYSUGGESTED);
            Debug.Assert(canSetFlags || !mustSetFlags);
            if (!canSetFlags && writesCPUFlags)
            {
                string error = String.Format("Opcode '{1}!' cannot write the CPU flags. Remove the '!' symbol, just do '{1}'",
                                             (int)opcode, opcode.ToString());
                throw new ArgumentException(error, "writesCPUFlags");
            }
            else if (mustSetFlags && !writesCPUFlags)
            {
                string error = String.Format(
                        "The only purpose of '{1}' is to set CPU flags. Please do '{1}!' instead of '{1}' to set the flags with this instruction",
                        (int)opcode, opcode.ToString());
                throw new ArgumentException(error, "writesCPUFlags");
            }

            #endregion


            this.BitWidth = bitWidth;
            this.Opcode = opcode;
            this.Operands = Array.AsReadOnly<Operand>(operands);
            this.WritesCPUFlags = writesCPUFlags;
        }

        public override string ToString()
        {
            string widthStr = (this.BitWidth == 0) ? "" : this.BitWidth.ToString();
            var shortOperands = this.Operands.Select((oper)=> oper.ToStringShort());
            string operandsStr = String.Join<string>(", ", shortOperands);
            string final = String.Format("{0}{3}{1} {2}",
                                         this.Opcode.ToString(),
                                         widthStr,
                                         operandsStr,
                                         this.WritesCPUFlags?"!":"");
            return final;
        }







        public override bool Equals(object obj)
        {
            Instruction inst = obj as Instruction;
            if (Object.ReferenceEquals(inst, null))
            {
                return false;
            }
            return this.Equals(inst);
        }


        public bool Equals(Instruction other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }

            Debug.Assert(this.Operands != null && other.Operands != null);

            return this.BitWidth == other.BitWidth &&
                   this.Opcode == other.Opcode &&
                   this.Operands.SequenceEqual(other.Operands);
               
        }

        public override int GetHashCode()
        {
            Debug.Assert(this.Operands != null);
            int hash = 17;
            hash = hash * 23 + this.BitWidth.GetHashCode();
            hash = hash * 23 + this.Opcode.GetHashCode();
            hash = hash * 23 + this.Operands.GetHashCode();
            return hash;
        }
    }
}
