﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFC;
using PFC.ASMParser;


namespace PFC.Tests.CPU
{
    using PFC.CPU;

    //Also tests lexer.
    public static class LexAndParserOperandTests
    {

        //functions to use during tests:
        private static List<LexToken> fTestLexOperands(string s, bool bReturnWhitespace = false)
        {
            var lexer = new LexerContext(s, 0);
            var tokenList = new List<LexToken>(lexer.ScanAllRemainingTokens(bReturnWhitespace));
            return tokenList;
        }

        private static List<PFC.CPU.Operand> fTestParseOperands(string s)
        {
            //this function is stupid. Why is it mimicing the parser?
            var parserContext = new PFC.ASMParser.Parser();
            parserContext.StartNewLine(s);

            OpcodeTextResult opcodeInfo = new OpcodeTextResult(OpcodeType.iadd,
                                                               InstructionInfo.Table[OpcodeType.iadd],
                                                               new LexToken(TokenType.Text, "iadd", 0, 0, 3),
                                                               32, writesCPUFlags:false);
            parserContext.SetLineOpcode(opcodeInfo);

            var operands = new List<PFC.CPU.Operand>();
            while (parserContext.Current().TokType != TokenType.EOL)
            {
                if (parserContext.Current().TokType == TokenType.Comma)
                {
                    parserContext.ConsumeExpected(TokenType.Comma);
                }

                var operand = PFC.ASMParser.PFCParser.ParseOperand(parserContext);
                operands.Add(operand);
            }

            return operands;
        }

        private static void printLexedStuff(string asm)
        {
            var toks = fTestLexOperands(asm);
            Log.Print(asm + "\n\t" + String.Join("\n\t", toks) + "\n...");
        }

        private static void printParsedStuff(string asm)
        {
            var toks = fTestParseOperands(asm);
            Log.Print(asm + "\n\t" + String.Join("\n\t", toks) + "\n...");
        }

        /// <summary>
        /// Checks to make sure a test throws an exception.
        /// </summary>
        /// <typeparam name="exceptionType"></typeparam>
        /// <param name="func"></param>
        private static void AssertThrows<exceptionType>(Action func) where exceptionType : Exception
        {
            bool bPassed = false;
            try
            {
                func();
            }
            catch (exceptionType e)
            {
                Debug.Assert(e.Message != null); //just to stop compiler complaining e is not used
                bPassed = true;
            }
            Debug.Assert(bPassed);
        }




        /// <summary>
        /// Ensures that the parser + lexer produces tokens that we expect
        /// </summary>
        private static void AssertExpectedLexSymbols(string sourceLine,
                                                     TokenType[] expectedLexerTokens)
        {
            var lexTokens = fTestLexOperands(sourceLine);
            Debug.Assert(lexTokens.Count() == expectedLexerTokens.Count());

            var tokenTypes = from token in lexTokens select token.TokType;
            Debug.Assert(tokenTypes.Count() == expectedLexerTokens.Count());
            Debug.Assert(tokenTypes.SequenceEqual(expectedLexerTokens));
        }

        /// <summary>
        /// Ensures that the parser + lexer produces tokens that we expect
        /// </summary>
        private static void AssertExpectedParsedSymbols(string sourceLine,
                                                        TokenType[] expectedLexerTokens,
                                                        PFC.CPU.Operand[] expectedParserOperands)
        {
            AssertExpectedLexSymbols(sourceLine, expectedLexerTokens);

            var results = fTestParseOperands(sourceLine);
            Debug.Assert(results.Count() == expectedParserOperands.Count());
            Debug.Assert(results.SequenceEqual(expectedParserOperands));
        }



        //tests the lexer handles blank stuff
        private static void LexTestEmptyLines()
        {
            //Expect comments and whitespace to be ignored.
            AssertExpectedParsedSymbols("", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols(" ", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("\r", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("\n", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("\r\n", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("\n\r", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("#comment", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols(" #comment", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("  #comment", new TokenType[0], new PFC.CPU.Operand[0]);
            AssertExpectedParsedSymbols("  #comment# also ignored", new TokenType[0], new PFC.CPU.Operand[0]);

            var tokensWS = fTestLexOperands("\n", bReturnWhitespace: true);
            Debug.Assert(tokensWS.Count == 1 && tokensWS[0].TokType == TokenType.WhiteSpace);

            tokensWS = fTestLexOperands("\r\n", bReturnWhitespace: true);
            Debug.Assert(tokensWS.Count == 1 && tokensWS[0].TokType == TokenType.WhiteSpace);

            tokensWS = fTestLexOperands("\n\r", bReturnWhitespace: true);
            Debug.Assert(tokensWS.Count == 1 && tokensWS[0].TokType == TokenType.WhiteSpace);

            tokensWS = fTestLexOperands("  #comment# also ignored", bReturnWhitespace: true);
            Debug.Assert(tokensWS.Count == 1 && tokensWS[0].TokType == TokenType.WhiteSpace);

            string smallempty = @"                                
                                     
    
";
            AssertExpectedParsedSymbols(smallempty, new TokenType[0], new PFC.CPU.Operand[0]);
            string bigempty =
@"
    
    
    
    
        

 

 
 
";
            AssertExpectedParsedSymbols(bigempty, new TokenType[0], new PFC.CPU.Operand[0]);

        }

        /// <summary>
        /// Test lexing of hex constants
        /// </summary>
        private static void LexTestHexLiterals()
        {
            AssertExpectedParsedSymbols("0x1", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x1) });
            AssertExpectedParsedSymbols("0X1", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x1) });
            AssertExpectedParsedSymbols("0x2", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x2) });
            AssertExpectedParsedSymbols("0xB", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0xb) });
            AssertExpectedParsedSymbols("0xF", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0xf) });
            AssertExpectedParsedSymbols("0xf", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0xf) });
            AssertExpectedParsedSymbols("0x12345678", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(0x12345678) });
            AssertExpectedParsedSymbols("0xDEADBEEF", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(0xDEADBEEF) });
            AssertExpectedParsedSymbols("0xdeadbEEF", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(0xdeadbEEF) });
            AssertExpectedParsedSymbols("0xdeadbeef", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(0xdeadbeef) });
            AssertExpectedParsedSymbols("0Xdeadbeef", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(0Xdeadbeef) });
            AssertExpectedParsedSymbols("0x0000_F000", new[] { TokenType.Integer },
                                                 new[] { OperandLiteral.undef(0x0000F000) });
            AssertExpectedParsedSymbols("0x_0F", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x0F) });
            AssertExpectedParsedSymbols("0xF0_", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0xF0) });
            AssertExpectedParsedSymbols("0x0_1_2_3_4_5_6_7_", new[] { TokenType.Integer },
                                                        new[] { OperandLiteral.undef(0x01234567) });
            AssertExpectedParsedSymbols("0x1 0x2 0x3 0x40 0xA 0x000000B",
                                   new[] { TokenType.Integer, TokenType.Integer, TokenType.Integer,
                                           TokenType.Integer, TokenType.Integer, TokenType.Integer, },
                                   new[] { OperandLiteral.undef(0x1), OperandLiteral.undef(0x2),
                                           OperandLiteral.undef(0x3), OperandLiteral.undef(0x40),
                                           OperandLiteral.undef(0xA), OperandLiteral.undef(0x000000B),
                                         });

#if test_64bit
            AssertExpectedParsedSymbols("0x0123456789ABCDEF",
                                  new[] { TokenType.Integer },
                                  new[] { PFC.VM.Operand.undef(0x0123456789ABCDEF) });
            AssertExpectedParsedSymbols("0x0_1_2_3_4_5_6_7_8_9_a_b_c_A_D",
                                  new[] { TokenType.Integer }, 
                                  new[] { PFC.VM.Operand.undef(0x0123456789abcad) });
#endif

            //if someone typed this, it's legit, but it could have been a mistake. SHAME.
            //AssertExpectedParsedSymbols("0x0b11001010", new[] { PFC.VM.Operand.undef(0x0b11001010) });
            AssertExpectedParsedSymbols("0x0b1010",
                                  new[] { TokenType.Integer },
                                  new[] { OperandLiteral.undef(0x0b1010) });

            //test for dodgy hex literals
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x_"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("_0x"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0xxA8"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0XX8A"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x 1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x_ 1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x _1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x_ _1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0xG"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0xX"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0x_T"));

        }

        /// <summary>
        /// Test lexing of binary constants
        /// </summary>
        private static void LexTestBinaryLiterals()
        {
            AssertExpectedParsedSymbols("0b1", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x1) });
            AssertExpectedParsedSymbols("0b0", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x0) });
            AssertExpectedParsedSymbols("0B0", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x0) });
            AssertExpectedParsedSymbols("0B1", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0x1) });
            AssertExpectedParsedSymbols("0b000", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x0) });
            AssertExpectedParsedSymbols("0b111", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x7) });
            AssertExpectedParsedSymbols("0b101", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x5) });
            AssertExpectedParsedSymbols("0b111010101010101011",
                                           new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x3aaab) });
            AssertExpectedParsedSymbols("0b1000_0000_0000_0000",
                                           new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x8000) });
            AssertExpectedParsedSymbols("0b__1__1000_0000_0000_0000",
                                           new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0x18000) });
            AssertExpectedParsedSymbols("0b0 0b1 0b10 0b11 0b100 0B101 0B110 0b111",
                                           new[] { TokenType.Integer, TokenType.Integer, TokenType.Integer, TokenType.Integer,
                                                   TokenType.Integer, TokenType.Integer, TokenType.Integer, TokenType.Integer,},
                                           new[] { OperandLiteral.undef(0x0),
                                                   OperandLiteral.undef(0x1),
                                                   OperandLiteral.undef(0x2),
                                                   OperandLiteral.undef(0x3),
                                                   OperandLiteral.undef(0x4),
                                                   OperandLiteral.undef(0x5),
                                                   OperandLiteral.undef(0x6),
                                                   OperandLiteral.undef(0x7),
                                                 });

            AssertExpectedParsedSymbols("0b1111_1111_1111_1111_1111_1111_1111_1111",
                                           new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(0xFFFFFFFF) });
#if test_64bit
            AssertExpectedParsedSymbols("0b1_1111_1111_1111_1111_1111_1111_1111_1111",
                                           new[] { TokenType.Integer },
                                           new[] { PFC.VM.Operand.undef(0x100000000) });
#endif
            //test for dodgy hex literals
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b_"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("_0b"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0bb1010101"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0BB1000001"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b 1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b_ 1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b _1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b_ _1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b210"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b1010101010101111010106"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0b1F"));
        }


        /// <summary>
        /// Test lexing of normal integers constants
        /// </summary>
        private static void LexTestIntegerLiterals()
        {
            AssertExpectedParsedSymbols("1", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(1) });
            AssertExpectedParsedSymbols("2", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(2) });
            AssertExpectedParsedSymbols("3", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(3) });
            AssertExpectedParsedSymbols("7", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(7) });
            AssertExpectedParsedSymbols("8", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(8) });
            AssertExpectedParsedSymbols("9", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(9) });
            AssertExpectedParsedSymbols("126", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(126) });
            AssertExpectedParsedSymbols("127", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(127) });
            AssertExpectedParsedSymbols("128", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(128) });
            AssertExpectedParsedSymbols("254", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(254) });
            AssertExpectedParsedSymbols("255", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(255) });
            AssertExpectedParsedSymbols("256", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(256) });
            AssertExpectedParsedSymbols("65534", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(65534) });
            AssertExpectedParsedSymbols("65535", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(65535) });
            AssertExpectedParsedSymbols("65536", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(65536) });
            AssertExpectedParsedSymbols("8388606", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(8388606) });
            AssertExpectedParsedSymbols("8388607", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(8388607) });
            AssertExpectedParsedSymbols("8388608", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(8388608) });
            AssertExpectedParsedSymbols("16777214", new[] { TokenType.Integer },
                                              new[] { OperandLiteral.undef(16777214) });
            AssertExpectedParsedSymbols("16777215", new[] { TokenType.Integer },
                                              new[] { OperandLiteral.undef(16777215) });
            AssertExpectedParsedSymbols("16777216", new[] { TokenType.Integer },
                                              new[] { OperandLiteral.undef(16777216) });
            AssertExpectedParsedSymbols("2147483646", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(2147483646) });
            AssertExpectedParsedSymbols("2147483647", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(2147483647) });
            AssertExpectedParsedSymbols("2147483648", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(2147483648) });
            AssertExpectedParsedSymbols("4294967294", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(4294967294) });
            AssertExpectedParsedSymbols("4294967295", new[] { TokenType.Integer },
                                                new[] { OperandLiteral.undef(4294967295) });

            //a leading zero to specify octal (e.g. in C) is stupid. leading 0 here is for normal numbers.
            AssertExpectedParsedSymbols("0", new[] { TokenType.Integer },
                                       new[] { OperandLiteral.undef(0) });
            AssertExpectedParsedSymbols("00000000", new[] { TokenType.Integer },
                                              new[] { OperandLiteral.undef(0) });
            AssertExpectedParsedSymbols("01", new[] { TokenType.Integer },
                                        new[] { OperandLiteral.undef(1) });
            AssertExpectedParsedSymbols("000002", new[] { TokenType.Integer },
                                            new[] { OperandLiteral.undef(2) });
            AssertExpectedParsedSymbols("00000000000000000000000000000003",
                                   new[] { TokenType.Integer },
                                   new[] { OperandLiteral.undef(3) });
            AssertExpectedParsedSymbols("0000004294967295",
                                   new[] { TokenType.Integer },
                                   new[] { OperandLiteral.undef(4294967295) });

            AssertExpectedParsedSymbols("12345", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(12345) });
            AssertExpectedParsedSymbols("678901234", new[] { TokenType.Integer },
                                               new[] { OperandLiteral.undef(678901234) });
            AssertExpectedParsedSymbols("18", new[] { TokenType.Integer },
                                        new[] { OperandLiteral.undef(18) });
            AssertExpectedParsedSymbols("3757", new[] { TokenType.Integer },
                                          new[] { OperandLiteral.undef(3757) });

            AssertExpectedParsedSymbols("0n0", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0) });
            AssertExpectedParsedSymbols("0n1", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(1) });
            AssertExpectedParsedSymbols("0n2", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(2) });
            AssertExpectedParsedSymbols("0N3", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(3) });
            AssertExpectedParsedSymbols("0N4", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(4) });
            AssertExpectedParsedSymbols("0n18", new[] { TokenType.Integer },
                                          new[] { OperandLiteral.undef(18) });
            AssertExpectedParsedSymbols("0n3757", new[] { TokenType.Integer },
                                            new[] { OperandLiteral.undef(3757) });
            AssertExpectedParsedSymbols("0n65535", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(65535) });
            AssertExpectedParsedSymbols("0n12345", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(12345) });
            AssertExpectedParsedSymbols("0N678901234", new[] { TokenType.Integer },
                                                 new[] { OperandLiteral.undef(678901234) });

            AssertExpectedParsedSymbols("0n1_000", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(1000) });
            AssertExpectedParsedSymbols("0n253_180_000", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(253180000) });
            AssertExpectedParsedSymbols("0n_0001", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(1) });
            AssertExpectedParsedSymbols("0n0006_", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(6) });

            AssertExpectedParsedSymbols("0n000_000", new[] { TokenType.Integer },
                                               new[] { OperandLiteral.undef(0) });
            AssertExpectedParsedSymbols("0n_000_", new[] { TokenType.Integer },
                                             new[] { OperandLiteral.undef(0) });

            AssertExpectedParsedSymbols("-6003", new[] { TokenType.Minus, TokenType.Integer },
                                           new[] { OperandLiteral.undef(unchecked((ulong)-6003)) });
            AssertExpectedParsedSymbols("+2", new[] { TokenType.Plus, TokenType.Integer },
                                        new[] { OperandLiteral.undef(+2) });
            AssertExpectedParsedSymbols("+0n2", new[] { TokenType.Plus, TokenType.Integer },
                                          new[] { OperandLiteral.undef(2) });
            AssertExpectedParsedSymbols("-0n5", new[] { TokenType.Minus, TokenType.Integer },
                                          new[] { OperandLiteral.undef(unchecked((ulong)-5)) });

            AssertExpectedParsedSymbols("+0", new[] { TokenType.Plus, TokenType.Integer },
                                        new[] { OperandLiteral.undef(0) });
            AssertExpectedParsedSymbols("-0", new[] { TokenType.Minus, TokenType.Integer },
                                        new[] { OperandLiteral.undef(0) });



            AssertExpectedParsedSymbols("1 2 3 4 6 0n7346 3738 1 _123 3_000 0n1_57843_3 0N3_333 +8 -144 +0n39 -0n3040",
                                  new[] { TokenType.Integer, TokenType.Integer, TokenType.Integer, TokenType.Integer,
                                          TokenType.Integer, TokenType.Integer, TokenType.Integer, TokenType.Integer,
                                          TokenType.Integer, TokenType.Integer, TokenType.Integer, TokenType.Integer,
                                          TokenType.Plus,  TokenType.Integer,
                                          TokenType.Minus, TokenType.Integer,
                                          TokenType.Plus,  TokenType.Integer,
                                          TokenType.Minus, TokenType.Integer,},
                                  new[] { OperandLiteral.undef(1),       OperandLiteral.undef(2),
                                          OperandLiteral.undef(3),       OperandLiteral.undef(4),
                                          OperandLiteral.undef(6),       OperandLiteral.undef(7346),
                                          OperandLiteral.undef(3738),    OperandLiteral.undef(1),
                                          OperandLiteral.undef(123),     OperandLiteral.undef(3000),
                                          OperandLiteral.undef(1578433), OperandLiteral.undef(3333),
                                          OperandLiteral.undef(8),       
                                          OperandLiteral.undef(unchecked((uint)-144)),
                                          OperandLiteral.undef(39),      
                                          OperandLiteral.undef(unchecked((uint)-3040)),
                                        });


            //not going to fit in 2*32, but the lexer doesn't know that:
#if test_64bit
            AssertExpectedParsedSymbols("4294967296", new[] { TokenType.Integer },
                                                new[] { PFC.VM.Operand.undef(4294967296) });
            AssertExpectedParsedSymbols("00000000000000000008000000000000004",
                                   new[] { TokenType.Integer },
                                   new[] { PFC.VM.Operand.undef(8000000000000004) });
#endif
            //test for dodgy int literals
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("25U"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("25L"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("25UL"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0nn18"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0NN19"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("_"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n_"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n_"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("_0n"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n 2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n_ 2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n _2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n_ _2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0nG"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0nN"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0Nn"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("+ABCDEF"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("-ABCDEF"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("ABCDEF"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1ABCDEF"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n1ABCDEF"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0nABCDEF"));

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("+ABCDE"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("-ABCDE"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("ABCDE"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1ABCDE"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n1ABCDE"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0nABCDE"));

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n_Test"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n_21Tes3t4s"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n-4"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0n+4"));

        }

        /// <summary>
        /// Test lexing of float constants
        /// </summary>
        private static void LexTestFloatLiterals()
        {
            //generic
            AssertExpectedParsedSymbols("0.0", new[] { TokenType.FloatSingle },
                                         new[] { OperandLiteral.f32(0.0f) });
            AssertExpectedParsedSymbols("1.0", new[] { TokenType.FloatSingle },
                                         new[] { OperandLiteral.f32(1.0f) });
            AssertExpectedParsedSymbols("18.0", new[] { TokenType.FloatSingle },
                                          new[] { OperandLiteral.f32(18.0f) });
            AssertExpectedParsedSymbols("+567.5", new[] { TokenType.Plus, TokenType.FloatSingle },
                                            new[] { OperandLiteral.f32(567.5f) });
            AssertExpectedParsedSymbols("-20.34", new[] { TokenType.Minus, TokenType.FloatSingle },
                                            new[] { OperandLiteral.f32(-20.34f) });
            AssertExpectedParsedSymbols("16777216.0", new[] { TokenType.FloatSingle },
                                                new[] { OperandLiteral.f32(16777216.0f) });
            AssertExpectedParsedSymbols("12.0244673473476", new[] { TokenType.FloatSingle },
                                                      new[] { OperandLiteral.f32(12.0244673473476f) });
            AssertExpectedParsedSymbols("0.012726876388128273799",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(0.012726876388128273799f) });
            AssertExpectedParsedSymbols("-0.993", new[] { TokenType.Minus, TokenType.FloatSingle },
                                            new[] { OperandLiteral.f32(-0.993f) });

            AssertExpectedParsedSymbols("340282000000000000000000000000000000000.0",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(340282000000000000000000000000000000000.0f) });
            AssertExpectedParsedSymbols("340282000000000000000000000000000000000.001",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(340282000000000000000000000000000000000.001f) });
            AssertExpectedParsedSymbols("3.40282e+38", new[] { TokenType.FloatSingle },
                                                 new[] { OperandLiteral.f32(3.40282e+38f) });
            AssertExpectedParsedSymbols("-3.40282e+38", new[] { TokenType.Minus, TokenType.FloatSingle },
                                                 new[] { OperandLiteral.f32(-3.40282e+38f) });
            AssertExpectedParsedSymbols("0.0000000000000000000000000000000000001",
                                  new[] { TokenType.FloatSingle },
                                  new[] { OperandLiteral.f32(0.0000000000000000000000000000000000001f) });
            AssertExpectedParsedSymbols("1.0e-37", new[] { TokenType.FloatSingle },
                                             new[] { OperandLiteral.f32(1.0e-37f) });

#if test_64bit
            //? where is this value from?
            AssertExpectedParsedSymbols("3.12345e+800", new[] { TokenType.FloatSingle },
                                                  new[] { PFC.VM.Operand.f32(float.PositiveInfinity) });

            AssertExpectedParsedSymbols("1.79769e+308", new[] { TokenType.FloatSingle },
                                                  new[] { PFC.VM.Operand.d64(1.79769e+308d) });
            AssertExpectedParsedSymbols("-1.79769e+308", new[] { TokenType.Minus, TokenType.FloatSingle },
                                                   new[] { PFC.VM.Operand.d64(-1.79769e+308d) });
#endif
            AssertExpectedParsedSymbols("01.2", new[] { TokenType.FloatSingle },
                                          new[] { OperandLiteral.f32(01.2f) });
            AssertExpectedParsedSymbols("0000.00", new[] { TokenType.FloatSingle },
                                             new[] { OperandLiteral.f32(0000.00f) });

            // :)
            AssertExpectedParsedSymbols("1.7976900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(1.79769f) });

            // f single
            AssertExpectedParsedSymbols("0.0f", new[] { TokenType.FloatSingle },
                                          new[] { OperandLiteral.f32(0.0f) });
            AssertExpectedParsedSymbols("1.0f", new[] { TokenType.FloatSingle },
                                          new[] { OperandLiteral.f32(1.0f) });
            AssertExpectedParsedSymbols("-1.0f", new[] { TokenType.Minus, TokenType.FloatSingle },
                                           new[] { OperandLiteral.f32(-1.0f) });
            AssertExpectedParsedSymbols("18.0f", new[] { TokenType.FloatSingle },
                                           new[] { OperandLiteral.f32(18.0f) });
            AssertExpectedParsedSymbols("274.0F", new[] { TokenType.FloatSingle },
                                            new[] { OperandLiteral.f32(274.0f) });
            AssertExpectedParsedSymbols("+4.0F", new[] { TokenType.Plus, TokenType.FloatSingle },
                                           new[] { OperandLiteral.f32(+4.0f) });
            AssertExpectedParsedSymbols("+0.3456f", new[] { TokenType.Plus, TokenType.FloatSingle },
                                              new[] { OperandLiteral.f32(0.3456f) });
            AssertExpectedParsedSymbols("16777216.0f",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(16777216.0f) });
            AssertExpectedParsedSymbols("12.0244673473476f",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(12.0244673473476f) });
            AssertExpectedParsedSymbols("0.012726876388128273799f",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(0.012726876388128273799f) });
            AssertExpectedParsedSymbols("0.43964647F",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(0.43964647F) });
            AssertExpectedParsedSymbols("-0.124F", new[] { TokenType.Minus, TokenType.FloatSingle },
                                             new[] { OperandLiteral.f32(-0.124F) });

            AssertExpectedParsedSymbols("340282000000000000000000000000000000000.0f",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(340282000000000000000000000000000000000.0f) });
            AssertExpectedParsedSymbols("340282000000000000000000000000000000000.001f",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(340282000000000000000000000000000000000.001f) });
            AssertExpectedParsedSymbols("3.40282e+38f", new[] { TokenType.FloatSingle },
                                                  new[] { OperandLiteral.f32(3.40282e+38f) });
            AssertExpectedParsedSymbols("-3.40282e+38f", new[] { TokenType.Minus, TokenType.FloatSingle },
                                                   new[] { OperandLiteral.f32(-3.40282e+38f) });
            AssertExpectedParsedSymbols("0.0000000000000000000000000000000000001f",
                                   new[] { TokenType.FloatSingle },
                                   new[] { OperandLiteral.f32(0.0000000000000000000000000000000000001f) });

            // d double
#if test_64bit
            AssertExpectedParsedSymbols("0.0d", new[] { TokenType.FloatSingle },
                                          new[] { PFC.VM.Operand.d64(0.0d) });
            AssertExpectedParsedSymbols("1.0d", new[] { TokenType.FloatSingle },
                                          new[] { PFC.VM.Operand.d64(1.0d) });
            AssertExpectedParsedSymbols("-1.0d", new[] { TokenType.Minus, TokenType.FloatSingle },
                                          new[] { PFC.VM.Operand.d64(-1.0d) });
            AssertExpectedParsedSymbols("3.0D", new[] { TokenType.FloatSingle },
                                          new[] { PFC.VM.Operand.d64(3.0d) });
            AssertExpectedParsedSymbols("+2.3456d", new[] { TokenType.Plus, TokenType.FloatSingle },
                                              new[] { PFC.VM.Operand.d64(+2.3456d) });
            AssertExpectedParsedSymbols("18.0d", new[] { TokenType.FloatSingle },
                                           new[] { PFC.VM.Operand.d64(18.0d) });
            AssertExpectedParsedSymbols("16777216.0d", new[] { TokenType.FloatSingle },
                                                 new[] { PFC.VM.Operand.d64(16777216.0d) });
            AssertExpectedParsedSymbols("12.0244673473476d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(12.0244673473476d) });
            AssertExpectedParsedSymbols("0.012726876388128273799d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(0.012726876388128273799d) });
            AssertExpectedParsedSymbols("0.994848383D", new[] { TokenType.FloatSingle },
                                                  new[] { PFC.VM.Operand.d64(0.994848383D) });

            AssertExpectedParsedSymbols("340282000000000000000000000000000000000.0d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(340282000000000000000000000000000000000.0d) });
            AssertExpectedParsedSymbols("340282000000000000000000000000000000000.001d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(340282000000000000000000000000000000000.001d) });
            AssertExpectedParsedSymbols("3.40282e+38d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(3.40282e+38d) });
            AssertExpectedParsedSymbols("-3.40282e+38d",
                                   new[] { TokenType.Minus, TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(-3.40282e+38d) });
            AssertExpectedParsedSymbols("0.0000000000000000000000000000000000001d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(0.0000000000000000000000000000000000001d) });
            AssertExpectedParsedSymbols("1.0e-37d",
                                   new[] { TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(1.0e-37d) });
            AssertExpectedParsedSymbols("3.12345e+800d", new[] { TokenType.FloatSingle },
                                                   new[] { PFC.VM.Operand.d64(double.PositiveInfinity) });

            AssertExpectedParsedSymbols("1.79769e+308d", new[] { TokenType.FloatSingle },
                                                   new[] { PFC.VM.Operand.d64(double.MaxValue) });
            AssertExpectedParsedSymbols("-1.79769e+308d", new[] { TokenType.Minus, TokenType.FloatSingle },
                                                     new[] { PFC.VM.Operand.d64(double.MinValue) });

            // :)
            AssertExpectedParsedSymbols("+1.7976900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000d",
                                   new[] { TokenType.Plus, TokenType.FloatSingle },
                                   new[] { PFC.VM.Operand.d64(1.797690d) });



            AssertExpectedParsedSymbols("1.0 2.0 0.1245 0.999f 235.0d 2.0e+5f",
                                   new[] { TokenType.FloatSingle, TokenType.FloatSingle,
                                           TokenType.FloatSingle, TokenType.FloatSingle,
                                           TokenType.FloatSingle,  TokenType.FloatSingle,},
                                   new[] { PFC.VM.Operand.f32(1.0f),
                                           PFC.VM.Operand.f32(2.0f),
                                           PFC.VM.Operand.f32(0.1245f),
                                           PFC.VM.Operand.f32(0.999f),
                                           PFC.VM.Operand.d64(235.0d),
                                           PFC.VM.Operand.f32(2.0e+5f),
                                         });
#endif

            //test for dodgy float literals
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1e-37")); //needs to be 1.0e-37
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1.0.0"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1..0"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands(".1235"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1235."));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands(".0"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0."));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1."));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1.0 f"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1.f"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1.0i"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1.0t"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1f"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("+f"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("-f"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("-.0"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("+.0"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0.0eU"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("145.32455e+-4"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("145.32455e-+4"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("145.32455e4-+"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("145.32455e.4"));
#if test_64bit
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0.1 d"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0.1 d1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1.d"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("1d"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("+d"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("-d"));
#endif
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0. 0f 1.0 d"));
        }


        /// <summary>
        /// Test lexing of float constants
        /// </summary>
        private static void LexTestOctalLiterals()
        {
            //generic
            AssertExpectedParsedSymbols("0o0", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(0) });
            AssertExpectedParsedSymbols("0o1", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(1) });
            AssertExpectedParsedSymbols("0o7", new[] { TokenType.Integer },
                                         new[] { OperandLiteral.undef(7) });
            AssertExpectedParsedSymbols("0o127", new[] { TokenType.Integer },
                                           new[] { OperandLiteral.undef(87) });
            AssertExpectedParsedSymbols("0O3777", new[] { TokenType.Integer },
                                            new[] { OperandLiteral.undef(2047) });
            AssertExpectedParsedSymbols("0O123_456", new[] { TokenType.Integer },
                                               new[] { OperandLiteral.undef(42798) });

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o8"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o3779"));

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o_"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("_0o"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0oo37"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0OO46"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o 2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o_ 5"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o _6"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o_ _2"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0oG"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0oO"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("0o_T"));
        }

        private static void LexTestBadRegisters()
        {
            //registers
            AssertExpectedLexSymbols("r0", new[] { TokenType.Text, TokenType.Integer });
            AssertExpectedLexSymbols("r10", new[] { TokenType.Text, TokenType.Integer });
            AssertExpectedLexSymbols("r15", new[] { TokenType.Text, TokenType.Integer });
            AssertExpectedLexSymbols("r8", new[] { TokenType.Text, TokenType.Integer });

            AssertExpectedParsedSymbols(
                "r8",
                new[] { TokenType.Text, TokenType.Integer },
                new[] { OperandReg.Register(8), });

            AssertExpectedParsedSymbols(
                "R8",
                new[] { TokenType.Text, TokenType.Integer },
                new[] { OperandReg.Register(8), });

            AssertExpectedLexSymbols("r0 r1 r2 r34",
                                      new[] { TokenType.Text, TokenType.Integer,
                                              TokenType.Text, TokenType.Integer,
                                              TokenType.Text, TokenType.Integer,
                                              TokenType.Text, TokenType.Integer,});

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("rr3"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r3x"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r0x3"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r0n4"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r300"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("rX"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r,4"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("r_4")); // maybe this should be illegal?
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("slurp"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("plodajs"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("pcccc"));


            //opcodes?
        }

        private static void LexTestCommas()
        {
            //commas
            AssertExpectedLexSymbols(",", new[] { TokenType.Comma });
            AssertExpectedLexSymbols(" ,    ", new[] { TokenType.Comma });
            AssertExpectedLexSymbols(" ,   ,, ", new[] { TokenType.Comma, TokenType.Comma, TokenType.Comma });
        }

        private static void LexTestLine()
        {
            AssertExpectedLexSymbols("mov r0,1",
                                     new[] { TokenType.Text, TokenType.Text, TokenType.Integer,
                                             TokenType.Comma, TokenType.Integer });

            //internal, seperating whitespace should be ignored
            AssertExpectedLexSymbols("mov r0,     1",
                                     new[] { TokenType.Text, TokenType.Text, TokenType.Integer,
                                             TokenType.Comma, TokenType.Integer });

            //external whitespace should be ignored
            AssertExpectedLexSymbols("  mov r0, 1  ",
                                     new[] { TokenType.Text, TokenType.Text, TokenType.Integer,
                                             TokenType.Comma, TokenType.Integer });

            AssertExpectedLexSymbols("  mov r0,1  ",
                                     new[] { TokenType.Text, TokenType.Text, TokenType.Integer,
                                             TokenType.Comma, TokenType.Integer });

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("movr0,1"));

            AssertExpectedLexSymbols("iadd r13, 0x1234, r6 #mov r0, r0",
                                      new[] { TokenType.Text, TokenType.Text,  TokenType.Integer, TokenType.Comma,
                                              TokenType.Integer, TokenType.Comma, TokenType.Text,  TokenType.Integer });

            AssertExpectedLexSymbols("iadd32 r13, 0x1234, r6 #mov r0, r0",
                                      new[] { TokenType.Text, TokenType.Integer,
                                              TokenType.Text, TokenType.Integer, TokenType.Comma,
                                              TokenType.Integer, TokenType.Comma,
                                              TokenType.Text, TokenType.Integer,});

            AssertExpectedLexSymbols(":label: iadd32 r13, 0x1234, r6 #mov r0, r0",
                                      new[] { TokenType.Label, TokenType.Text, TokenType.Integer,
                                              TokenType.Text, TokenType.Integer, TokenType.Comma,
                                              TokenType.Integer, TokenType.Comma,
                                              TokenType.Text, TokenType.Integer,});


            //todo: another fun thing: if we have a valid opcode like 'ldr32 r0, r0'

        }

        private static void LextTestTypecast()
        {
            AssertExpectedParsedSymbols("i8{r0}",
                                        new[] { TokenType.DataType, TokenType.LCurly,
                                                TokenType.Text, TokenType.Integer,
                                                TokenType.RCurly },
                                        new[] { OperandReg.Register(0, PFC.CPU.DataType.i8) });
            AssertExpectedParsedSymbols("i16{r1}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i16) });
            AssertExpectedParsedSymbols("i32{r4}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(4, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("u8{r3}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(3, PFC.CPU.DataType.u8) });
            AssertExpectedParsedSymbols("u16{r15}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(15, PFC.CPU.DataType.u16) });
            AssertExpectedParsedSymbols("u24{r0}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(0, PFC.CPU.DataType.u24) });
            AssertExpectedParsedSymbols("u32{r0}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(0, PFC.CPU.DataType.u32) });
            AssertExpectedParsedSymbols("f32{r1}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.f32) });

            AssertExpectedParsedSymbols("i32{r1}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32{r1} ",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32{r1 }",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32{ r1}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32{ r1 }",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32 {r1 }",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32 { r1}",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });
            AssertExpectedParsedSymbols("i32 { r1 }",
                                         new[] { TokenType.DataType, TokenType.LCurly,
                                                 TokenType.Text, TokenType.Integer,
                                                 TokenType.RCurly },
                                         new[] { OperandReg.Register(1, PFC.CPU.DataType.i32) });


            AssertExpectedParsedSymbols("i32{0xeeee5432}",
                             new[] { TokenType.DataType, TokenType.LCurly,
                                     TokenType.Integer,
                                     TokenType.RCurly },
                             new[] { OperandLiteral.i32(unchecked((int)0xeeee5432)) });

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32{r1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32{"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32{}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32 r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("32{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i r1"));
            //AssertThrows<PFCAsmInputError>(() => fTestParseOperands("32 r1 "));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("{i32 r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("iX{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i3{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i2{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("ii32{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("iu32{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i0{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i-1{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i-32{r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i999{r1}"));

            //+ caps?

            //AssertExpectedLexSymbols("", TokenType[]
            
        }

        private static void LexTestDereference()
        {
            //see ideas.txt for more examples.
            AssertExpectedParsedSymbols("@(r0)",
                                        new[] { TokenType.At, TokenType.LParen,
                                                TokenType.Text, TokenType.Integer,
                                                TokenType.RParen },
                                        new[] { OperandReg.RegisterDeref(0, 0, PFC.CPU.DataType.undef) });

            AssertExpectedParsedSymbols("u8{@(r3)}",
                                        new[] { TokenType.DataType, TokenType.LCurly,
                                                TokenType.At, TokenType.LParen,
                                                TokenType.Text, TokenType.Integer,
                                                TokenType.RParen,
                                                TokenType.RCurly },
                                        new[] { OperandReg.RegisterDeref(3, 0, PFC.CPU.DataType.u8) });
            AssertExpectedParsedSymbols("u32{@(r4)}",
                                        new[] { TokenType.DataType, TokenType.LCurly,
                                                TokenType.At, TokenType.LParen,
                                                TokenType.Text, TokenType.Integer,
                                                TokenType.RParen,
                                                TokenType.RCurly },
                                        new[] { OperandReg.RegisterDeref(4, 0, PFC.CPU.DataType.u32) });
            AssertExpectedParsedSymbols("@(0xDEADBEEF)",
                                        new[] { TokenType.At, TokenType.LParen,
                                                TokenType.Integer, TokenType.RParen, },
                                        new[] { OperandReg.LiteralAddress(0xDEADBEEF, PFC.CPU.DataType.undef) });
            AssertExpectedParsedSymbols("u8{@(123)}",
                                        new[] { TokenType.DataType, TokenType.LCurly,
                                                TokenType.At, TokenType.LParen,
                                                TokenType.Integer,TokenType.RParen,
                                                TokenType.RCurly },
                                        new[] { OperandReg.LiteralAddress(123, PFC.CPU.DataType.u8) });




            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@(r1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@(r1))"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@((r1))"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@r1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@(r1"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("(r1)"));


            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("u8 @(r1)"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("u8{ (r1)}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("u8{ @r1}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("u8(@{r1})"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@(u8{r0})"));


            //can't have signed address
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i8{@(123)}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i16{@(r3)}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("f32{@(r3)}"));

        }

        private static void LexTestLabel()
        {
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands("::"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":    :"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":\t:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la bel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la\tbel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la\nbel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la\rbel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":label\n:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(": label:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":label :"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(": label :"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(": lab  el :"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands("label:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":label"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la!bel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la@bel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la$bel:"));
            AssertThrows<PFCAsmInputError>(() => fTestLexOperands(":la#bel:"));


            AssertExpectedParsedSymbols(":myfirstlabel:",
                                        new[] { TokenType.Label },
                                        new[] { new OperandLabel(":myfirstlabel:") });
            AssertExpectedParsedSymbols(":a-nice-long-label:",
                                        new[] { TokenType.Label },
                                        new[] { new OperandLabel(":a-nice-long-label:") });
            AssertExpectedParsedSymbols(":label.1:",
                                        new[] { TokenType.Label },
                                        new[] { new OperandLabel(":label.1:") });
            AssertExpectedParsedSymbols(":you_can-use_underscores-if-you.want:",
                                        new[] { TokenType.Label },
                                        new[] { new OperandLabel(":you_can-use_underscores-if-you.want:") });

            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i16{:label:}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i8{:label:}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i32{:label:}"));



            AssertExpectedParsedSymbols("u16{:label:}",
                                        new[] { TokenType.DataType, TokenType.LCurly,
                                                TokenType.Label,
                                                TokenType.RCurly },
                                        new[] { new OperandLabel(":label:", DataType.u16),  });
            AssertExpectedParsedSymbols("u32{:label:}",
                                        new[] { TokenType.DataType, TokenType.LCurly,
                                                TokenType.Label,
                                                TokenType.RCurly },
                                        new[] { new OperandLabel(":label:", DataType.u32) });

        }


        private static void LexTestFlagsmod()
        {
            AssertExpectedLexSymbols("!", new[] { TokenType.Exclaim });

            AssertExpectedLexSymbols("iadd!", new[] { TokenType.Text, TokenType.Exclaim });
            AssertExpectedLexSymbols("iadd32!", new[] { TokenType.Text, TokenType.Integer, TokenType.Exclaim });

            //not valid, but the lexer doesn't know that
            AssertExpectedLexSymbols("!!!", new[] { TokenType.Exclaim, TokenType.Exclaim, TokenType.Exclaim });
            AssertExpectedLexSymbols("iadd!32", new[] { TokenType.Text, TokenType.Exclaim, TokenType.Integer });
        }



        private static bool LexTestPC()
        {
            AssertExpectedParsedSymbols("PC",
                                     new[] { TokenType.Text},
                                     new[] { OperandReg.ProgramCounter()});
            AssertExpectedParsedSymbols("pc",
                                     new[] { TokenType.Text },
                                     new[] { OperandReg.ProgramCounter() });
            AssertExpectedParsedSymbols("Pc",
                                     new[] { TokenType.Text },
                                     new[] { OperandReg.ProgramCounter() });
            AssertExpectedParsedSymbols("pC",
                                     new[] { TokenType.Text },
                                     new[] { OperandReg.ProgramCounter() });
            AssertExpectedLexSymbols("mov r0, PC",
                                     new[] { TokenType.Text, TokenType.Text, TokenType.Integer,
                                             TokenType.Comma, TokenType.Text });

            //pc: can't have data type?
            //pc: can't be dereferenced?
            //pc: can't be negated?
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("i8{PC}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("u16{PC}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("undef{PC}"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("@(PC)"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("-PC"));
            AssertThrows<PFCAsmInputError>(() => fTestParseOperands("u16{@(PC)}"));

            return true;
        }

        public static bool RunAllParserOperandTests()
        {
            LexTestBadRegisters();
            LexTestBinaryLiterals();
            LexTestCommas();
            LexTestEmptyLines();
            LexTestFloatLiterals();
            LexTestHexLiterals();
            LexTestIntegerLiterals();
            LexTestLine();
            LexTestOctalLiterals();
            LextTestTypecast();
            LexTestDereference();
            LexTestLabel();
            LexTestFlagsmod();
            LexTestPC();


            //this test file should only test the lexer. OR it should test both. But not just mimic the parser?
            //wht about if we move literal decoing into lexer?




            //do we want any multi-line?? iadd32  r0,
            //                                    r0,
            //                                    r1

            //see ideas.txt for more examples.
            //ILLEGAL ld32 r0, @(i8{r1} + i16{r[r8 + 4]} * 4)
            //ld32 r0, i8{@(r1 + r[r8 + 4] * 4)}
            //ld32 r0, i8 { @(r1 + r[r8 + 4] * 4) }
            //iadd32 r0, r0, i8 { r1.2 }
            //printLexedStuff("i8{r0}, i8{r0}, i8{r1}");

            return true;
        }

    }
}

