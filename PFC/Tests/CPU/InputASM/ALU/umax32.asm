	umax32    r0, 0, 4            #0 r0 = 4
	assert_eq r0, 4
	umax32    r1, 3, 8            #2 r1 = 8
	assert_eq r1, 8
	umax32    r2, r0, r1          #4 r2 = 8
	assert_eq r2, 8
	umax32    r0, r0, -7          #6 r0 = 0xfffffff9
	assert_eq r0, 0xfffffff9
	umax32    r3, -1, r1          #8 r3 = 0xffffffff
	assert_eq r3, 0xffffffff
	umax32    r4, 0xFFFFFFFF, r1  #10 r4 = 0xffffffff
	assert_eq r4, 0xffffffff
	umax32    r0, r0, r0          #12 r0 = 0xfffffff9
	assert_eq r0, 0xfffffff9

	halt
