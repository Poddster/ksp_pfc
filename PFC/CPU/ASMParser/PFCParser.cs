﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Diagnostics;
using PFC.CPU;
using JetBrains.Annotations;

namespace PFC.ASMParser
{
    public class Parser
    {
        private int lineNumber;

        [NotNull]
        private string sourceLine;

        [CanBeNull]
        private LexerContext lexer;

        [CanBeNull]
        private OpcodeTextResult opcodeInfo;

        [NotNull]
        private readonly ParserOperandContext operandContext;

        [NotNull]
        private readonly Dictionary<string, Tuple<LexToken, int>> labelMap;

        [NotNull]
        public ParserOperandContext OperandContext
        {
            get { return this.operandContext; }
        }

        [NotNull]
        public OpcodeTextResult OpcodeInfo
        {
            get
            {
                Debug.Assert(this.opcodeInfo != null, "this.opcodeInfo != null");
                return this.opcodeInfo;
            }
        }

        [NotNull]
        public Dictionary<string, Tuple<LexToken, int>> LabelMap
        {
            get { return this.labelMap; }
        }


        public Parser()
        {
            this.lineNumber = 0;
            this.sourceLine = "";
            this.lexer = null;
            this.operandContext = new ParserOperandContext();
            this.labelMap = new Dictionary<string, Tuple<LexToken, int>>();
            this.opcodeInfo = null;
        }

        internal void StartNewLine([NotNull] string sourceLine)
        {
            this.lineNumber++;
            this.sourceLine = sourceLine;
            this.lexer = new LexerContext(sourceLine, this.lineNumber);
            this.operandContext.ResetOperandContext();
            this.opcodeInfo = null;
        }

        internal void SetLineOpcode([NotNull] OpcodeTextResult opcodeInfo)
        {
            this.opcodeInfo = opcodeInfo;
        }

        internal void AddLabel([NotNull] string label, [NotNull] LexToken labelTok, int programCounter)
        {
            if (this.LabelMap.ContainsKey(label))
            {
                var preTokTuple = this.LabelMap[label];
                Debug.Assert(preTokTuple != null, "preTokTuple != null");
                var preTok = preTokTuple.Item1;
                Debug.Assert(preTok != null, "preTok != null");
                string err = String.Format("Label {0} already declared on line {1}", label, preTok.LineNumber);
                throw this.MakeUserError(err, labelTok);
            }
            this.LabelMap.Add(label,
                              new Tuple<LexToken, int>(labelTok, programCounter));
        }

        /*
         *  tok = Consume()
         *  tok = ConsumeExpected(TokenType)
         *  opt<tok> = ConsumeOptional(TokenType);
         *  bool = LookAhead(TokenType first)
         *  bool = LookAhead(TokenType first, TokenType second) etc
         */

        /*
        public LexToken Consume()
        {
            return this._lexer.NextToken();
        }

        public LexToken ConsumeExpected(TokenType type)
        {
            this.ExpectSymbol(type);
            return this._lexer.NextToken();
        }
         */

        [NotNull]
        internal LexToken Current(bool bWhiteSpaceIsImportant = false)
        {
            Debug.Assert(this.lexer != null, "this._lexer != null");
            return this.lexer.PeekToken(bWhiteSpaceIsImportant);
        }

        public Option<LexToken> ConsumeOptionalSymbol(TokenType optionalType)
        {
            Debug.Assert(this.lexer != null, "this._lexer != null");

            //buggy: if we don't take whitespace, then it'll peek to the next token.
            // But if we then want expected whitespace after optinally looking for something, the
            //whitespace will have been skipped.
            var nextTok = this.lexer.PeekToken(bReturnWhiteSpace: true);
            if (nextTok.TokType == optionalType)
            {
                var outTok = this.lexer.NextToken(bReturnWhiteSpace: true);
                Debug.Assert(outTok == nextTok);
                return new Some<LexToken>(outTok);
            }
            else
            {
                return new None<LexToken>();
            }
        }


        private void CheckExpected([NotNull] TokenType[] expectedTypes, [NotNull] LexToken nextTok)
        {
            if (expectedTypes.Contains(nextTok.TokType))
            {
                return;
            }
            string types = String.Join(" | ", expectedTypes);
            string err = String.Format(
                "Expected {0} '{1}' but found type '{2}': '{3}'",
                (types.Length == 1) ? "type" : "types",
                types,
                nextTok.TokType,
                nextTok.Text);
            throw this.MakeUserError(err, nextTok);
        }

        public void ExpectAny([NotNull] TokenType[] types)
        {
            this.CheckExpected(types, this.Current());
        }

        public void Expect(TokenType type)
        {
            this.ExpectAny(new [] { type });
        }

        [NotNull]
        public LexToken ConsumeExpectedAny([NotNull] TokenType[] expectedTypes)
        {
            Debug.Assert(this.lexer != null, "this._lexer != null");

            Debug.Assert(expectedTypes.Length > 0);
            bool bReturnWhiteSpace = expectedTypes.Contains(TokenType.WhiteSpace);
            var nextTok = this.lexer.NextToken(bReturnWhiteSpace);
            this.CheckExpected(expectedTypes, nextTok);
            return nextTok;
        }

        [NotNull]
        public LexToken ConsumeExpected(TokenType expectedType)
        {
            return this.ConsumeExpectedAny(new [] { expectedType });
        }

        [NotNull]
        public T ConsumeExpected<T>(TokenType expectedType) where T : class
        {
            var tok = this.ConsumeExpectedAny(new [] { expectedType });
            var typedTok = tok as T;
            if (typedTok == null)
            {
                string err = String.Format("TokenType.{0} doesn't match type param {1}?",
                                            expectedType, typeof(T));
                throw this.MakeInternalError(err, tok);
            }
            return typedTok;
        }


        #region error-factories
        //todo remove these and just use static ones in first place?
        [NotNull]
        public ParserInternalError MakeInternalError(string message, int col)
        {
            return new ParserInternalError(message, this.sourceLine, this.lineNumber, col);
        }

        [NotNull]
        internal ParserInternalError MakeInternalError([NotNull] string message, [NotNull] LexToken opcodeToken)
        {
            return new ParserInternalError(message, this.sourceLine, this.lineNumber, opcodeToken.ColStart);
        }

        [NotNull]
        internal ParserInternalError MakeInternalError([NotNull] string message, [NotNull] LexToken opcodeToken, [NotNull] Exception e)
        {
            return new ParserInternalError(message, this.sourceLine, this.lineNumber, opcodeToken.ColStart, e);
        }

        [NotNull]
        internal PFCAsmInputError MakeUserError([NotNull] string message, int column)
        {
            return new PFCAsmInputError(message, this.sourceLine, this.lineNumber, column);
        }
        
        [NotNull]
        internal PFCAsmInputError MakeUserError([NotNull] string message, [NotNull] LexToken opcodeToken)
        {
            return new PFCAsmInputError(message, this.sourceLine, this.lineNumber, opcodeToken.ColStart);
        }
        #endregion
    }

    public class ParserOperandContext
    {
        public DataType DataType;

        public bool IsAddress;

        public bool IsNegated;

        public void ResetOperandContext()
        {
            this.DataType = DataType.undef;
            this.IsAddress = false;
            this.IsNegated = false;
        }

        public ParserOperandContext()
        {
            ResetOperandContext();
        }
    }

    public class OpcodeTextResult
    {
        public readonly OpcodeType OpcodeType;
        public readonly uint BitWidth;
        [NotNull]
        public readonly InstructionInfo InstInfo;
        [NotNull]
        public readonly LexToken OpcodeToken;
        public readonly bool WritesCPUFlags;

        public OpcodeTextResult(OpcodeType opcodeType,
                                [NotNull] InstructionInfo instInfo,
                                [NotNull] LexToken opcodeToken,
                                uint bitWidth, bool writesCPUFlags)
        {
            if (instInfo == null)
            {
                throw new ArgumentNullException("instInfo");
            }
            if (opcodeToken == null)
            {
                throw new ArgumentNullException("opcodeToken");
            }

            this.OpcodeType = opcodeType;
            this.BitWidth = bitWidth;
            this.InstInfo = instInfo;
            this.OpcodeToken = opcodeToken;
            this.WritesCPUFlags = writesCPUFlags;
        }
    }

    internal static class PFCParser
    {

        private static uint ParseBitWidth([NotNull] Parser context)
        {
            if (context.Current(bWhiteSpaceIsImportant:true).TokType != TokenType.Integer)
            {
                return 0;
            }

            var numberToken = context.ConsumeExpected<LexTokenInteger>(TokenType.Integer);
            switch (numberToken.Value)
            {
                case 8:
                case 16:
                case 24:
                case 32:
                case 64:
                    return numberToken.Value;
                default:
                    throw context.MakeUserError(String.Format("Invalid opcode bitWidth '{0}'", numberToken.Text),
                                                numberToken);
            }
        }

        [NotNull]
        private static OpcodeTextResult ParseOpcodeText([NotNull] Parser context)
        {
            var opcodeToken = context.ConsumeExpected(TokenType.Text);

            var rawOpcodeText = opcodeToken.Text;
            var enumStrings = Enum.GetNames(typeof(OpcodeType));

            var matchedEnumName = enumStrings.FirstOrDefault(possibleOpcodeName =>
                                                                rawOpcodeText == possibleOpcodeName);
            if (matchedEnumName == null)
            {
                throw context.MakeUserError(String.Format("Invalid opcode '{0}'", rawOpcodeText),
                                            opcodeToken);
            }


            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE, "\t" + opcodeToken);
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE, "");

            OpcodeType opTokenEnum;
            bool bFound = Enum.TryParse(matchedEnumName, out opTokenEnum);
            if (!bFound || opTokenEnum.ToString() != matchedEnumName)
            {
                string err = String.Format("Failed enum conversion for {0} on token {1}",
                                            matchedEnumName, opcodeToken);
                throw context.MakeInternalError(err, opcodeToken.ColStart);
            }



            uint bitWidth = ParseBitWidth(context);


            bool writesCpuFlags = false;
            if (context.Current(bWhiteSpaceIsImportant: true).TokType == TokenType.Exclaim)
            {
                var exlaim = context.ConsumeExpected<LexToken>(TokenType.Exclaim);
                writesCpuFlags = true;
            }

            context.ConsumeExpectedAny(new [] { TokenType.WhiteSpace, TokenType.EOL});



            var instInfo = InstructionInfo.Table[opTokenEnum];
            if (instInfo == null || instInfo.Opcode != opTokenEnum)
            {
                string err = String.Format("Failed to get inst info for enum {0} on token {1}",
                                            opTokenEnum, opcodeToken);
                throw context.MakeInternalError(err, opcodeToken.ColStart);
            }
            return new OpcodeTextResult(opTokenEnum, instInfo, opcodeToken, bitWidth, writesCpuFlags);
        }

        [NotNull]
        private static Operand ConsumeFramePointerRegister([NotNull] Parser context)
        {
            var lexToken = context.ConsumeExpected(TokenType.Text);
            if (lexToken.Text.ToLower() != "fp")
            {
                throw context.MakeInternalError(
                    "ConsumeFramePointerRegister called on string that isn't 'FP'?!",
                    lexToken);
            }

            string illegal = null;
            if (context.OperandContext.DataType != DataType.undef)
            {
                illegal = "FP can't be evaluated with a data type";
            }
            else if (context.OperandContext.IsNegated)
            {
                illegal = "FP can't be negated";
            }

            if (illegal != null)
            {
                throw context.MakeUserError(illegal, lexToken);
            }

            return OperandReg.FramePointer(deref: context.OperandContext.IsAddress);
        }

        [NotNull]
        private static Operand ConsumeLinkRegister([NotNull] Parser context)
        {
            var lexToken = context.ConsumeExpected(TokenType.Text);
            if (lexToken.Text.ToLower() != "lr")
            {
                throw context.MakeInternalError(
                    "ConsumeLinkRegister called on string that isn't 'LR'?!",
                    lexToken);
            }

            string illegal = null;
            if (context.OperandContext.DataType != DataType.undef)
            {
                illegal = "LR can't be evaluated with a data type";
            }
            else if (context.OperandContext.IsAddress)
            {
                illegal = "LR can't be evaluated in a dereference";
            }
            else if (context.OperandContext.IsNegated)
            {
                illegal = "LR can't be negated";
            }

            if (illegal != null)
            {
                throw context.MakeUserError(illegal, lexToken);
            }

            return OperandReg.LinkRegister();
        }

        [NotNull]
        private static Operand ConsumeStackPointerRegister([NotNull] Parser context)
        {
            var lexToken = context.ConsumeExpected(TokenType.Text);
            if (lexToken.Text.ToLower() != "sp")
            {
                throw context.MakeInternalError(
                    "ConsumeStackPointerRegister called on string that isn't 'SP'?!",
                    lexToken);
            }

            string illegal = null;
            if (context.OperandContext.DataType != DataType.undef)
            {
                illegal = "SP can't be evaluated with a data type";
            }
            else if (context.OperandContext.IsNegated)
            {
                illegal = "SP can't be negated";
            }

            if (illegal != null)
            {
                throw context.MakeUserError(illegal, lexToken);
            }

            return OperandReg.StackPointer(deref: context.OperandContext.IsAddress);
        }

        [NotNull]
        private static Operand ConsumePCRegister([NotNull] Parser context)
        {
            var lexToken = context.ConsumeExpected(TokenType.Text);
            if (lexToken.Text.ToLower() != "pc")
            {
                throw context.MakeInternalError(
                    "ConsumePCRegister called on string that isn't 'PC'?!",
                    lexToken);
            }


            string illegal = null;
            if (context.OperandContext.DataType != DataType.undef)
            {
                illegal = "PC can't be evaluated with a data type";
            }
            else if (context.OperandContext.IsAddress)
            {
                illegal = "PC can't be evaluated in a dereference";
            }
            else if (context.OperandContext.IsNegated)
            {
                illegal = "PC can't be negated";
            }

            if (illegal != null)
            {
                throw context.MakeUserError(illegal, lexToken);
            }

            return OperandReg.ProgramCounter();
        }

        [NotNull]
        private static Operand ConsumeTempRegister([NotNull] Parser context)
        {
            var rTok = context.ConsumeExpected(TokenType.Text);
            if (rTok.Text.ToLower() != "r")
            {
                throw context.MakeUserError(String.Format("Invalid register bank '{0}'", rTok.Text),
                                            rTok);
            }
            var indexTok = context.ConsumeExpected(TokenType.Integer);


            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                      "\t ConsumeTempRegister:" + rTok + "\t" + indexTok);


            uint index;
            bool bOK = UInt32.TryParse(indexTok.Text, out index);
            if (!bOK || index > 99)
            {
                throw context.MakeUserError(String.Format("Invalid register number '{0}{1}'", rTok.Text, indexTok.Text),
                                            indexTok);
            }

            try
            {
                if (context.OperandContext.IsAddress)
                {
                    return OperandReg.RegisterDeref(index,
                                                    byteOffset: 0,
                                                    dataType: context.OperandContext.DataType);
                }
                else
                {
                    return OperandReg.Register(index,
                                               context.OperandContext.DataType);
                }
            }
            catch (ArgumentException e)
            {
                throw context.MakeUserError(e.StripArgumentException(), indexTok);
            }

        }

        [NotNull]
        private static Operand ConsumeFloat([NotNull] Parser context)
        {
            var floatTok = context.ConsumeExpected<LexTokenFloat>(TokenType.FloatSingle);
            float val = floatTok.Value.f;
            if (context.OperandContext.IsNegated)
            {
                val = -val;
            }
            if (context.OperandContext.DataType == DataType.undef)
            {
                return OperandLiteral.f32(val);
            }
            else
            {
                try
                {
                    return new OperandLiteral(new Float_Union(val),
                                                     context.OperandContext.DataType);
                }
                catch (ArgumentException e)
                {
                    throw context.MakeUserError(e.StripArgumentException(), floatTok);
                }
            }
        }

        [NotNull]
        private static Operand ConsumeInteger([NotNull] Parser context)
        {
            var intTok = context.ConsumeExpected<LexTokenInteger>(TokenType.Integer);
            uint val = intTok.Value;
            if (context.OperandContext.IsNegated)
            {
                val = unchecked((uint)-val);
            }

            try
            {
                if (context.OperandContext.IsAddress)
                {
                    return OperandReg.LiteralAddress(val, context.OperandContext.DataType);
                }
                else
                {
                    if (context.OperandContext.DataType == DataType.undef)
                    {
                        return OperandLiteral.undef(val);
                    }
                    else
                    {
                        return new OperandLiteral(new Float_Union(val), context.OperandContext.DataType);
                    }
                }
            }
            catch (ArgumentException e)
            {
                throw context.MakeUserError(e.StripArgumentException(), intTok);
            }
        }

        [NotNull]
        private static Operand ConsumeNumber([NotNull] Parser context)
        {
            context.ExpectAny(new[] { TokenType.Integer, TokenType.FloatSingle});
            var currentToken = context.Current();
            if (currentToken.TokType == TokenType.Integer)
            {
                return ConsumeInteger(context);
            }
            else
            {
                return ConsumeFloat(context);

            }
        }

        [NotNull]
        private static Operand ConsumeLabelOperand([NotNull] Parser context)
        {
            var labelTok = context.ConsumeExpected(TokenType.Label);

            #region arg-check
            var dataType = context.OperandContext.DataType;
            uint opcodeWidth = context.OpcodeInfo.BitWidth;
            if (dataType == DataType.undef &&
                opcodeWidth != 0 && opcodeWidth < 16)
            {
                string err = string.Format("Labels are implicitly {0}bit or higher." +
                                           " If you wish to use it with in an {1}bit operation, " +
                                           "please explicitly cast it to u{1}",
                                            16, opcodeWidth);
                throw context.MakeUserError(err, labelTok);
            }
            else if (!dataType.IsValidMemType())
            {
                string err = string.Format("Label must be an unsigned or undef type. {0} given",
                                            dataType);
                throw context.MakeUserError(err, labelTok);
            }
            #endregion

            return new OperandLabel(labelTok.Text, context.OperandContext.DataType);
        }

        [NotNull]
        private static Operand ParseTextOperand([NotNull] Parser context)
        {
            context.Expect(TokenType.Text);

            LexToken currentTok = context.Current();
            string rawOperandText = currentTok.Text;
            if (rawOperandText.Length == 0)
            {
                throw context.MakeUserError(String.Format("Invalid operand '{0}'", rawOperandText), currentTok);
            }

            //todo: lexer should probably be chomping these up into TokenType.Register or something
            if (rawOperandText.ToLower() == "sp")
            {
                return ConsumeStackPointerRegister(context);
            }
            else if (rawOperandText.ToLower() == "lr")
            {
                return ConsumeLinkRegister(context);
            }
            else if (rawOperandText.ToLower() == "pc")
            {
                return ConsumePCRegister(context);
            }
            else if (rawOperandText.ToLower() == "fp")
            {
                return ConsumeFramePointerRegister(context);
            }

            switch (rawOperandText[0])
            {
                case 'r':
                case 'R':
                    return ConsumeTempRegister(context);
                default:
                    throw context.MakeUserError(String.Format("Invalid operand '{0}'", rawOperandText), currentTok);
            }
        }

        [NotNull]
        private static Operand ConsumePlus([NotNull] Parser context)
        {
            var tokPlus = context.ConsumeExpected(TokenType.Plus);

            if (context.OperandContext.IsNegated)
            {
                throw context.MakeUserError("negative and positive?",
                                            tokPlus);
            }
            
            context.OperandContext.IsNegated = false;
            return ConsumeNumber(context);
        }

        [NotNull]
        private static Operand ConsumeMinus([NotNull] Parser context)
        {
            var tokPlus = context.ConsumeExpected(TokenType.Minus);

            if (context.OperandContext.IsNegated)
            {
                throw context.MakeUserError("double negative?",
                                            tokPlus);
            }

            context.OperandContext.IsNegated = true;
            return ConsumeNumber(context);
        }

        [NotNull]
        private static Operand ParseDereferencedOperand([NotNull] Parser context)
        {
            // @ ( operand )   : e.g.   @(r0)
            var dataTypeTok = context.ConsumeExpected(TokenType.At);

            if (context.OperandContext.IsAddress)
            {
                throw context.MakeUserError("Can't dereference a dereference",
                                            dataTypeTok);
            }
            if (context.OperandContext.IsNegated)
            {
                throw context.MakeUserError("negate outside the dereference?",
                                            dataTypeTok);
            }

            context.OperandContext.IsAddress = true;

            context.ConsumeExpected(TokenType.LParen);
            var operand = ParseOperandExpression(context);
            Debug.Assert(operand.AddressMode == AddressMode.reg_deref ||
                         operand.AddressMode == AddressMode.reg_deref_offset_scale);
            context.ConsumeExpected(TokenType.RParen);
            return operand;
        }

        [NotNull]
        private static Operand ParseTypedOperand([NotNull] Parser context)
        {
            // type { operand }   : e.g.   u8 { r0 }
            var dataTypeTok = context.ConsumeExpected<LexTokenDataType>(TokenType.DataType);


            if (context.OperandContext.DataType != DataType.undef)
            {
                throw context.MakeUserError("Can't have a type cast inside of a type cast",
                                            dataTypeTok);
            }
            if (context.OperandContext.IsAddress || context.OperandContext.IsNegated)
            {
                throw context.MakeUserError("type cast should surround the entire operand.",
                                            dataTypeTok);
            }

            context.OperandContext.DataType = dataTypeTok.DataType;


            context.ConsumeExpected(TokenType.LCurly);

            var operand = ParseOperandExpression(context);

            context.ConsumeExpected(TokenType.RCurly);
            return operand;
        }

        [NotNull]
        private static Operand ParseOperandExpression([NotNull] Parser context)
        {
            var currentTok = context.Current();

            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                      "\t ParseOperandExpression:" + currentTok);

            switch (currentTok.TokType)
            {
                case TokenType.Plus:
                    return ConsumePlus(context);
                case TokenType.Minus:
                    return ConsumeMinus(context);
                case TokenType.Integer:
                    return ConsumeInteger(context);
                case TokenType.FloatSingle:
                    return ConsumeFloat(context);
                case TokenType.Text:
                    return ParseTextOperand(context);
                case TokenType.At:
                    return ParseDereferencedOperand(context);
                case TokenType.DataType:
                    return ParseTypedOperand(context);
                case TokenType.Label:
                    return ConsumeLabelOperand(context);
                case TokenType.Comma:
                case TokenType.WhiteSpace:
                case TokenType.EOL:
                case TokenType.LCurly:
                case TokenType.RCurly:
                case TokenType.LParen:
                case TokenType.RParen:
                    string usererr = String.Format("Symbol '{0}'", currentTok.TokType);
                    throw context.MakeUserError(usererr, currentTok);
                default:
                    string err = String.Format("Unhandled token '{0}'", currentTok.TokType);
                    throw context.MakeInternalError(err, currentTok);
            }
        }


        internal static Operand ParseOperand([NotNull] Parser context)
        {
            Debug.Assert(context.OpcodeInfo != null);
            context.OperandContext.ResetOperandContext();
            return ParseOperandExpression(context);
        }

        [NotNull]
        private static Operand[] ParseAllOperands([NotNull] Parser context)
        {
            var numOperands = context.OpcodeInfo.InstInfo.NumOperands;
            Operand[] operands = new Operand[numOperands];

            for (int i = 0; i < numOperands; i++)
            {
                if (i > 0)
                {
                    context.ConsumeExpected(TokenType.Comma);
                }

                var operand = ParseOperand(context);
                operands[i] = operand;
            }
            return operands;
        }


        private static void ConsumeLineLabel([NotNull] Parser context, int programCounter)
        {
            var labelTok = context.ConsumeExpected(TokenType.Label);
            string label = labelTok.Text;
            context.AddLabel(label, labelTok, programCounter);
        }

        [NotNull]
        private static Instruction MakeNewInstruction([NotNull] Parser context,
                                                      [NotNull] Operand[] operands)
        {
            Instruction inst;
            try
            {
                inst = new Instruction(context.OpcodeInfo.OpcodeType,
                                       operands,
                                       context.OpcodeInfo.BitWidth,
                                       context.OpcodeInfo.WritesCPUFlags);
            }
            catch (ArgumentException e)
            {
                throw context.MakeUserError(e.StripArgumentException(), context.OpcodeInfo.OpcodeToken);
            }
            return inst;
        }

        private static void ReplaceLabelOperandsWithLiterals([NotNull] List<Instruction> instructions,
                                                             [NotNull] Parser context)
        {
            //for now, once the program is parsed, resolve any labels into absolute addresses
            //todo: later we should probably leave them as labels and return a symbol table as well, to allow
            //them to be relocatable, and let the cpu do the picking. We should probably still validate labels in the parser,
            //though.
            //we could also just keep the jump destination in OperandLabel?
            for (int i = 0; i < instructions.Count; i++)
            {
                var inst = instructions[i];
                Debug.Assert(inst != null, "inst != null");

                //inst.Operands is read only collection, so rather than just replace it, make a whole new inst! :)
                var newOperands = inst.Operands.Map(
                    operand =>
                    {
                        Debug.Assert(operand != null, "operand != null");
                        if (operand.AddressMode == AddressMode.label)
                        {
                            OperandLabel labelOper = operand as OperandLabel;
                            if (labelOper == null)
                            {
                                throw context.MakeInternalError("`AddressMode.label` but `as OperandLabel` failed?", col: 0);
                            }

                            string label = labelOper.LabelText;

                            if (!context.LabelMap.ContainsKey(label))
                            {
                                //todo, record, per instruction, which source line it comes from.
                                //currently this will highlight the last line
                                string err = string.Format(
                                    "label `{0}` used as an operand but not declared in the program",
                                    label);
                                throw context.MakeUserError(err, column: 0);
                            }
                            var tuple = context.LabelMap[label];
                            Debug.Assert(tuple != null, "tuple != null");
                            var pcUnion = new Float_Union(tuple.Item2);
                            return new OperandLiteral(pcUnion, labelOper.DataType);
                        }
                        else
                        {
                            return operand;
                        }
                    });
                Debug.Assert(newOperands != null, "newOperands != null");

                instructions[i] = new Instruction(inst.Opcode, newOperands.ToArray(),
                                                  inst.GetBitWidth(0), inst.WritesCPUFlags);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly"></param>
        [NotNull]
        internal static Instruction[] ParseAssembly([NotNull] string assembly)
        {
            Debug.Assert(assembly != null, "assembly != null");

            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                     "----------------------------");
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                     "Parsing new assembly string:");
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                     assembly);
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                     "    --------------------    ");

            var context = new Parser();

            string[] lines = assembly.ToLower().Split('\n');
            var instructions = new List<Instruction>(lines.Length);

            foreach (var originalLine in lines)
            {
                Debug.Assert(originalLine != null, "originalLine != null");

                Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE, originalLine);
                Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                          String.Concat(Enumerable.Repeat("0123456789", ((originalLine.Length + 9) / 10))));


                context.StartNewLine(originalLine);

                if (context.Current().TokType == TokenType.Label)
                {
                    ConsumeLineLabel(context, instructions.Count);
                }

                if (context.Current().TokType == TokenType.EOL)
                {
                    continue;
                }

                OpcodeTextResult opcodeInfo = ParseOpcodeText(context);
                context.SetLineOpcode(opcodeInfo);

                var operands = ParseAllOperands(context);

                if (context.Current().TokType != TokenType.EOL)
                {
                    string err = string.Format(
                        "opcode `{0}` has {1} operands. Expecting EOL but found `{2}`",
                        context.OpcodeInfo.OpcodeType,
                        context.OpcodeInfo.InstInfo.NumOperands,
                        context.Current().Text);
                    throw context.MakeUserError(err, context.Current());
                }
                context.ConsumeExpected(TokenType.EOL);

                var inst = MakeNewInstruction(context, operands);

                instructions.Add(inst);

                Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE, "");
            }


            // Bit of a bodge for now.
            // Parser should really return a Program which contains a symbol table and instructions
            // rather than just a list of instructions. 
            ReplaceLabelOperandsWithLiterals(instructions, context);

            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                      "end parser");
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_VERBOSE,
                      "----------------------------");


            

            return instructions.ToArray();
        }

    }
}
