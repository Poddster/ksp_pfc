                or          r0,     0xFFFFFFFF, 0x00000000
                assert_eq   r0,     0xFFFFFFFF
                or          r0,     0xAAAAAAAA, 0x55555555
                assert_eq   r0,     0xFFFFFFFF

                or32        r0,     0x0F0F0F0F, 0x55555555
                assert_eq   r0,     0x5F5F5F5F
                or32        r0,     0xFFFFFFFF, 0x55555555
                assert_eq   r0,     0xFFFFFFFF

                or8         r1,     0x13, 0xED
                assert_eq   r1,     0xff
                or8         r1,     0x12, 0x80
                assert_eq   r1,     0x92

                mov32       r1,     0x76543210
                mov32       r2,     0xFEDBCA98
                or32        r3,     r2,         r1
                or32        r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0xfedffa98

                mov32       r3,     0x00000000
                mov32       r4,     0x00000000
                or16        r3,     r2,         r1
                or16        r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0xfa98

                halt
