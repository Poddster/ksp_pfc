		iadd32!     r0, 0X80000000, 0X80000000          #  1 -2147483648 + -2147483648 =          +0 |  zvc
		assert_eq   r0, 0X00000000                      #  2
		flags_read  r1                                  #  3
		assert_eq   r1, 0b0111                          #  4

		iadd32      r0, 0X80000000, 0X7FFFFFFF          #  5 -2147483648 + +2147483647 =          -1 | n
		assert_eq   r0, 0XFFFFFFFF                      #  6
		flags_read  r1                                  #  7
		assert_eq   r1, 0b0111                          #  8

		halt
