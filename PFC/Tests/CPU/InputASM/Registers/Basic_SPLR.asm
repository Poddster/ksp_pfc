                    assert_eq  SP,  0x0              #0
                    mov        SP,  0x8080           #1

                    jump       :test:                #2


                    #manually push 3 things onto the stack
             :func:                                  #
                    nop
                    ld32       r0,   SP              #4

                    iadd       r13,  SP,  4          #5
                    ld32       r1,   r13             #6

                    iadd32     r0,  r0,  r1          #7
                    assert_eq  r0,  0xedbccdfe       #8

                    jump       LR                    #9

                    #manually push 3 things onto the stack
             :test:
                    isub       SP,  SP,   0x4        #10
                    st32       SP,  0xDEADBEEF       #11

                    isub       SP,  SP,   0x4        #12
                    st32       SP,  0x0F0F0F0F       #13

                    iadd       LR,  PC,   2          #14

                    jump       :func:                #15

                    iadd       sp,  sp,   0x8        #16
                    assert_eq  SP,  0x8080           #17
                    halt                             #18