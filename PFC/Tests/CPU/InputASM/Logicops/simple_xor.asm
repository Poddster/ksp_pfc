                xor         r0,     0xFFFFFFFF, 0x00000000
                assert_eq   r0,     0xFFFFFFFF
                xor         r0,     0xAAAAAAAA, 0x55555555
                assert_eq   r0,     0xFFFFFFFF

                xor32       r0,     0x0F0F0F0F, 0x55555555
                assert_eq   r0,     0x5A5A5A5A
                xor32       r0,     0xFFFFFFFF, 0x55555555
                assert_eq   r0,     0xAAAAAAAA

                xor8        r1,     0x13, 0xED
                assert_eq   r1,     0xfe
                xor8        r1,     0x12, 0x80
                assert_eq   r1,     0x92

                mov32       r1,     0x76543210
                mov32       r2,     0xFEDBCA98
                xor32       r3,     r2,         r1
                xor32       r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0x888ff888

                mov32       r3,     0x00000000
                mov32       r4,     0x00000000
                xor16       r3,     r2,         r1
                xor16       r4,     r1,         r2
                assert_eq   r3,     r4
                assert_eq   r3,     0xf888

                halt
