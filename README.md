# README #

##Kerbal Space Program - Programmable Flight Computer##

Project is currently suspended and will probably never be resumed. And if it is resumed it'll be as a standalone circuit + component simulation that has nothing to do with KSP (but might have a little 2d space ship to fly ;))

### What is this repository for? ###

Full system computer emulation inside of KSP to allow automation, programmable flights and circuit-design-mistake-based-fun.

Idea is to have an emulated circuit board that players can use to build computer system for their ships. They can build the useable ingame interface (e.g. switches dials, readouts etc)  and the circuitry for their space ship using emulated components (e.g. CPUs, LCD/8-segs, resistors, caps, transistors, gates, power stuff, etc). 

They could use this as a more realistic option to things like SAS -- i.e. you have to design/develop/program your own, and also build entire programs that will fly to the Mun, with Jeb only having to change program etc. Just like Apollo! :) This would all come at a complexity and weight cost, of course.

There's also the possibility to have valves pop mid flight and have the players fix it re-route (and potentially makes things worse). 


Currently it's just a standalone library that has a mostly done CPU emulation. The only major things missing are a million more operations, but they're trivial to add.

Circuit board simulation was vauged out in my head and on paper and I did a lot of research into "how" circuit simulation is done IRL and which option was best for PFC, but it never really made it into the repo.

Integration with kerbal space program was left until a later date. (The plan was to be able to do it all standalone, as a general circuit board + cpu emulator)

I saw KOS and basically gave up on PFC, as even though KOS was pretty terrible and hugely buggy at the time -- and I think players would find writing actual assembly and piecing together a circuit board much more fun -- it had gotten there "first".


There's a huge amount of work and refactoring to be done in the CPU emulation. There's whole heaps of abstraction missing. It was my first major project in C# and I couldn't be arsed figuring out how to any of the standard unit test frameworks worked. I just made an exe that called the libraries test functions :)


### How do I get set up? ###

Just open PFC.sln and hit go. It just runs tests right now. There's nothing interactable.  It should be a completely self contained project, with no external dependencies right now. (not even KSP!)