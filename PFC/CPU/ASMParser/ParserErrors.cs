﻿using System;
using System.Diagnostics;

namespace PFC.ASMParser
{
    using JetBrains.Annotations;

    using PFC.CPU;
    using System.Runtime.Serialization;


    /// <summary>
    /// The exception that should be seen by the user if they enter bad input.
    /// </summary>
    [Serializable]
    public class PFCAsmInputError : PFCBaseException
    {
        public PFCAsmInputError()
        {
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_BAD_USER_SYMBOL, "");
        }

        public PFCAsmInputError([NotNull] string message)
            : base(message)
        {
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_BAD_USER_SYMBOL, message);
        }

        public PFCAsmInputError(string message, [NotNull] Exception inner)
            : base(message, inner)
        {
            string err = message + inner;
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_BAD_USER_SYMBOL, err);
        }

        protected PFCAsmInputError(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.PARSER_BAD_USER_SYMBOL, "");
        }

        [NotNull]
        private static string UnderlineError(
            [NotNull] string errorMessage,
            [NotNull] string sourceLine,
                      int    lineNumber,
                      int    column)
        {
            Debug.Assert(column >= 0);

            //find where the erroneous charracter is in the trimmedLine, then find where that
            //substring of the trimmed trimmedLine is in the overall string.
            int nextWhitespaceIndex = sourceLine.IndexOfAny(new[] { ' ', '\t', '\n', '\r' }, column);
            if (column >= nextWhitespaceIndex)
            {
                //no -1
                nextWhitespaceIndex = sourceLine.Length;
            }
            //makes N spaces then make M ^ characters
            string underline = new string(' ', column) + new string('^', nextWhitespaceIndex - column);
            string header = string.Format("Bad Syntax line{0}:col{1}: {2}", lineNumber, column, errorMessage);
            string fullError = header + "\n" + sourceLine + "\n" + underline;
            return fullError;
        }

        [NotNull]
        private static string MakeUserError(
            [NotNull] string message,
            [NotNull] string sourceLine,
                      int    lineNumber,
                      int    column)
        {
            string err = UnderlineError(message, sourceLine, lineNumber, column);
            return err;
        }

        public PFCAsmInputError(
            [NotNull] string message,
            [NotNull] string sourceLine,
                      int    lineNumber,
                      int    column)
            : this(MakeUserError(message, sourceLine, lineNumber, column))
        {
        }

    }

    /// <summary>
    /// Error that should not usually be seen by the user, as it means something internal has gone wrong.
    /// </summary>
    [Serializable]
    public class ParserInternalError : PFCBaseException
    {
        public ParserInternalError()
        {
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.INTERNAL_ERROR, "");
        }

        public ParserInternalError(string message)
            : base(message)
        {
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.INTERNAL_ERROR, message);
        }

        public ParserInternalError([NotNull] string message, [NotNull] Exception inner)
            : base(message, inner)
        {
            string err = message + inner;
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.INTERNAL_ERROR, err);
        }

        protected ParserInternalError(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            Log.Print(DPFCLASS.PARSER, DPFMASK_PARSER.INTERNAL_ERROR, "");
        }

        [NotNull]
        private static string MakeInternalError(
            [NotNull] string  message,
            [NotNull] string  sourceLine,
                      int     lineNumber,
                      int     column)
        {
            string err = String.Format("Parser Internal Error on line {0}. Error: {1}. Source Text: {2}",
                                        lineNumber, message, sourceLine);
            return err;
        }
        public ParserInternalError([NotNull] string errMessage, [NotNull] string sourceLine, int lineNumber, int column)
            : this(MakeInternalError(errMessage, sourceLine, lineNumber, column))
        {
        }
        public ParserInternalError(
            [NotNull] string    errMessage,
            [NotNull] string    sourceLine,
                      int       lineNumber,
                      int       column,
            [NotNull] Exception e)
            : this(MakeInternalError(errMessage, sourceLine, lineNumber, column), e)
        {
        }
    }


}
