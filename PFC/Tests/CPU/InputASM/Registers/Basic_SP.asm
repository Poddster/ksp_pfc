                    assert_eq  SP,  0x0              #0
                    mov        SP,  0x8080           #1
                                                     #
                    jump        :test:               #2
                                                     #
                                                     #
            #manually push 3 things onto the stack   #
             :func:                                  #
                    ld32       r14,  SP              #3

                    iadd       r13,  SP,  4          #4
                    ld32       r0,   r13             #5

                    iadd       r13,  SP,  8          #6
                    ld32       r1,   r13             #7

                    iadd32     r0,  r0,  r1          #8
                    assert_eq  r0,  0xedbccdfe       #9

                    jump        r14                  #10

                    #manually push 3 things onto the stack
             :test:                                  #
                    isub       SP,  SP,   0x4        #11
                    st32       SP,  0xDEADBEEF       #12

                    isub       SP,  SP,   0x4        #13
                    st32       SP,  0x0F0F0F0F       #14

                    isub       SP,  SP,   0x4        #15
                    iadd       r0,  PC,   3          #16
                    st32       SP,  r0               #17

                    jump       :func:                #18

                    iadd       sp,  sp,   0xC        #19
                    assert_eq  SP,  0x8080           #20
                    halt                             #21