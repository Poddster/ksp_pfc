                 :test:
                        mov        SP,  0x8000            #0
                        push_data  0x0F0F0F0F             #1
                        push_data  0xDEADBEEF             #2
                        call       :func:                 #3
                        assert_eq  r0,   0xcf9eafe0       #4 0xDEADBEEF - 0x0F0F0F0F

                        push_data  0x00000001             #5
                        push_data  0x00000008             #6
                        call       :func:                 #7
                        assert_eq  r0,   0x00000007       #8


                        assert_eq  SP,  0x8000            #9

                        halt                              #10

                 :func:
                        pop_data  r0                      #11
                        pop_data  r1                      #12
                        isub32    r0,    r0,   r1         #13
                        ret                               #14