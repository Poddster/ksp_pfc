
            mov         sp, 0x500
            mov         fp, 0x123
            
            push_data   0
            call        :fib:
            assert_eq   r0, 0       #fib 0 == 0
            stack_free  4
            
            assert_eq   sp, 0x500
            assert_eq   fp, 0x123
            
            push_data   1
            call        :fib:
            assert_eq   r0, 1       #fib 1 == 1
            stack_free  4
            
            push_data   2
            call        :fib:
            assert_eq   r0, 1       #fib 2 == 1
            stack_free  4
            
            push_data   3
            call        :fib:
            assert_eq   r0, 2       #fib 3 == 2
            stack_free  4
            
            # 15 is pretty much the max you can call,
            # with a default ctrl_stack size of 30
            push_data   15
            call        :fib:
            assert_eq   r0,  610    #fib 15 == 610
            stack_free  4
            
            assert_eq   sp, 0x500
            assert_eq   fp, 0x123
            
            halt

    :fib:
            # Recursive Fibonacci
            # def fib(n):
            #    if n < 2:
            #       return n
            #    else:
            #       a = fib(n-1)
            #       b = fib(n-2)
            #       c = a + b
            #       return c
            #
            # returs a value in r0
            
            # save room for saving variable 'a'
            frame_enter 4
            
            # @(fp) = n (aka function arg0)
            #    if n < 2:
            #       return n
            icmp!       @(fp),         0x2
            mov         r0, @(fp)
            bslt        :fib-return:

            #else:
            #       Do this using the value of fp before a function
            #       call. Should be fine after return
            #       a = fib(n-1)
            isub        r0,         @(fp),  0x1  # r0 = n - 1
            push_data   r0                       # arg0 = r0
            call        :fib:                    # r0 = fib(n-1)
            stack_free  4                        # free args to fib()
                                                 #
            # could also just use 'st32 sp, r0'
            # could also just mov it into r2.. but want to use fp here
            # to ensure frame_enter/fp/sp work
            isub        r1,         fp,     0x4  # r1 = &a
            st32        r1,         r0           # a = r0
            
            
            
            #       b = fib(n-2)
            isub        r0,       @(fp),    0x2  # r0 = n - 2
            push_data   r0                       # arg0 = r0
            call        :fib:                    # r0 = fib(n-2)
            stack_free  4                        # free args to fib()
                                                 # :. r0 = b

            #restore 'a' from the frame_enter'd area
            isub        r1,         fp,     0x4  # r1 = &a
            ld32        r2,         r1           # r2 = a
            
            iadd32      r0,         r0,     r2   # r0 = c = a + b
    :fib-return:
            frame_leave                          # free room for 'a'
            ret         #r0
            
