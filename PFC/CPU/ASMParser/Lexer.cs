﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using PFC.CPU;

namespace PFC.ASMParser
{
    using JetBrains.Annotations;

    public enum TokenType
    {
        Text,
        Comma,
        WhiteSpace,
        EOL,
        LCurly,
        RCurly,
        At,
        LParen,
        RParen,
        DataType,
        Integer,
        FloatSingle,
        Plus,
        Minus,
        Label,
        Exclaim,
    }

    public class LexToken
    {
        public readonly TokenType TokType;

        [NotNull]
        public readonly string Text;

        public readonly int ColStart;

        public readonly int ColEnd;

        public readonly int LineNumber;
        //the length of the string consumed from the source to make this token.
        //can be smaller than Text.Length if, e.g., there were _ characters
        public readonly int OriginalSourceLength;

        internal LexToken(TokenType tokType, [NotNull] string token, int lineNumber, int colStart, int colEnd)
        {
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }
            this.TokType = tokType;
            this.Text = token;
            this.ColStart = colStart;
            this.LineNumber = lineNumber;
            this.ColEnd = colEnd;
            this.OriginalSourceLength = colEnd - colStart + 1;
            Debug.Assert(this.OriginalSourceLength > 0);
        }

        public override string ToString()
        {
            string outStr = String.Format("LexToken {3,-12}@line {4,-3} col[{1,2},{2,2}] '{0}'",
                                            this.Text, this.ColStart,
                                            this.ColEnd, this.TokType, this.LineNumber);
            return outStr;
        }
    }

    public class LexTokenDataType : LexToken
    {
        internal readonly DataType DataType;
        internal LexTokenDataType(TokenType tokType,
                                  [NotNull] string token,
                                  int lineNumber,
                                  int colStart, int colEnd,
                                  DataType dataType) :
            base(tokType, token, lineNumber, colStart, colEnd)
        {
            Debug.Assert(tokType == TokenType.DataType);
            this.DataType = dataType;
        }

        public override string ToString()
        {
            string outStr = base.ToString() + "\t:: DataType " + this.DataType.ToString();
            return outStr;
        }
    }

    public class LexTokenInteger : LexToken
    {
        internal readonly uint Value;
        internal LexTokenInteger(TokenType tokType,
                                 [NotNull] string token, int lineNumber,
                                 int colStart, int colEnd,
                                 uint value) :
            base(tokType, token, lineNumber, colStart, colEnd)
        {
            Debug.Assert(tokType == TokenType.Integer);
            this.Value = value;
        }
        public override string ToString()
        {
            string extend = String.Format("\t:: Integer 0x{0:X8}", this.Value);
            string outStr = base.ToString() + extend;
            return outStr;
        }
    }

    public class LexTokenFloat : LexToken
    {
        internal readonly Float_Union Value;
        internal LexTokenFloat(TokenType tokType,
                               [NotNull] string token, int lineNumber,
                                 int colStart, int colEnd,
                                 Float_Union value) :
            base(tokType, token, lineNumber, colStart, colEnd)
        {
            Debug.Assert(tokType == TokenType.FloatSingle);
            this.Value = value;
        }
        public override string ToString()
        {
            string extend = String.Format("\t:: Float Single {0}", this.Value);
            string outStr = base.ToString() + extend;
            return outStr;
        }
    }


    public class LexerContext
    {

        private static bool IsValidDecChar(char c)
        {
            return (c >= '0') && (c <= '9') || c == '_';
        }

        private static bool IsValidBinChar(char c)
        {
            switch (c)
            {
                case '0': case '1': 
                case '_':
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidOctChar(char c)
        {
            return (c >= '0') && (c <= '7') || c == '_';
        }

        private static bool IsValidHexChar(char c)
        {
            return (c >= '0') && (c <= '9') ||
                   (c >= 'A') && (c <= 'F') ||
                   (c >= 'a') && (c <= 'f') ||
                   c == '_';
        }

        private static bool IsValidIdentifierChar(char c)
        {
            return Char.IsLetter(c) || c == '_';
        }

        private static bool IsValidLabelContents(char c)
        {
            /* We used to allow everything but EOF and :, but you could do this:
             * @":mov r0, 0x1 !!? :  mov r0, 0x1"
             */
            //return !(c == ':' || c == '\n' || c == '\r' || c == '\0');
            switch (c)
            {
                case '_':
                case '-':
                case '.':
                    return true;
                default:
                    return Char.IsLetterOrDigit(c);
            }
        }




        private static char PeekChar([NotNull] string sourceLine, int col, int num = 0)
        {
            Debug.Assert(sourceLine != null);
            if (col + num >= sourceLine.Length)
            {
                return '\0';
            }
            return sourceLine[col + num];
        }


        [NotNull]
        private static string LexSubStringToRule(
            [NotNull] string sourceLine,
            int tokStart,
            [NotNull] Predicate<char> fPredicate)
        {
            int digitsEnd = LexToRule(sourceLine, tokStart, fPredicate);
            return sourceLine.Substring(tokStart, digitsEnd - tokStart + 1);
        }

        private static int LexToRule(
            [NotNull] string sourceLine,
            int tokStart,
            [NotNull] Predicate<char> fPredicate)
        {
            int tokEnd;
            for (tokEnd = tokStart; tokEnd < sourceLine.Length; tokEnd++)
            {
                if (!fPredicate(PeekChar(sourceLine, tokEnd)))
                {
                    break;
                }
            }
            return tokEnd - 1;
        }

        private static int LexDigits([NotNull] string sourceLine, int lineNumber, int digitsStart)
        {
            int digitsEnd;
            for (digitsEnd = digitsStart + 1; digitsEnd < sourceLine.Length; digitsEnd++)
            {
                if (!IsValidDecChar(sourceLine[digitsEnd]))
                {
                    break;
                }
            }
            return digitsEnd - 1;
        }

        [NotNull]
        private static LexToken ConvertIntegerDigits(
            [NotNull] string intDigits,
            [NotNull] string intType,
            int radix,
            [NotNull] string sourceLine,
            int lineNumber,
            int startCol,
            int endCol)
        {
            string strippedText = intDigits.Replace("_", "");
            if (strippedText.Length == 0)
            {
                string err = String.Format("Invalid {0} literal", intType);
                throw new PFCAsmInputError(err, sourceLine, lineNumber, startCol);
            }

            uint result;
            try
            {
                result = Convert.ToUInt32(strippedText, radix);
            }
            catch (Exception e)
            {
                if (e is FormatException || e is OverflowException)
                {
                    string err = String.Format("Invalid {0} literal", intType);
                    throw new PFCAsmInputError(err, sourceLine, lineNumber, startCol);
                }
                throw new ParserInternalError(
                    "ConvertIntegerDigits: Unexpected parse error",
                    sourceLine, lineNumber, startCol, e);
            }

            string originalText = sourceLine.Substring(startCol, endCol - startCol + 1);

            //Check if the character directly after the string is a char.
            //e.g. 0b116  -- if this check wasn't here then we'd lex this as {int 3, int 6}, rather than ERROR.
            //if we didn't have this check, the parser would pick it up anyway
            if (IsValidHexChar(PeekChar(sourceLine, startCol, originalText.Length)))
            {
                string err = String.Format("Invalid {0} literal", intType);
                throw new PFCAsmInputError(err, sourceLine, lineNumber, startCol);
            }

            var tok = new LexTokenInteger(TokenType.Integer, originalText, lineNumber, startCol, endCol, result);


            return tok;
        }

        [NotNull]
        private static LexToken ConsumeIntegerLiteral(
            char radixChar,
            int radixSize,
            [NotNull] string radixString,
            [NotNull] Predicate<char> fPredicate,
            [NotNull] string sourceLine,
            int lineNumber,
            int startCol)
        {
            if (PeekChar(sourceLine, startCol) == '0' &&
                Char.ToLower(PeekChar(sourceLine, startCol, 1)) == radixChar &&
                fPredicate(PeekChar(sourceLine, startCol, 2)))
            {
                string digits = LexSubStringToRule(sourceLine, startCol + 2, fPredicate);
                int endCol = startCol + 2 + digits.Length - 1;
                return ConvertIntegerDigits(digits, radixString, radixSize, sourceLine, lineNumber, startCol, endCol);
            }

            string err = String.Format("Invalid {0} literal", radixString);
            throw new PFCAsmInputError(err, sourceLine, lineNumber, startCol);
        }

        private static int LexOptionalFloatType([NotNull] string sourceLine, int originalTokenEnd)
        {
            char optFloatType = PeekChar(sourceLine, originalTokenEnd, 1);
            if (optFloatType == 'f' || optFloatType == 'F')
            {
                originalTokenEnd += 1;
            }
            else if (optFloatType == 'd' || optFloatType == 'D')
            {
                originalTokenEnd += 1;
                throw new NotImplementedException("VM doesn't support doubles yet");
            }
            return originalTokenEnd;
        }

        private static int LexOptionalFloatExp([NotNull] string sourceLine, int lineNumber, int fracEnd)
        {
            //([eE][-+]?[0-9]+)?
            int originalTokenEnd = fracEnd;
            if (Char.ToLower(PeekChar(sourceLine, fracEnd, 1)) == 'e')
            {
                char plusOrMinusOrDigit = PeekChar(sourceLine, fracEnd, 2);
                int expDigitStart;
                if (plusOrMinusOrDigit == '+' || plusOrMinusOrDigit == '-')
                {
                    expDigitStart = fracEnd + 3;
                }
                else
                {
                    expDigitStart = fracEnd + 2;
                }

                if (!IsValidDecChar(PeekChar(sourceLine, expDigitStart)))
                {
                    throw new PFCAsmInputError(
                        "Invalid Float Literal. Bad symbol after exponent",
                        sourceLine,
                        lineNumber,
                        fracEnd);
                }
                originalTokenEnd = LexDigits(sourceLine, lineNumber, expDigitStart);
            }
            return originalTokenEnd;
        }

        [NotNull]
        private static LexToken ConsumeFloatTail(
            [NotNull] string sourceLine,
            int lineNumber,
            int startCol,
            int pointCol)
        {
            Debug.Assert((PeekChar(sourceLine, pointCol) == '.'));
            if (!IsValidDecChar(PeekChar(sourceLine, pointCol, 1)))
            {
                throw new PFCAsmInputError(
                    "Invalid Float Literal. Bad symbol after decimal-point",
                    sourceLine,
                    lineNumber,
                    startCol);
            }

            int fracEnd = LexDigits(sourceLine, lineNumber, pointCol + 1);
            var originalTokenEnd = LexOptionalFloatExp(sourceLine, lineNumber, fracEnd);

            //Parse explodes if on the last char if it's 'f' or 'd', but we still want that
            //passing to the parser, so store where the float ends before the optional f/d.
            int parseableTokEnd = originalTokenEnd;
            originalTokenEnd = LexOptionalFloatType(sourceLine, originalTokenEnd);

            var originalFloatText = sourceLine.Substring(startCol, originalTokenEnd - startCol + 1);
            var strippedFloatText = sourceLine.Substring(startCol, parseableTokEnd - startCol + 1);
            strippedFloatText = strippedFloatText.Replace("_", "");

            try
            {
                float result = Convert.ToSingle(strippedFloatText);
                var fu = new Float_Union(result);
                LexTokenFloat tok = new LexTokenFloat(
                    TokenType.FloatSingle,
                    originalFloatText,
                    lineNumber,
                    startCol,
                    originalTokenEnd,
                    fu);
                return tok;
            }
            catch (Exception e)
            {
                if (e is FormatException || e is OverflowException)
                {
                    throw new PFCAsmInputError("Invalid float literal {0}", sourceLine, lineNumber, startCol);
                }
                else
                {
                    throw new ParserInternalError(
                        "ConsumeFloatLiteral: Unexpected parse error",
                        sourceLine,
                        lineNumber,
                        startCol,
                        e);
                }
            }
        }

        [NotNull]
        private static LexToken ConsumeDecIntOrFloatLiteral([NotNull] string sourceLine, int lineNumber, int startCol)
        {
            //lexes ints or floats, as we won't know what they are based off the first digit.
            //int:
            //  _*[0-9][0-9_]*"
            //float:
            //  [0-9]+[.][0-9]+([eE][-+]?[0-9]+)?[fFdD]?


            //At this point we have a digit, but we don't know if it's a float or a int.
            Debug.Assert(IsValidDecChar(sourceLine[startCol]));

            int intEnd = LexDigits(sourceLine, lineNumber, startCol);
            if (PeekChar(sourceLine, intEnd + 1) != '.')
            {
                //no decimal-point, must have just been a plain on int.
                string digits = sourceLine.Substring(startCol, intEnd - startCol + 1);
                return ConvertIntegerDigits(digits, "int", 10, sourceLine, lineNumber, startCol, intEnd);
            }

            //at this point we definitely know that it should be a float.
            return ConsumeFloatTail(sourceLine, lineNumber, startCol, intEnd + 1);
        }

        [NotNull]
        private static LexToken ConsumeLeadingZero([NotNull] string sourceLine, int lineNumber, int startCol)
        {
            Debug.Assert(PeekChar(sourceLine, startCol) == '0');
            char nextChar = PeekChar(sourceLine, startCol, 1);
            switch (nextChar)
            {
                case '.':
                    return ConsumeFloatTail(sourceLine, lineNumber, startCol, startCol + 1);
                case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8':
                case '9': case '0':
                    return ConsumeDecIntOrFloatLiteral(sourceLine, lineNumber, startCol);
                case 'x': case 'X':
                    return ConsumeIntegerLiteral('x', 16, "hex", IsValidHexChar, sourceLine, lineNumber, startCol);
                case 'n': case 'N':
                    return ConsumeIntegerLiteral('n', 10, "dec", IsValidDecChar, sourceLine, lineNumber, startCol);
                case 'o': case 'O':
                    return ConsumeIntegerLiteral('o', 08, "oct", IsValidOctChar, sourceLine, lineNumber, startCol);
                case 'b': case 'B':
                    return ConsumeIntegerLiteral('b', 02, "bin", IsValidBinChar, sourceLine, lineNumber, startCol);
                default:
                    return new LexTokenInteger(TokenType.Integer, "0", lineNumber, startCol, startCol, 0);
            }
        }

        [NotNull]
        private static LexToken ConsumeWhitespace([NotNull] string sourceLine, int lineNumber, int startCol)
        {
            string ws = LexSubStringToRule(sourceLine, startCol, Char.IsWhiteSpace);
            return new LexToken(TokenType.WhiteSpace, ws, lineNumber, startCol,
                                           startCol + ws.Length - 1);
        }

        private static DataType ConvertToDataType([NotNull] string sourceLine, int lineNumber, int startCol,
                                                  char typeChar, byte size)
        {
            switch (typeChar)
            {
                default:
                    throw new ParserInternalError("ConsumeDataType called on invalid string",
                                                   sourceLine, lineNumber, startCol);
                case 's':
                case 'S':
                case 'i':
                case 'I':
                    switch (size)
                    {
                        case 8: return DataType.i8;
                        case 16: return DataType.i16;
                        case 32: return DataType.i32;
                    }
                    break;
                case 'u':
                case 'U':
                    switch (size)
                    {
                        case 8: return DataType.u8;
                        case 16: return DataType.u16;
                        case 24: return DataType.u24;
                        case 32: return DataType.u32;
                        case 64: return DataType.u64;
                    }
                    break;
                case 'f':
                case 'F':
                    switch (size)
                    {
                        case 16: return DataType.f16;
                        case 32: return DataType.f32;
                    }
                    break;
            }
            throw new PFCAsmInputError("Invalid DataType size", sourceLine, lineNumber, startCol);
        }

        [NotNull]
        private static Option<LexToken> ConsumeDataType([NotNull] string sourceLine, int lineNumber, int startCol)
        {
            int digitsStart = startCol + 1;
            if (!Char.IsDigit(PeekChar(sourceLine, digitsStart)))
            {
                return new None<LexToken>();
            }


            int digitsEnd = LexDigits(sourceLine, lineNumber, digitsStart);
            string digits = sourceLine.Substring(digitsStart, digitsEnd - digitsStart + 1);
            if (digits.Length == 0)
            {
                throw new ParserInternalError("ConsumeDataType: LexDigits returned empty string?",
                                               sourceLine, lineNumber, startCol);
            }

            byte size;
            try
            {
                size = Byte.Parse(digits);
            }
            catch (Exception e)
            {
                if (e is FormatException || e is OverflowException)
                {
                    throw new PFCAsmInputError("Invalid DataType size", sourceLine, lineNumber, startCol);
                }
                else
                {
                    throw new ParserInternalError("ConsumeDataType: Unexpected parse error",
                                                  sourceLine, lineNumber, startCol, e);
                }
            }

            char typeChar = sourceLine[startCol];
            DataType type = ConvertToDataType(sourceLine, lineNumber, startCol, typeChar, size);

            LexToken tok = new LexTokenDataType(TokenType.DataType, typeChar + digits, lineNumber,
                                                startCol, startCol + digits.Length, type);
            return new Some<LexToken>(tok);
        }

        [NotNull]
        private LexToken ConsumeLabel()
        {
            int labelStartDelim = this.scanCol;
            var labelText = LexSubStringToRule(this.sourceLine, labelStartDelim + 1, IsValidLabelContents);

            int labelLastDelim = this.scanCol + labelText.Length + 1;
            char lastDelim = PeekChar(this.sourceLine, labelLastDelim);
            if (lastDelim != ':')
            {
                if (lastDelim == '\n' || lastDelim == '\r' || lastDelim == '\0')
                {
                    throw new PFCAsmInputError(
                        "Invalid label: Expected ':' before EOL",
                        this.sourceLine,
                        this.lineNumber,
                        this.scanCol);
                }
                else if (Char.IsWhiteSpace(lastDelim))
                {
                    throw new PFCAsmInputError(
                        "Labels can't contain whitespace",
                        this.sourceLine,
                        this.lineNumber,
                        this.scanCol);
                }
                else
                {
                    string err = string.Format("Invalid character in label '{0}'", lastDelim);
                    throw new PFCAsmInputError(err, this.sourceLine, this.lineNumber, this.scanCol);
                }
            }
            else if (labelText.Length == 0)
            {
                throw new PFCAsmInputError(
                    "Invalid label: Label with no text found.",
                    this.sourceLine,
                    this.lineNumber,
                    this.scanCol);
            }

            string fullLabelText = ":" + labelText + ":";
            var labelToken = new LexToken(TokenType.Label, fullLabelText, this.lineNumber, labelStartDelim, labelLastDelim);
            return labelToken;
        }




        private readonly int lineNumber;

        [NotNull]
        private readonly string sourceLine;

        private int scanCol;

        [CanBeNull]
        private LexToken peekedToken;

        public LexerContext([NotNull] string sourceLine, int lineNumber, int scanCol = 0)
        {
            //this should immediately parse the line?
            //and then maintain the token queue (or construct it on first scan).
            //then if peek + whitespace, peek at top
            //then if peek + nowhitespace, peek at top, and keep peeking if whitespace.
            //same for next token etc.

            this.lineNumber = lineNumber;
            this.scanCol = scanCol;
            this.sourceLine = sourceLine;
        }

        [NotNull]
        private LexToken ScanForNextToken()
        {
            // ReSharper disable RedundantIfElseBlock

            char c = PeekChar(this.sourceLine, this.scanCol);
            if (c == '\0')
            {
                return new LexToken(TokenType.EOL, "", lineNumber, this.scanCol, this.scanCol);
            }
            else if (c == '#')
            {
                //Ignore everything to the right of a comment character.
                return new LexToken(TokenType.EOL, "", lineNumber, this.scanCol, this.scanCol);
            }
            else if (c == ',')
            {
                var tok = new LexToken(TokenType.Comma, ",", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == '{')
            {
                var tok = new LexToken(TokenType.LCurly, "{", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == '}')
            {
                var tok = new LexToken(TokenType.RCurly, "}", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == '@')
            {
                var tok = new LexToken(TokenType.At, "@", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == '(')
            {
                var tok = new LexToken(TokenType.LParen, "(", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == ')')
            {
                var tok = new LexToken(TokenType.RParen, ")", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (Char.IsWhiteSpace(c))
            {
                var tok = ConsumeWhitespace(this.sourceLine, this.lineNumber, this.scanCol);
                this.scanCol += tok.OriginalSourceLength;
                return tok;
            }
            else if (c == '0')
            {
                var tok = ConsumeLeadingZero(this.sourceLine, this.lineNumber, this.scanCol);
                this.scanCol += tok.OriginalSourceLength;
                return tok;
            }
            else if (IsValidDecChar(c))
            {
                var tok = ConsumeDecIntOrFloatLiteral(this.sourceLine, this.lineNumber, this.scanCol);
                this.scanCol += tok.OriginalSourceLength;
                return tok;
            }
            else if (c == '+')
            {
                var tok = new LexToken(TokenType.Plus, "+", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == '-')
            {
                var tok = new LexToken(TokenType.Minus, "-", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == '!')
            {
                var tok = new LexToken(TokenType.Exclaim, "!", this.lineNumber, this.scanCol, this.scanCol);
                this.scanCol++;
                return tok;
            }
            else if (c == ':')
            {
                var labelToken = this.ConsumeLabel();
                this.scanCol += labelToken.Text.Length;
                return labelToken;
            }
            else
            {
                if (!Char.IsLetter(c))
                {
                    //todo: allow // as comment?
                    //fixme
                    string err = String.Format("Bad symbol in source file. '{0};.", c);
                    throw new PFCAsmInputError(err, this.sourceLine, this.lineNumber, this.scanCol);
                }

                if (c == 'i' || c == 'u' || c == 'I' || c == 'U' || c == 'f' || c == 'F' || c == 's' || c == 'S')
                {
                    var optTok = ConsumeDataType(this.sourceLine, this.lineNumber, this.scanCol);
                    if (optTok.HasValue)
                    {
                        var tok = ((Some<LexToken>)optTok).Value;
                        this.scanCol += tok.OriginalSourceLength;
                        return tok;
                    }
                }

                var tokString = LexSubStringToRule(this.sourceLine, this.scanCol, IsValidIdentifierChar);
                Debug.Assert(tokString.Length > 0);
                var token = new LexToken(
                    TokenType.Text,
                    tokString,
                    this.lineNumber,
                    this.scanCol,
                    this.scanCol + tokString.Length - 1);
                this.scanCol += tokString.Length;
                return token;
            }
            // ReSharper restore RedundantIfElseBlock
        }

        [NotNull]
        private LexToken _NextToken(bool bReturnWhiteSpace)
        {
            var tok = this.ScanForNextToken();
            if (tok.TokType == TokenType.WhiteSpace && !bReturnWhiteSpace)
            {
                tok = this.ScanForNextToken();
                Debug.Assert(tok.TokType != TokenType.WhiteSpace);
            }
            return tok;
        }

        [NotNull]
        public LexToken NextToken(bool bReturnWhiteSpace = false)
        {
            if (this.peekedToken != null)
            {
                var peek = this.peekedToken;
                this.peekedToken = null;
                return peek;
            }
            return this._NextToken(bReturnWhiteSpace);
        }

        [NotNull]
        public LexToken PeekToken(bool bReturnWhiteSpace = false)
        {
            if (this.peekedToken == null)
            {
                this.peekedToken = this._NextToken(bReturnWhiteSpace);
            }

            Debug.Assert(this.peekedToken != null);
            return this.peekedToken;
        }

        public IEnumerable<LexToken> ScanAllRemainingTokens(bool bReturnWhiteSpace = false)
        {
            while (true)
            {
                var token = this.NextToken(bReturnWhiteSpace);
                if (token.TokType == TokenType.EOL)
                {
                    break;
                }
                yield return token;
            }
        }

    }

}
