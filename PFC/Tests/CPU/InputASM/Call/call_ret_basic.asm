:test:
	mov        r0,   0xDEADBEEF       #0
	mov        r1,   0x0F0F0F0F       #1
	call       :func:                 #3 
	assert_eq  r0,   0xedbccdfe       #4

	mov        r0,   0x00000001       #5
	mov        r1,   0x00000002       #6
	call       :func:                 #7 
	assert_eq  r0,   0x00000003       #8 

	halt                              #9

:func:
	iadd32    r0,    r0,   r1         #10
	ret                               #11