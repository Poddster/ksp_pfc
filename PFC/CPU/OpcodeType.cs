﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFC.CPU
{
    //currently the same as OpcodeToken.. might not be, e.g. might just group all iadd?
    public enum OpcodeType
    {
        iadd,
        iaddc,
        isub,
        isubc,
        mov,
        nop,
        umin,
        umax,
        ld,
        st,
        halt,
        jump,
        assert_eq,
        assert_neq,
        assert_true,
        assert_false,
        breakpoint,
        icmp,
        flags_read,


        // function call
        call_i,
        call_leaf,
        call,
        call_frame,
        ret_i,
        ret,
        ret_leaf,
        ret_free,
        ret_frame,

        // stack ops
        stack_alloc,
        stack_free,
        push_data,
        pop_data,
        ctrl_stack_alloc,
        ctrl_stack_free,
        push_ctrl,
        pop_ctrl,
        frame_enter,
        frame_leave,

        //these should probably all be a single b with optional condition code...
        bz,
        bnz,
        bv,
        bnv,
        bc,
        bnc,
        bneg,
        bpos,
        beq,
        bneq,
        bugt,
        bule,
        buge,
        bult,
        bsgt,
        bsle,
        bsge,
        bslt,

        is_iz,
        is_inz,
        is_ineg,
        is_ipos,
        is_ieq,
        is_ineq,
        is_iugt,
        is_iule,
        is_iuge,
        is_iult,
        is_isgt,
        is_isle,
        is_isge,
        is_islt,
        movc,
        btrue,
        bfalse,

        not,
        and,
        or,
        xor,
        nand,
        nor,
        xnor,
    }
}
