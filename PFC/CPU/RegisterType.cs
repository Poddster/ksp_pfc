﻿
namespace PFC.CPU
{
    //todo rename to bank
    enum RegisterType
    {
        scalar_reg,
        scalar_reg_base, //for indexing
        vector_reg,
        address_reg,
        memory,   //actual RAM?
        program_counter,
        stack_pointer,
        frame_pointer,
        link_register,
        control_stack_pointer,

        control_stack_val,

    }
}
