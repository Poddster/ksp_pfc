				iadd32      r0, 0, 4
				assert_eq   r0, 4

				iadd32      r1, r0, 10850
				assert_eq   r0, 4
				assert_eq   r1, 10854

				mov32       r2, 1
				iadd32      r0, 2, r1
				assert_eq   r0, 10856
				assert_eq   r1, 10854
				assert_eq   r2, 1

				iadd32      r0, r0, r1
				assert_eq   r0, 21710
				assert_eq   r1, 10854
				assert_eq   r2, 1

				iadd32      r1, r1, r1
				assert_eq   r0, 21710
				assert_eq   r1, 21708
				assert_eq   r2, 1

				iadd32      r2, r0, -2
				assert_eq   r0, 21710
				assert_eq   r1, 21708
				assert_eq   r2, 21708

				iadd32      r0, -5, -8
				assert_eq   r0, -13
				assert_eq   r1, 21708
				assert_eq   r2, 21708

				halt
