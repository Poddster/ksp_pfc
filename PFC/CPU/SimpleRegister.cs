﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PFC.CPU
{
    using JetBrains.Annotations;

    //should probably be a structure?
    //and shoud probably overload == for SimpleRegister
    class SimpleRegister : IEquatable<SimpleRegister>
    {
        //should make these byte,ushort,byte?
        readonly public RegisterType RegType; // scalar/vector/memory/etc
        readonly public uint Index;
        readonly public uint ByteOffset;

        private const int MAX_REG_INDEX = 0xF;
        private const int MAX_BYTE_OFFSET = 0x7;
        private const int MAX_MEM_INDEX = 0xFFFF;

        private SimpleRegister(RegisterType regType,
                              uint offset)
        {
            if (regType.GetBankDimension() != 0)
            {
                throw new ArgumentException("This constructor only accepts 0d register types", "regtype");
            }
            this.RegType = regType;
            this.Index = 0;
            this.ByteOffset = 0;
        }

        [NotNull]
        public static SimpleRegister Reg0D(RegisterType regType, uint offset = 0)
        {
            //todo: should probably use inheritance rather than factories?
            return new SimpleRegister(regType: regType, offset: offset);
        }

        private SimpleRegister(RegisterType regType, uint index, uint offset)
        {
            #region arg-check
            if (regType.GetBankDimension() != 1)
            {
                throw new ArgumentException("This constructor only accepts 1d registers, i.e. register + index",
                                            "regtype");
            }
            if (regType == RegisterType.memory &&
                offset != 0)
            {
                throw new ArgumentException("Can't use byteOffset on memory register", "offset");
            }
            if (!Enum.IsDefined(typeof(RegisterType), regType))
            {
                throw new ArgumentException("Invalid regType", "regType");
            }


            if (offset > SimpleRegister.MAX_BYTE_OFFSET)
            {
                string error = string.Format("byte offset must be in range [0, {0}]",
                                              SimpleRegister.MAX_BYTE_OFFSET);
                throw new ArgumentException(error, "offset");
            }

            if (regType == RegisterType.scalar_reg &&
                index > SimpleRegister.MAX_REG_INDEX)
            {
                string error = string.Format("mem index must be in range [0, {0}]",
                                              SimpleRegister.MAX_REG_INDEX);
                throw new ArgumentException(error, "index");
            }
            else if (regType == RegisterType.memory && 
                     index > SimpleRegister.MAX_MEM_INDEX)
            {
                string error = string.Format("mem index must be in range [0, {0}]",
                                              SimpleRegister.MAX_MEM_INDEX);
                throw new ArgumentException(error, "index");
            }
            #endregion

            this.RegType = regType;
            this.Index = index;
            this.ByteOffset = offset;
        }

        [NotNull]
        public static SimpleRegister Reg1D(RegisterType regType, uint index, uint offset = 0)
        {
            return new SimpleRegister(regType: regType, index: index, offset: offset);
        }

        #region equality
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            SimpleRegister other = obj as SimpleRegister;
            return this.Equals(other);
        }

        public bool Equals(SimpleRegister other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }
            return this.ByteOffset == other.ByteOffset &&
                   this.Index == other.Index &&
                   this.RegType == other.RegType;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.RegType.GetHashCode();
            hash = hash * 23 + this.Index.GetHashCode();
            hash = hash * 23 + this.ByteOffset.GetHashCode();
            return hash;
        }
        #endregion





        public override string ToString()
        {
            switch (this.RegType.GetBankDimension())
            {
                case 0:
                    return this.ToString0D();
                case 1:
                    return this.ToString1D();
                default:
                    throw new NotImplementedException();
            }
        }

        [NotNull]
        private string ToString0D()
        {
            return this.RegType.RegisterTypeToString();
        }

        [NotNull]
        private string ToString1D()
        {
            var regStr = this.RegType.RegisterTypeToString();
            var offsetStr = "";
            if (this.ByteOffset != 0)
            {
                offsetStr = string.Format(".{0}", this.ByteOffset);
            }
            string indexStr;
            if (this.RegType == RegisterType.memory)
            {
                indexStr = this.Index.ToHex();
            }
            else
            {
                indexStr = this.Index.ToString();
            }
            return String.Format("{0}{1}{2}", regStr, indexStr, offsetStr);
        }
    }
}
