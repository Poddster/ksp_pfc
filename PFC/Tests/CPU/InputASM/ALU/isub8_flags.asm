	 ######
	 isub8!      r0, 0X000000FF, 0X000000FF          #  0          -1 -          -1 =          +0 |  z c
	 flags_read  r1                                  #  1
	 assert_eq   r0, 0X00000000                      #  2
	 assert_eq   r1, 0b0101                          #  3

	 isub8!      r0, 0X000000FF, 0X00000000          #  4          -1 -          +0 =          -1 | n  c
	 flags_read  r1                                  #  5
	 assert_eq   r0, 0X000000FF                      #  6
	 assert_eq   r1, 0b1001                          #  7

	 isub8!      r0, 0X000000FF, 0X00000001          #  8          -1 -          +1 =          -2 | n  c
	 flags_read  r1                                  #  9
	 assert_eq   r0, 0X000000FE                      # 10
	 assert_eq   r1, 0b1001                          # 11

	 isub8!      r0, 0X000000FF, 0X0000007F          # 12          -1 -        +127 =        -128 | n  c
	 flags_read  r1                                  # 13
	 assert_eq   r0, 0X00000080                      # 14
	 assert_eq   r1, 0b1001                          # 15

	 isub8!      r0, 0X000000FF, 0X00000080          # 16          -1 -        -128 =        +127 |    c
	 flags_read  r1                                  # 17
	 assert_eq   r0, 0X0000007F                      # 18
	 assert_eq   r1, 0b0001                          # 19

	 isub8!      r0, 0X000000FF, 0X00000081          # 20          -1 -        -127 =        +126 |    c
	 flags_read  r1                                  # 21
	 assert_eq   r0, 0X0000007E                      # 22
	 assert_eq   r1, 0b0001                          # 23

	 ######
	 isub8!      r0, 0X00000000, 0X000000FF          # 24          +0 -          -1 =          +1 |     
	 flags_read  r1                                  # 25
	 assert_eq   r0, 0X00000001                      # 26
	 assert_eq   r1, 0b0000                          # 27

	 isub8!      r0, 0X00000000, 0X00000000          # 28          +0 -          +0 =          +0 |  z c
	 flags_read  r1                                  # 29
	 assert_eq   r0, 0X00000000                      # 30
	 assert_eq   r1, 0b0101                          # 31

	 isub8!      r0, 0X00000000, 0X00000001          # 32          +0 -          +1 =          -1 | n   
	 flags_read  r1                                  # 33
	 assert_eq   r0, 0X000000FF                      # 34
	 assert_eq   r1, 0b1000                          # 35

	 isub8!      r0, 0X00000000, 0X0000007F          # 36          +0 -        +127 =        -127 | n   
	 flags_read  r1                                  # 37
	 assert_eq   r0, 0X00000081                      # 38
	 assert_eq   r1, 0b1000                          # 39

	 isub8!      r0, 0X00000000, 0X00000080          # 40          +0 -        -128 =        -128 | n v 
	 flags_read  r1                                  # 41
	 assert_eq   r0, 0X00000080                      # 42
	 assert_eq   r1, 0b1010                          # 43

	 isub8!      r0, 0X00000000, 0X00000081          # 44          +0 -        -127 =        +127 |     
	 flags_read  r1                                  # 45
	 assert_eq   r0, 0X0000007F                      # 46
	 assert_eq   r1, 0b0000                          # 47

	 ######
	 isub8!      r0, 0X00000001, 0X000000FF          # 48          +1 -          -1 =          +2 |     
	 flags_read  r1                                  # 49
	 assert_eq   r0, 0X00000002                      # 50
	 assert_eq   r1, 0b0000                          # 51

	 isub8!      r0, 0X00000001, 0X00000000          # 52          +1 -          +0 =          +1 |    c
	 flags_read  r1                                  # 53
	 assert_eq   r0, 0X00000001                      # 54
	 assert_eq   r1, 0b0001                          # 55

	 isub8!      r0, 0X00000001, 0X00000001          # 56          +1 -          +1 =          +0 |  z c
	 flags_read  r1                                  # 57
	 assert_eq   r0, 0X00000000                      # 58
	 assert_eq   r1, 0b0101                          # 59

	 isub8!      r0, 0X00000001, 0X0000007F          # 60          +1 -        +127 =        -126 | n   
	 flags_read  r1                                  # 61
	 assert_eq   r0, 0X00000082                      # 62
	 assert_eq   r1, 0b1000                          # 63

	 isub8!      r0, 0X00000001, 0X00000080          # 64          +1 -        -128 =        -127 | n v 
	 flags_read  r1                                  # 65
	 assert_eq   r0, 0X00000081                      # 66
	 assert_eq   r1, 0b1010                          # 67

	 isub8!      r0, 0X00000001, 0X00000081          # 68          +1 -        -127 =        -128 | n v 
	 flags_read  r1                                  # 69
	 assert_eq   r0, 0X00000080                      # 70
	 assert_eq   r1, 0b1010                          # 71

	 ######
	 isub8!      r0, 0X0000007F, 0X000000FF          # 72        +127 -          -1 =        -128 | n v 
	 flags_read  r1                                  # 73
	 assert_eq   r0, 0X00000080                      # 74
	 assert_eq   r1, 0b1010                          # 75

	 isub8!      r0, 0X0000007F, 0X00000000          # 76        +127 -          +0 =        +127 |    c
	 flags_read  r1                                  # 77
	 assert_eq   r0, 0X0000007F                      # 78
	 assert_eq   r1, 0b0001                          # 79

	 isub8!      r0, 0X0000007F, 0X00000001          # 80        +127 -          +1 =        +126 |    c
	 flags_read  r1                                  # 81
	 assert_eq   r0, 0X0000007E                      # 82
	 assert_eq   r1, 0b0001                          # 83

	 isub8!      r0, 0X0000007F, 0X0000007F          # 84        +127 -        +127 =          +0 |  z c
	 flags_read  r1                                  # 85
	 assert_eq   r0, 0X00000000                      # 86
	 assert_eq   r1, 0b0101                          # 87

	 isub8!      r0, 0X0000007F, 0X00000080          # 88        +127 -        -128 =          -1 | n v 
	 flags_read  r1                                  # 89
	 assert_eq   r0, 0X000000FF                      # 90
	 assert_eq   r1, 0b1010                          # 91

	 isub8!      r0, 0X0000007F, 0X00000081          # 92        +127 -        -127 =          -2 | n v 
	 flags_read  r1                                  # 93
	 assert_eq   r0, 0X000000FE                      # 94
	 assert_eq   r1, 0b1010                          # 95

	 ######
	 isub8!      r0, 0X00000080, 0X000000FF          # 96        -128 -          -1 =        -127 | n   
	 flags_read  r1                                  # 97
	 assert_eq   r0, 0X00000081                      # 98
	 assert_eq   r1, 0b1000                          # 99

	 isub8!      r0, 0X00000080, 0X00000000          #100        -128 -          +0 =        -128 | n  c
	 flags_read  r1                                  #101
	 assert_eq   r0, 0X00000080                      #102
	 assert_eq   r1, 0b1001                          #103

	 isub8!      r0, 0X00000080, 0X00000001          #104        -128 -          +1 =        +127 |   vc
	 flags_read  r1                                  #105
	 assert_eq   r0, 0X0000007F                      #106
	 assert_eq   r1, 0b0011                          #107

	 isub8!      r0, 0X00000080, 0X0000007F          #108        -128 -        +127 =          +1 |   vc
	 flags_read  r1                                  #109
	 assert_eq   r0, 0X00000001                      #110
	 assert_eq   r1, 0b0011                          #111

	 isub8!      r0, 0X00000080, 0X00000080          #112        -128 -        -128 =          +0 |  z c
	 flags_read  r1                                  #113
	 assert_eq   r0, 0X00000000                      #114
	 assert_eq   r1, 0b0101                          #115

	 isub8!      r0, 0X00000080, 0X00000081          #116        -128 -        -127 =          -1 | n   
	 flags_read  r1                                  #117
	 assert_eq   r0, 0X000000FF                      #118
	 assert_eq   r1, 0b1000                          #119

	 ######
	 isub8!      r0, 0X00000081, 0X000000FF          #120        -127 -          -1 =        -126 | n   
	 flags_read  r1                                  #121
	 assert_eq   r0, 0X00000082                      #122
	 assert_eq   r1, 0b1000                          #123

	 isub8!      r0, 0X00000081, 0X00000000          #124        -127 -          +0 =        -127 | n  c
	 flags_read  r1                                  #125
	 assert_eq   r0, 0X00000081                      #126
	 assert_eq   r1, 0b1001                          #127

	 isub8!      r0, 0X00000081, 0X00000001          #128        -127 -          +1 =        -128 | n  c
	 flags_read  r1                                  #129
	 assert_eq   r0, 0X00000080                      #130
	 assert_eq   r1, 0b1001                          #131

	 isub8!      r0, 0X00000081, 0X0000007F          #132        -127 -        +127 =          +2 |   vc
	 flags_read  r1                                  #133
	 assert_eq   r0, 0X00000002                      #134
	 assert_eq   r1, 0b0011                          #135

	 isub8!      r0, 0X00000081, 0X00000080          #136        -127 -        -128 =          +1 |    c
	 flags_read  r1                                  #137
	 assert_eq   r0, 0X00000001                      #138
	 assert_eq   r1, 0b0001                          #139

	 isub8!      r0, 0X00000081, 0X00000081          #140        -127 -        -127 =          +0 |  z c
	 flags_read  r1                                  #141
	 assert_eq   r0, 0X00000000                      #142
	 assert_eq   r1, 0b0101                          #143


	 halt
