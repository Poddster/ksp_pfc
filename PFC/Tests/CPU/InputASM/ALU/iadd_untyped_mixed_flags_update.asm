        mov           r0,      0x12345678
        mov           r1,      0x20FEDCBA
                                                     #                         NZVC
        iadd!         r2,      r0,      r1           #2  r2 = 0x33333332     |-----
        assert_eq     r2,      0x33333332            #3                      |
        flags_read    r2                             #4                      |
        assert_eq     r2,      0b0000                #5                      |

        iadd!         r3,      r0,   i8{r1}          #6  r3 = 0x12345632     |----C
        assert_eq     r3,      0x12345632            #7                      |
        flags_read    r3                             #8                      |
        assert_eq     r3,      0b0001                #9                      |

        iadd!     i16{r4},     r0,      r1           #10 r4 = 0x3332         |-----
        assert_eq     r4,      0x3332                #11                     |
        flags_read    r4                             #12                     |
        assert_eq     r4,      0b0000                #13                     |

        iadd!      i8{r5},  i8{r0},  i8{r1}          #14 r5 = 0x32           |----C
        assert_eq     r5,      0x32                  #15                     |
        flags_read    r5                             #16                     |
        assert_eq     r5,      0b0001                #17                     |

        halt
