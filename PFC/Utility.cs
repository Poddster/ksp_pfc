﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System;

namespace PFC
{
    using JetBrains.Annotations;

    public static class Utility
    {
        
        [Pure]
        [NotNull]
        public static string ToHex(this int value)
        {
            return ToHex(unchecked((uint)value));
        }

        [Pure]
        [NotNull]
        public static string ToHex(this uint value)
        {
            return String.Format("0x{0:X}", value);
        }

        [Pure]
        [NotNull]
        public static string ToBin(this int value)
        {
            return ToHex(unchecked((uint)value));
        }

        [Pure]
        [NotNull]
        public static string ToBin(this uint value)
        {
            string binary = Convert.ToString(value, 2);
            return "0b" + binary;
        }

        [Pure]
        [NotNull]
        public static string DictToString<TKey, TValue>(this IDictionary<TKey, TValue> items)
        {
            return DictToString(items, null);
        }

        [Pure]
        [NotNull]
        public static string DictToString<TKey, TValue>(this IDictionary<TKey, TValue> items, string format)
        {
            format = string.IsNullOrEmpty(format) ? "{0}='{1}' " : format;
            if (items == null || items.Count == 0)
            {
                return "empty";
            }
            return items.Aggregate(new StringBuilder(), (sb, kvp) => sb.AppendFormat(format, kvp.Key, kvp.Value)).ToString();
        }

        [Pure]
        public static int IndexOf<T>(this IEnumerable<T> obj, T value)
        {
            return obj.IndexOf(value, null);
        }

        public static int IndexOf<T>(this IEnumerable<T> obj, T value, IEqualityComparer<T> comparer)
        {
            comparer = comparer ?? EqualityComparer<T>.Default;
            var found = obj
                .Select((a, i) => new { a, i })
                .FirstOrDefault(x => comparer.Equals(x.a, value));
            return found == null ? -1 : found.i;
        }



        public static T[] Slice<T>(this T[] source, int start, int end)
        {
            uint actualEnd;
            uint actualStart;

            unchecked
            {
                if (end < 0)
                {
                    int adjusted = source.Length + end;
                    actualEnd = (uint)Math.Max(0, adjusted);
                }
                else
                {
                    actualEnd = (uint)end;
                }

                if (start < 0)
                {
                    int adjusted = source.Length + start;
                    actualStart = (uint)Math.Max(0, adjusted);
                }
                else
                {
                    actualStart = (uint)start;
                }
            }

            if (actualStart < actualEnd)
            {
                uint newLength = actualEnd - actualStart;
                T[] newArray = new T[newLength];

                Array.Copy(source, actualStart, newArray, 0, newLength);

                return newArray;
            }
            else
            {
                return new T[0];
            }
        }


        public static IEnumerable<T> Slice<T>(this IEnumerable<T> source, int start, int end)
        {
            int actualEnd;
            int actualStart;

            if (end < 0)
            {
                int adjusted = source.Count() + end;
                actualEnd = Math.Max(0, adjusted);
            }
            else
            {
                actualEnd = end;
            }

            if (start < 0)
            {
                int adjusted = source.Count() + start;
                actualStart = Math.Max(0, adjusted);
            }
            else
            {
                actualStart = start;
            }

            if (actualStart < actualEnd)
            {
                int newLength = actualEnd - actualStart;
                return source.Skip(actualStart).Take(newLength);
            }
            else
            {
                return Enumerable.Empty<T>();
            }
        }

        /// <summary>
        /// Alias for IEnumerable.Select
        /// </summary>
        public static IEnumerable<TResult> Map<TSource, TResult>(this IEnumerable<TSource> source,
                                                                      Func<TSource, TResult> selector)
        {
            return source.Select(selector);
        }

        /// <summary>
        /// Alias for IEnumerable.Select
        /// </summary>
        public static IEnumerable<TResult> Map<TSource, TResult>(this IEnumerable<TSource> source,
                                                                      Func<TSource, int, TResult> selector)
        {
            return source.Select(selector);
        }


        /// <summary>
        /// Alias for IEnumerable.Aggregate
        /// </summary>
        public static TSource Fold<TSource>(this IEnumerable<TSource> source,
                                                 Func<TSource, TSource, TSource> func)
        {
            return source.Aggregate(func);
        }

        /// <summary>
        /// Alias for IEnumerable.Aggregate
        /// </summary>
        public static TAccumulate Fold<TSource, TAccumulate>(this IEnumerable<TSource> source,
                                                                  TAccumulate seed,
                                                                  Func<TAccumulate, TSource, TAccumulate> func)
        {
            return source.Aggregate(seed, func);
        }

        /// <summary>
        /// Alias for IEnumerable.Aggregate
        /// </summary>
        public static TResult Fold<TSource, TAccumulate, TResult>(this IEnumerable<TSource> source,
                                                                       TAccumulate seed,
                                                                       Func<TAccumulate, TSource, TAccumulate> func,
                                                                       Func<TAccumulate, TResult> resultSelector)
        {
            return source.Aggregate(seed, func, resultSelector);
        }

        /// <summary>
        /// Alias for IEnumerable.Aggregate
        /// </summary>
        public static TSource Reduce<TSource>(this IEnumerable<TSource> source,
                                                   Func<TSource, TSource, TSource> func)
        {
            return source.Aggregate(func);
        }

        /// <summary>
        /// Alias for IEnumerable.Aggregate
        /// </summary>
        public static TAccumulate Reduce<TSource, TAccumulate>(this IEnumerable<TSource> source,
                                                                    TAccumulate seed,
                                                                    Func<TAccumulate, TSource, TAccumulate> func)
        {
            return source.Aggregate(seed, func);
        }

        /// <summary>
        /// Alias for IEnumerable.Aggregate
        /// </summary>
        public static TResult Reduce<TSource, TAccumulate, TResult>(this IEnumerable<TSource> source,
                                                                         TAccumulate seed,
                                                                         Func<TAccumulate, TSource, TAccumulate> func,
                                                                         Func<TAccumulate, TResult> resultSelector)
        {
            return source.Aggregate(seed, func, resultSelector);
        }


        /// <summary>
        /// Alias for IEnumerable.Where
        /// </summary>
        public static IEnumerable<TSource> Filter<TSource>(this IEnumerable<TSource> source,
                                                               Func<TSource, bool> predicate)
        {
            return source.Where(predicate);
        }

        /// <summary>
        /// Alias for IEnumerable.Where
        /// </summary>
        public static IEnumerable<TSource> Filter<TSource>(this IEnumerable<TSource> source,
                                                               Func<TSource, int, bool> predicate)
        {
            return source.Where(predicate);
        }




        //Todo: is it possible to rearrange all of that to be x * somenumber ? That way we can precompute
        // the somenumber and only apply that to all values in a range, rather than redo the calculation
        //for each number
        public static float RangeConvert(float x, float inMin, float inMax, float outMin, float outMax)
        {
            float a = (x - inMin) / (inMax - inMin);
            return (a * (outMax - outMin)) + outMin;
        }


        public static ulong GetMask(this sbyte numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this byte numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this short numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this ushort numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this uint numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this long numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this ulong numBits)
        {
            return GetMask((int)numBits);
        }
        public static ulong GetMask(this int numBits)
        {
            const int maxBits = (sizeof(ulong) * 8);
            if (numBits < 1)
            {
                throw new ArgumentException("numBits must be >= 1", "numBits");
            }
            else if (numBits > maxBits)
            {
                throw new ArgumentException("numBits must be <= " + maxBits.ToString(), "numBits");
            }
            else if (numBits == maxBits)
            {
                return 0xFFFFFFFFFFFFFFFF;
            }
            else
            {
                return (0x1UL << numBits) - 0x1UL;
            }
        }



        public static ulong SignExtend(this sbyte val)
        {
            long l = (long)val;
            return (ulong)l;
        }
        public static ulong SignExtend(this byte val)
        {
            return SignExtendArb(val, sizeof(byte) * 8);
        }
        public static ulong SignExtend(this short val)
        {
            long l = (long)val;
            return (ulong)l;
        }
        public static ulong SignExtend(this ushort val)
        {
            return SignExtendArb(val, sizeof(ushort) * 8);
        }
        public static ulong SignExtend(this int val)
        {
            long l = (long)val;
            return (ulong)l;
        }
        public static ulong SignExtend(this uint val)
        {
            return SignExtendArb(val, sizeof(uint) * 8);
        }

        /*
        public static ulong SignExtend(this long val)
        {
            return (ulong)val;
        }
        public static ulong SignExtend(this ulong val)
        {
            return val;
        }*/




        public static ulong SignExtendArb(this long val, uint in_width)
        {
            return SignExtendArb((ulong)val, (int)in_width);
        }
        public static ulong SignExtendArb(this long val, int in_width)
        {
            return SignExtendArb((ulong)val, in_width);
        }


        public static ulong SignExtendArb(this ulong val, uint in_width)
        {
            return SignExtendArb(val, (int)in_width);
        }
        public static ulong SignExtendArb(this ulong val, int in_width)
        {
            return (ulong) SignExtendArbL(val, in_width);
        }

        public static long SignExtendArbL(this ulong val, uint in_width)
        {
            return SignExtendArbL(val, (int) in_width);
        }

        public static long SignExtendArbL(this ulong val, int in_width)
        {
            unchecked
            {
                int gap = ((sizeof(ulong) * 8) - in_width);
                ulong unsignedVal = (val << gap);
                //cast to a signed type before shifting so that the shift is
                //arithmetic
                long signedVal = ((long)unsignedVal >> gap);
                return signedVal;
            }
        }

        [NotNull]
        public static string StripArgumentException([NotNull] this ArgumentException e)
        {
            string paramName = "\nParameter name: " + e.ParamName;
            int stringIndex = e.Message.LastIndexOf(paramName);
            string err = e.Message.Substring(0, stringIndex);
            return err;
        }


        public static bool IsDefinedEx(this Enum yourEnum)
        {
            char firstDigit = yourEnum.ToString()[0];
            if (Char.IsDigit(firstDigit) || firstDigit == '-')  //Account for signed enums too
                return false;

            return true;
        }
    }

    public abstract class Option<T> where T : class
    {
        public abstract bool HasValue { get; }
    }

    public sealed class Some<T> : Option<T> where T : class 
    {
        [NotNull]
        private readonly T val;

        public Some([NotNull] T val)
        {
            if (val == null)
            {
                throw new ArgumentNullException("val", "Val may not be null");
            }
            this.val = val;
        }

        public override bool HasValue
        {
            get { return true; }
        }

        [NotNull]
        public T Value
        {
            get { return this.val; }
        }
    }

    public sealed class None<T> : Option<T> where T : class 
    {
        public override bool HasValue
        {
            get { return false; }
        }
    }

    public struct Float_Union : IEquatable<Float_Union>
    {
        public readonly UInt32 u;
        public readonly Int32 i;
        public readonly float f;

        internal Float_Union(float num)
        {
            this.f = num;
            this.i = BitConverter.ToInt32(BitConverter.GetBytes(num), 0);
            this.u = unchecked((uint)this.i);
        }
        internal Float_Union(Int32 num)
        {
            this.i = num;
            this.u = unchecked((uint)this.i);
            this.f = BitConverter.ToSingle(BitConverter.GetBytes(num), 0);
        }
        internal Float_Union(UInt32 num)
        {
            this.u = num;
            this.i = unchecked((int)this.u);
            this.f = BitConverter.ToSingle(BitConverter.GetBytes(num), 0);
        }

        internal bool Negative()
        {
            return (i >> 31) != 0;
        }
        internal Int32 RawMantissa()
        {
            return i & ((1 << 23) - 1);
        }
        internal Int32 RawExponent()
        {
            return (i >> 23) & 0xFF;
        }

        public override string ToString()
        {
            return String.Format("[{0}|{1}|{2}]",
                                 this.u.ToHex(),
                                 this.u,
                                 this.f);
        }

        #region equals,==
        public static bool operator ==(Float_Union left, Float_Union right)
        {
            return left.Equals(right);
        }
        public static bool operator !=(Float_Union left, Float_Union right)
        {
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (obj is Float_Union)
            {
                return this.Equals((Float_Union)obj);
            }
            return false;
        }

        public bool Equals(Float_Union other)
        {
            bool bEqual = this.u == other.u;
            if (bEqual)
            {
                Debug.Assert(this.i == other.i &&
                            (this.f == other.f ||
                               float.IsNaN(this.f) == float.IsNaN(other.f)));

            }
            else
            {
                Debug.Assert(this.i != other.i &&
                             this.f != other.f);
            }
            return bEqual;
        }

        public override int GetHashCode()
        {
            return this.u.GetHashCode();
        }
        #endregion
    }

    public static class FloatExt
    {
        //http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
        //However, maxDiff is only needed for numbers that should be at or near zero.
        //It is only needed to handle cases like sin(pi). For relative error maxUlpsDiff is the
        //appropriate parameter to use, and this should suffice for most cases.
        public static bool AlmostEqualUlpsAndAbs(this float A, float B, float maxDiff, int maxUlpsDiff)
        {
            // Check if the numbers are really close -- needed
            // when comparing numbers near zero.
            float absDiff = Math.Abs(A - B);
            if (absDiff <= maxDiff)
                return true;

            Float_Union uA = new Float_Union(A);
            Float_Union uB = new Float_Union(B);

            // Different signs means they do not match.
            if (uA.Negative() != uB.Negative())
                return false;

            // Find the difference in ULPs.
            int ulpsDiff = Math.Abs(uA.i - uB.i);
            if (ulpsDiff <= maxUlpsDiff)
                return true;

            return false;
        }
        public static bool AlmostEqualUlpsAndAbs(this float A, float B)
        {
            return AlmostEqualRelativeAndAbs(A, B, 0.0001f, 4);
        }

        //http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
        //maxDiff 
        public static bool AlmostEqualRelativeAndAbs(this float A, float B, float maxDiff, float maxRelDiff)
        {
            // Check if the numbers are really close -- needed
            // when comparing numbers near zero.
            float diff = Math.Abs(A - B);
            if (diff <= maxDiff)
                return true;

            A = Math.Abs(A);
            B = Math.Abs(B);
            float largest = (B > A) ? B : A;

            var x = largest * maxRelDiff;
            if (diff <= x)
                return true;
            return false;
        }
        public static bool AlmostEqualRelativeAndAbs(this float A, float B)
        {
            //this isn't a very precise game :)
            return AlmostEqualRelativeAndAbs(A, B, 0.0001f, float.Epsilon);
        }

        public static float SelectClosestTo0(float a, float b)
        {
            Debug.Assert(Math.Sign(a) == Math.Sign(b));
            if (a < 0.0f)
            {
                return Math.Max(a, b);
            }
            else
            {
                return Math.Min(a, b);
            }
        }

    }

    public enum DPFCLASS : uint
    {
        MESSAGE,
        ERROR,
        WARN,
        //other categories go here, e.g. INSTRUCTION, REGISTER (write?read?) etc.
        PARSER,
        CPU,
    }

    [Flags]
    public enum DPFMASK_PARSER : uint
    {
        INTERNAL_ERROR = 0x00000001,

        LEX_VERBOSE = 0x00000010,
        LEX_BAD_USER_SYMBOL = 0x00000020,

        PARSER_VERBOSE = 0x00000100,
        PARSER_BAD_USER_SYMBOL = 0x00000200,
    }

    [Flags]
    public enum DPFMASK_CPU : uint
    {
        ASSERT = 0x00000001,
        INVALID_INSTRUCTION = 0x00000002,
        TRACE_INST = 0x00000004,

    }

    //used pass 'all' value between bridging overloads.
    [Flags]
    enum DPFMASK_DBG : uint
    {
        ALL = 0xFFFFFFFF,
    }


    public sealed class LogInst
    {

        //todo: release print? #ifdef debug?
        //todo: print to file?

        //use Log.Assert instead of debug.assert?

        //rename instance to 'dpf'?
        private static readonly LogInst instance = new LogInst();

        //terrible camel case used to make sure I don't cast auiDBGClasses[4] to (int) instead of uint.
        //Probably will never cause a bug
        //0 = most important, int_max = least important, etc.
        private int iDBGLevel;
        private uint[] auiDBGClasses;


        public LogInst()
        {
            Type enumType = Type.GetType("PFC.DPFCLASS");
            int numElementsInEnum = enumType.GetEnumValues().Length;
            //Type underType = enumType.GetEnumUnderlyingType();
            //this.dbgClasses = Array.CreateInstance(underType, numElementsInEnum);

            this.auiDBGClasses = new uint[numElementsInEnum];
            this.iDBGLevel = Log.DPF_LVL_0;
            this.auiDBGClasses[(uint)DPFCLASS.MESSAGE] = (uint)DPFMASK_DBG.ALL;
            this.auiDBGClasses[(uint)DPFCLASS.ERROR] = (uint)DPFMASK_DBG.ALL;
            this.auiDBGClasses[(uint)DPFCLASS.WARN] = (uint)DPFMASK_DBG.ALL;

            this.auiDBGClasses[(uint)DPFCLASS.CPU] = (uint)(DPFMASK_DBG.ALL);
            //this.auiDBGClasses[(uint)DPFCLASS.PARSER] = (uint)(DPFMASK_PARSER.INTERNAL_ERROR);
            //this.auiDBGClasses[(uint)DPFCLASS.PARSER] |= (uint)(DPFMASK_PARSER.PARSER_BAD_USER_SYMBOL);

            
        }


        internal void Print(string format)
        {
            this.Print(format, new object[0]);
        }
        internal void Print(DPFCLASS eClass, string format)
        {
            this.Print(eClass, format, new object[0]);
        }

        internal void Print(DPFCLASS eClass, Enum uDPFMask, string format)
        {
            this.Print(eClass, uDPFMask, format, new object[0]);
        }

        internal void Print(DPFCLASS eClass, Enum eDPFMask, int level, string format)
        {
            this.Print(eClass, eDPFMask, level, format, new object[0]);
        }



        internal void Print(string format, params object[] args)
        {
            this.Print(DPFCLASS.MESSAGE, format, args);
        }

        internal void Print(DPFCLASS eClass, string format, params object[] args)
        {
            //defauly mask of ALL
            this.Print(eClass, DPFMASK_DBG.ALL, format, args);
        }

        internal void Print(DPFCLASS eClass, Enum eDPFMask, string format, params object[] args)
        {
            //default level of -1
            this.Print(eClass, eDPFMask, Log.DPF_LVL_ALL, format, args);
        }


        //level: 0 = most important, int_max = least important, etc.
        internal void Print(DPFCLASS eClass, Enum eDPFMask, int level, string format, params object[] args)
        {
            Debug.Assert(Enum.IsDefined(eClass.GetType(), eClass));
            //Debug.Assert((uint) eClass < this.auiDBGClasses.GetLength(0));
            uint uDPFMask = Convert.ToUInt32(eDPFMask);


            if (level <= this.iDBGLevel &&
                ((this.auiDBGClasses[(uint)eClass] & uDPFMask) != 0x0))
            {
                format = eClass.ToString() + ": " + format;
                if (args.Length > 0)
                {
                    Debug.Print(format, args);
                }
                else
                {
                    Debug.Print(format);
                }
            }
        }

    }

    public static class Log
    {
        public const int DPF_LVL_ALL = -1;
        public const int DPF_LVL_0 = 0;
        public const int DPF_LVL_1 = 1;
        private static readonly LogInst instance = new LogInst();
        private static LogInst Instance
        {
            get
            {

                return instance;
            }
        }

        public static void Print(string format)
        {
            Instance.Print(format, new object[0]);
        }
        public static void Print(DPFCLASS eClass, string format)
        {
            Instance.Print(eClass, format, new object[0]);
        }

        public static void Print(DPFCLASS eClass, Enum uDPFMask, string format)
        {
            Instance.Print(eClass, uDPFMask, format, new object[0]);
        }

        public static void Print(DPFCLASS eClass, Enum eDPFMask, int level, string format)
        {
            Instance.Print(eClass, eDPFMask, level, format, new object[0]);
        }



        public static void Print(string format, params object[] args)
        {
            Instance.Print(DPFCLASS.MESSAGE, format, args);
        }

        public static void Print(DPFCLASS eClass, string format, params object[] args)
        {
            //defauly mask of ALL
            Instance.Print(eClass, DPFMASK_DBG.ALL, format, args);
        }

        public static void Print(DPFCLASS eClass, Enum eDPFMask, string format, params object[] args)
        {
            //default level of -1
            Instance.Print(eClass, eDPFMask, DPF_LVL_ALL, format, args);
        }

        //level: 0 = most important, int_max = least important, etc.
        public static void Print(DPFCLASS eClass, Enum eDPFMask, int level, string format, params object[] args)
        {
            Instance.Print(eClass, eDPFMask, level, format, args);
        }

    }



}