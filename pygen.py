
insts3 =[
        "ieq",
        "ineq",
        "iugt",
        "iule",
        "iuge",
        "iult",
        "isgt",
        "isle",
        "isge",
        "islt",]
insts2 = [
        "iz",
        "inz",
        "ineg",
        "ipos",
]

headerstart = "private void Execute_generic_is_"
headerend = "(CPUStateUpdate stateUpdate, bool bFlagUpdate, ulong[] dstVals, ulong[] srcVals)"
body3 = """
{
	Debug.Assert(!bFlagUpdate);
	Debug.Assert(dstVals.Length == 1);
	Debug.Assert(srcVals.Length == 2);
	throw new NotImplementedException();
}
"""

body2 = """
{
	Debug.Assert(!bFlagUpdate);
	Debug.Assert(dstVals.Length == 1);
	Debug.Assert(srcVals.Length == 1);
	throw new NotImplementedException();
}
"""

for inst in insts3:
    print headerstart + inst + headerend
    print body3
    
for inst in insts2:
    print headerstart + inst + headerend
    print body2


#for inst in (insts3 + insts2):
#    print "case OpcodeType.is_" + inst + ":"
#    print "\texecutor = this.Execute_generic_is_" + inst + ";"
#    print "\tbreak;"

