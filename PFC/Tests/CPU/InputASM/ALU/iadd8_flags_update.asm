	######
	iadd8!      r0, 0X0FF, 0X0FF                    #  0          -1 +          -1 =        +254 | n  c
	assert_eq   r0, 0X0FE                           #  1
	flags_read  r1                                  #  2
	assert_eq   r1, 0b1001                          #  3

	iadd8!      r0, 0X0FF, 0X000                    #  4          -1 +          +0 =        +255 | n   
	assert_eq   r0, 0X0FF                           #  5
	flags_read  r1                                  #  6
	assert_eq   r1, 0b1000                          #  7

	iadd8!      r0, 0X0FF, 0X001                    #  8          -1 +          +1 =          +0 |  z c
	assert_eq   r0, 0X000                           #  9
	flags_read  r1                                  # 10
	assert_eq   r1, 0b0101                          # 11

	iadd8!      r0, 0X0FF, 0X07F                    # 12          -1 +        +127 =        +126 |    c
	assert_eq   r0, 0X07E                           # 13
	flags_read  r1                                  # 14
	assert_eq   r1, 0b0001                          # 15

	iadd8!      r0, 0X0FF, 0X080                    # 16          -1 +        -128 =        +127 |   vc
	assert_eq   r0, 0X07F                           # 17
	flags_read  r1                                  # 18
	assert_eq   r1, 0b0011                          # 19

	iadd8!      r0, 0X0FF, 0X081                    # 20          -1 +        -127 =        +128 | n  c
	assert_eq   r0, 0X080                           # 21
	flags_read  r1                                  # 22
	assert_eq   r1, 0b1001                          # 23

	######
	iadd8!      r0, 0X000, 0X000                    # 24          +0 +          +0 =          +0 |  z  
	assert_eq   r0, 0X000                           # 25
	flags_read  r1                                  # 26
	assert_eq   r1, 0b0100                          # 27

	iadd8!      r0, 0X000, 0X001                    # 28          +0 +          +1 =          +1 |     
	assert_eq   r0, 0X001                           # 29
	flags_read  r1                                  # 30
	assert_eq   r1, 0b0000                          # 31

	iadd8!      r0, 0X000, 0X07F                    # 32          +0 +        +127 =        +127 |     
	assert_eq   r0, 0X07F                           # 33
	flags_read  r1                                  # 34
	assert_eq   r1, 0b0000                          # 35

	iadd8!      r0, 0X000, 0X080                    # 36          +0 +        -128 =        +128 | n   
	assert_eq   r0, 0X080                           # 37
	flags_read  r1                                  # 38
	assert_eq   r1, 0b1000                          # 39

	iadd8!      r0, 0X000, 0X081                    # 40          +0 +        -127 =        +129 | n   
	assert_eq   r0, 0X081                           # 41
	flags_read  r1                                  # 42
	assert_eq   r1, 0b1000                          # 43

	######
	iadd8!      r0, 0X001, 0X001                    # 44          +1 +          +1 =          +2 |     
	assert_eq   r0, 0X002                           # 45
	flags_read  r1                                  # 46
	assert_eq   r1, 0b0000                          # 47

	iadd8!      r0, 0X001, 0X07F                    # 48          +1 +        +127 =        +128 | n v 
	assert_eq   r0, 0X080                           # 49
	flags_read  r1                                  # 50
	assert_eq   r1, 0b1010                          # 51

	iadd8!      r0, 0X001, 0X080                    # 52          +1 +        -128 =        +129 | n   
	assert_eq   r0, 0X081                           # 53
	flags_read  r1                                  # 54
	assert_eq   r1, 0b1000                          # 55

	iadd8!      r0, 0X001, 0X081                    # 56          +1 +        -127 =        +130 | n   
	assert_eq   r0, 0X082                           # 57
	flags_read  r1                                  # 58
	assert_eq   r1, 0b1000                          # 59

	######
	iadd8!      r0, 0X07F, 0X07F                    # 60        +127 +        +127 =        +254 | n v 
	assert_eq   r0, 0X0FE                           # 61
	flags_read  r1                                  # 62
	assert_eq   r1, 0b1010                          # 63

	iadd8!      r0, 0X07F, 0X080                    # 64        +127 +        -128 =        +255 | n   
	assert_eq   r0, 0X0FF                           # 65
	flags_read  r1                                  # 66
	assert_eq   r1, 0b1000                          # 67

	iadd8!      r0, 0X07F, 0X081                    # 68        +127 +        -127 =          +0 |  z c
	assert_eq   r0, 0X000                           # 69
	flags_read  r1                                  # 70
	assert_eq   r1, 0b0101                          # 71

	######
	iadd8!      r0, 0X080, 0X080                    # 72        -128 +        -128 =          +0 |  zvc
	assert_eq   r0, 0X000                           # 73
	flags_read  r1                                  # 74
	assert_eq   r1, 0b0111                          # 75

	iadd8!      r0, 0X080, 0X081                    # 76        -128 +        -127 =          +1 |   vc
	assert_eq   r0, 0X001                           # 77
	flags_read  r1                                  # 78
	assert_eq   r1, 0b0011                          # 79

	######
	iadd8!      r0, 0X081, 0X081                    # 80        -127 +        -127 =          +2 |   vc
	assert_eq   r0, 0X002                           # 81
	flags_read  r1                                  # 82
	assert_eq   r1, 0b0011                          # 83


	halt
