

        mov         sp,     0x800

        #shouldn't really be used like this, to store random data...
        push_ctrl   0x80
        push_ctrl   0x02
        

        pop_ctrl    r0
        pop_ctrl    r1
        iadd        r0,     r0,     r1
        assert_eq   r0,     0x82
        
        #push func parms
        push_data   0x1234
        push_data   0x5678
        #push PC and SP onto control stack
        push_ctrl   sp

        iadd        r0,     pc,     3
        push_ctrl   r0
        jump        :func:
        assert_eq   r0,     0x68ac
        assert_eq   sp,     0x800

        # check that sp was preserved in the control stack
        # it should still have the two args on it
        pop_ctrl    sp
        assert_eq   sp,     0x7f8
        pop_data    r0
        assert_eq   r0,     0x5678
        pop_data    r0
        assert_eq   r0,     0x1234

        
        #assert_eq   csp,    0x0  #valid to read csp?
        ctrl_stack_alloc    30
        ctrl_stack_free     30
        
        # test pushing the max amount without overflowing
        # for a 30 sized ctrl stack:
        #   mov 0 to ctrl 0
        #   mov 1 to ctrl 1
        #   mov ....
        #   mov 29 to ctrl 29
        mov         r0,     0
    :pusher:
        push_ctrl   r0
        iadd        r0,     r0,     0x1
        icmp!       r0,     30
        bult        :pusher:
        
        
        mov         r0,     29
    :popper:
        pop_ctrl    r1
        assert_eq   r1,     r0
        isub!       r0,     r0,     0x1
        bnz         :popper:
        
        halt
        
        
        
    :func:
        pop_data    r0
        pop_data    r1
        iadd        r0,     r0,     r1
        #todo: ideally this would be pop_ctrl pc? would that ever be used?
        pop_ctrl    r1
        jump        r1
