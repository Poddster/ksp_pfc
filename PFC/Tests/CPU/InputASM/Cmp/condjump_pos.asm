:ok001: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bz          :ok002:
	 assert_true 1

:ok002: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bz          :ok003:
	 assert_true 2

:ok003: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bz          :ok004:
	 assert_true 3

:ok004: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bz          :ok005:
	 assert_true 4


:ok005: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bnz         :ok006:
	 assert_true 5

:ok006: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bnz         :ok007:
	 assert_true 6

:ok007: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bnz         :ok008:
	 assert_true 7

:ok008: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bnz         :ok009:
	 assert_true 8

:ok009: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bnz         :ok010:
	 assert_true 9

:ok010: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bnz         :ok011:
	 assert_true 10

:ok011: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bnz         :ok012:
	 assert_true 11

:ok012: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bnz         :ok013:
	 assert_true 12

:ok013: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bnz         :ok014:
	 assert_true 13

:ok014: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bnz         :ok015:
	 assert_true 14

:ok015: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bnz         :ok016:
	 assert_true 15

:ok016: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bnz         :ok017:
	 assert_true 16


:ok017: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bneg        :ok018:
	 assert_true 17

:ok018: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bneg        :ok019:
	 assert_true 18

:ok019: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bneg        :ok020:
	 assert_true 19

:ok020: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bneg        :ok021:
	 assert_true 20

:ok021: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bneg        :ok022:
	 assert_true 21

:ok022: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bneg        :ok023:
	 assert_true 22

:ok023: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bneg        :ok024:
	 assert_true 23

:ok024: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bneg        :ok025:
	 assert_true 24


:ok025: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bpos        :ok026:
	 assert_true 25

:ok026: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bpos        :ok027:
	 assert_true 26

:ok027: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bpos        :ok028:
	 assert_true 27

:ok028: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bpos        :ok029:
	 assert_true 28

:ok029: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bpos        :ok030:
	 assert_true 29

:ok030: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bpos        :ok031:
	 assert_true 30

:ok031: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bpos        :ok032:
	 assert_true 31

:ok032: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bpos        :ok033:
	 assert_true 32


:ok033: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bv          :ok034:
	 assert_true 33

:ok034: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bv          :ok035:
	 assert_true 34

:ok035: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bv          :ok036:
	 assert_true 35

:ok036: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bv          :ok037:
	 assert_true 36


:ok037: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bnv         :ok038:
	 assert_true 37

:ok038: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bnv         :ok039:
	 assert_true 38

:ok039: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bnv         :ok040:
	 assert_true 39

:ok040: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bnv         :ok041:
	 assert_true 40

:ok041: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bnv         :ok042:
	 assert_true 41

:ok042: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bnv         :ok043:
	 assert_true 42

:ok043: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bnv         :ok044:
	 assert_true 43

:ok044: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bnv         :ok045:
	 assert_true 44

:ok045: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bnv         :ok046:
	 assert_true 45

:ok046: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bnv         :ok047:
	 assert_true 46

:ok047: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bnv         :ok048:
	 assert_true 47

:ok048: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bnv         :ok049:
	 assert_true 48


:ok049: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bc          :ok050:
	 assert_true 49

:ok050: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bc          :ok051:
	 assert_true 50

:ok051: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bc          :ok052:
	 assert_true 51

:ok052: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bc          :ok053:
	 assert_true 52

:ok053: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bc          :ok054:
	 assert_true 53

:ok054: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bc          :ok055:
	 assert_true 54

:ok055: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bc          :ok056:
	 assert_true 55

:ok056: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bc          :ok057:
	 assert_true 56

:ok057: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bc          :ok058:
	 assert_true 57

:ok058: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bc          :ok059:
	 assert_true 58


:ok059: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bnc         :ok060:
	 assert_true 59

:ok060: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bnc         :ok061:
	 assert_true 60

:ok061: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bnc         :ok062:
	 assert_true 61

:ok062: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bnc         :ok063:
	 assert_true 62

:ok063: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bnc         :ok064:
	 assert_true 63

:ok064: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bnc         :ok065:
	 assert_true 64


:ok065: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 beq         :ok066:
	 assert_true 65

:ok066: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 beq         :ok067:
	 assert_true 66

:ok067: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 beq         :ok068:
	 assert_true 67

:ok068: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 beq         :ok069:
	 assert_true 68


:ok069: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bneq        :ok070:
	 assert_true 69

:ok070: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bneq        :ok071:
	 assert_true 70

:ok071: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bneq        :ok072:
	 assert_true 71

:ok072: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bneq        :ok073:
	 assert_true 72

:ok073: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bneq        :ok074:
	 assert_true 73

:ok074: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bneq        :ok075:
	 assert_true 74

:ok075: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bneq        :ok076:
	 assert_true 75

:ok076: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bneq        :ok077:
	 assert_true 76

:ok077: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bneq        :ok078:
	 assert_true 77

:ok078: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bneq        :ok079:
	 assert_true 78

:ok079: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bneq        :ok080:
	 assert_true 79

:ok080: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bneq        :ok081:
	 assert_true 80


:ok081: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bugt        :ok082:
	 assert_true 81

:ok082: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bugt        :ok083:
	 assert_true 82

:ok083: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bugt        :ok084:
	 assert_true 83

:ok084: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bugt        :ok085:
	 assert_true 84

:ok085: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bugt        :ok086:
	 assert_true 85

:ok086: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bugt        :ok087:
	 assert_true 86


:ok087: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bule        :ok088:
	 assert_true 87

:ok088: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bule        :ok089:
	 assert_true 88

:ok089: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bule        :ok090:
	 assert_true 89

:ok090: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bule        :ok091:
	 assert_true 90

:ok091: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bule        :ok092:
	 assert_true 91

:ok092: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bule        :ok093:
	 assert_true 92

:ok093: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bule        :ok094:
	 assert_true 93

:ok094: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bule        :ok095:
	 assert_true 94

:ok095: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bule        :ok096:
	 assert_true 95

:ok096: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bule        :ok097:
	 assert_true 96


:ok097: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 buge        :ok098:
	 assert_true 97

:ok098: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 buge        :ok099:
	 assert_true 98

:ok099: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 buge        :ok100:
	 assert_true 99

:ok100: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 buge        :ok101:
	 assert_true 100

:ok101: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 buge        :ok102:
	 assert_true 101

:ok102: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 buge        :ok103:
	 assert_true 102

:ok103: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 buge        :ok104:
	 assert_true 103

:ok104: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 buge        :ok105:
	 assert_true 104

:ok105: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 buge        :ok106:
	 assert_true 105

:ok106: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 buge        :ok107:
	 assert_true 106


:ok107: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bult        :ok108:
	 assert_true 107

:ok108: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bult        :ok109:
	 assert_true 108

:ok109: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bult        :ok110:
	 assert_true 109

:ok110: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bult        :ok111:
	 assert_true 110

:ok111: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bult        :ok112:
	 assert_true 111

:ok112: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bult        :ok113:
	 assert_true 112


:ok113: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bsgt        :ok114:
	 assert_true 113

:ok114: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bsgt        :ok115:
	 assert_true 114

:ok115: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bsgt        :ok116:
	 assert_true 115

:ok116: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bsgt        :ok117:
	 assert_true 116

:ok117: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bsgt        :ok118:
	 assert_true 117

:ok118: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bsgt        :ok119:
	 assert_true 118


:ok119: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bsle        :ok120:
	 assert_true 119

:ok120: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bsle        :ok121:
	 assert_true 120

:ok121: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bsle        :ok122:
	 assert_true 121

:ok122: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bsle        :ok123:
	 assert_true 122

:ok123: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bsle        :ok124:
	 assert_true 123

:ok124: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bsle        :ok125:
	 assert_true 124

:ok125: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bsle        :ok126:
	 assert_true 125

:ok126: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bsle        :ok127:
	 assert_true 126

:ok127: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bsle        :ok128:
	 assert_true 127

:ok128: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bsle        :ok129:
	 assert_true 128


:ok129: icmp!       0x00000000, 0x00000000  #          +0 -          +0 = 00000000 =  Z C
	 bsge        :ok130:
	 assert_true 129

:ok130: icmp!       0x00000000, 0x80000000  #          +0 - -2147483648 = 80000000 = N V 
	 bsge        :ok131:
	 assert_true 130

:ok131: icmp!       0x00000000, 0xFFFFFFFF  #          +0 -          -1 = 00000001 =     
	 bsge        :ok132:
	 assert_true 131

:ok132: icmp!       0x7FFFFFFF, 0x00000000  # +2147483647 -          +0 = 7fffffff =    C
	 bsge        :ok133:
	 assert_true 132

:ok133: icmp!       0x7FFFFFFF, 0x7FFFFFFF  # +2147483647 - +2147483647 = 00000000 =  Z C
	 bsge        :ok134:
	 assert_true 133

:ok134: icmp!       0x7FFFFFFF, 0x80000000  # +2147483647 - -2147483648 = ffffffff = N V 
	 bsge        :ok135:
	 assert_true 134

:ok135: icmp!       0x7FFFFFFF, 0xFFFFFFFF  # +2147483647 -          -1 = 80000000 = N V 
	 bsge        :ok136:
	 assert_true 135

:ok136: icmp!       0x80000000, 0x80000000  # -2147483648 - -2147483648 = 00000000 =  Z C
	 bsge        :ok137:
	 assert_true 136

:ok137: icmp!       0xFFFFFFFF, 0x80000000  #          -1 - -2147483648 = 7fffffff =    C
	 bsge        :ok138:
	 assert_true 137

:ok138: icmp!       0xFFFFFFFF, 0xFFFFFFFF  #          -1 -          -1 = 00000000 =  Z C
	 bsge        :ok139:
	 assert_true 138


:ok139: icmp!       0x00000000, 0x7FFFFFFF  #          +0 - +2147483647 = 80000001 = N   
	 bslt        :ok140:
	 assert_true 139

:ok140: icmp!       0x80000000, 0x00000000  # -2147483648 -          +0 = 80000000 = N  C
	 bslt        :ok141:
	 assert_true 140

:ok141: icmp!       0x80000000, 0x7FFFFFFF  # -2147483648 - +2147483647 = 00000001 =   VC
	 bslt        :ok142:
	 assert_true 141

:ok142: icmp!       0x80000000, 0xFFFFFFFF  # -2147483648 -          -1 = 80000001 = N   
	 bslt        :ok143:
	 assert_true 142

:ok143: icmp!       0xFFFFFFFF, 0x00000000  #          -1 -          +0 = ffffffff = N  C
	 bslt        :ok144:
	 assert_true 143

:ok144: icmp!       0xFFFFFFFF, 0x7FFFFFFF  #          -1 - +2147483647 = 80000000 = N  C
	 bslt        :ok145:
	 assert_true 144


:ok145: halt

