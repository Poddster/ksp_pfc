    #0x301234 + 0x300233 = 0x601467
    mov8      r0,     0x34
    mov8      r1,     0x12
    mov8      r2,     0x30

    mov8      r3,     0x33
    mov8      r4,     0x02
    mov8      r5,     0x30

    iadd8!    r6,     r0,     r3
    iaddc8!   r7,     r1,     r4
    iaddc8    r8,     r2,     r5

    assert_eq r6,     0x67
    assert_eq r7,     0x14
    assert_eq r8,     0x60


    # 0x812345 + 0xFFFF01            = 0x1_812246
    # 8463173 + -255      =  8462918 = 0x  812246
    mov8      r0,     0x45
    mov8      r1,     0x23
    mov8      r2,     0x81

    mov8      r3,     0x01
    mov8      r4,     0xff
    mov8      r5,     0xff

    iadd8!    r6,     r0,     r3
    iaddc8!   r7,     r1,     r4
    iaddc8    r8,     r2,     r5

    assert_eq r6,     0x46
    assert_eq r7,     0x22
    assert_eq r8,     0x81

    halt


